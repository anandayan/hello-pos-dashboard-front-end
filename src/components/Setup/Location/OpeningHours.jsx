import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as LocationsActions from '../../../actions/setup/locations_actions';
import { bindActionCreators } from 'redux';


let OpeningHr = React.createClass({
    getInitialState() {
        return {
             addOpenHourClicked:false,ListOrderHourTypeCalled:false,OrderHourListData:null,ListOrderHourCalled:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        let nexporps = nextProps.LocationData;
        if(this.state.addOpenHourClicked){
          if(nexporps.AddOpeningHour){
            alert(nexporps.AddOpeningHour.message);
            this.setState({addOpenHourClicked:false})
            this.OrderHourListType(1);
          }
      }
   
      if(this.state.ListOrderHourTypeCalled){
        nexporps.GetOpeningHour && this.setState({ListOrderHourTypeCalled:false,OrderHourListData:nexporps.GetOpeningHour.data})
      }

      if(this.state.ListOrderHourCalled){
          this.OrderHourListType();
        this.setState({ListOrderHourTypeCalled:true,ListOrderHourCalled:false})
      }
    },
    componentDidMount() {
        this.OrderHourListType(1);
    },

    openingHour(){
       
        let data = {};
        data.name = this.refs.name.value;
        data.location_id = this.refs.location_id.value;
        data.day_of_week = this.refs.day_of_week.value;
        data.open_time = this.refs.open_time.value;
        data.close_time = this.refs.close_time.value;
        this.props.dispatch(LocationsActions.addOpeningHourData(data));
        this.setState({addOpenHourClicked:true});
        console.log(data)
    },

    OrderHourListType(page){
        debugger;
        let data = {};
       
        this.props.dispatch(LocationsActions.getListOpeningHourData(data));
        this.setState({ListOrderHourCalled:true})

    },

    EditOpeningHour(event){
   
        let data = {};
          let id = event.target.dataset.id;
          let OrderHourListData = this.props.LocationData.GetOpeningHour.data;
          
          data = _.filter(OrderHourListData, function (i) {
          return i._id == id ;
          });
      this.props.dispatch(LocationsActions.OpeningHourById(data[0]));
      this.props.router.push('/setup/location/edit-opningHour');
    },

    deleteOrderHrType(event){

        let data = {};
        data.id = parseInt(event.target.dataset.id);
        this.props.dispatch(LocationsActions.deleteOpeningHour(data));
        this.setState({DeleteOrderHrCalled:true})
    },

    addStaff(){
        this.props.router.push('staff/add-staff');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   generateOrderHourList(){
        let orderHourList = this.state.OrderHourListData;
        let orderHourTemplate = orderHourList ?  orderHourList.map(function (listItem, key) {
          return (<tr key={key}> <td>{listItem.name}</td><td>{listItem.location_id}</td><td>{listItem.day_of_week}</td><td>{listItem.open_time}</td><td>{listItem.close_time}</td><td><span data-id={listItem._id} onClick={this.EditOpeningHour} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteOrderHrType}></span></td></tr>)
          }, this):("No Data Found");
        return orderHourTemplate ;
    },
    render() {
        let list = this.generateOrderHourList();
        return (

            <div class>
               
                 <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Opening Hours</h1>
                    </div>

                    <div className="filter-by">
                        <h3>Add Opening Hours</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="name">Name:</label>
                            <input className="form-control"  ref="name" type="text"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="location_id">Location:</label>
                            <select  ref="location_id" id="location_id" className="form-control">
                                <option selected="selected" value="1">All Locations</option>
                                <option value="2">Hornchurch</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="day_of_week">Day Of Week:</label>
                            <select  ref="day_of_week" id="day_of_week" className="form-control">
                                <option value="0">All Days</option>
                                <option value="1">Sunday</option>
                                <option value="2">Monday</option>
                                <option value="3">Tuesday</option>
                                <option value="4">Wednesday</option>
                                <option value="5">Thursday</option>
                                <option value="6">Friday</option>
                                <option value="7">Saturday</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="open_time">Open Time:</label>
                            <input className="form-control"  ref="open_time" type="text"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="close_time">Close Time:</label>
                            <input className="form-control"  ref="close_time" type="text"/>
                        </div>
                        <div className="buttonBtn">
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.openingHour} value="Add" type="button" id=""/>
                          </div>
                        </div>
                    </div>

                    <div className="filter-by">
                        <h3>Filter Opening Hours</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="staff_location">Filter by Location:</label>
                            <select  ref="staff_location" id="staff_location" className="form-control">
                               <option selected="selected" value="-1">Show All</option>
                                <option value="15875">Hornchurch</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="staff_location">Filter by Day of the Week:</label>
                            <select  ref="staff_location" id="staff_location" className="form-control">
                                <option value="-1">All Days</option>
                                <option value="0">Sunday</option>
                                <option value="1">Monday</option>
                                <option value="2">Tuesday</option>
                                <option value="3">Wednesday</option>
                                <option value="4">Thursday</option>
                                <option value="5">Friday</option>
                                <option value="6">Saturday</option>
                           </select>
                        </div>
                    </div>

                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Location</th>
                                    <th>Day Of Week</th>
                                    <th>Opening Hour</th>
                                    <th>Close Time</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      LocationData:state.Location,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(LocationsActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(OpeningHr));
