import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as LocationsActions from '../../../actions/setup/locations_actions';
import { bindActionCreators } from 'redux';


let ShowDevice = React.createClass({
    getInitialState() {
        return {
             DeviceListRquested:false, deviceListData:null,deleteClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        let nextDevice = nextProps.LocationData.GetDevice;
        if(this.state.DeviceListRquested && nextDevice){
                if(nextDevice.status_code === 200){
                    this.setState({deviceListData:nextDevice.data,DeviceListRquested:false})
                }
            }

            let nextDeviceDelete = nextProps.LocationData.DeleteDevice;
            if(this.state.deleteClicked && nextDeviceDelete){
                if(nextDeviceDelete.status_code === 200){
                  this.GetDeviceList();
                  this.setState({deleteClicked:false});
                }
            }
    },

    componentDidMount() {
        
        this.GetDeviceList();
    },

    GetDeviceList(){
         
        let l_id=window.location.search.slice(4);
        this.props.dispatch(LocationsActions.getListDeviceData({location_id :l_id}));
        this.setState({DeviceListRquested:true});
    },

    deleteLocation(event){
      let data = {};
       
      data.id = event.target.dataset.id;
      this.setState({deleteClicked:true});
      this.props.dispatch(LocationsActions.deleteDevice(data));
    },

    editDevice(event){
      let data = {};
      let id = event.target.dataset.id;
      let LocationListData = this.props.LocationData.GetDevice.data;
      
      data = _.filter(LocationListData, function (i) {
      return i._id == id ;
      });
      this.props.dispatch(LocationsActions.deviceById(data[0]));
      this.props.router.push('/setup/location/edit-device');
},

    addDevive(){
        this.props.router.push('/setup/location/add-devices?id='+window.location.search.slice(4));
    },

    // editDevice(){
    //     this.props.router.push('/setup/location/add-location');
    // },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

    generateDeviceList(){
        let deviceList = this.state.deviceListData;
        let deviceTemplate = deviceList ?  deviceList.map(function (listItem, key) {
        return (<tr key={key}><td>{listItem.name}</td><td>{listItem.description}</td><td>{listItem.type}</td><td>{listItem.price_mode}</td><td>{listItem.default_purchase_type}</td><td>{listItem.location}</td><td></td><td>{listItem.default_booking_item}</td><td> </td><td><span data-id={listItem._id} onClick={this.editDevice} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteDevice}></span></td></tr>)
        }, this):("No Data Found");
        return deviceTemplate ;
    },

    render() {
        let list = this.generateDeviceList();
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Devices</h1>
                        <input type="button" onClick={this.addDevive} className="add-btn pull-right" value="Add Device" />
                    </div>
                    <div className="parainfo">
                        <p>A Device (Till) is used to process transactions (selling products).</p>
                    </div>
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Discription</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                    <th>Enabled</th>
                                    <th>Price Mode</th>
                                    <th>Default Purchase type</th>
                                    <th>Default booking item</th>
                                    <th>Unassign License</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      LocationData:state.Location,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(LocationsActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(ShowDevice));
