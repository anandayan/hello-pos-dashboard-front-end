import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as LocationsActions from '../../../actions/setup/locations_actions';
import { bindActionCreators } from 'redux';


let EditOpeningHr = React.createClass({
    getInitialState() {
        return {
             editOpenHourClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        
        if(this.state.editOpenHourClicked){
        // alert(nextProps.LocationData.UpdateOpeningHour.message)
        this.setState({editOpenHourClicked:false});
      }
      
    },
    componentDidMount() {
    },

    EditOpeningHour(){
       
        let data = {};
        data.id = this.props.LocationData.OpeningHourById._id;
        data.location = this.refs.location.value;
        data.day_of_week = this.refs.dayOfWeek.value;
        data.open_time = this.refs.openTime.value;
        data.close_time = this.refs.closeTime.value;
        this.props.dispatch(LocationsActions.getUpdateOpeningHourData(data));
        this.setState({editOpenHourClicked:true});
        console.log(data)
    },

    
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   
    render() {
        return (

            <div class>
               
                 <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        <a href="#"><strong>Click here</strong> to upgrade your license to include support</a>
                    </div>
                    <div className="common-info">
                        <h1 className="title">Edit Opening Hours</h1>
                    </div>

                    <div className="filter-by">
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="location">Location:</label>
                            <select  ref="location" id="location" className="form-control" defaultValue={this.props.LocationData.OpeningHourById.location}>
                                <option selected="selected" value="1">All Locations</option>
                                <option value="2">Hornchurch</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="dayOfWeek">Day Of Week:</label>
                            <select  ref="dayOfWeek" id="dayOfWeek" className="form-control" defaultValue={this.props.LocationData.OpeningHourById.dayOfWeek}>
                                <option value="0">All Days</option>
                                <option value="1">Sunday</option>
                                <option value="2">Monday</option>
                                <option value="3">Tuesday</option>
                                <option value="4">Wednesday</option>
                                <option value="5">Thursday</option>
                                <option value="6">Friday</option>
                                <option value="7">Saturday</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="openTime">Open Time:</label>
                            <input className="form-control"  ref="openTime" type="text" defaultValue={this.props.LocationData.OpeningHourById.openTime}/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="closeTime">Close Time:</label>
                            <input className="form-control"  ref="closeTime" type="text" defaultValue={this.props.LocationData.OpeningHourById.closeTime}/>
                        </div>
                        <div className="buttonBtn">
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.EditOpeningHour} value="Update" type="button" id=""/>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      LocationData:state.Location,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(LocationsActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(EditOpeningHr));
