import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as LocationsActions from '../../../actions/setup/locations_actions';
import { bindActionCreators } from 'redux';


let EditOrderPrint = React.createClass({
    getInitialState() {
        return {
             editOrderPrinterClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      
        if(this.state.editOrderPrinterClicked){
        // alert(nextProps.LocationData.UpdateOpeningHour.message)
        this.setState({editOrderPrinterClicked:false});
      }

    },
    componentDidMount() {
    },

    UpdateOrderPrint(){
     
      let data = {};
        data.id = this.props.LocationData.orderPrinterById._id;
        data.device_id = this.refs.device.value;
        data.printer_type = this.refs.printerType.value;
        this.props.dispatch(LocationsActions.getUpdateOrderPrinterData(data));
        this.setState({editOrderPrinterClicked:true});
        console.log(data)
    },

    

    


    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   

    render() {
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        <a href="#"><strong>Click here</strong> to upgrade your license to include support</a>
                    </div>
                    <div className="common-info">
                        <h1 className="title">Order Printer Advanced</h1>
                    </div>
                    <div className="filter-by">
                      <h3>Order Printer Filter</h3>

                    <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label htmlFor="device">Device:</label>
                       <select  ref="device" id="device" className="form-control" defaultValue={this.props.LocationData.orderPrinterById.device}>
                          <option value="1">* Select Device</option>
                          <option value="2">Calculator 1</option>
                          <option value="3">Change Calculator 1</option>
                          <option value="4">Employee Working Hours 1</option>
                          <option value="5">Notice Board 1</option>
                          <option value="6">PayPal Here 2.0 1</option>
                          <option value="7">PayPal Here 2.0 1</option>
                          <option value="8">Rapid Edit 1</option>
                          <option value="9">Till 1</option>
                          <option value="10">TILL 2</option>
                          <option value="11">TripAdvisor 1</option>
                          <option value="12">Zapier Integration</option>
                       </select>
                    </div>
                    <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label htmlFor="printerType">Printer Type:</label>
                       <select  ref="printerType" id="printerType" className="form-control" defaultValue={this.props.LocationData.orderPrinterById.printerType}>
                           <option value="">All Printer Types</option>
                            <option value="1">BAR</option>
                            <option value="2">KITCHEN</option>
                            <option value="3">Order A</option>
                            <option value="4">Order B</option>
                            <option value="5">Order C</option>
                            <option value="6">Rear</option>
                            <option value="7">Front</option>
                       </select>
                    </div>
                    <div className="buttonBtn">
                      <div className="pull-right add-otherBtn">
                         <input class onClick={this.UpdateOrderPrint}  value="Update" type="button" id=""/>
                      </div>
                    </div>
                    </div>

                    

                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      LocationData:state.Location,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(LocationsActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(EditOrderPrint));
