import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as LocationsActions from '../../../actions/setup/locations_actions';
import { bindActionCreators } from 'redux';


let UpdateLocation = React.createClass({
    getInitialState() {
        return {
             updateClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      if(this.state.updateClicked){
        alert(nextProps.LocationData.UpdateLocation.message)
        this.setState({updateClicked:false});
      }
    },
    componentDidMount() {

    },

    updateDevice(){
      
      let data = {};
    
        data.id = this.props.LocationData.deviceById._id;
        data.name = this.refs.name.value;
        data.description = this.refs.description.value;
        data.type = this.refs.type.value;
        data.price_mode = this.refs.price_mode.value;
        data.default_purchase_type = this.refs.default_purchase_type.value;
        data.location_id = this.refs.location.value;        
        data.default_booking_item = this.refs.default_booking_item.value;
        this.props.dispatch(LocationsActions.getUpdateDeviceData(data));
        this.setState({updateClicked:true});
        console.log(data);
    },


    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   back(event) {
        this.props.router.push('/setup/location/locationDevices');
    },
    render() {
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Add Device</h1>
                    </div>
                    <div className="filter-by">
                        <h3>Add Device</h3>

                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="name">Name:</label>
                          <input className="form-control"  ref="name" type="text"  id="" />
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="description">Description:</label>
                          <input className="form-control"  ref="description" type="text"  id="" />
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="type">Type</label>
                         <select  ref="type" id="type" className="form-control">
                             <option value="">PC</option>
                              <option value="1">mac</option>
                         </select>
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="location">Location</label>
                         <select  ref="location" id="location" className="form-control">
                             <option value="">Default</option>
                              <option value="1">Hornchurch</option>
                         </select>
                      </div>
                      <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                         <input  type="checkbox" ref="isEnabled" id="isEnabled" value="1"/>
                         <label htmlFor="isEnabled">Enabled</label>
                     </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="price_mode">Price Mode:</label>
                          <select  ref="price_mode" id="price_mode" className="form-control">
                             <option value="">Exc. Tax</option>
                              <option value="1">Inc. Tax</option>
                         </select>
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="default_purchase_type">Default Purchase type</label>
                          <select  ref="default_purchase_type" id="default_purchase_type" className="form-control">
                             <option value="">Walk In</option>
                              <option value="1">Take Away</option>
                              <option value="1">Delivery</option>
                         </select>
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="default_booking_item">Default booking item:</label>
                         <select  ref="default_booking_item" id="default_booking_item" className="form-control">
                             <option value="1">Default booking item</option>
                              
                         </select>
                      </div>
                      <div className="buttonBtn">
                        <div className="cancelBtn">
                           <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                        </div>
                        <div className="pull-right add-otherBtn">
                           <input class onClick={this.updateDevice} value="Update" type="button" id=""/>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       LocationData:state.Location,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(LocationsActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(UpdateLocation));
