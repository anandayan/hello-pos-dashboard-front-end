import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as LocationsActions from '../../../actions/setup/locations_actions';
import { bindActionCreators } from 'redux';
import * as utils from '../../../util';
import qq from '../../../img/question-mark.png';
import { Popover,OverlayTrigger } from 'react-bootstrap';
const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Brands">
      <p>This page can be used to filter certain Orders from certain devices.</p>
                        <p>Choose a Device and Printer Type and click 'Add' below to stop the selected Order Printer from printing out orders from the selected Device.</p>
    </Popover>
  );

let OrderPrint = React.createClass({
    getInitialState() {
        return {
             addOrderPrinterClicked:false,
             OrderListData:null,
             ListOrderTypeCalled:false,
             DeleteOrderCalled:false,
        }
    },
    componentWillMount() {
       
    },
    componentWillReceiveProps(nextProps) {
      

      
      let nexporps = nextProps.LocationData;
        if(this.state.addOrderPrinterClicked){
          if(nexporps.AddOrderPrinter){
            alert(nexporps.AddOrderPrinter.message);
            this.setState({addOrderPrinterClicked:false})
            this.OrderPrintListType(1);
          }
      }
      
      if(this.state.ListOrderTypeCalled){
        nexporps.GetOrderPrinter && this.setState({ListOrderTypeCalled:false,OrderListData:nexporps.GetOrderPrinter.data})
      }

      if(this.state.DeleteOrderCalled){
          this.OrderPrintListType(1);
        this.setState({ListOrderTypeCalled:true,DeleteOrderCalled:false})
      }


    },
    componentDidMount() {
      debugger;
      this.props.dispatch(LocationsActions.getListOrderPrinterData());
      this.props.dispatch(LocationsActions.getListDeviceData());
      this.OrderPrintListType();
    },

    OrderPrint(){
   
      let data = {};
        data.device_id = this.refs.device.value;
        data.printer_type = this.refs.printer_type.value;
        this.props.dispatch(LocationsActions.addOrderPrinterData(data));
        this.setState({addOrderPrinterClicked:true});
        console.log(data)
    },

    OrderPrintListType(page){
        let data = {};
       
        this.props.dispatch(LocationsActions.getListOrderPrinterData(data));
        this.setState({ListOrderTypeCalled:true})

    },

    deleteOrderPrintType(event){

        let data = {};
        data.id = parseInt(event.target.dataset.id);
        this.props.dispatch(LocationsActions.deleteOrderPrinter(data));
        this.setState({DeleteOrderCalled:true})
    },
    editOrderPrintType(event){
      
        let data = {};
          let id = event.target.dataset.id;
          let OrderListData = this.props.LocationData.GetOrderPrinter.data;
       
          data = _.filter(OrderListData, function (i) {
          return i._id == id ;
          });
      this.props.dispatch(LocationsActions.orderPrinterById(data[0]));
      this.props.router.push('/setup/location/edit-order-print');
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   generateOrderList(){
        let orderList = this.state.OrderListData;
        let orderTemplate = orderList ?  orderList.map(function (listItem, key) {
          return (<tr key={key}> <td>{listItem.device_id}</td><td>{listItem.printer_type}</td><td><span data-id={listItem._id} onClick={this.editOrderPrintType} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteOrderPrintType}></span></td></tr>)
          }, this):("No Data Found");
        return orderTemplate ;
    },

    render() {
      let list = this.generateOrderList();
        return (

            <div className="">
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                  
                    <div className="common-info">
                    
                        <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                        <h1 className="title">Order Printer Advanced <img src = {qq} alt="qus mark"/></h1>
                    </OverlayTrigger>
                    </div>
                                        <div className="filter-by">
       
                    <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label htmlFor="device">Device:</label>
                       <select  ref="device" id="device" className="form-control">
                          { utils.genderateOptionTemplate(this.props.device.data)}
                       </select>
                    </div>
                    <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label htmlFor="printer_type">Printer Type:</label>
                       <select  ref="printer_type" id="printer_type" className="form-control">
                           { utils.genderateOptionTemplate(this.props.orderPrintType.data)}
                       </select>
                    </div>
                    <div className="buttonBtn">
                      <div className="pull-right add-otherBtn">
                         <input class onClick={this.OrderPrint}  value="Add" type="button" id=""/>
                      </div>
                    </div>
                    </div>

                    

                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Device</th>
                                    
                                    <th>Printer Type</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                              {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      LocationData:state.Location,
      orderPrintType:state.Location.GetOrderPrinter,
      device:state.Location.GetDevice,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(LocationsActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(OrderPrint));
