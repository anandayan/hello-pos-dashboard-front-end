import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as LocationsActions from '../../../actions/setup/locations_actions';
import { bindActionCreators } from 'redux';


let AddDevice = React.createClass({
    getInitialState() {
        return {
             addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      if(this.state.addClicked){
        this.props.router.push('setup/location/add-device?id='+window.location.search.slice(4));
        this.setState({addClicked:false});
      }
    },
    componentDidMount() {

    },

    addDevice(){
      
      let data = {};
         
        data.name = this.refs.name.value;
        data.description = this.refs.description.value;
        data.type = this.refs.type.value;
        data.price_mode = this.refs.price_mode.value;
        data.default_purchase_type = this.refs.default_purchase_type.value;
        data.location_id = window.location.search.slice(4);
        data.is_enabled = this.refs.isEnabled.value;
        data.default_booking_item = this.refs.default_booking_item.value;
        debugger;
        this.props.dispatch(LocationsActions.addDeviceData(data));
        this.setState({addClicked:true});
      
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    render() {
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Add Device</h1>
                    </div>
                    <div className="filter-by">
                        <h3>Add Device</h3>

                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="name">Name:</label>
                          <input className="form-control"  ref="name" type="text"  id="" />
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="description">Description:</label>
                          <input className="form-control"  ref="description" type="text"  id="" />
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="type">Type</label>
                         <select  ref="type" id="type" className="form-control">
                             <option value="pc">PC</option>
                              <option value="mac">mac</option>
                         </select>
                      </div>
                     
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="price_mode">Price Mode:</label>
                          <select  ref="price_mode" id="price_mode" className="form-control">
                             <option value="">Exc. Tax</option>
                              <option value="1">Inc. Tax</option>
                         </select>
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="default_purchase_type">Default Purchase type</label>
                          <select  ref="default_purchase_type" id="default_purchase_type" className="form-control">
                             <option value="0">Walk In</option>
                              <option value="1">Take Away</option>
                              <option value="2">Delivery</option>
                         </select>
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="default_booking_item">Default booking item:</label>
                         <select  ref="default_booking_item" id="default_booking_item" className="form-control">
                             <option value="1">Default booking item</option>
                              
                         </select>
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                      <div className="checkbox checkbox-inline">
                         <input  type="checkbox" ref="isEnabled" id="isEnabled" value="1"/>
                         <label htmlFor="isEnabled">Enabled</label>
                     </div>
                      </div>
                    
                      <div className="buttonBtn">
                        
                        <div className="pull-right add-otherBtn">
                           <input   onClick={this.addDevice} value="Add" type="button" id=""/>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       LocationData:state.Location,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(LocationsActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddDevice));
