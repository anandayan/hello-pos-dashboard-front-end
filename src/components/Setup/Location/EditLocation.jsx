import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as LocationsActions from '../../../actions/setup/locations_actions';
import { bindActionCreators } from 'redux';


let AddLocation = React.createClass({
    getInitialState() {
        return {
             addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      if(this.state.addClicked){
 
         alert(nextProps.LocationData.UpdateLocation.message)
        this.setState({addClicked:false});
      }
    },
    componentDidMount() {

    },

    editLocation(){
      
      let data = {};
        data.id = this.props.LocationData.LocationById._id; 
        data.name = this.refs.name.value;
        data.description = this.refs.description.value;
        data.address1 = this.refs.address1.value;
        data.address2 = this.refs.address2.value;
        data.town = this.refs.town.value;
        data.country = this.refs.country.value;
        data.post_code = this.refs.postCode.value;
        data.stock_email = this.refs.stockEmail.value;
        data.locale = this.refs.locale.value;
        data.time_zone = this.refs.timezone.value;
        data.show_tabs = this.refs.showTabs.value;
        data.number_of_tabs = this.refs.numberOfTabs.value;
        data.tab_spending_limit = this.refs.tabSpendingLimit.value;
        this.props.dispatch(LocationsActions.updtLocationData(data));
        this.setState({addClicked:true});
        console.log(data)
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   back(event) {
        this.props.router.push('/setup/location/locationDevices');
    },
    render() {
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Add No Sale Reason</h1>
                    </div>
                    <div className="filter-by">
                        <h3>Add Location</h3>

                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="name">Name:</label>
                          <input className="form-control"  ref="name" defaultValue={this.props.LocationData.LocationById.name} type="text"  id="" />
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="">Description:</label>
                          <input className="form-control" ref="description" type="text" defaultValue={this.props.LocationData.LocationById.description}  id="" />
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="address1">Address 1:</label>
                          <input className="form-control"  ref="address1" type="text" defaultValue={this.props.LocationData.LocationById.address1}   id="" />
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="address2">Address 2:</label>
                          <input className="form-control"  ref="address2" type="text" defaultValue={this.props.LocationData.LocationById.address2}  id="" />
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="town">Town:</label>
                          <input className="form-control"  ref="town" type="text" defaultValue={this.props.LocationData.LocationById.town}  id="" />
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="country">County:</label>
                          <input className="form-control"  ref="country" type="text" defaultValue={this.props.LocationData.LocationById.country}  id="" />
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="stockEmail">Stock Email Address:</label>
                          <input className="form-control"  ref="stockEmail" type="text" defaultValue={this.props.LocationData.LocationById.stockEmail}  id="" />
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="staff_location">Locate:</label>
                         <select  ref="locale" id="locale" className="form-control">
                             <option value="">Default (English (UK))</option>
                              <option value="1">English (UK)</option>
                         </select>
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="timezone">Time Zone:</label>
                         <select  ref="timezone" id="timezone" className="form-control">
                             <option value="">Default</option>
                              <option value="1">UTC</option>
                         </select>
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="tabSpendingLimit">Tab Default Spend Limit:</label>
                          <input className="form-control"  ref="tabSpendingLimit" type="text" defaultValue={this.props.LocationData.LocationById.tabSpendingLimit}  id="" />
                      </div>

                      <div className="checkbox checkbox-inline col-xs-4 col-sm-12 col-md-12 col-lg-13">
                        <input  type="checkbox" ref="showTabs" id="showTabs" value="1"/>
                        <label htmlFor="showTabs">Show Tabs:</label>
                      </div>
                      <div className= "col-md-12">
                        <input ref="numberOfTabs" className="form-control input-text-fields" id="numberOfTabs" defaultValue={this.props.LocationData.LocationById.numberOfTabs}  type="text" />
                      </div>
                      <div className="filter-by">
                        <div className="col-sm-4">
                            <span className="help-block txtalignRight">Postcode</span>
                        </div>
                        <div className="col-sm-8">
                          <input ref="postCode" className="form-control input-text-fields" id="postCode"  type="text" defaultValue={this.props.LocationData.LocationById.postCode}   autoComplete="off"/>
                          <input className="add-other" value="Complete Address" type="button" id=""/>
                        </div>
                      </div>
                      <div className="buttonBtn">
                        <div className="cancelBtn">
                           <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                        </div>
                        <div className="pull-right add-otherBtn">
                           <input class onClick={this.editLocation} value="Update" type="button" id=""/>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       LocationData:state.Location,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(LocationsActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddLocation));
