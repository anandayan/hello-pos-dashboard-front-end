import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as LocationsActions from '../../../actions/setup/locations_actions';
import { bindActionCreators } from 'redux';
import qq from '../../../img/question-mark.png';
import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Locations and Devices">
         <p>Locations are used to group sales figures into geographical locations as well as define locations to help monitor stock levels. A location is typically a store or warehouse.</p>
                        <p>Click "Show Devices" to show all of the devices at a given location and they will be shown below.</p>
    </Popover>
  );

let LocationsDevice = React.createClass({
    getInitialState() {
        return {
             LocationListRquested:false, LocationListData:null,deleteClicked:false,editClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        let nextLocation = nextProps.LocationData.GetLocation;
            if(this.state.LocationListRquested && nextLocation){
                if(nextLocation.status_code === 200){
                    this.setState({LocationListData:nextLocation.data,LocationListRquested:false})
                }
            }

            let nextLocationDelete = nextProps.LocationData.DeleteLocation;
            if(this.state.deleteClicked && nextLocationDelete){
                if(nextLocationDelete.status_code === 200){
                  this.GetLocationList();
                  this.setState({deleteClicked:false});
                }
            }
    },

    componentDidMount() {
        this.GetLocationList();
    },

    GetLocationList(){
                
        this.props.dispatch(LocationsActions.getListLocation());
        this.setState({LocationListRquested:true});
    },
    deleteLocation(event){
      let data = {};
     
      data.id = event.target.dataset.id;

      this.setState({deleteClicked:true});
      this.props.dispatch(LocationsActions.dltLocationData(data));
    },
    
    editLocation(event){
      let data = {};
      let id = event.target.dataset.id;
      let LocationListData = this.props.LocationData.GetLocation.data;
      
      data = _.filter(LocationListData, function (i) {
      return i._id == id ;
      });
      this.props.dispatch(LocationsActions.LocationById(data[0]));
      this.props.router.push('/setup/location/edit-location');
},

    addLocation(){
        this.props.router.push('/setup/location/add-location');
    },

    ShowDevice(e){
       
        let id = e.target.dataset.id
        
        this.props.router.push('/setup/location/add-device'+'?id='+id);
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   generateLoctionList(){
        let locationList = this.state.LocationListData;
        let locationTemplate = locationList ?  locationList.map(function (listItem, key) {
        return (<tr key={key}><td>{listItem.name}</td><td>{listItem.description}</td> <td>{listItem.address1}</td><td>{listItem.address2}</td><td>{listItem.town}</td><td>{listItem.country}</td><td>{listItem.post_code}</td><td>{listItem.stock_email}</td><td>{listItem.locale}</td><td>{listItem.time_zone}</td><td>{listItem.show_tabs}</td><td>{listItem.number_of_tabs}</td><td>{listItem.tab_spending_limit}</td><td><button data-id={listItem._id} onClick={this.ShowDevice} className="edit btn btn-primary ">Show Device</button></td><td><span data-id={listItem._id} onClick={this.editLocation} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteLocation}></span></td></tr>)
        }, this):("No Data Found");
        return locationTemplate ;
    },
    render() {
        let list = this.generateLoctionList();
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>

                        <h1 className="title">Locations <img src = {qq} alt="qus mark"/></h1>
                    </OverlayTrigger>
                        <input type="button" onClick={this.addLocation} className="add-btn pull-right" value="Add Location" />
                    </div>
                   
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Discription</th>
                                    <th>Address1</th>
                                    <th>Address2</th>
                                    <th>town</th>
                                    <th>country</th>
                                    <th>postCode</th>
                                    <th>Stock Email Address</th>
                                    <th>Locate</th>
                                    <th>Time zone</th>
                                    <th>Tabs</th>
                                    <th>Number of bar tabs</th>
                                    <th>tabSpendingLimit</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      LocationData:state.Location,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(LocationsActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(LocationsDevice));
