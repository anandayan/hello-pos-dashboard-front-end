import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as RestoreActions from '../../../actions/setup/restore_actions';
import { bindActionCreators } from 'redux';


let RestoreD = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    addStaff(){
        this.props.router.push('staff/add-staff');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    render() {
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                   <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Restore Data</h1>
                    </div>
                    <div className="parainfo">
                    <p>On this page you can view and restore data items that have been deleted.</p>
                    <p>To restore data item(s), select the Restore checkbox for the items and click the Restore button.</p>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      RestoreDetail:state.Restores,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(RestoreActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(RestoreD));
