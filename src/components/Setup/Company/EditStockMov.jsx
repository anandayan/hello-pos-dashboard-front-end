import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let AddStockReason = React.createClass({
    getInitialState() {
        return {
            addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        if(this.state.addClicked){
            alert(nextProps.CompanyData.UpdateStockMovement.message)
        this.setState({addClicked:false});
      }
    },
    componentDidMount() {

    },

    updateStock(){
      let data = {};
        data.id = this.props.CompanyData.StockMovementById._id;
        data.reason = this.refs.reason.value;
        this.props.dispatch(CompanyActions.updtStockMovementData(data));
        this.setState({addClicked:true});
        console.log(data)
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   back(event) {
        this.props.router.push('/setup/company/StockMovementReasons');
    },
    render() {
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Stock Movement Reasons</h1>
                    </div>
                    <div className="filter-by">
                      <h3>Add Stock Movement Reason</h3>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="reason">Description:</label>
                          <input ref="reason" className="form-control" defaultValue={this.props.CompanyData.StockMovementById.reason} id="reason"  type="text"   autoComplete="off"/>
                      </div>
                      <div className="buttonBtn">
                      <div className="cancelBtn">
                         <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                      </div>
                      <div className="pull-right add-otherBtn">
                         <input class onClick={this.updateStock} value="Update" type="button" id=""/>
                      </div>
                    </div>
                    </div>
                    
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddStockReason));
