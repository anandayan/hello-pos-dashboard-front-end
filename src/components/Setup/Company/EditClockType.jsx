import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let EditClockingType = React.createClass({
    getInitialState() {
        return {
             editClockTypeClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
 
        if(this.state.editClockTypeClicked){
        // alert(nextProps.LocationData.UpdateOpeningHour.message)
        this.setState({editClockTypeClicked:false});
      }

    },
    componentDidMount() {

    },

    editClockType(){
      
        let data = {};
        data.id = this.props.CompanyData.ClockById._id;
        data.name = this.refs.clock_name.value;
        data.pay_multiplier = this.refs.pay_multiplier.value;
        this.props.dispatch(CompanyActions.updtClockData(data));
        this.setState({editClockTypeClicked:true});
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
  
    render() {
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        <a href="#"><strong>Click here</strong> to upgrade your license to include support</a>
                    </div>
                    <div className="common-info">
                        <h1 className="title">Course List</h1>
                    </div>
                    
                    <div className="filter-by">
                      <h3>Add Clocking Type</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-md-offset-2 col-lg-4">
                            <label htmlFor="clock_name">Name:</label>
                            <input className="form-control"  ref="clock_name" type="text" defaultValue={this.props.CompanyData.ClockById.name} />
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="pay_multiplier">Pay MUltiplier:</label>
                            <input className="form-control"  ref="pay_multiplier" type="text" defaultValue={this.props.CompanyData.ClockById.payMultiplier}/>
                        </div>

                        <div className="buttonBtn">
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.editClockType}  value="Update" type="button" id=""/>
                          </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(EditClockingType));
