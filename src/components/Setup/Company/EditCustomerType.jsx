import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let EditCustomerTypes = React.createClass({
    getInitialState() {
        return {
              editCustomerTypeClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
       
        if(this.state.editCustomerTypeClicked){
        // alert(nextProps.LocationData.UpdateOpeningHour.message)
        this.setState({editCustomerTypeClicked:false});
      }
    },
    componentDidMount() {
      
    },
    edit_Customer_Type(){
        let data = {};
        data.id = this.props.CompanyData.customerTypeById._id;
        data.name = this.refs.customer_type.value;
        data.description = this.refs.cust_type_descp.value;
        data.discount= this.refs.discount.value;
         this.props.dispatch(CompanyActions.updateCustomerData(data));
         this.setState({editCustomerTypeClicked:true});
    },
  

    render() {
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        <a href="#"><strong>Click here</strong> to upgrade your license to include support</a>
                    </div>
                    <div className="common-info">
                        <h1 className="title">Customer Types</h1>
                    </div>
                    <div className="parainfo">
                    <p>Customer Types can be used to give out discounts on products to valued customers and can also be used for memberships.</p>
                    </div>
                    <div className="filter-by">
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="staff_swipe_code">Name:</label>
                            <input className="form-control"  ref="customer_type" type="text" defaultValue={this.props.CompanyData.customerTypeById.name} />
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="staff_swipe_code">Discription:</label>
                            <input className="form-control"  ref="cust_type_descp" type="text" defaultValue={this.props.CompanyData.customerTypeById.description} />
                        </div>

                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="staff_swipe_code">Discount(%):</label>
                            <input className="form-control"  ref="discount" type="number" defaultValue={this.props.CompanyData.customerTypeById.discount} />
                        </div>
                        <div className="buttonBtn">

                          <div className="pull-right add-otherBtn">
                          <button className="add btn btn-primary"    onClick={this.edit_Customer_Type}>Update</button>
                          </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(EditCustomerTypes));
