import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let ClockingType = React.createClass({
    getInitialState() {
        return {
             ListClockData :null,
             addClockTypeClicked: false,
             ListClockTypeCalled:false,
             DeleteClockTypeCalled:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {


      let nexporps = nextProps.CompanyData;
        if(this.state.addClockTypeClicked){
          if(nexporps.AddClock){
            alert(nexporps.AddClock.message);
            this.setState({addClockTypeClicked:false})
            this.listClockType(1);
          }
      }
      // debugger;
      if(this.state.ListClockTypeCalled){
        nexporps.GetClockType && this.setState({ListClockTypeCalled:false,ListClockData:nexporps.GetClockType.data})
      }

      if(this.state.DeleteClockTypeCalled){
          this.listClockType(1);
        this.setState({ListClockTypeCalled:true,DeleteClockTypeCalled:false})
      }

    },
    componentDidMount() {
        this.listClockType();
    },
    listClockType(page){
        // debugger;
        let data = {};
        
        this.props.dispatch(CompanyActions.getListClock(data));
        this.setState({ListClockTypeCalled:true})

    },
    editClockType(event){
       
        let data = {};
          let id = event.target.dataset.id;
          let ListClockData = this.props.CompanyData.GetClockType.data;
          
          data = _.filter(ListClockData, function (i) {
          return i._id == id ;
          });
      this.props.dispatch(CompanyActions.ClockById(data[0]));
      this.props.router.push('/setup/company/edit-Clock-type');
    },

    deleteClockType(event){

        let data = {};
        data.id = event.target.dataset.id;
        this.props.dispatch(CompanyActions.dltClockData(data));
        this.setState({DeleteClockTypeCalled:true})
    },

    addClockType(){

        let data = {};
        data.name = this.refs.clock_name.value;
        data.pay_multiplier  = this.refs.pay_multiplier.value;
        this.props.dispatch(CompanyActions.addClockData(data));
        this.setState({addClockTypeClicked:true});
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   generateClockList(){
        let ClockTypeList = this.state.ListClockData;
        let clockTemplate = ClockTypeList ?  ClockTypeList.map(function (listItem, key) {
          return (<tr key={key}> <td>{listItem.name}</td><td>{listItem.pay_multiplier}</td><td><span data-id={listItem._id} onClick={this.editClockType} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteClockType}></span></td></tr>)
          }, this):("No Data Found");
        return clockTemplate ;
    },
    render() {
        let list = this.generateClockList();
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">

                    </div>
                   
                    <div className="filter-by">
                      <h3>Add Clocking Type</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="clock_name">Name:</label>
                            <input className="form-control"  ref="clock_name" type="text"  id="" />
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="pay_multiplier">Pay MUltiplier:</label>
                            <input className="form-control"  ref="pay_multiplier" type="text"/>
                        </div>

                        <div className="buttonBtn">
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.addClockType}  value="Add" type="button" id=""/>
                          </div>
                        </div>
                    </div>



                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Pay Multiplier</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(ClockingType));
