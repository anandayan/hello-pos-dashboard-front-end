import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import qq from '../../../img/question-mark.png';
import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Receipts">
         <p>This can be used to change the information on your receipt. You can leave any fields blank, except 'Company Name', if you do not wish for them to be displayed on your receipt.</p>
    </Popover>
  );

let Receipts = React.createClass({
    getInitialState() {
        return {
             addClicked:false,getReciptedClicked:false,receiptsDetails:null
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

            if(this.state.addClicked){
            // alert(nextProps.StaffData.HourAdded.message)
            this.setState({addClicked:false});
          }
          if(this.state.getReciptedClicked && nextProps.CompanyData.GetReciptAdded && nextProps.CompanyData.GetReciptAdded.status_code === 200){
            this.setState({receiptsDetails:nextProps.CompanyData.GetReciptAdded.data[0]})
          }
    },
    componentDidMount() {
      this.getrecipetDetails();
    },
    getrecipetDetails(){

      let data = {};
      this.props.dispatch(CompanyActions.getReceiptDetails(data));
      this.setState({getReciptedClicked:true});
    },
    ReceiptDetails(){

      let data = {};
        data.display_name = this.refs.display_name.value;
        data.tax_number = this.refs.tax_number.value;
        data.email = this.refs.email.value;
        data.website = this.refs.website.value;
        data.refund_days = this.refs.refund_days.value;
        data.receipt_msg = this.refs.receipt_msg.value;
        data.show_tax_breakdown = this.refs.show_tax_breakdown.checked ? 1 : 0;
        data.email_receipt = this.refs.email_receipt.checked ? 1 : 0;
        data.show_bal = this.refs.show_bal.checked ? 1 :0;
        data.print_addr_trans = this.refs.print_addr_trans.checked ? 1 :0;
        data.show_item_notes = this.refs.show_item_notes.checked ? 1 :0;
        data.grp_items_promotions_receipts = this.refs.grp_items_promotions_receipts.checked ? 1 :0;
        data.grp_items_ord_prints = this.refs.grp_items_ord_prints.checked ? 1 :0;
        data.use_product_name = this.refs.use_product_name.checked ? 1 :0;
        data.font_size = this.refs.fontsize.value;
        data.bar_code_type = this.refs.barCodeType.value;

        data.logo = this.refs.logo.value;
        this.props.dispatch(CompanyActions.sendUpdateReceiptData(data));
        this.setState({addClicked:true});

    },


    render() {

        return (

            <div className="">
            { this.state.receiptsDetails &&
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                      <h1 className="add-title">Receipts <img src = {qq} alt="qus mark"/></h1>
                    </OverlayTrigger>
                        
                    </div>
                   
                    <div className="filter-by">
                        <p>Company Information</p>
                        
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="display_name">Company Display Name:</label>
                            <input ref="display_name" className="form-control" id="display_name" defaultValue={this.state.receiptsDetails.display_name} type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="tax_number">Tax Number:</label>
                            <input ref="tax_number" className="form-control" id="tax_number" defaultValue={this.state.receiptsDetails.tax_number} type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="email">Email Address:</label>
                            <input ref="email" className="form-control" id="email" defaultValue={this.state.receiptsDetails.email} type="text"   autoComplete="off"/>
                        </div>

                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="website">Website Address:</label>
                            <input ref="website" className="form-control" id="website" defaultValue={this.state.receiptsDetails.website} type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="refund_days">Refund Days:</label>
                            <input ref="refund_days" className="form-control" id="refund_days" defaultValue={this.state.receiptsDetails.refund_days} type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="receipt_msg">Receipt Message:</label>
                            <input ref="receipt_msg" className="form-control" id="receipt_msg"  defaultValue={this.state.receiptsDetails.receipt_msg} type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="fontsize">Set custom font size:</label>
                            <select  ref="fontsize" className="form-control" defaultValue={this.state.receiptsDetails.fontSize}>
                                <option value="8.0">8.0</option>
                                <option value="8.5">8.5</option>
                                <option value="9.0">9.0</option>
                                <option value="9.5">9.5</option>
                                <option value="10.0">10.0</option>
                                <option value="10.5">10.5</option>
                                <option value="11.0">11.0</option>
                                <option value="11.5">11.5</option>
                                <option selected="selected" value="12.0">12.0</option>
                                <option value="12.5">12.5</option>
                                <option value="13.0">13.0</option>
                                <option value="13.5">13.5</option>
                                <option value="14.0">14.0</option>
                                <option value="14.5">14.5</option>
                                <option value="15.0">15.0</option>
                                <option value="15.5">15.5</option>
                                <option value="16.0">16.0</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="barCodeType"> Barcode type:</label>
                            <select ref="barCodeType" className="form-control" defaultValue={this.state.receiptsDetails.barCodeType}>
                                <option value="0">* Default - Code 39 - Wider Printer Support</option>
                                <option value="1">Code 39 - Wider Printer Support</option>
                                <option  value="2">Code 128 - Compatible with iOS and Android scanners</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="show_tax_breakdown" id="show_tax_breakdown"  defaultChecked={this.state.receiptsDetails.show_tax_breakdown ? true :false}/>
                                <label htmlFor="show_tax_breakdown">Show Tax Breakdown:</label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="email_receipt" id="email_receipt"   defaultChecked={this.state.receiptsDetails.email_receipt ? true :false} />
                                <label htmlFor="email_receipt">Email Receipt: (a copy of the receipt will be emailed to the customer)</label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="show_bal" id="show_bal" defaultChecked={this.state.receiptsDetails.show_bal ? true :false} />
                                <label htmlFor="showCustomerBalance">Show Customer Balance:</label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="print_addr_trans" id="print_addr_trans"  defaultChecked={this.state.receiptsDetails.print_addr_trans ? true :false} />
                                <label htmlFor="printCustomerAddressOnEatOutTrans">Print customer address on eat out transactions:</label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="show_item_notes" id="show_item_notes" defaultChecked={this.state.receiptsDetails.show_item_notes ? true :false}/>
                                <label htmlFor="showItemNotes">Show item notes and free multiple choice products on receipt:</label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="grp_items_promotions_receipts" id="grp_items_promotions_receipts"  defaultChecked={this.state.receiptsDetails.grp_items_promotions_receipts ? true :false} />
                                <label htmlFor="groupItemsAndPromotionsOnReceipt">Group items and promotions on receipt:</label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="grp_items_ord_prints" id="grp_items_ord_prints"  defaultChecked={this.state.receiptsDetails.grp_items_ord_prints ? true :false}/>
                                <label htmlFor="grp_items_ord_prints">Group items on order prints: </label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="use_product_name" id="use_product_name"  defaultChecked={this.state.receiptsDetails.use_product_name ? true :false} />
                                <label htmlFor="useProductName">Use product name rather than description on receipts</label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div class>
                                <input type="file" name="pic" ref="logo" accept="image/*" />
                                <label htmlFor="logo">Company logo</label>
                            </div>
                        </div>


                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="name">GUID (Used by Support only):</label>
                            <span className="pdleft35"></span>
                        </div>
                        <div className="buttonBtn">
                          
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.ReceiptDetails} value="Save" type="button" id=""/>
                          </div>
                        </div>
                    </div>


                </div>}
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(Receipts));
