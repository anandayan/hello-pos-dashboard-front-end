import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let CompanyDetail = React.createClass({
    getInitialState() {
        return {
             addClicked:false,getCompanyClicked:false,companyData:null
        }
    },
    componentWillMount() {
        
    },
    componentWillReceiveProps(nextProps) {
        
        if(this.state.addClicked){
          debugger;
        // alert(nextProps.CompanyData.GetCompanyAdded.message)
        this.setState({addClicked:false,getCompanyClicked:true});
        this.getCompanyDetail();
      }
      if(this.state.getCompanyClicked && nextProps.CompanyData.UpdateCompanyAdded){
        // /  debugger;
        this.getCompanyDetail();
      this.setState({companyData:nextProps.CompanyData.UpdateCompanyAdded.data,getCompanyClicked:false});

      }
    },
    componentDidMount() {
        this.getCompanyDetail();
    },
    getCompanyDetail(){
      let data = {};
      data.id =this.props.UserData.loginDetail.data._id;
      this.props.dispatch(CompanyActions.CompanyDetails());
      this.setState({getCompanyClicked:true});
    },
    CompanyDetails(){
            
      let data = {};
       
       data.name  = this.refs.company_name.value;
        data.tax_number = this.refs.taxNumber.value;
       data.currency = this.refs.currency.value;
         data.update_cost_price = this.refs.updateCostPrice.checked ? 1 : 0;
         data.set_covers_on_table_plan =this.refs.setCoversOnTablePlan.checked ? 1 : 0;
       data.number_of_covers  =this.refs.numOfCovers.value;
          data.service_charge =this.refs.serviceCharge.value;
       data.show_tax_option =this.refs.showTaxOption.checked ? 1 : 0;
       data.show_instructions =this.refs.showInstructions.checked ? 1:0;
      
      // debugger;
       this.props.dispatch(CompanyActions.upadeCompanyData(data));
        this.setState({addClicked:true});
      
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    render() {
       
        return (

            <div>
              { this.state.companyData &&
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                      <h1 className="add-title">Company: {this.props.UserData.loginDetail.data.business_name}</h1>
                        
                    </div>
                    <div className="filter-by">
                      <h3>Company Information</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="company_name">Company Name:</label>
                            <input ref="company_name"  value={this.props.UserData.loginDetail.data.business_name} className="form-control" readOnly  type="text"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-4 col-md-6 col-lg-4">
                            <label htmlFor="taxNumber">Tax Number:</label>
                            <input ref="taxNumber" defaultValue={this.state.companyData && this.state.companyData.tax_number} className="form-control" id="taxNumber"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="currency">Custom Currency:</label>
                            <select  ref="currency" defaultValue ={this.state.companyData && this.state.companyData.currency} className="form-control">
                                <option value="en-GB">Pounds (£)</option>
                                <option value="en-ie">Euro (€)</option>
                                <option value="en-US">US Dollars ($)</option>
                                <option value="ar-BH">Bahraini Dinar (BD)</option>
                                <option value="en-BW">Botswana Pula (P)</option>
                                <option value="en-CA">CAD Dollars ($)</option>
                                <option value="en-fr-XAF">Central African CFA franc (CFA)</option>
                                <option value="ar-EG">Egyptian Pound (E£)</option>
                                <option value="en-GH">Ghana Cedi (GH¢)</option>
                                <option value="ar-iq">Iraqi Dinar (د.ع)</option>
                                <option value="he-IL">Israeli Shekel (₪)</option>
                                <option value="ar-JO">Jordanian Dinar (JD)</option>
                                <option value="sw-KE">Kenyan Shilling (KSh)</option>
                                <option value="ar-KW">Kuwaiti Dinar(KD)</option>
                                <option value="ms-MY">Malaysian Ringgit (RM)</option>
                                <option value="en-MY">Mayanmar Kyat (K)</option>
                                <option value="es-MX">Mexican Peso ($)</option>
                                <option value="yo-NG">Naira (₦)</option>
                                <option value="en-NG">Naira (₦) (Western Calendar)</option>
                                <option value="nb-NO">Norwegian Krone (kr)</option>
                                <option value="ar-OM">Omani Rial (ر.ع.)</option>
                                <option value="ur-PK">Pakistani Rupee (Rs)</option>
                                <option value="es-PE">Peruvian Sol (S/.)</option>
                                <option value="ar-QA">Qatari Riyal (QR)</option>
                                <option value="ar-SA">Saudi Riyal (SR)</option>
                                <option value="sr-SP-Cyrl">Serbian Dinar (РСД)</option>
                                <option value="en-fr-SCR">Seychellois Rupee (Rs)</option>
                                <option value="en-za">South African Rand (R)</option>
                                <option value="SL">Sierra Leonean leone (Le)</option>
                                <option value="fr-CH">Swiss Franc (fr.)</option>
                                <option value="th-TH">Thai Baht (฿)</option>
                                <option value="en-TH">Thai Baht (฿) (Western Calendar)</option>
                                <option value="ar-ae">UAE Dirham (د.إ)</option>
                                <option value="sw-ug-UG">Ugandan Shilling (USh)</option>
                                <option value="zh-CN">Yuan (¥)</option>
                                <option value="en-blank">Blank Currency</option>
                            </select>
                        </div>

                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="serviceCharge">Service Charge (%):</label>
                            <input ref="serviceCharge" defaultValue={this.state.companyData && this.state.companyData.service_charge} className="form-control" id="serviceCharge"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="numOfCovers">Amount of Covers to activate Service Charge:</label>
                            <input ref="numOfCovers" defaultValue={this.state.companyData && this.state.companyData.number_of_covers} className="form-control" id="numOfCovers"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="updateCostPrice" id="updateCostPrice"   defaultChecked={this.state.companyData && this.state.companyData.update_cost_price ? true:false}/>
                                <label htmlFor="updateCostPrice"> Update Cost Price of Products when Master Product Cost Prices are updated </label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="setCoversOnTablePlan" id="setCoversOnTablePlan"   defaultChecked={this.state.companyData && this.state.companyData.set_covers_on_table_plan ? true:false}/>
                                <label htmlFor="setCoversOnTablePlan"> Set Covers on Table Plan: </label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="showTaxOption" id="showTaxOption"   defaultChecked={this.state.companyData && this.state.companyData.show_tax_option ? true:false}/>
                                <label htmlFor="showTaxOption"> Show Inc/Exc Tax Option: </label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="showInstructions" id="showInstructions"  defaultChecked={this.state.companyData && this.state.companyData.show_instructions ? true:false}/>
                                <label htmlFor="showInstructions">Show instructions on startup (Dashboard):</label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label htmlFor="name">Maximum Number of Devices:</label>
                            <span className="pdleft35">{this.state.companyData && this.state.companyData.maximumDevices}</span>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label htmlFor="name">Max number of Locations on your current price plan:</label>
                            <span className="pdleft35">{this.state.companyData && this.state.companyData.maximumLocations}</span>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label htmlFor="name">GUID:</label>
                            <span className="pdleft35">{this.state.companyData && this.state.companyData.GUID}</span>
                        </div>
                    </div>

                    <div className="buttonBtn">
                     
                      <div className="pull-right add-otherBtn">
                         <input class onClick={this.CompanyDetails} value="Save" type="button" id=""/>
                      </div>
                    </div>
                </div> }
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserData:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(CompanyDetail));
