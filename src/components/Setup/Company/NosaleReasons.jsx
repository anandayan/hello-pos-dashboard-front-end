import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let Nosale = React.createClass({
    getInitialState() {
        return {
             NosaleListData:null,NosaleListRquested:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        
            let nextNosale = nextProps.CompanyData.GetNoSale;
            if(this.state.NosaleListRquested && nextNosale){
                if(nextNosale.status_code === 200){
                    this.setState({NosaleListData:nextNosale.data,NosaleListRquested:false})
                }
            }

            let nextNosaleDelete = nextProps.CompanyData.DeleteNoSale;
            if(this.state.deleteClicked && nextNosaleDelete){
                if(nextNosaleDelete.status_code === 200){
                  this.GetNosaleList();
                  this.setState({deleteClicked:false});
                }
            }
    },
    componentDidMount() {
        this.GetNosaleList();
    },
    GetNosaleList(){
        // 
        let data = {};
       
        this.props.dispatch(CompanyActions.getListNoSale(data));
        this.setState({NosaleListRquested:true});
    },

    deleteRefund(event){
      let data = {};
      data.id = event.target.dataset.id;

      this.setState({deleteClicked:true});
      this.props.dispatch(CompanyActions.dltNoSaleData(data));
    },

    editNosale(event){
      let data = {};
      let id = event.target.dataset.id;
      let NosaleListData = this.props.CompanyData.GetNoSale.data;
      data = _.filter(NosaleListData, function (i) {
      return i._id == id ;
      });
 
      this.props.dispatch(CompanyActions.noSaleById(data[0]));
      this.props.router.push('/setup/company/edit-nosale');
    },

    noSale(){
        this.props.router.push('/setup/Company/NosaleReasons/add-noSale');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   generateNosaleList(){
        let nosaleList = this.state.NosaleListData;
        let nosaleTemplate = nosaleList ?  nosaleList.map(function (listItem, key) {
        return (<tr key={key}><td>{listItem.reason}</td><td><span data-id={listItem._id} onClick={this.editNosale} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteRefund}></span></td></tr>)
        }, this):("No Data Found");
        return nosaleTemplate ;
    },

    render() {
        let list = this.generateNosaleList();
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="cutomers">
                        <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">No Sale Reasons</h1>
                        <input type="button" onClick={this.noSale} className="add-btn pull-right" value="Add NoSale Reasons" />
                    </div>
                    </div>
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Reason</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(Nosale));
