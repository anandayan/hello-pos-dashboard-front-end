import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let AddNoSale = React.createClass({
    getInitialState() {
        return {
             addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        if(this.state.addClicked){
 
        alert(nextProps.CompanyData.AddNoSale.message)
        this.setState({addClicked:false});
      }
    },
    componentDidMount() {

    },

    addNosale(){
         
        let data = {};
        data.reason  = this.refs.reason.value;
        this.props.dispatch(CompanyActions.addNoSaleData(data));
        this.setState({addClicked:true});
        console.log(data)
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   back(event) {
        this.props.router.push('/setup/Company/NosaleReasons');
    },
    render() {
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Add No Sale Reason</h1>
                    </div>
                    <div className="filter-by-brand">
                      <div className="col-sm-2">
                          <span className="help-block txtalignRight">Reason:</span>
                      </div>
                      <div className="col-sm-8">
                        <input ref="reason" className="form-control input-text-fields" id="reason"  type="text"   autoComplete="off"/>
                      </div>
                    </div>
                    <div className="buttonBtn">
                      <div className="cancelBtn">
                         <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                      </div>
                      <div className="pull-right add-otherBtn">
                         <input class onClick={this.addNosale} value="Add" type="button" id=""/>
                      </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddNoSale));
