import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';

import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let DisscountList = React.createClass({
    getInitialState() {
        return {
             DisscountListRquested:false,DisscountListData:null,deleteClicked:false,editClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        // 
        let nextDisscount = nextProps.CompanyData.GetDisscount;
            if(this.state.DisscountListRquested && nextDisscount){
                if(nextDisscount.status_code === 200){
                    this.setState({DisscountListData:nextDisscount.data,DisscountListRquested:false})
                }
            }
            let nextDisscountDelete = nextProps.CompanyData.DeleteDisscount;
            if(this.state.deleteClicked && nextDisscountDelete){
                if(nextDisscountDelete.status_code === 200){
                  this.GetDisscountList();
                  this.setState({deleteClicked:false});
                }
            }
    },

    componentDidMount() {
        this.GetDisscountList();
    },

    GetDisscountList(){
        let data = {};
        
        this.props.dispatch(CompanyActions.getListDisscounts(data));
        this.setState({DisscountListRquested:true});
    },

    deleteDisscont(event){
      let data = {};
      
      data.id = event.target.dataset.id;

      this.setState({deleteClicked:true});
      this.props.dispatch(CompanyActions.dltDisscountsData(data));
    },

    editproduct(event){
      let data = {};
      let id = event.target.dataset.id;
      let DisscountListData = this.props.CompanyData.GetDisscount.data;
      
      data = _.filter(DisscountListData, function (i) {
      return i._id == id ;
      });
      this.props.dispatch(CompanyActions.disscountById(data[0]));
      this.props.router.push('/setup/company/edit-disscount');
},

    Discount(){
        this.props.router.push('/setup/Company/add-Discount');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   
   generateDisscoutList(){
        let disscountList = this.state.DisscountListData;
        let disscountTemplate = disscountList ?  disscountList.map(function (listItem, key) {
        return (<tr key={key}><td>{listItem.reason}</td> <td>{listItem.value}</td><td><span data-id={listItem._id} onClick={this.editproduct} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteDisscont}></span></td></tr>)
        }, this):("No Data Found");
        return disscountTemplate ;
    },
    render() {
        let list = this.generateDisscoutList();
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="cutomers">
                        <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Discount Reasons</h1>
                        <input type="button" onClick={this.Discount} className="add-btn pull-right" value="Add Discount Reasons" />
                    </div>
                    </div>
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Reason</th>
                                    <th>Default Reason</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(DisscountList));
