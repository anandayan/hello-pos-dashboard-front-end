import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let CourcesDetails = React.createClass({
    getInitialState() {
        return {
             addClicked:false,CourseListData:null,CourseListRquested:false,deleteClicked:false,editClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

      let nextCourse = nextProps.CompanyData.GetCourse;
            if(this.state.CourseListRquested && nextCourse){
                if(nextCourse.status_code === 200){
                    this.setState({CourseListData:nextCourse.data,CourseListRquested:false})
                }
            }
            let nextCourseDelete = nextProps.CompanyData.DeleteCourse;
            if(this.state.deleteClicked && nextCourseDelete){
                if(nextCourseDelete.status_code === 200){
                  this.GetCourseList();
                  this.setState({deleteClicked:false});
                }
            }
    },
    componentDidMount() {
        this.GetCourseList();
    },
    GetCourseList(){
        
        let data = {};
        this.props.dispatch(CompanyActions.getListCourses(data));
        this.setState({CourseListRquested:true});
    },

    deleteRefund(event){
      let data = {};
      data.id = event.target.dataset.id;

      this.setState({deleteClicked:true});
      this.props.dispatch(CompanyActions.dltCourseData(data));
    },


    editCourse(event){
      let data = {};
      let id = event.target.dataset.id;
      let CourseListData = this.props.CompanyData.GetCourse.data;
      
      data = _.filter(CourseListData, function (i) {
      return i._id == id ;
      });
      this.props.dispatch(CompanyActions.CourseById(data[0]));
      this.props.router.push('/setup/company/edit-stock');
},
    addCourse(){
        this.props.router.push('/setup/Company/add-cources');
    },

    
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   generateCourseList(){
        let courseList = this.state.CourseListData;
        let courseTemplate = courseList ?  courseList.map(function (listItem, key) {
        return (<tr key={key}><td>{listItem.name}</td> <td>{listItem.course_number}</td><td><span data-id={listItem._id} onClick={this.editCourse} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteRefund}></span></td></tr>)
        }, this):("No Data Found");
        return courseTemplate ;
    },

    render() {
        let list = this.generateCourseList();
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">

                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Course List</h1>
                        <input type="button" onClick={this.addCourse} className="add-btn pull-right" value="Add Course" />
                    </div>


                        <div className="staff-table">
                            <table className="table table-striped staff-list-tabel">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Course Number</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {list}
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(CourcesDetails));


