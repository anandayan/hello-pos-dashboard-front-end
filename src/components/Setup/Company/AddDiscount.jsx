import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let AddDisscount = React.createClass({
    getInitialState() {
        return {
             addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        if(this.state.addClicked){
 
        alert(nextProps.CompanyData.AddDisscount.message)
        this.setState({addClicked:false});
      }
    },
    componentDidMount() {

    },
    AddDisscount(){
            
      let data = {};
        data.reason = this.refs.reason.value;
        data.value = this.refs.value_def.value;
        this.props.dispatch(CompanyActions.addDisscountsData(data));
        this.setState({addClicked:true});
        console.log(data)
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   backbtn(event) {
        this.props.router.push('/setup/Company/discountReasons');
    },
    render() {
        return (

            <div>

               <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Add a Discount Reason</h1>
                    </div>
                    <div className="filter-by">
                      <h3>Add a Discount Reason</h3>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="reason">Reason:</label>
                          <input ref="reason" className="form-control" id="reason"  type="text"   autoComplete="off"/>
                      </div>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="value">Course Number:</label>
                          <input ref="value_def" className="form-control" id=""  type="text"   autoComplete="off"/>
                      </div>
                      <div className="buttonBtn">
                        <div className="cancelBtn">
                           <input className="cancel btn-danger" value="Back" type="button" onClick={this.backbtn}/>
                        </div>
                        <div className="pull-right add-otherBtn">
                           <input class onClick={this.AddDisscount}  value="Add" type="button" id=""/>
                        </div>
                      </div>
                      </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserData:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddDisscount));
