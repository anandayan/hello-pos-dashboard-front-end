import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let StockReason = React.createClass({
    getInitialState() {
        return {
             StockListRquested:false,StockListData:null,deleteClicked:false,editClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        let nextStock = nextProps.CompanyData.GetStockMovement;
            if(this.state.StockListRquested && nextStock){
                if(nextStock.status_code === 200){
                    this.setState({StockListData:nextStock.data,StockListRquested:false})
                }
            }

            let nextStockDelete = nextProps.CompanyData.DeleteStockMovement;
            if(this.state.deleteClicked && nextStockDelete){
                if(nextStockDelete.status_code === 200){
                  this.GetStockList();
                  this.setState({deleteClicked:false});
                }
            }
    },
    GetStockList(){
        let data = {};
        this.props.dispatch(CompanyActions.getListStockMovements(data));
        this.setState({StockListRquested:true});
    },

    componentDidMount() {
        this.GetStockList();
    },

    deleteStock(event){
      let data = {};
      data.id = event.target.dataset.id;

      this.setState({deleteClicked:true});
      this.props.dispatch(CompanyActions.dltStockMovementData(data));
    },

    editStock(event){
      let data = {};
      let id = event.target.dataset.id;
      let StockListData = this.props.CompanyData.GetStockMovement.data;
      
      data = _.filter(StockListData, function (i) {
      return i._id == id ;
      });
      this.props.dispatch(CompanyActions.StockMovementById(data[0]));
      this.props.router.push('/setup/Company/edit-stockReason');
},
    stockReason(){
        this.props.router.push('setup/Company/add-stockReason');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    generateStockList(){
        let stockList = this.state.StockListData;
        let stockTemplate = stockList ?  stockList.map(function (listItem, key) {
        return (<tr key={key}><td>{listItem.reason}</td> <td><span data-id={listItem._id} onClick={this.editStock} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteStock}></span></td></tr>)
        }, this):("No Data Found");
        return stockTemplate ;
    },
    render() {
        let list = this.generateStockList();
        return (
            
            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="cutomers">
                        <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Stock Movement Reasons</h1>
                        <input type="button" onClick={this.stockReason} className="add-btn pull-right" value="Add Stock Movement Reasons" />
                    </div>
                    </div>
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Discription</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(StockReason));
