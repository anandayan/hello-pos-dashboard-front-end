import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let AddCourse = React.createClass({
    getInitialState() {
        return {
            addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        if(this.state.addClicked){

        alert(nextProps.CompanyData.AddCourse.message)
        this.setState({addClicked:false});
      }
    },
    componentDidMount() {

    },

    addCourse(){
      
          let data={};
          data.name = this.refs.name.value;
          data.course_number = this.refs.course_number.value;
          this.props.dispatch(CompanyActions.addCourseData(data));
          this.setState({addClicked:true});
          console.log(data)
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   back(event) {
        this.props.router.push('/setup/Company/cources');
    },
    render() {
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Stock Movement Reasons</h1>
                    </div>
                    <div className="filter-by">
                      <h3>Add Course</h3>
                      <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                              <label htmlFor="name">Name:</label>
                              <input ref="name" className="form-control"   type="text"   autoComplete="off"/>
                          </div>
                          <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                              <label htmlFor="course_number">Course Number:</label>
                              <input ref="course_number" className="form-control" id="course_number"  type="text"   autoComplete="off"/>
                          </div>
                      <div className="buttonBtn">
                        <div className="cancelBtn">
                           <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                        </div>
                        <div className="pull-right add-otherBtn">
                           <input class onClick={this.addCourse} value="Add" type="button" id=""/>
                        </div>
                      </div>
                    </div>
                    
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddCourse));
