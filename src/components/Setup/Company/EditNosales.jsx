import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let EditNoSale = React.createClass({
    getInitialState() {
        return {
             editNosaleList:false, 
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
         if(this.state.editNosaleList){
          alert(nextProps.CompanyData.UpdateNoSale.message);
          if(nextProps.CompanyData.UpdateNoSale.status_code === 200){
            this.props.router.push('/setup/company/edit-nosale');
          }
            this.setState({editNosaleList:false});
         }
 // 
      
    },
    componentDidMount() {

    },

    updateNosale(){
         
        let data = {};
        data.id = this.props.CompanyData.noSaleById._id;
        data.reason = this.refs.reason.value;
        this.props.dispatch(CompanyActions.updtNoSaleData(data));
        this.setState({editNosaleList:true});
        
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   Back(event) {
        this.props.router.push('/setup/company/NosaleReasons');
    },
    render() {
      
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Add No Sale Reason</h1>
                    </div>
                    <div className="filter-by-brand">
                      <div className="col-sm-4">
                          <span className="help-block txtalignRight">Reason:</span>
                      </div>
                      <div className="col-sm-8">
                        <input ref="reason" defaultValue={this.props.CompanyData.noSaleById.reason} className="form-control input-text-fields" id="reason"  type="text"   autoComplete="off"/>
                      </div>
                    </div>
                    <div className="buttonBtn">
                      <div className="cancelBtn">
                         <input className="cancel btn-danger" value="Back" type="button" onClick={this.Back}/>
                      </div>
                      <div className="pull-right add-otherBtn">
                         <input class onClick={this.updateNosale} value="Update" type="button" id=""/>
                      </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(EditNoSale));
