import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let EditDisscount = React.createClass({
    getInitialState() {
        return {
             editRefundsList:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        if(this.state.editRefundsList){
 
        alert(nextProps.CompanyData.UpdateRefund.message)
        this.setState({editRefundsList:false});
      }
    },
    componentDidMount() {

    },
    EditRefund(){
            
      let data = {};
        data.id = this.props.CompanyData.disscountById._id;
        data.reason = this.refs.reason.value;
        data.value = this.refs.value.value;
        this.props.dispatch(CompanyActions.updtDisscountsData(data));
        this.setState({editRefundsList:true});
        console.log(data)
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   back(event) {
        this.props.router.push('/setup/Company/discountReasons');
    },
    render() {
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Disscount</h1>
                    </div>
                     
                    <div className="filter-by">
                      <h3>Refund Reason</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-md-offset-2 col-lg-4">
                            <label htmlFor="reason">Description:</label>
                            <input ref="reason" defaultValue={this.props.CompanyData.disscountById.reason} className="form-control" id="description"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="value">Short Description:</label>
                            <input ref="value" defaultValue={this.props.CompanyData.disscountById.value} className="form-control" id="value"  type="text"   autoComplete="off"/>
                        </div>
                        
                        <div className="buttonBtn">
                          <div className="cancelBtn">
                             <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                          </div>
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.EditRefund} value="Add" type="button" id=""/>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserData:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(EditDisscount));
