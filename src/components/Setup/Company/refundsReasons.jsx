import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let Refunds = React.createClass({
    getInitialState() {
        return {
             RefundListRquested:false,RefundsListData:null,deleteClicked:false,editClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      debugger;
            let nextRefund = nextProps.CompanyData.GetRefund;
            if(this.state.RefundListRquested && nextRefund){
                if(nextRefund.status_code === 200){
                    this.setState({RefundsListData:nextRefund.data,RefundListRquested:false})
                }
            }

            let nextRefundsDelete = nextProps.CompanyData.DeleteDisscount;
            if(this.state.deleteClicked && nextRefundsDelete){
                if(nextRefundsDelete.status_code === 200){
                  this.GetDisscountList();
                  this.setState({deleteClicked:false});
                }
            }
    },
    componentDidMount() {
      debugger;
        this.GetDisscountList();
    },
    GetDisscountList(){
         let data = {};
        this.props.dispatch(CompanyActions.getListRefunds(data));
        this.setState({RefundListRquested:true});
    },

    deleteRefund(event){
      let data = {};
      data.id = event.target.dataset.id;

      this.setState({deleteClicked:true});
      this.props.dispatch(CompanyActions.dltRefundData(data));
    },

    editRefund(event){
      let data = {};
      let id = event.target.dataset.id;
      let RefundsListData = this.props.CompanyData.GetRefund.data;
      
      data = _.filter(RefundsListData, function (i) {
      return i._id == id ;
      });
      this.props.dispatch(CompanyActions.refundById(data[0]));
      this.props.router.push('/setup/company/edit-refund');
},

    Refund(){
        this.props.router.push('/setup/Company/add-Refund');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   generateRefundsList(){
        let refundsList = this.state.RefundsListData;
        let refunndsTemplate = refundsList ?  refundsList.map(function (listItem, key) {
        return (<tr key={key}>
          <td>{listItem.desc }</td>
         <td>{listItem.short_desc}</td>
         <td>
          <div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.ret_to_stock ? true : false} /><label></label></div>
         </td>
         <td><span data-id={listItem._id} onClick={this.editRefund} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteRefund}></span></td></tr>)
        }, this):("No Data Found");
        return refunndsTemplate ;
    },

    render() {
        let list = this.generateRefundsList();
        return (

            <div>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="cutomers">
                        <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Refund Reasons</h1>
                        <input type="button" onClick={this.Refund} className="add-btn pull-right" value="Add Refund Reasons" />
                    </div>
                    </div>
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Discription</th>
                                    <th>Short Disrcription</th>
                                    <th>Return to stock</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(Refunds));
