import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';
import qq from '../../../img/question-mark.png';

import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Customer Types">
         <p>Customer Types can be used to give out discounts on products to valued customers and can also be used for memberships.</p>
    </Popover>
  );

let CustomerTypes = React.createClass({
    getInitialState() {
        return {
              ListCustomerData :null,
             addCustomerTypeClicked: false,
             ListCustomerTypeCalled:false,
             DeleteCustomerTypeCalled:false,
             UpdateCustomerTypeCalled:false,
        }
    },
    componentWillMount() {
    //  
    },
    componentWillReceiveProps(nextProps) {
      let nexporps = nextProps.CompanyData;

      if(this.state.addCustomerTypeClicked){
          if(nexporps.CustomerType.status_code === 200 ){
            alert(nexporps.CustomerType.message);
            this.setState({addCustomerTypeClicked:false})
            this.listCustomerType(1);
          }
      }
      if(this.state.ListCustomerTypeCalled){
        nexporps.ListCustomerType.status_code === 200 && this.setState({ListCustomerTypeCalled:false,ListCustomerData:nexporps.ListCustomerType.data})
      }
      if(this.state.DeleteCustomerTypeCalled){
          this.listCustomerType(1);
        this.setState({ListCustomerTypeCalled:true,DeleteCustomerTypeCalled:false})
      }
    },
    componentDidMount() {
      this.listCustomerType();
    },
    add_Customer_Type(){
        let data = {};
        data.name = this.refs.customer_type.value;
        data.description = this.refs.cust_type_descp.value;
        data.discount= this.refs.discount.value;
         this.props.dispatch(CompanyActions.sendCustomerData(data));
         this.setState({addCustomerTypeClicked:true});
    },
    listCustomerType(page){
        let data = {};
       
        this.props.dispatch(CompanyActions.getCustomerData(data));
        this.setState({ListCustomerTypeCalled:true})

    },

    editCustomerType(event){
        
        let data = {};
          let id = event.target.dataset.id;
          let ListCustomerData = this.props.CompanyData.ListCustomerType.data;
         
          data = _.filter(ListCustomerData, function (i) {
          return i._id == id ;
          });
      this.props.dispatch(CompanyActions.customerTypeById(data[0]));
      this.props.router.push('/setup/company/edit-Customer-type');
    },
    deleteCustomerType(event){

        let data = {};
        data.id = event.target.dataset.id;
        this.props.dispatch(CompanyActions.delteCustomerData(data));
        this.setState({DeleteCustomerTypeCalled:true})
    },

    generateStaffList(){
        let customerList = this.state.ListCustomerData;
        let custTemplate = customerList ?  customerList.map(function (listItem, key) {
          return (<tr key={key}> <td>{listItem.name}</td><td>{listItem.description}</td> <td>{listItem.discount+" %"}</td><td><span data-id={listItem._id} onClick={this.editCustomerType} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteCustomerType}></span></td></tr>)
          }, this):("No Data Found");
        return custTemplate ;
    },

    render() {
      let list = this.generateStaffList();
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>

                        <h1 className="title">Customer Types <img src = {qq} alt="qus mark"/></h1>
                  </OverlayTrigger>
                    </div>
                    
                    <div className="filter-by">
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="staff_swipe_code">Name:</label>
                            <input className="form-control"  ref="customer_type" type="text"  id="" />
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="staff_swipe_code">Discription:</label>
                            <input className="form-control"  ref="cust_type_descp" type="text"  id="" />
                        </div>

                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="staff_swipe_code">Discount(%):</label>
                            <input className="form-control"  ref="discount" type="number"  id="" />
                        </div>
                        <div className="buttonBtn">

                          <div className="pull-right add-otherBtn">
                          <button className="add btn btn-primary"    onClick={this.add_Customer_Type}>Add</button>
                          </div>
                        </div>
                    </div>

                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Discription</th>
                                    <th>Discount</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                              {list}
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(CustomerTypes));
