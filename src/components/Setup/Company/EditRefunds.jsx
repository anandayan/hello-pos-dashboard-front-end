import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';


let EditRefunds = React.createClass({
    getInitialState() {
        return {
             addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
          if(this.state.addClicked){
 
        alert(nextProps.CompanyData.UpdateRefund.message)
        this.setState({addClicked:false});
      }
    },
    componentDidMount() {

    },

    EditRefund(){
            
      let data = {};
        data.id = this.props.CompanyData.refundById._id;        
        data.desc  = this.refs.desc.value;
        data.short_desc  = this.refs.short_desc.value;
        data.ret_to_stock  = this.refs.ret_to_stock.value;
        this.props.dispatch(CompanyActions.updtRefundData(data));
        this.setState({addClicked:true});
        console.log(data)
    },

    back(event) {
        this.props.router.push('/setup/company/refundsReasons');
    },
    render() {
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Add a Refund Reason</h1>
                    </div>
                    <div className="parainfo">
                        <p>On this page you can add a Refund Reason. In order to give a Refund to a customer on the till, you need to provide a reason why the refund is being made.</p>
                    </div>
                    <div className="filter-by">
                      <h3>Refund Reason</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-md-offset-2 col-lg-4">
                            <label htmlFor="desc">Description:</label>
                            <input ref="desc" defaultValue={this.props.CompanyData.refundById.desc} className="form-control" id="description"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="short_desc">Short Description:</label>
                            <input ref="short_desc" defaultValue={this.props.CompanyData.refundById.short_desc} className="form-control" id="short_desc"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="checkbox checkbox-inline col-xs-4 col-sm-4 col-md-4 col-lg-4 col-md-offset-2">
                            <input  type="checkbox" ref="ret_to_stock" defaultChecked={this.props.CompanyData.refundById.ret_to_stock?true:false} id="ret_to_stock" value="1"/>
                            <label htmlFor="ret_to_stock">Return to Stock:</label>
                        </div>
                        <div className="buttonBtn">
                          <div className="cancelBtn">
                             <input className="cancel btn-danger"  onClick={this.back} value="Back" type="button" id=""/>
                          </div>
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.EditRefund} value="Update" type="button" id=""/>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CompanyData:state.Company,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CompanyActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(EditRefunds));
