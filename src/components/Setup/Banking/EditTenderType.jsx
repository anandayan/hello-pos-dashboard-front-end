import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as BankingActions from '../../../actions/setup/banking_actions';
import { bindActionCreators } from 'redux';


let EditTenderTypes = React.createClass({
    getInitialState() {
        return {
             updateTenderClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        if(this.state.updateTenderClicked){

        alert(nextProps.BankingDetail.UpdateTenderType.message)
        this.setState({updateTenderClicked:false});
      }
    },
    componentDidMount() {

    },

    updateTender(){
   
        let data = {};
        data.id = this.props.BankingDetail.TenderTypeById._id;
        data.name = this.refs.tender_name.value;
        data.description = this.refs.tender_description.value;
        data.button_color = this.refs.tender_buttonColor.value;
        data.till_order = this.refs.tillOrder.value;
        data.classification = this.refs.classification.value;
        this.props.dispatch(BankingActions.updtTenderTypeData(data));
        this.setState({updateTenderClicked:true});
        console.log(data)
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   back(event) {
        this.props.router.push('/setup/banking/tender');
    },
    render() {
        return (

            <div class>
             
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        <a href="#"><strong>Click here</strong> to upgrade your license to include support</a>
                    </div>
                    <div className="common-info">
                        <h1 className="title">Tender Types</h1>
                    </div>
                     <div className="filter-by">
                        <h3>Add a Tender Type</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="tender_name">Name:</label>
                          <input className="form-control"  ref="tender_name" type="text" defaultValue={this.props.BankingDetail.TenderTypeById.name}/>
                        </div>
                          <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                              <label htmlFor="tender_description">Description:</label>
                              <input className="form-control"  ref="tender_description" type="text" defaultValue={this.props.BankingDetail.TenderTypeById.description} />
                          </div>
                          <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="tender_buttonColor">Button Colour: (Leave blank for default)</label>
                            <select  ref="tender_buttonColor" id="tender_buttonColor" className="form-control" defaultValue={this.props.BankingDetail.TenderTypeById.buttonColor} >
                               <option selected="selected" value="">None</option>
                                <option value="7">Black</option>
                                <option value="4">Dark Blue</option>
                                <option value="8" >Dark Green</option>
                                <option value="2">Dark Grey</option>
                                <option value="3" >Dark Orange</option>
                                <option value="14">Deep Green</option>
                                <option value="1" >Grey</option>
                                <option value="12">Grey Yellow</option>
                                <option value="4"  >Light Blue</option>
                                <option value="5"  >Light Orange</option>
                                <option value="10"  >Light Red</option>
                                <option value="13"  >Navy Blue</option>
                                <option value="11" >Pink</option>
                                <option value="9" >Red</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="tillOrder">Till Order:</label>
                            <input className="form-control"  ref="tillOrder" type="text" defaultValue={this.props.BankingDetail.TenderTypeById.tillOrder}/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="classification">Classification:</label>
                            <select  ref="classification" id="classification" className="form-control" defaultValue={this.props.BankingDetail.TenderTypeById.classification} >
                               <option value="1">Other</option>
                                <option value="2">Cash</option>
                                <option value="3">Card</option>
                            </select>
                        </div>
                        <div className="buttonBtn">
                          <div className="cancelBtn">
                             <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                          </div>
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.updateTender}  value="Update" type="button" id=""/>
                          </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      BankingDetail:state.Banking,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(BankingActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(EditTenderTypes));
