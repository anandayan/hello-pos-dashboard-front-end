import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as BankingActions from '../../../actions/setup/banking_actions';
import { bindActionCreators } from 'redux';


let PettyCash = React.createClass({
    getInitialState() {
        return {
             PettyCashListRquested:false,PettyCashListData:null, deleteClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
       
            let nextPetty = nextProps.BankingDetail.GetPettyCashReason;
            if(this.state.PettyCashListRquested && nextPetty){
                if(nextPetty.status_code === 200){
                    this.setState({PettyCashListData:nextPetty.data,PettyCashListRquested:false})
                }
            }

            let nextPettyDelete = nextProps.BankingDetail.DeletePettyCashReason;
            if(this.state.deleteClicked && nextPettyDelete){
                if(nextPettyDelete.status_code === 200){
                  this.GetPettyList();
                  this.setState({deleteClicked:false});
                }
            }
    },
    componentDidMount() {
        this.GetPettyList();
    },

    GetPettyList(){
       
        let data = {};
        
        this.props.dispatch(BankingActions.getListPettyCashReason(data));
        this.setState({PettyCashListRquested:true});
        
    },

    deletePetty(event){
      let data = {};
      data.id = event.target.dataset.id;

      this.setState({deleteClicked:true});
      this.props.dispatch(BankingActions.dltPettyCashReasonData(data));
    },

    editPettyCash(event){
      let data = {};
      let id = event.target.dataset.id;
      let PettyCashListData = this.props.BankingDetail.GetPettyCashReason.data;
      data = _.filter(PettyCashListData, function (i) {
      return i._id == id ;
      });
      this.props.dispatch(BankingActions.PettyCashReasonById(data[0]));
      this.props.router.push('/setup/company/edit-pettyCash');
    }, 

    PettyCash(){
        this.props.router.push('/setup/banking/add-PettyCash');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   generatePettyCashList(){
        let pettyCashList = this.state.PettyCashListData;
        let pettyTemplate = pettyCashList ?  pettyCashList.map(function (listItem, key) {
        return (<tr key={key}><td>{listItem.reason}</td><td><span data-id={listItem._id} onClick={this.editPettyCash} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deletePetty}></span></td></tr>)
        }, this):("No Data Found");
        return pettyTemplate ;
    },

    render() {
        let list = this.generatePettyCashList();
        return (

            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Petty Cash Reasons</h1>
                        <input type="button" onClick={this.PettyCash} className="add-btn pull-right" value="Add Petty Cash" />
                    </div>
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Reason</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      BankingDetail:state.Banking,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(BankingActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(PettyCash));
