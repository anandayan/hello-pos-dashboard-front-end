import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as BankingActions from '../../../actions/setup/banking_actions';
import { bindActionCreators } from 'redux';


let TenderType = React.createClass({
    getInitialState() {
        return {
             TenderTypesListRquested:false,TenderTypeListData:null, deleteClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
            // debugger;
            let nextTender = nextProps.BankingDetail.GetTenderType;
            if(this.state.TenderTypesListRquested && nextTender){
                if(nextTender.status_code === 200){
                    this.setState({TenderTypeListData:nextTender.data,TenderTypesListRquested:false})
                }
            }

            let nextTenderDelete = nextProps.BankingDetail.DeleteTenderType;
            if(this.state.deleteClicked && nextTenderDelete){
                if(nextTenderDelete.status_code === 200){
                  this.GetTenderTypesList();
                  this.setState({deleteClicked:false});
                }
            }

    },
    componentDidMount() {
        this.GetTenderTypesList();
    },

    GetTenderTypesList(){
        // debugger;
        let data = {};
        this.props.dispatch(BankingActions.getListTenderType(data));
        this.setState({TenderTypesListRquested:true});
        
    },

    deleteTender(event){
      let data = {};
      data.id = event.target.dataset.id;

      this.setState({deleteClicked:true});
      this.props.dispatch(BankingActions.dltTenderTypeData(data));
    },

    editTender(event){
      let data = {};
      let id = event.target.dataset.id;
      let TenderTypeListData = this.props.BankingDetail.GetTenderType.data;
      data = _.filter(TenderTypeListData, function (i) {
      return i._id == id ;
      });
      this.props.dispatch(BankingActions.TenderTypeById(data[0]));
      this.props.router.push('/setup/company/edit-tender');
    }, 

    addTender(){
        this.props.router.push('/setup/banking/add-Tender');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   generateTenderTypeList(){
        let tenderTypeList = this.state.TenderTypeListData;
        let tenderTemplate = tenderTypeList ?  tenderTypeList.map(function (listItem, key) {
        return (<tr key={key}><td>{listItem.name}</td><td>{listItem.description}</td><td>{listItem.button_color}</td><td>{listItem.till_order}</td><td>{listItem.classification}</td><td><span data-id={listItem._id} onClick={this.editTender} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteTender}></span></td></tr>)
        }, this):("No Data Found");
        return tenderTemplate ;
    },
    render() {

        let list = this.generateTenderTypeList();
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Tender Types</h1>
                        <input type="button" onClick={this.addTender} className="add-btn pull-right" value="Add Tender Types" />
                    </div>

                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Discription</th>
                                    <th>Button color</th>
                                    <th>Till Order</th>
                                    <th>Classification</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      BankingDetail:state.Banking,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(BankingActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(TenderType));
