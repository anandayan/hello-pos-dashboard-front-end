import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as BankingActions from '../../../actions/setup/banking_actions';
import { bindActionCreators } from 'redux';


let AddTaxType = React.createClass({
    getInitialState() {
        return {
             addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      if(this.state.addClicked){
 
        alert(nextProps.BankingDetail.AddTaxRate.message)
        this.setState({addClicked:false});
      }
    },
    componentDidMount() {

    },

    addTaxRate(){
    
        let data = {};
        data.name = this.refs.tax_name.value;
        data.description = this.refs.tax_description.value;
        data.percentage = this.refs.tax_percentage.value;
        data.tax_code = this.refs.taxCode.value;
        data.tax_type = this.refs.taxType.value;
        this.props.dispatch(BankingActions.addTaxRateData(data));
        this.setState({addClicked:true});
        console.log(data)
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   back(event) {
        this.props.router.push('/setup/banking/taxRate');
    },
    render() {
        return (

            <div class>
             
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Add a Tax Rate</h1>
                    </div>
                    <div className="parainfo">
                        <p>On this page you can create a new tax rate. Select 'Combined' to have greater flexibility over tax charged at different locations.</p>
                        <p>Once you add a new Tax Rate you will then be able to apply it to any product you like.</p>
                    </div>
                    <div className="filter-by">
                        <h3>Tax Rate</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="tax_name">Name:</label>
                          <input className="form-control"  ref="tax_name" type="text"  id="" />
                        </div>
                          <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                              <label htmlFor="tax_percentage">Percentage:</label>
                              <input className="form-control"  ref="tax_percentage" type="text"  id="" />
                          </div>
                          <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="taxCode">Tax Code:</label>
                            <input className="form-control"  ref="taxCode" type="text"  id="" />
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="tax_description">Description:</label>
                            <input className="form-control"  ref="tax_description" type="text"  id="" />
                        </div>
                          <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="taxType">Tax Type:</label>
                            <select  ref="taxType" id="taxType" className="form-control">
                               <option selected="selected" value="1">Standard</option>
                            </select>
                        </div>

                        <div className="buttonBtn">
                          <div className="cancelBtn">
                             <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                          </div>
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.addTaxRate} value="Add" type="button" id=""/>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      BankingDetail:state.Banking,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(BankingActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddTaxType));
