import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as BankingActions from '../../../actions/setup/banking_actions';
import { bindActionCreators } from 'redux';


let AddPettyCash = React.createClass({
    getInitialState() {
        return {
            addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      if(this.state.addClicked){ 
        alert(nextProps.BankingDetail.AddPettyCashReason.message)
        this.setState({addClicked:false});
      }
    },
    componentDidMount() {

    },

    addPettyReason(){
 
        let data = {};
        data.reason = this.refs.petty_reason.value;
        this.props.dispatch(BankingActions.addPettyCashReasonData(data));
        this.setState({addClicked:true});
        console.log(data)
    },
    
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   back(event) {
        this.props.router.push('/setup/banking/pettyCash');
    },
    render() {
        return (

            <div class>
                 
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Petty Cash Reasons</h1>
                    </div>
                    <div className="parainfo">
                        <p>On this page you can add a Petty Cash Reason. You need to insert a reason why the Petty Cash has been withdrawn.</p>
                    </div>
                    <div className="filter-by">
                        <h3>Add Petty Cash Reason</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                          <label htmlFor="petty_reason">Reason:</label>
                          <input className="form-control"  ref="petty_reason" type="text"  id="" />
                        </div>
                        <div className="buttonBtn">
                          <div className="cancelBtn">
                             <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                          </div>
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.addPettyReason} value="Add" type="button" id=""/>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      BankingDetail:state.Banking,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(BankingActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddPettyCash));
