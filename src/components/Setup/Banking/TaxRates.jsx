import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as BankingActions from '../../../actions/setup/banking_actions';
import { bindActionCreators } from 'redux';
import qq from '../../../img/question-mark.png';

import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Tax Rates">
         <p>This page can be used to filter certain Orders from certain devices.</p>
        <p>Choose a Device and Printer Type and click 'Add' below to stop the selected Order Printer from printing out orders from the selected Device.</p>
    </Popover>
  );

let TaxType = React.createClass({
    getInitialState() {
        return {
             TaxRateListRquested:false,TaxRateListData:null, deleteClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        
            let nextTax = nextProps.BankingDetail.GetTaxRate;
            if(this.state.TaxRateListRquested && nextTax){
                if(nextTax.status_code === 200){
                    this.setState({TaxRateListData:nextTax.data,TaxRateListRquested:false})
                }
            }

            let nextTaxDelete = nextProps.BankingDetail.DeleteTaxRate;
            if(this.state.deleteClicked && nextTaxDelete){
                if(nextTaxDelete.status_code === 200){
                  this.GetTaxList();
                  this.setState({deleteClicked:false});
                }
            }

    },
    componentDidMount() {
        this.GetTaxList();
    },

    GetTaxList(){
       
        let data = {};
        this.props.dispatch(BankingActions.getListTaxRate(data));
        this.setState({TaxRateListRquested:true});
        
    },
    deleteTax(event){
      let data = {};
      data.id = event.target.dataset.id;

      this.setState({deleteClicked:true});
      this.props.dispatch(BankingActions.dltTaxRateData(data));
    },

    editTaxRate(event){
      let data = {};
      let id = event.target.dataset.id;
      let TaxRateListData = this.props.BankingDetail.GetTaxRate.data;
      data = _.filter(TaxRateListData, function (i) {
      return i._id == id ;
      });
      this.props.dispatch(BankingActions.TaxRateById(data[0]));
      this.props.router.push('/setup/company/edit-tax-rate');
    },  

    addTax(){
        this.props.router.push('/setup/banking/add-tax');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   generateTenderList(){
        let taxList = this.state.TaxRateListData;
        let taxTemplate = taxList ?  taxList.map(function (listItem, key) {
        return (<tr key={key}><td>{listItem.name}</td><td>{listItem.description}</td><td>{listItem.percentage + "%"}</td><td>{listItem.tax_code}</td><td>{listItem.tax_type}</td><td><span data-id={listItem._id} onClick={this.editTaxRate} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteTax}></span></td></tr>)
        }, this):("No Data Found");
        return taxTemplate ;
    }, 
    render() {
        let list = this.generateTenderList();
        return (
            
            <div class>
               
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>

                        <h1 className="title">Tax Rates <img src = {qq} alt="qus mark"/></h1>
                        </OverlayTrigger>
                        <input type="button" onClick={this.addTax} className="add-btn pull-right" value="Add Tax Rate" />
                    </div>
                    
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Discription</th>
                                    <th>percentage</th>
                                    <th>Tax Code</th>
                                    <th>Tax Type</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      BankingDetail:state.Banking,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(BankingActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(TaxType));
