import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { ButtonGroup, Button } from 'react-bootstrap';
import * as TransactionActions from '../../../actions/reports/transaction_actions';
import { bindActionCreators } from 'redux';
//http://c3js.org/examples.html
let TimeComparisons = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    
    
    render() {
      
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                   <div className="common-info">
                        <h1 className="title">Time interval</h1>
                    </div>
                    
                   <div className="filter-by-brand">
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        
                            <label htmlFor="currency">Metric:</label>
                            <select  ref="currency" className="form-control">
                                <option selected="selected" value="NoOfTrans">Transaction Qty</option>
                                <option value="RefundQty">Refund Qty</option>
                                <option value="RefundValue">Refund Value</option>
                                <option value="NoSaleQty">No Sale Qty</option>
                                <option value="VoidLineQty">Voided Qty</option>
                                <option value="VoidLineValue">Voided Value</option>
                                <option value="ItemQty">Item Qty</option>
                                <option value="Value">Sales Inc. Tax</option>
                                <option value="Discount">Discounts &amp; Promotions</option>
                                <option value="AvgValueIncVAT">Average Transaction Net Sales</option>
                                <option value="ValueIncVAT">Net Sales Inc. Vat</option>
                                <option value="ValueExcVAT">Net Sales Exc. Tax</option>
                                <option value="AvgMargin">Average Transaction Margin</option>
                                <option value="Margin">Gross Margin</option>
                                <option value="MarginPerc">Margin %</option>
                                <option value="NoOfCustomers">Customer Qty</option>
                                <option value="NoOfRatings">Rating Qty</option>
                                <option value="AvgRating">Avg Rating</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="currency">Date Range:</label>
                            <select  ref="currency" className="form-control">
                                <option value="1">Today</option>
                                <option value="Yesterday">Yesterday</option>
                                <option value="ThisWeek">This Week</option>
                                <option value="ThisMonth">This Month</option>
                                <option value="ThisQuarter">This Quarter</option>
                                <option value="LastWeek">Last Week</option>
                                <option value="LastMonth">Last Month</option>
                                <option selected="selected" value="7">Last 7 days</option>
                                <option value="14">Last 14 days</option>
                                <option value="30">Last 30 days</option>
                                <option value="60">Last 60 days</option>
                                <option value="90">Last 90 days</option>
                                <option value="Custom">Custom</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="currency">Interval:</label>
                            <select  ref="currency" className="form-control">
                                <option value="mm">Monthly</option>
                                <option selected="selected" value="ww">Weekly</option>
                                <option value="dd">Daily</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="currency">Compare To:</label>
                            <select  ref="currency" className="form-control">
                                <option selected="selected" value="PreviousWeek">Previous Week</option>
                                <option value="PreviousMonth">Previous Month</option>
                                <option value="PreviousQuarter">Previous Quarter</option>
                                <option value="PreviousYear">Previous Year</option>
                                <option value="7">7 days ago</option>
                                <option value="14">14 days ago</option>
                                <option value="30">30 days ago</option>
                                <option value="60">60 days ago</option>
                                <option value="90">90 days ago</option>
                                <option value="Custom">Custom</option>
                            </select>
                        </div>
                    </div>
                   
                   
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
       TransactionData:state.Transactions,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(TransactionActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(TimeComparisons));
