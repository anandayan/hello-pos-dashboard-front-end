import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { ButtonGroup, Button } from 'react-bootstrap';
import * as TransactionActions from '../../../actions/reports/transaction_actions';
import { bindActionCreators } from 'redux';
//http://c3js.org/examples.html
let TillCategories = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    addStaff(){
        // this.props.router.push('staff/add-staff');
    },
    
    render() {
      
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                   
                   <div className="common-info">
                        <h1 className="title">Turnover By Till Category</h1>
                        <input type="button" onClick={this.Refresh} className="add-btn pull-right" value="REFRESH" />
                    </div>
                    <div className="parainfo">
                        <p>Reporting categories can be used to group categories in a more manageable way for reporting. You can set a reporting category for each category in your back office. This report will then allow you to compare sales between these categories.</p>
                        <p>This report does not include Miscellaneous products or Service Charges. </p>
                        <p>Select a from and to date using the two calendars and the results will be shown below.</p>
                        <p>Scroll down to the bottom of the page to view export buttons, you can export to CSV, Microsoft Word and Excel. You can also print out the results.</p>
                    </div>

                    <div className="filter-by-brand">
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="currency">Show data from:</label>
                            <select  ref="currency" className="form-control">
                                <option value="1">Today</option>
                                <option value="Yesterday">Yesterday</option>
                                <option value="ThisWeek">This Week</option>
                                <option value="ThisMonth">This Month</option>
                                <option value="ThisQuarter">This Quarter</option>
                                <option value="LastWeek">Last Week</option>
                                <option value="LastMonth">Last Month</option>
                                <option selected="selected" value="7">Last 7 days</option>
                                <option value="14">Last 14 days</option>
                                <option value="30">Last 30 days</option>
                                <option value="60">Last 60 days</option>
                                <option value="90">Last 90 days</option>
                                <option value="Custom">Custom</option>
                            </select>
                        </div>
                        
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="currency">Show data from:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">All location</option>
                                <option value="">VBDZHFVG</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="currency">Filter by Device:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">All Device</option>
                                <option value="">Till</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="currency">Filter by Wet/Dry:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">All</option>
                                <option value="">Wet</option>
                                <option value="">Dry</option>
                            </select>
                        </div>
                    </div>
                   
                   
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
       TransactionData:state.Transactions,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(TransactionActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(TillCategories));
