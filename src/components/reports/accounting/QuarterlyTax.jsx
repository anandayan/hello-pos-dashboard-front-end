import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { ButtonGroup, Button } from 'react-bootstrap';
import * as TransactionActions from '../../../actions/reports/transaction_actions';
import { bindActionCreators } from 'redux';
import Calendar from 'react-calendar';
//http://c3js.org/examples.html
let QuartlyTax = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    addStaff(){
        // this.props.router.push('staff/add-staff');
    },
    
    render() {
      
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                   
                   <div className="common-info">
                        <h1 className="title">End of Year Tax</h1>
                        <input type="button" onClick={this.Refresh} className="add-btn pull-right" value="REFRESH" />
                    </div>
                    <div className="parainfo">
                        
                    </div>

                    <div className="filter-by-brand">
                        
                        
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="currency">Year Starting:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">Year</option>
                                <option value="">2017</option>
                            </select>
                        </div>
                        <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-12 col-lg-12">
                           <input  type="checkbox" ref="tillAdmin" id="tillAdmin" value="1"/>
                           <label htmlFor="tillAdmin"> Run from specific date</label>
                       </div>

                        <div className="clear"></div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <Calendar/>
                        </div>
                        
                        <div className="clear"></div>

                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="currency">Filter by Location:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">All location</option>
                                <option value="">VBDZHFVG</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="currency">Filter by Device:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">All Device</option>
                                <option value="">Till</option>
                            </select>
                        </div>
                        
                        
                    </div>
                   
                   
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
       TransactionData:state.Transactions,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(TransactionActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(QuartlyTax));
