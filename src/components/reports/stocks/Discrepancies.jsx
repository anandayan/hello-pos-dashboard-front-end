import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { ButtonGroup, Button } from 'react-bootstrap';
import * as TransactionActions from '../../../actions/reports/transaction_actions';
import { bindActionCreators } from 'redux';
//http://c3js.org/examples.html
let Discripancies = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    addStaff(){
        // this.props.router.push('staff/add-staff');
    },
    
    render() {
      
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                   
                  <div className="common-info">
                        <h1 className="title">Stock Discrepancies</h1>
                        <input type="button" onClick={this.Refresh} className="add-btn pull-right" value="REFRESH" />
                    </div>
                    <div className="parainfo">
                        <p>On this page you will find a list of discrepancies in your stock movements and stock takes. Use the calendars and the dropdowns to filter.</p>
                    </div>

                    <div className="filter-by-brand">
                        
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="currency">Show data from:</label>
                            <select  ref="currency" className="form-control">
                                <option value="1">Today</option>
                                <option value="Yesterday">Yesterday</option>
                                <option value="ThisWeek">This Week</option>
                                <option value="ThisMonth">This Month</option>
                                <option value="ThisQuarter">This Quarter</option>
                                <option value="LastWeek">Last Week</option>
                                <option value="LastMonth">Last Month</option>
                                <option selected="selected" value="7">Last 7 days</option>
                                <option value="14">Last 14 days</option>
                                <option value="30">Last 30 days</option>
                                <option value="60">Last 60 days</option>
                                <option value="90">Last 90 days</option>
                                <option value="Custom">Custom</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="currency">Filter by Reason:</label>
                            <select  ref="currency" className="form-control">
                                <option selected="selected" value="">* All Reasons</option>
                                <option value="112140">Excess Stock</option>
                                <option value="112136">External Branch Movement</option>
                                <option value="112137">Internal Movement</option>
                                <option value="112143">Missing Stock</option>
                                <option value="112139">New Stock</option>
                                <option value="112138">Returns</option>
                                <option value="112142">Shrinkage</option>
                                <option value="112144">Stock Take</option>
                                <option value="112141">Wastage</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="currency">Filter by Location:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">All location</option>
                                <option value="">VBDZHFVG</option>
                            </select>
                        </div>
                        
                        
                        
                    </div>
                   
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
       TransactionData:state.Transactions,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(TransactionActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(Discripancies));
