import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
// import { ButtonGroup, Button } from 'react-bootstrap';
import * as TransactionActions from '../../../actions/reports/transaction_actions';
import { bindActionCreators } from 'redux';
import Calendar from 'react-calendar';
// import Calendar from 'react-calendar/build/entry.nostyle'; 

//http://c3js.org/examples.html
let Transaction = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {


    },

    Refresh(){

    },

    render() {
      
        return (

            <div>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="common-info">
                        <h1 className="title">Completed Transactions</h1>
                        <input type="button" onClick={this.Refresh} className="add-btn pull-right" value="REFRESH" />
                    </div>
                    <div className="parainfo">
                        <p>This page shows all the transactions that have been completed, you can click on 'Show Items' to see what products were within that certain transaction when using the Till.</p>
                        <p>Simply click on a date using the calendar to show the completed transactions on a certain day and the results will be shown below. You can also use the dropdown below the calendar to filter by location.</p>
                    </div>

                    <div className="clear"></div>

                    <Calendar/>
                    <div className="clear"></div>
                    <div className="filter-by-brand">
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="currency">Filter by Location:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">All location</option>
                                <option value="">VBDZHFVG</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="currency">Filter by Device:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">All Device</option>
                                <option value="">Till</option>
                            </select>
                        </div>
                    </div>


                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
       TransactionData:state.Transactions,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(TransactionActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(Transaction));
