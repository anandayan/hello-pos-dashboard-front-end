import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { ButtonGroup, Button } from 'react-bootstrap';
import * as TransactionActions from '../../../actions/reports/transaction_actions';
import { bindActionCreators } from 'redux';
import C3Chart from 'react-c3js';
import 'c3/c3.css';
//http://c3js.org/examples.html
let Transaction = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    addStaff(){
        // this.props.router.push('staff/add-staff');
    },
    
    render() {
      
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                   
                   <div className="common-info">
                        <h1 className="title">Ordered Products</h1>
                        <input type="button" onClick={this.Refresh} className="add-btn pull-right" value="REFRESH" />
                    </div>
                    <div className="parainfo">
                        <p>On this page you can view all products currently on order.</p>
                        <p>Scroll down to the bottom of the page to view export buttons, you can export to CSV, Microsoft Word and Excel. You can also print out the results.</p>
                    </div>
                   <div className="filter-by-brand">
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="currency">Filter by Location:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">All location</option>
                                <option value="">VBDZHFVG</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="currency">Filter by Device:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">All Device</option>
                                <option value="">Till</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="currency">Filter by Staff:</label>
                            <select  ref="currency" className="form-control">
                                <option value="">All Staff</option>
                                <option value="">jp</option>
                                <option value="">jp</option>
                                <option value="">jp</option>
                                <option value="">Till</option>
                            </select>
                        </div>
                    </div>
                    <div className="filter-by">
                      <div className="col-sm-4">
                          <span className="help-block txtalignRight">Search by Product Name</span>
                      </div>
                      <div className="col-sm-8">
                        <input ref="role_discription" className="form-control input-text-fields" id="role_discription"  type="text"   autoComplete="off"/>
                        <input className="add-other" value="Search" type="button" id=""/>
                      </div>
                    </div>
                   
                   
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
       TransactionData:state.Transactions,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(TransactionActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(Transaction));
