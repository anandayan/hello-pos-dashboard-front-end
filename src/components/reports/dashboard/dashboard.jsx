import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { ButtonGroup, Button } from 'react-bootstrap';
import * as CustomersActions from '../../../actions/management/customer_actions';
import { bindActionCreators } from 'redux';
import C3Chart from 'react-c3js';
import 'c3/c3.css';
//http://c3js.org/examples.html
let Dashboard = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    addStaff(){
        // this.props.router.push('staff/add-staff');
    },
    generateMyExpReport(){
        let myexp_data = this.state.MyExperience;
          let exp ={
            data:{
                //data: [200, -230, 190, 40, 130, 220], //MyExperience
                "data1":  [30, 200, 100, 400, 150, 250],
                "data2":  [50, 20, 10, 40, 15, 25],
            },
            
            configLine:{
                x: {type: 'category',
                       categories: ['cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6']
                      
                     },
                   y:{
                       label:{position: 'outer-right',text:"Qty"}
                   }
              } ,  
            configColor: {
                 pattern: ['#2EB7CE', '#5A9D9D', '#68A4CE', '#77C4D0', '#25608D', '#05A3DA']
             }, 
            width:{
               width:20
            }
          };
          return exp;
        },
    render() {
        
      let chartData = this.generateMyExpReport();
      

      var graph_confi = {
        grid: {
            x: {
                show: false
            },
            y: {
                show: true
            }
        },
        bar: {
             width:50 // this makes bar width 100px
        }
        ,
        legend: {
              position: 'bottom-righ'
          },
           color: {
                   pattern: ['#53A7E1','#F5A523','#50E2C1']
               }

      }
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                   
                    <div className="common-info">
                        <h1 className="title col-xs-12 col-sm-12 col-md-4 col-lg-4">Dashboard</h1>
                        
                    </div>
                    <div className="filter-by">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <p> <label htmlFor="staff_location">Show data from:</label></p>
                            <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                               
                                <select  ref="staff_location" id="staff_location" className="form-control">
                                    <option selected="selected" value="home">Hornchurch</option>
                                    <option selected="selected" value="home">Hornchurch</option>
                                    <option selected="selected" value="home">Hornchurch</option>
                                </select>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                 
                                <select  ref="staff_location"   className="form-control">
                                     <option selected="selected" value="home">Last 14 days</option>
                                    <option selected="selected" value="home">Yesterday</option>
                                    <option selected="selected" value="home">This Week</option>
                                    
                                </select>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <ButtonGroup>
                                    <Button className="marr5">Month</Button>
                                    <Button className="marr5">Week</Button>
                                    <Button className="marr5">Day</Button>
                                    <Button>Hourly</Button>
                                 </ButtonGroup>
                            </div>  
                        </div>
                    </div>
                   <div class>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div className="filter-by">
                            <C3Chart data={{ json: chartData.data, type: 'bar' }} axis = {chartData.configLine} bar={graph_confi.bar} grid =  {graph_confi.grid} bar={chartData.width}  color={chartData.configColor} />
                        </div> 
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div className="filter-by">
                        <C3Chart data={{ json: chartData.data }} axis = {chartData.configLine} grid =  {graph_confi.grid} bar={graph_confi.bar} color={graph_confi.configColor} />  
                        </div> 
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div className="filter-by">
                            <C3Chart data={{ json: chartData.data, type: 'pie' }} axis = {chartData.configLine} bar={graph_confi.bar} grid =  {graph_confi.grid} bar={chartData.width}  color={chartData.configColor} />
                        </div> 
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div className="filter-by">
                        <C3Chart data={{ json: chartData.data,type:'area' }} axis = {chartData.configLine} grid =  {graph_confi.grid} bar={graph_confi.bar} color={graph_confi.configColor} />  
                        </div> 
                    </div>
                   </div>
                   
                   
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
       CustomerData:state.Customer,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CustomersActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(Dashboard));
