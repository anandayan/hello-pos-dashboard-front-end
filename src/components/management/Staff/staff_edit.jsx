import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import * as StaffActions from '../../../actions/management/staff_actions';
import { bindActionCreators } from 'redux';
import * as LocationsActions from '../../../actions/setup/locations_actions';
import * as utils from '../../../util';
let StaffListEdit = React.createClass({
getInitialState() {
return {
updateClicked:false,
}
},
componentWillMount() {
;
},
componentWillReceiveProps(nextProps) {
;
if(this.state.updateClicked){
//
alert(nextProps.StaffData.UpdateStaff.message)
this.setState({updateClicked:false});
this.props.router.goBack()
}
},
componentDidMount() {
this.props.dispatch(LocationsActions.getListLocation());
},
updateUser(){
// ;
let data = {};
data.name = this.refs.staff_name.value;
data.location = this.refs.staff_location.value;
data.all_places = 1 //this.refs.allPlaces.value;
data.role = 1;//this.refs.staff_role.value;
data.password =this.refs.staff_password.value;
data.swipe_code =this.refs.staff_swipe_code.value;
data.hourly_rate =this.refs.staff_hourly_rate.value;
this.props.dispatch(StaffActions.updtStaffData(data));
this.setState({updateClicked:true});
},
cancel(){
this.setState({editMode:false});
},
menuClicked(event) {
this.props.router.push(event.target.dataset.routerpath)
},
back(event) {
this.props.router.push('/management/Staff/staff');
console.log("clicked");
},
render() {
let locationList =  utils.genderateOptionTemplate(this.props.Location.data);
return (
<div>
  
  <div className="col-sm-12 col-md-12 col-lg-12 right-container">
    <div className="filter-by">

            <div className="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
              <label htmlFor="name">Name:</label>
              <input ref="staff_name" className="form-control" type="text" />
            </div>
            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <label htmlFor="staff_location">Main Location</label>
              <select  ref="staff_location" id="staff_location" className="form-control">
                {locationList}
              </select>
            </div>
            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <label>Show Staff at all Locations:</label>
              <input   ref="allPlaces" id="" type="checkbox" />
            </div>
            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <label>Role
              </label>
              <select className="form-control" ref="staff_role">
                <option value="">None</option>
                <option value="">Level 2</option>
                <option value="">level 3</option>
                <option value="">Manager</option>
              </select>
            </div>
            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <label>Password:</label>
              <input className="form-control" ref="staff_password" defaultValue = {this.props.StaffData.StaffById[0].password}  type="password"  id=""  autoComplete="off"/>
            </div>
            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <label>Swipe Login:</label>
              <input className="form-control" defaultValue = {this.props.StaffData.StaffById[0].swipe_code}  ref="staff_swipe_code" type="password"  id="" />
            </div>
            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <label>Hourly Rate:</label>
              <input className="form-control" defaultValue = {this.props.StaffData.StaffById[0].hourly_rate}   ref="staff_hourly_rate" type="text"  id=""/>
            </div>
          </div>
          
          <div className="buttonBtn">
            <div className="cancelBtn">
              <input className="cancel btn-danger" value="Back" type="button" onClick={this.back} />
            </div>
            <div className="pull-right add-otherBtn">
              
              <input class onClick={this.updateUser} value="Update" type="button"  />
            </div>
          </div>
          
        </div>
      </div>
);
}
});
let mapStateToProps = (state) => {
return {
UserDetail:state.User,
StaffData:state.Staff,
Location:state.Location.GetLocation
};
};
let mapDispatchToProps = (dispatch) => ({
StaffActions: bindActionCreators(StaffActions, dispatch),
dispatch: dispatch
});
export default connect(mapStateToProps,
mapDispatchToProps)(withRouter(StaffListEdit));