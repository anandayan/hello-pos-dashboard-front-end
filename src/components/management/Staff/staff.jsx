import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import qq from '../../../img/question-mark.png';
import * as StaffActions from '../../../actions/management/staff_actions';
import { bindActionCreators } from 'redux';
import { Popover,OverlayTrigger } from 'react-bootstrap';
const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Brands">
         <p>Here you can edit your current employees by clicking on the Edit button or add new staff by clicking on the 'Add Staff' button and finally you can edit roles.</p>
            <p>This page displays all staff employed by your company.</p>
    </Popover>
  );
let StaffMember = React.createClass({
    getInitialState() {
        return {
        someState:'SomeValue',dltClicked:false,staffListCalled:false,StaffList:null,deleteStaffClicked:false,
        editMode:false,
    }
},
componentWillMount() {

},
componentWillReceiveProps(nextProps) {
    let staffData = nextProps.StaffData;
        if(this.state.staffListCalled){
            staffData.ListStaff.status_code === 200 && this.setState({StaffList:staffData.ListStaff.data,staffListCalled:false})
        }
        if(this.state.deleteStaffClicked){
            let data = {};
           
            this.props.dispatch(StaffActions.addListStaffData(data));
            this.setState({staffListCalled:true,deleteStaffClicked:false});
        }
},
componentDidMount() {
 
    this.props.dispatch(StaffActions.addListStaffData());
    this.setState({staffListCalled:true});
},

addStaff(){
    this.props.router.push('/management/Staff/add-staff');
},

menuClicked(event) {
    //this.props.router.push(event.target.dataset.routerpath)
},

deltStaff(event){
  // 
    let data = {};
    data.id = event.target.dataset.id;
    this.props.dispatch(StaffActions.dltStaffData(data));
    this.setState({deleteStaffClicked:true});
},

editUser(event){
//this.setState({editMode:true});

    let id = event.target.dataset.id;
    let staffList = this.props.StaffData.ListStaff.data;
    let data = _.filter(staffList, function (i) {

    return i._id == id ;
    });
    // 
    this.props.StaffData.StaffById = data;
    this.props.router.push('/management/Staff/staff-edit');
    //this.props.dispatch(StaffActions.getStaffDataByID(data));
},
generateStaffList(){
    let staffList = this.state.StaffList;
    let staffTemplate = staffList ?  staffList.map(function (listItem, key) {
    return (<tr key={key}><td>{listItem.email}</td><td>{listItem.lctn}</td><td>{listItem.all_places ? 'YES' : 'NO'}</td><td>{listItem.role_id}</td><td>{listItem.hourly_rate}</td><td><input readOnly type="password" value={listItem.swap_code}/></td><td><input type="password" readOnly value={listItem.swap_code}/></td><td><button data-id={listItem._id} onClick={this.editUser} className="edit btn btn-primary ">edit</button></td><td><button data-id={listItem._id} className="delete btn btn-danger" onClick={this.deltStaff}>delete</button></td></tr>)
    }, this):("No Data Found");
    return staffTemplate ;
},

render() {
let list = this.generateStaffList();
return (
<div class>
  
    <div className="col-sm-12 col-md-12 col-lg-12 right-container">
        <div className="upgrate license-link">
            
        </div>
        <div className="common-info">
           
            <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
            <h1 className="title">Staff <img src = {qq} alt="qus mark"/></h1>
                    </OverlayTrigger>
            <input type="button" onClick={this.addStaff} className="add-btn pull-right" value="Add Staff" />
        </div>
        
        <div className="filter-by">
          <div className="col-sm-4">
              <span className="help-block txtalignRight">Filter by Name, Location or Role:</span>
          </div>
          <div className="col-sm-8">
            <input ref="role_discription" className="form-control input-text-fields" id="role_discription"  type="text"   autoComplete="off"/>
            <input className="add-other" value="Search" type="button" id=""/>
          </div>
        </div>
        <div className="staff-table">
            <table className="table table-striped staff-list-tabel">
                <thead>
                    <tr>
                        <th>Display Name</th>
                        <th>Main location</th>
                        <th>Show all locations</th>
                        <th>Role</th>
                        <th>Hourly Rate</th>
                        <th>password</th>
                        <th>Swipe card</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {list}
                </tbody>
            </table>
        </div>
    </div>
</div>
);
}
});
let mapStateToProps = (state) => {
return {
UserDetail:state.User,
StaffData:state.Staff,
};
};
let mapDispatchToProps = (dispatch) => ({
UserActions: bindActionCreators(StaffActions, dispatch),
dispatch: dispatch
});
export default connect(mapStateToProps,
mapDispatchToProps)(withRouter(StaffMember));
