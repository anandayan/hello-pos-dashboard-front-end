import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as StaffActions from '../../../actions/management/staff_actions';
import { bindActionCreators } from 'redux';


let HourListEdit = React.createClass({
    getInitialState() {
        return {
             updateClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        ;
        if(this.state.updateClicked){
//
        alert(nextProps.StaffData.UpdateHour.message)
        this.setState({updateClicked:false});
         this.props.router.goBack()
        }


    },
    componentDidMount() {

    },
    updateHours(){
        
        let data = {};
        data.name = this.refs.staff_name.value;
        data.location = this.refs.hour_location.value;
        data.clock_in = this.refs.clock_in.value;
        data.clock_out = this.refs.clock_out.value;
        data.clock_type =this.refs.clock_type.value;
        data.notes =this.refs.hour_msg.value;
        this.props.dispatch(StaffActions.updateHourData(data));
        this.setState({updateClicked:true});

    },
    cancel(){
        this.setState({editMode:false});
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
    },

    render() {
        return (
            <div class>
              
               <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Add Hours</h1>
                    </div>
                    <div className="parainfo">
                        <p>Use the form on this page to enter the details of the new hours and click 'Add' when finished.</p>
                    </div>
                    <div className="filter-by">
                        <h3>Hours</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="name">Staff Name:</label>
                            <select  ref="staff_name" id="staff_name" className="form-control">
                              <option selected="selected" value="87544">Andreea</option>
                                <option value="87540">Bianca T</option>
                                <option value="87546">Diana Bigu</option>
                                <option value="87548">Diana Burtea</option>
                                <option value="92084">Ioana Balan </option>
                                <option value="87545">Koe</option>
                                <option value="87543">Raluca</option>
                                <option value="85962">Sam A</option>
                                <option value="87547">Shakeel</option>
                                <option value="87550">Zafar</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="hour_location">Location:</label>
                            <select  ref="hour_location" id="hour_location" className="form-control">
                                <option value="">All Staff</option>
                                <option value="15875">Hornchurch</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="clock_in">Clocking In Date & Time:</label>
                            <input ref="clock_in" className="form-control" id="clock_in"  type="text"  autoComplete="off" placeholder="DD/MM/YYYY HH:MM:SS"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="clock_out">Clocking Out Date & Time:</label>
                            <input ref="clock_out" className="form-control" id="clock_out"  type="text"  autoComplete="off" placeholder="DD/MM/YYYY HH:MM:SS"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="clock_type">Clocking Type:</label>
                            <select  ref="clock_type" id="clock_type" className="form-control">
                              <option selected="selected" value="">All Staff</option>
                                <option selected="selected" value="">No Type</option>
                                <option value="37896">Absent</option>
                                <option value="37894">Annual Leave</option>
                                <option value="37893">Overtime</option>
                                <option value="37895">Sick Pay</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label htmlFor="hour_msg">Notes:</label>
                            <textarea className="form-control" ref="hour_msg" rows="5" colSpan="10" id="comment"></textarea>
                        </div>
                        <div className="buttonBtn">
                            <div className="cancelBtn">
                               <input className="cancel btn-danger" value="Back" type="button" id=""/>
                            </div>
                            <div className="pull-right add-otherBtn">
                               <input class onClick={this.updateHours}  value="Add" type="button" id=""/>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      StaffData:state.Staff,
    };
};

let mapDispatchToProps = (dispatch) => ({
    StaffActions: bindActionCreators(StaffActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(HourListEdit));
