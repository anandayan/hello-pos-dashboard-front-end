import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as UserActions from '../../../actions/user_actions';
import * as StaffActions from '../../../actions/management/staff_actions';
import { bindActionCreators } from 'redux';
import { Popover,OverlayTrigger } from 'react-bootstrap';
import qq from '../../../img/question-mark.png';
const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Brands">
        <p>Here you can edit your current employees by clicking on the Edit button or add new staff by clicking on the 'Add Staff' button and finally you can edit roles.</p>
        <p>This page displays all staff employed by your company.</p>
    </Popover>
  );

let StaffRoles = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue',RoleList:null,roleListCalled:false,
        }
    },
    componentWillMount() {
    },
    componentWillReceiveProps(nextProps) {
     
      let roleListData = nextProps.StaffData;
        if(this.state.roleListCalled){
          if(roleListData.ListRole){
          roleListData.ListRole && this.setState({RoleList:roleListData.ListRole.data})
        }
      }
    },
    componentDidMount() {
      
      let data = {};
      
      this.props.dispatch(StaffActions.addtListRoleData(data));
      this.setState({roleListCalled:true})
    },

    editRole(event){
    let id = event.target.dataset.id;
    let staffList = this.props.StaffData.ListRole.data;
    let data = _.filter(staffList, function (i) {
    return i._id == id ;
    });
   
    this.props.StaffData.StaffById = data[0];
    this.props.router.push('/management/Staff/roles/edit-roles');
},

    AddRoles(){
        this.props.router.push('/management/Staff/roles/add-roles');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   generateRoleList(){
             let RoleList = this.state.RoleList;

             let roleListTemplate = RoleList ?  RoleList.map(function (listItem, key) {

           return (<tr key={key}>
           <td>{listItem.name}</td>
           <td colSpan="2">{listItem.description}</td>
            <td colSpan="2"><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.all_manual_customer_selection ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.all_void_lines ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.allow_refunds ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.back_office_from_till ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.basket_discount_limit ? true : false} /><label></label></div></td>
            <td>{listItem.basket_discount_limit_percent}</td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.basket_discounts ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.clearing ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.disable_service_charges ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.float_adjust ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.item_discount_limit ? true : false} /><label></label></div></td>
            <td>{listItem.item_discount_limit_percent}</td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.item_discounts ? true : false} /><label></label></div></td>
            <td>{listItem.max_credit_limit}</td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.no_sales ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.petty_cashes ? true : false} /><label></label></div></td>
            <td>{listItem.refund_limit}</td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.self_tip_out ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.show_discount_options ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.show_till_admin ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.show_till_customer_options ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.show_till_stock ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.show_till_transactions ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.stock_receive ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.stock_send_edit ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.stock_take ? true : false} /><label></label></div></td>
            <td>{listItem.tab_spending_limit}</td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.table_planning ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.till_access ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.till_admin ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.till_close ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.till_misc_products ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.till_settings ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.tip_out_others ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.transaction_holding ? true : false} /><label></label></div></td>
            <td><div className="checkbox checkbox-inline "><input  type="checkbox" defaultChecked = {listItem.unordered_void_lines ? true : false} /><label></label></div></td>
            
            <td><span data-id={listItem._id} onClick={this.editRole} className="edit glyphicon glyphicon-edit "></span></td>
            </tr>)
         }, this):("No Data Found");


             return roleListTemplate ;
   },
    render() {
      let roleListTemp = this.generateRoleList();
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        
                        <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                        <h1 className="title">Roles <img src = {qq} alt="qus mark"/></h1>
                    </OverlayTrigger>
                        <input type="button" onClick={this.AddRoles} className="add-btn pull-right" value="Add Role" />
                    </div>
                  
                    <div className="staff-table">
                        <table className="table table-striped  staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th colSpan="2" >Description</th>
                                     <th colSpan="2">all manual customer selection</th>
                                     <th>all void lines</th>
                                     <th>allow refunds </th>
                                     <th>back office from till</th>
                                     <th>basket discount limit</th>
                                     <th>basket discount limit percent</th>
                                     <th>basket discounts</th>
                                     <th>clearing</th>
                                     <th>disable service charges</th>
                                     <th>float adjust</th>
                                     <th>item discount limit</th>    
                                     <th>item discount limit percent</th>
                                     <th>item discounts</th>
                                     <th>max credit limit</th>
                                     <th>no sales</th>
                                     <th>petty cashes</th>
                                     <th>refund limit</th>
                                     <th>self tip out</th>
                                     <th>show discount options</th>
                                     <th>show till admin</th>
                                     <th>show till customer options</th>
                                     <th>show till stock</th>
                                     <th>show till transactions</th>
                                     <th>stock receive</th>
                                     <th>stock send edit</th>
                                     <th>stock take</th>
                                     <th>tab spending limit</th>
                                     <th>table planning</th>
                                      <th>till access</th>
                                      <th>till admin</th>  
                                      <th>till close</th>
                                      <th>till misc products</th>
                                      <th>till settings</th>
                                      <th>tip out others</th>
                                      <th>transaction holding</th>
                                      <th>unordered void lines</th>
                                     <th></th>
                                    </tr>
                            </thead>
                            <tbody> {roleListTemp}</tbody>
                          </table>
                    </div>
                    
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
        StaffData:state.Staff,
    };
};

let mapDispatchToProps = (dispatch) => ({
    StaffActions: bindActionCreators(StaffActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(StaffRoles));
