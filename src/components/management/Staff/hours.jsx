import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as StaffActions from '../../../actions/management/staff_actions';
import { bindActionCreators } from 'redux';
import { Popover,OverlayTrigger } from 'react-bootstrap';
import qq from '../../../img/question-mark.png';
const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Hours">
         <p>Here you can amend the hours worked by your staff by clicking on the 'Edit' button or add new hours by clicking on the 'Add Hours' button.</p>
            <p>You can filter your clocking information using the drop downs and calendars below. Clockings will be shown if they begin OR end between the dates selected.</p>
    </Popover>
  );


let StaffHours = React.createClass({
    getInitialState() {
        return {
             hourListCalled:false,HourList:null,deleteStaffClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        
    let hoursListData = nextProps.StaffData;
        if(this.state.hourListCalled){

            if(hoursListData.ListHour){

            hoursListData.ListHour && this.setState({HourList:hoursListData.ListHour.data})
            }

            if(this.state.deleteStaffClicked){
            let data = {};
           
            this.props.dispatch(StaffActions.addListHourData(data));
            this.setState({hoursListData:true,deleteStaffClicked:false});
            }
        }
    },
    componentDidMount() {
        
        let data = {};
        
        this.props.dispatch(StaffActions.addListHourData(data));
        this.setState({hourListCalled:true});  
    },
    Addhours(){
        this.props.router.push('/management/Staff/hours/clock-Add');
    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   deltStaff(event){
  // 
    let data = {};
    data.id = event.target.dataset.id;
    this.props.dispatch(StaffActions.deleteHourData(data));
    this.setState({deleteStaffClicked:true});
},
   editUser(event){
    let id = event.target.dataset.id;
    let hourList = this.props.StaffData.ListHour.data;
    let data = _.filter(hourList, function (i) {

    return i._id == id ;
    });
    // 
    this.props.StaffData.StaffById = data;
    this.props.router.push('/management/Staff/hours/edit-hour');
    //this.props.dispatch(StaffActions.getStaffDataByID(data));
},
   generateHourList(){
    let hourList = this.state.HourList;
    let staffTemplate = hourList ?  hourList.map(function (listItem, key) {
    return (<tr key={key}><td>{listItem.name}</td><td>{listItem.location_id}</td><td>{listItem.clock_in}</td><td>{listItem.clock_out}</td><td></td><td>{listItem.clock_type_id}</td><td><button data-id={listItem._id} onClick={this.editUser} className="edit btn btn-primary ">edit</button></td><td><button data-id={listItem._id} className="delete btn btn-danger" onClick={this.deltStaff}>delete</button></td></tr>)
    }, this):("No Data Found");
    return staffTemplate ;
    },
    render() {
        let list = this.generateHourList();
        return (

            <div class>
                
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                     <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                        <h1 className="title">Hours <img src = {qq} alt="qus mark"/></h1>
                        </OverlayTrigger>
                        <input type="button" onClick={this.Addhours} className="add-btn pull-right" value="Add Hours" />
                    </div>
                    
                    <div className="filter-by">
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="name">FILTER BY LOCATION:</label>
                            <select  ref="staff_location" id="staff_location" className="form-control">
                               <option selected="selected" value="">All Locations</option>
                                <option value="15875">Hornchurch</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="name">Filter by staff:</label>
                            <select  ref="staff_location" id="staff_location" className="form-control">
                              <option selected="selected" value="">All Staff</option>
                                <option value="87544">Andreea</option>
                                <option value="87540">Bianca T</option>
                                <option value="87546">Diana Bigu</option>
                                <option value="87548">Diana Burtea</option>
                                <option value="92084">Ioana Balan </option>
                                <option value="87545">Koe</option>
                                <option value="87543">Raluca</option>
                                <option value="85962">Sam A</option>
                                <option value="87547">Shakeel</option>
                                <option value="87550">Zafar</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="name">Filter by Clocking Type:</label>
                            <select  ref="staff_location" id="staff_location" className="form-control">
                              <option selected="selected" value="">All Clocking Types</option>
                                <option value="-1">No Type</option>
                                <option value="37896">Absent</option>
                                <option value="37894">Annual Leave</option>
                                <option value="37893">Overtime</option>
                                <option value="37895">Sick Pay</option>
                           </select>
                        </div>
                    </div>
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Staff Name</th>
                                    <th>Location</th>
                                    <th>Clocking in date & time</th>
                                    <th>Clocking out date & time</th>
                                    <th>Hours worked</th>
                                    <th>Clocking type</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {list}
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
        StaffData:state.Staff,
    };
};

let mapDispatchToProps = (dispatch) => ({
    StaffActions: bindActionCreators(StaffActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(StaffHours));
