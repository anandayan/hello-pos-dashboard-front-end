import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as StaffActions from '../../../actions/management/staff_actions';
import { bindActionCreators } from 'redux';


let AddRole = React.createClass({
    getInitialState() {
        return {
              updateClicked:false,
              permissionsAllowed:true,
              showTillOption:true,
              showTillStockOption:true,
              showTillTransactionOption:true,
              showTillDiscountOption:true,
              showTillCustomerOption:true,
            }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

        if(this.state.updateClicked){
//
        alert(nextProps.StaffData.UpdateRole.message)
        this.setState({updateClicked:false});
         this.props.router.goBack()
        }
    },
    componentDidMount() {
        
    },
    cancel(){
      this.props.router.goBack()
    },
    updateRoles(){
      
        let data = {};
   
        data.name = this.refs.role_name.value;
        data.description = this.refs.role_discription.value; 
        data.till_access = this.refs.tillAccess.checked ? 1 : 0;
        data.show_till_admin = this.refs.showTillAdmin.checked ? 1 : 0;
        data.till_admin = this.refs.tillAdmin.checked ? 1 : 0;
        data.till_settings = this.refs.tillSettings.checked ? 1 : 0;
        data.petty_cashes = this.refs.pettyCashes.checked ? 1 : 0;
        data.float_adjust = this.refs.floatAdjust.checked ? 1 : 0;
        data.no_sales = this.refs.noSales.checked ? 1 : 0;
        data.back_office_from_till = this.refs.backOfficeFromTill.checked ? 1 : 0;
        data.till_close = this.refs.tillClose.checked ? 1 : 0;
        data.self_tip_out = this.refs.selfTipOut.checked ? 1 : 0;
        data.tip_out_others = this.refs.tipOutOthers.checked ? 1 : 0;
        data.show_till_stock = this.refs.showTillStock.checked ? 1 : 0;
        data.stock_send_edit = this.refs.stockSendEdit.checked ? 1 : 0;
        data.stock_receive = this.refs.stockReceive.checked ? 1 : 0;
        data.stock_take = this.refs.stockTake.checked ? 1 : 0;

        data.show_till_transactions = this.refs.showTillTransactions.checked ? 1 : 0;
        data.all_void_lines = this.refs.allVoidLines.checked ? 1 : 0;
        data.unordered_void_lines = this.refs.unorderedVoidLines.checked ? 1 : 0;
        data.clearing = this.refs.clearing.checked ? 1 : 0;
        data.table_planning = this.refs.tablePlanning.checked ? 1 : 0;
        data.disable_service_charges = this.refs.disableServiceCharges.checked ? 1 : 0;
        data.transaction_holding = this.refs.transactionHolding.checked ? 1 : 0;
        data.tab_spending_limit = this.refs.tabSpendingLimit.checked ? 1 : 0;
        data.till_misc_products = this.refs.tillMiscProducts.checked ? 1 : 0;

        data.show_discount_options = this.refs.showDiscountOptions.checked ? 1 : 0;
        data.item_discounts = this.refs.itemDiscounts.checked ? 1 : 0;
        data.item_discount_limit = this.refs.itemDiscountLimit.checked ? 1 : 0;
        data.item_discountLimit_percent = this.refs.itemDiscountLimitPercent.checked ? 1 : 0;
        data.basket_discounts = this.refs.basketDiscounts.checked ? 1 : 0;
        data.basket_discount_limit = this.refs.basketDiscountLimit.checked ? 1 : 0;
        data.basket_discountLimit_percent = this.refs.basketDiscountLimitPercent.checked ? 1 : 0;
        data.allow_refunds = this.refs.allowRefunds.checked ? 1 : 0;
        data.refund_limit = this.refs.refundLimit.checked ? 1 : 0;

        data.show_till_customer_options = this.refs.showTillCustomerOptions.checked ? 1 : 0;
        data.allow_manual_customer_selection = this.refs.allowManualCustomerSelection.checked ? 1 : 0;
        data.max_credit_limit = this.refs.maxCreditLimit.checked ? 1 : 0;
        this.props.dispatch(StaffActions.updateRoleData(data));
        this.setState({updateClicked:true});
    },
 
   HandelTillPermissiosn(event){

     let permissionValue = this.state.permissionsAllowed ? false : true ;
     
     this.setState({permissionsAllowed:permissionValue});


   },
   handelTillAdmin(event){

     let permissionValue = this.state.showTillOption ? false : true ;
     this.setState({showTillOption:permissionValue});

   },
   handelTillStock(event){

        let permissionValueStock = this.state.showTillStockOption ? false : true ;
        this.setState({showTillStockOption:permissionValueStock});
   },

   handelTillTransaction(event){
            let permissionValueTransaction = this.state.showTillTransactionOption ? false : true ;
            this.setState({showTillTransactionOption:permissionValueTransaction});

     },
     handelTillAdminDiscount(event){
            let permissionValueDiscount = this.state.showTillDiscountOption ? false : true ;
            this.setState({showTillDiscountOption:permissionValueDiscount});

     },
     handelTillCustomer(event){
            let permissionValueCustomer = this.state.showTillCustomerOption ? false : true ;
            this.setState({showTillCustomerOption:permissionValueCustomer});
     },
    render() {
  
        return (

            <div class>

            <div className="col-sm-12 col-md-10 col-lg-10">
            <div class>
                <div className="row">
                    <div className="col-md-12">
                        <h1 className="staff-title"> Edit Roles

                        </h1>

                    </div>

                </div>

                    <div class>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                          <label htmlFor="name">Name:</label>

                          <input ref="role_name" className="form-control"   type="text" defaultValue={this.props.StaffData.StaffById.name}/>
                        </div>

                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                          <label htmlFor="name">description:</label>

                          <input ref="role_discription" className="form-control" id="role_discription"  type="text" defaultValue={this.props.StaffData.StaffById.description} />
                        </div>

                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p><label>Permissions</label></p>
                         <div className="checkbox checkbox-inline">
                         <input onChange={this.HandelTillPermissiosn} type="checkbox" ref="tillAccess" id="tillAccess" defaultChecked="true"/>
                         <label htmlFor="tillAccess"> Allow Till Access (check to view more permissions) </label>
                         </div>
                        </div>

                        <div className={ this.state.permissionsAllowed ? "col-xs-12 col-sm-12 col-md-12 col-lg-12" : "row hideDiv"}>
                            <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">

                               <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                               <div className="checkbox">
                                   <input onChange={this.handelTillAdmin} type="checkbox" ref="showTillAdmin" id="showTillAdmin" defaultChecked="true" />
                                   <label htmlFor="showTillAdmin"> Show Till Admin Options </label>
                               </div>


                               <div className={ this.state.showTillOption ? "inner-checkBox row" : "hideDiv"}>
                                       <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                           <input  type="checkbox" ref="tillAdmin" id="tillAdmin"   defaultChecked={this.props.StaffData.StaffById.till_admin ? true : false}/>
                                           <label htmlFor="tillAdmin"> Allow Admin Access on the Till</label>

                                       </div>
                                       <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                           <input  type="checkbox" ref="tillSettings" id="tillSettings"   defaultChecked={this.props.StaffData.StaffById.till_settings ? true : false}/>
                                           <label htmlFor="tillSettings"> Allow Till Settings</label>

                                       </div>
                                       <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                           <input  type="checkbox" ref="pettyCashes" id="pettyCashes" defaultChecked={this.props.StaffData.StaffById.petty_cashes ? true : false}/>
                                           <label htmlFor="pettyCashes">Allow Petty Cashes</label>

                                       </div>
                                       <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                           <input  type="checkbox" ref="floatAdjust" id="floatAdjust"  defaultChecked={this.props.StaffData.StaffById.float_adjust ? true : false}/>
                                           <label htmlFor="floatAdjust">Allow Float Adjustments</label>

                                       </div>
                                       <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                           <input  type="checkbox" ref="noSales" id="noSales"  defaultChecked={this.props.StaffData.StaffById.no_sales ? true : false}/>
                                           <label htmlFor="noSales"> Allow No Sales</label>

                                       </div>
                                       <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                           <input  type="checkbox" ref="backOfficeFromTill" id="backOfficeFromTill"   defaultChecked={this.props.StaffData.StaffById.back_office_from_till ? true : false}/>
                                           <label htmlFor="backOfficeFromTill">Allow Back Office Access From Till</label>

                                       </div>
                                       <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                           <input  type="checkbox" ref="tillClose" id="tillClose"   defaultChecked={this.props.StaffData.StaffById.till_close ? true : false}/>
                                           <label htmlFor="tillClose">Allow Till Closing</label>

                                       </div>
                                       <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                           <input  type="checkbox" ref="selfTipOut" id="selfTipOut"   defaultChecked={this.props.StaffData.StaffById.self_tip_out ? true : false}/>
                                           <label htmlFor="selfTipOut">Allow Self Tip Out </label>

                                       </div>
                                       <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                           <input  type="checkbox" ref="tipOutOthers" id="tipOutOthers"   defaultChecked={this.props.StaffData.StaffById.tip_out_others ? true : false}/>
                                           <label htmlFor="tipOutOthers">Allow Tiping Out Others </label>

                                       </div>
                                </div>
                               </div>
                                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div className="checkbox">
                                    <input onChange={this.handelTillStock} type="checkbox" ref="showTillStock" id="showTillStock" defaultChecked="true"/>
                                    <label htmlFor="showTillStock"> Show Till Stock Options </label>
                                </div>


                                <div className={this.state.showTillStockOption ? "inner-checkbox row" : "hideDiv"}>
                                    <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="stockSendEdit" id="stockSendEdit"  defaultChecked={this.props.StaffData.StaffById.stock_send_edit ? true : false} />
                                       <label htmlFor="stockSendEdit"> Allow Stock Sending and Editing</label>

                                   </div>
                                   <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="stockReceive" id="stockReceive"   defaultChecked={this.props.StaffData.StaffById.stock_receive ? true : false}/>
                                       <label htmlFor="stockReceive"> Allow Stock Receive</label>

                                   </div>
                                   <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="stockTake" id="stockTake" defaultChecked={this.props.StaffData.StaffById.stock_take ? true : false}/>
                                       <label htmlFor="stockTake">Allow Stock Take </label>

                                   </div>
                                </div>
                              </div>
                                <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div className="checkbox">
                                    <input onChange={this.handelTillTransaction} type="checkbox" ref="showTillTransactions" id="showTillTransactions"  defaultChecked="true"/>
                                   <label htmlFor="showTillTransactions"> Show Till Transaction Options </label>
                                </div>
                                <div className={this.state.showTillTransactionOption ? "inner-checkbox row" : "hideDiv"}>
                                    <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="allVoidLines" id="allVoidLines"   defaultChecked={this.props.StaffData.StaffById.all_void_lines ? true : false}/>
                                       <label htmlFor="allVoidLines"> Allow Void Lines</label>

                                   </div>
                                   <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="unorderedVoidLines" id="unorderedVoidLines"   defaultChecked={this.props.StaffData.StaffById.unordered_void_lines ? true : false}/>
                                       <label htmlFor="unorderedVoidLines"> Allow Unordered Void Lines</label>

                                   </div>
                                   <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="clearing" id="clearing"  defaultChecked={this.props.StaffData.StaffById.clearing ? true : false}/>
                                       <label htmlFor="clearing"> Allow Clearing</label>

                                   </div>
                                   <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="tablePlanning" id="tablePlanning"  defaultChecked={this.props.StaffData.StaffById.table_planning ? true : false}/>
                                       <label htmlFor="tablePlanning">Allow Table Planning </label>

                                   </div>
                                   <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="disableServiceCharges" id="disableServiceCharges"   defaultChecked={this.props.StaffData.StaffById.disable_service_charges ? true : false}/>
                                       <label htmlFor="disableServiceCharges">Allow Disabling Service Charge </label>

                                   </div>
                                   <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="transactionHolding" id="transactionHolding"  defaultChecked={this.props.StaffData.StaffById.transaction_holding ? true : false}/>
                                       <label htmlFor="transactionHolding">Allow Transaction Holding </label>
                                   </div>
                                   <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="tabSpendingLimit" id="tabSpendingLimit"  defaultChecked={this.props.StaffData.StaffById.tab_spending_limit ? true : false}/>
                                       <label htmlFor="tabSpendingLimit">Allow Changing Tab Spending Limit </label>
                                   </div>
                                   <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="tillMiscProducts" id="tillMiscProducts"  defaultChecked={this.props.StaffData.StaffById.till_misc_products ? true : false}/>
                                       <label htmlFor="tillMiscProducts"> Allow Till Misc Products </label>
                                   </div>
                                </div>
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div className="checkbox">
                                    <input onChange={this.handelTillAdminDiscount} type="checkbox" ref="showDiscountOptions" id="showDiscountOptions" defaultChecked="true" />
                                    <label htmlFor="showDiscountOptions"> Show Till Discount Options </label>
                                </div>
                                <div className={this.state.showTillDiscountOption ? "inner-checkbox row" : "hideDiv"}>
                                    <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="itemDiscounts" id="itemDiscounts"   defaultChecked={this.props.StaffData.StaffById.item_discounts ? true : false}/>
                                       <label htmlFor="itemDiscounts"> Allow Item Discounts</label>
                                   </div>
                                   <input ref="itemDiscountLimit" className="form-control" id="itemDiscountLimit" type="text" placeholder="Item discount limit"    defaultValue={this.props.StaffData.StaffById.item_discount_limit} />
                                   <input ref="itemDiscountLimitPercent" className="form-control" id="itemDiscountLimitPercent" type="text" placeholder="Item discount limit %"   defaultValue={this.props.StaffData.StaffById.item_discount_limit_percent}/>

                                   <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="basketDiscounts" id="basketDiscounts"   defaultChecked={this.props.StaffData.StaffById.basketDiscounts ? true : false}/>
                                       <label htmlFor="basketDiscounts"> Allow Basket Discounts</label>
                                   </div>
                                   <input ref="basketDiscountLimit" className="form-control" id="basketDiscountLimit" type="text" placeholder="Basket discount limit"   defaultValue={this.props.StaffData.StaffById.basket_discount_limit}  />
                                   <input ref="basketDiscountLimitPercent" className="form-control" id="basketDiscountLimitPercent" type="text" placeholder="Basket discount limit %"   defaultValue={this.props.StaffData.StaffById.basket_discount_limit_percent}  />

                                   <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="allowRefunds" id="allowRefunds" defaultChecked={this.props.StaffData.StaffById.allow_refunds ? true : false}/>
                                       <label htmlFor="allowRefunds">Allow Refunds </label>
                                   </div>
                                   <input ref="refundLimit" className="form-control" id="refundLimit" type="text" placeholder="Refunds limit"   defaultValue={this.props.StaffData.StaffById.refund_limit}   />
                                </div>
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div className="checkbox">
                                    <input onChange={this.handelTillCustomer} type="checkbox" ref="showTillCustomerOptions" id="showTillCustomerOptions" defaultChecked="true"/>
                                    <label htmlFor="showTillCustomerOptions"> Show Till Customer Options </label>
                                </div>
                                <div className={this.state.showTillCustomerOption ? "inner-checkbox row" : "hideDiv"}>
                                    <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                       <input  type="checkbox" ref="allowManualCustomerSelection" id="allowManualCustomerSelection"  defaultChecked={this.props.StaffData.StaffById.all_manual_customer_selection ? true : false}/>
                                       <label htmlFor="allowManualCustomerSelection"> Allow Manual Customer Selection</label>
                                   </div>
                                   <input ref="maxCreditLimit" className="form-control" id="maxCreditLimit" type="text" placeholder="Max credit limit"    defaultValue={this.props.StaffData.StaffById.max_credit_limit}  />
                                </div>
                                </div>
                        </div>

                    </div>


                    <button  onClick={this.updateRoles} type="button" className="pull-right btn btn-primary add-staff-btn">update Roles
                   </button>
                   <button  onClick={this.cancel} type="button" className="pull-right btn btn-primary add-staff-btn">Back
                  </button>
                    </div>
            </div>

            </div>

            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
        UserData:state.User,
        StaffData:state.Staff,
    };
};

let mapDispatchToProps = (dispatch) => ({
    StaffActions: bindActionCreators(StaffActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddRole));
