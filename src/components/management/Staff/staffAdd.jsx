import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as StaffActions from '../../../actions/management/staff_actions';
import * as LocationsActions from '../../../actions/setup/locations_actions';
import * as utils from '../../../util';
import { bindActionCreators } from 'redux';


let StaffAdder = React.createClass({
    getInitialState() {
        return {
             addClicked:false,
             dltClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

      if(this.state.addClicked){ 
        alert(nextProps.StaffData.StaffAdded.message)
        this.setState({addClicked:false});
      }
      if(this.state.dltClicked){
        alert(nextProps.StaffData.StaffAdded.message)
        this.setState({dltClicked:false});
      }
    },
    componentDidMount() {
       
      this.props.dispatch(StaffActions.addtListRoleData());
      this.props.dispatch(LocationsActions.getListLocation());
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
    },
    
    addStaff(){
        let data = {};
     
        data.email = this.refs.staff_name.value;
        data.location =  this.refs.staff_location.value;
        data.all_places = this.refs.allPlaces.checked ? 1 :0;
        //data.role = this.refs.staff_role.value;
        data.role_id = this.refs.staff_role.value;
        data.password =this.refs.staff_password.value;
        data.swap_code =this.refs.staff_swipe_code.value;
        data.hourly_rate =this.refs.staff_hourly_rate.value;
        console.log(data)
        this.props.dispatch(StaffActions.addStaffData(data));
        this.setState({addClicked:true});
    },

    addRole(){
      this.props.router.push('/management/Staff/roles/add-roles');
    },

  back(event) {
        this.props.router.push('/management/Staff/staff');
    },
    render() {
      let rolList =   utils.genderateOptionTemplate(this.props.StaffData.ListRole.data);
      let locationList =  utils.genderateOptionTemplate(this.props.Location.data);
    
        return (
            <div>
              
              <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                <div className="upgrate license-link">
                  <h1 className="add-title">Add a Member of Staff</h1>
                    
                </div>
                <div className="parainfo">
                    <p>This page displays all staff employed by your company.On this page you can add new members of staff, these members of staff are listed when logging into the Till. You can also click on the 'Add Roles' button, this allows you to give certain access to staff.</p>
                </div>
                <div className="filter-by">
                  <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <label htmlFor="name">Name:</label>
                    <input ref="staff_name" className="form-control"   type="text"    autoComplete="off"/>
                  </div>

                  <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <label htmlFor="staff_location">Main Location</label>
                       <select  ref="staff_location" id="staff_location" className="form-control">
                           {locationList}
                       </select>
                  </div>

                  <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <label htmlFor="staff_password">Password:</label>
                    <input className="form-control" ref="staff_password"  type="password"     autoComplete="off"/>
                  </div>

                  <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <label htmlFor="staff_swipe_code">Swipe Login:</label>
                    <input className="form-control"  ref="staff_swipe_code" type="password"    />
                  </div>

                  <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <label htmlFor="staff_hourly_rate">Hourly Rate:</label>
                    <input className="form-control"   ref="staff_hourly_rate" type="text"   />
                  </div>

                  <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <label htmlFor="staff_role">Role 
                    </label>
                    <select className="form-control"  ref="staff_role"  >
                       {rolList}
                    </select>
                  </div>
                  <div className="checkbox checkbox-inline col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <input  type="checkbox" ref="allPlaces" id="allPlaces" value=""/>
                    <label htmlFor="allPlaces"> Show Staff at all Locations:</label>
                  </div>
                  
                  <div className="buttonBtn">
                    <div className="cancelBtn">
                       <input className="cancel btn-danger" value="Cancel" type="button" onClick={this.back} />
                    </div>
                    <div className="pull-right add-otherBtn">
                        
                       <input class onClick={this.addStaff} value="Add Staff" type="button"  />
                    </div>
                  </div>
                </div>

              </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserData:state.User,
      StaffData:state.Staff,
      Location:state.Location.GetLocation
    };
};

let mapDispatchToProps = (dispatch) => ({
    StaffActions: bindActionCreators(StaffActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(StaffAdder));
