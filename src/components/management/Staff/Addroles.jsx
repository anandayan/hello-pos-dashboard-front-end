import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as StaffActions from '../../../actions/management/staff_actions';
import { bindActionCreators } from 'redux';
let AddRole = React.createClass({
    getInitialState() {
      return {
        addClicked:false,
        permissionsAllowed:false,
        showTillOption:false,
        showTillStockOption:false,
        showTillTransactionOption:false,
        showTillDiscountOption:false,
        showTillCustomerOption:false,        }
      },
    componentWillMount() {
    },
    componentWillReceiveProps(nextProps) {
       if(this.state.addClicked){
        // alert(nextProps.StaffData.RoleAdded.message)
        this.setState({addClicked:false});
      }
    },
      componentDidMount() {
    },
    cancel(){
    this.props.router.goBack()
    },
    addRoles(){
      
            let data = {};
          
            data.name = this.refs.role_name.value;
            data.code=this.refs.role_name.value;
            data.description = this.refs.role_discription.value;
            data.till_access = this.refs.tillAccess.checked ? 1 : 0;
            data.show_till_admin = this.refs.showTillAdmin.checked ? 1 : 0;
            data.till_admin = this.refs.tillAdmin.checked ? 1 : 0;
            data.till_settings = this.refs.tillSettings.checked ? 1 : 0;
            data.petty_cashes = this.refs.pettyCashes.checked ? 1 : 0;
            data.float_adjust = this.refs.floatAdjust.checked ? 1 : 0;
            data.no_sales = this.refs.noSales.checked ? 1 : 0;
            data.back_office_from_till = this.refs.backOfficeFromTill.checked ? 1 : 0;
            data.till_close = this.refs.tillClose.checked ? 1 : 0;
            data.self_tip_out = this.refs.selfTipOut.checked ? 1 : 0;
            data.tip_out_others = this.refs.tipOutOthers.checked ? 1 : 0;
            data.show_till_stock = this.refs.showTillStock.checked ? 1 : 0;
            data.stock_send_edit = this.refs.stockSendEdit.checked ? 1 : 0;
            data.stock_receive = this.refs.stockReceive.checked ? 1 : 0;
            data.stock_take = this.refs.stockTake.checked ? 1 : 0;

            data.show_till_transactions = this.refs.showTillTransactions.checked ? 1 : 0;
            data.all_void_lines = this.refs.allVoidLines.checked ? 1 : 0;
            data.unordered_void_lines = this.refs.unorderedVoidLines.checked ? 1 : 0;
            data.clearing = this.refs.clearing.checked ? 1 : 0;
            data.table_planning = this.refs.tablePlanning.checked ? 1 : 0;
            data.disable_service_charges = this.refs.disableServiceCharges.checked ? 1 : 0;
            data.transaction_holding = this.refs.transactionHolding.checked ? 1 : 0;
            data.tab_spending_limit = this.refs.tabSpendingLimit.checked ? 1 : 0;
            data.till_misc_products = this.refs.tillMiscProducts.checked ? 1 : 0;

            data.show_discount_options = this.refs.showDiscountOptions.checked ? 1 : 0;
            data.item_discounts = this.refs.itemDiscounts.checked ? 1 : 0;
            data.item_discount_limit = this.refs.itemDiscountLimit.checked ? 1 : 0;
            data.item_discount_limit_percent = this.refs.itemDiscountLimitPercent.checked ? 1 : 0;
            data.basket_discounts = this.refs.basketDiscounts.checked ? 1 : 0;
            data.basket_discount_limit = this.refs.basketDiscountLimit.checked ? 1 : 0;
            data.basket_discountLimit_percent = this.refs.basketDiscountLimitPercent.checked ? 1 : 0;
            data.allow_refunds = this.refs.allowRefunds.checked ? 1 : 0;
            data.refund_limit = this.refs.refundLimit.checked ? 1 : 0;

            data.show_till_customer_options = this.refs.showTillCustomerOptions.checked ? 1 : 0;
            data.allow_manual_customer_selection = this.refs.allowManualCustomerSelection.checked ? 1 : 0;
            data.max_credit_limit = this.refs.maxCreditLimit.checked ? 1 : 0;
            this.props.dispatch(StaffActions.addRoleData(data));
            this.setState({addClicked:true});
    },
      menuClicked(event) {
      this.props.router.push(event.target.dataset.routerpath)
      },
      HandelTillPermissiosn(event){
      let permissionValue = this.state.permissionsAllowed ? false : true ;
      this.setState({permissionsAllowed:permissionValue});
      },
      handelTillAdmin(event){
      let permissionValue = this.state.showTillOption ? false : true ;
      this.setState({showTillOption:permissionValue});
      },
      handelTillStock(event){
      let permissionValueStock = this.state.showTillStockOption ? false : true ;
      this.setState({showTillStockOption:permissionValueStock});
      },
      handelTillTransaction(event){
      let permissionValueTransaction = this.state.showTillTransactionOption ? false : true ;
      this.setState({showTillTransactionOption:permissionValueTransaction});
      },
      handelTillAdminDiscount(event){
      let permissionValueDiscount = this.state.showTillDiscountOption ? false : true ;
      this.setState({showTillDiscountOption:permissionValueDiscount});
      },
      handelTillCustomer(event){
      let permissionValueCustomer = this.state.showTillCustomerOption ? false : true ;
      this.setState({showTillCustomerOption:permissionValueCustomer});
      },
render() {
return (
<div class>
  
  <div className="col-sm-12 col-md-12 col-lg-12 right-container">
    <div className="upgrate license-link">
      <h1 className="add-title">Role</h1>
      
    </div>
    <div className="parainfo">
    </div>
    <div className="filter-by">
      <form className="roles-details" id="roles-details">
        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <label htmlFor="name">Name:</label>
          <input ref="role_name" className="form-control"   type="text"   autoComplete="off"/>
        </div>
        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <label htmlFor="name">description:</label>
          <input ref="role_discription" className="form-control" id="role_discription"  type="text"   autoComplete="off"/>
        </div>
        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <p><label>Permissions</label></p>
          <div className="checkbox checkbox-inline">
            <input onChange={this.HandelTillPermissiosn} type="checkbox" ref="tillAccess" id="tillAccess" value="1"/>
            <label htmlFor="tillAccess"> Allow Till Access (check to view more permissions) </label>
          </div>
        </div>
        <div className={ this.state.permissionsAllowed ? "col-xs-12 col-sm-12 col-md-12 col-lg-12" : "row hideDiv"}>
          <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div className="checkbox">
                <input onChange={this.handelTillAdmin} type="checkbox" ref="showTillAdmin" id="showTillAdmin" value="1"/>
                <label htmlFor="showTillAdmin"> Show Till Admin Options </label>
              </div>
              <div className={ this.state.showTillOption ? "inner-checkBox row" : "hideDiv"}>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="tillAdmin" id="tillAdmin" value="1"/>
                   <label htmlFor="tillAdmin"> Allow Admin Access on the Till</label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="tillSettings" id="tillSettings" value="1"/>
                   <label htmlFor="tillSettings"> Allow Till Settings</label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="pettyCashes" id="pettyCashes" value="1"/>
                   <label htmlFor="pettyCashes">Allow Petty Cashes</label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="floatAdjust" id="floatAdjust" value="1"/>
                   <label htmlFor="floatAdjust">Allow Float Adjustments</label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="noSales" id="noSales" value="1"/>
                   <label htmlFor="noSales"> Allow No Sales</label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="backOfficeFromTill" id="backOfficeFromTill" value="1"/>
                   <label htmlFor="backOfficeFromTill">Allow Back Office Access From Till</label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="tillClose" id="tillClose" value="1"/>
                   <label htmlFor="tillClose">Allow Till Closing</label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="selfTipOut" id="selfTipOut" value="1"/>
                   <label htmlFor="selfTipOut">Allow Self Tip Out </label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="tipOutOthers" id="tipOutOthers" value="1"/>
                   <label htmlFor="tipOutOthers">Allow Tiping Out Others </label>
               </div>
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div className="checkbox">
                <input onChange={this.handelTillStock} type="checkbox" ref="showTillStock" id="showTillStock" value="1"/>
                <label htmlFor="showTillStock"> Show Till Stock Options </label>
              </div>
              <div className={this.state.showTillStockOption ? "inner-checkbox row" : "hideDiv"}>
                  <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                     <input  type="checkbox" ref="stockSendEdit" id="stockSendEdit" value="1"/>
                     <label htmlFor="stockSendEdit"> Allow Stock Sending and Editing</label>
                 </div>
                 <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                     <input  type="checkbox" ref="stockReceive" id="stockReceive" value="1"/>
                     <label htmlFor="stockReceive"> Allow Stock Receive</label>
                 </div>
                 <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                     <input  type="checkbox" ref="stockTake" id="stockTake" value="1"/>
                     <label htmlFor="stockTake">Allow Stock Take </label>
                 </div>
              </div>
            </div>
            <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div className="checkbox">
                  <input onChange={this.handelTillTransaction} type="checkbox" ref="showTillTransactions" id="showTillTransactions" value="1"/>
                 <label htmlFor="showTillTransactions"> Show Till Transaction Options </label>
              </div>
              <div className={this.state.showTillTransactionOption ? "inner-checkbox row" : "hideDiv"}>
                <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="allVoidLines" id="allVoidLines" value="1"/>
                   <label htmlFor="allVoidLines"> Allow Void Lines</label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="unorderedVoidLines" id="unorderedVoidLines" value="1"/>
                   <label htmlFor="unorderedVoidLines"> Allow Unordered Void Lines</label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="clearing" id="clearing" value="1"/>
                   <label htmlFor="clearing"> Allow Clearing</label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="tablePlanning" id="tablePlanning" value="1"/>
                   <label htmlFor="tablePlanning">Allow Table Planning </label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="disableServiceCharges" id="disableServiceCharges" value="1"/>
                   <label htmlFor="disableServiceCharges">Allow Disabling Service Charge </label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="transactionHolding" id="transactionHolding" value="1"/>
                   <label htmlFor="transactionHolding">Allow Transaction Holding </label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="tabSpendingLimit" id="tabSpendingLimit" value="1"/>
                   <label htmlFor="tabSpendingLimit">Allow Changing Tab Spending Limit </label>
               </div>
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="tillMiscProducts" id="tillMiscProducts" value="1"/>
                   <label htmlFor="tillMiscProducts"> Allow Till Misc Products </label>
               </div>
            </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div className="checkbox">
                <input onChange={this.handelTillAdminDiscount} type="checkbox" ref="showDiscountOptions" id="showDiscountOptions" value="1"/>
                <label htmlFor="showDiscountOptions"> Show Till Discount Options </label>
              </div>
              <div className={this.state.showTillDiscountOption ? "inner-checkbox row" : "hideDiv"}>
                <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="itemDiscounts" id="itemDiscounts" value="1"/>
                   <label htmlFor="itemDiscounts"> Allow Item Discounts</label>
               </div>
               <input ref="itemDiscountLimit" className="form-control" id="itemDiscountLimit" type="text" placeholder="Item discount limit"   />
               <input ref="itemDiscountLimitPercent" className="form-control" id="itemDiscountLimitPercent" type="text" placeholder="Item discount limit %"   />
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="basketDiscounts" id="basketDiscounts" value="1"/>
                   <label htmlFor="basketDiscounts"> Allow Basket Discounts</label>
               </div>
               <input ref="basketDiscountLimit" className="form-control" id="basketDiscountLimit" type="text" placeholder="Basket discount limit"   />
               <input ref="basketDiscountLimitPercent" className="form-control" id="basketDiscountLimitPercent" type="text" placeholder="Basket discount limit %"   />
               <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="allowRefunds" id="allowRefunds" value="1"/>
                   <label htmlFor="allowRefunds">Allow Refunds </label>
               </div>
               <input ref="refundLimit" className="form-control" id="refundLimit" type="text" placeholder="Refunds limit"   />
            </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div className="checkbox">
                  <input onChange={this.handelTillCustomer} type="checkbox" ref="showTillCustomerOptions" id="showTillCustomerOptions" value="1"/>
                  <label htmlFor="showTillCustomerOptions"> Show Till Customer Options </label>
              </div>
              <div className={this.state.showTillCustomerOption ? "inner-checkbox row" : "hideDiv"}>
                <div className="checkbox checkbox-inline col-xs-6 col-sm-6 col-md-3 col-lg-3">
                   <input  type="checkbox" ref="allowManualCustomerSelection" id="allowManualCustomerSelection" value="1"/>
                   <label htmlFor="allowManualCustomerSelection"> Allow Manual Customer Selection</label>
               </div>
               <input ref="maxCreditLimit" className="form-control" id="maxCreditLimit" type="text" placeholder="Max credit limit"   />
            </div>
            </div>
          </div>
        </div>
        </form>
      <div className="buttonBtn">
      <div className="cancelBtn">
         <input className="cancel btn-danger" onClick={this.cancel} value="Back" type="button" id=""/>
      </div>
      <div className="pull-right add-otherBtn">
         <input class onClick={this.addRoles} value="Add role" type="button" id=""/>
      </div>
    </div>
    </div>

  </div>
</div>
);
}
});
let mapStateToProps = (state) => {
return {
UserData:state.User,
StaffData:state.Staff,
};
};
let mapDispatchToProps = (dispatch) => ({
StaffActions: bindActionCreators(StaffActions, dispatch),
dispatch: dispatch
});
export default connect(mapStateToProps,
mapDispatchToProps)(withRouter(AddRole));
