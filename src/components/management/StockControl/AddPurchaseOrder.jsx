import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Popover,OverlayTrigger } from 'react-bootstrap';
import * as StockActions from '../../../actions/management/stock_actions';
import { bindActionCreators } from 'redux';
const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Purchase Order">
     <p>On this page you can add new members of staff, these members of staff are listed when logging into the Till. You can also click on the 'Add Roles' button, this allows you to give certain access to staff.</p>
      <p>Choose a supplier and set the number of units to be ordered. Contact the supplier with the order details. Then click "Place Order" to add the order details to the system.</p>
    </Popover>
  );


let AddPucrchaseOrder = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
    },
    render() {
        return (
            <div>
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Add Purchase Order</h1>
                    </div>
                     <div className="filter-by">
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="filter_category">Deliver stock to:</label>
                            <select  className="form-control">
                                <option selected="selected" value="">* Select Delivery Location</option>
                                <option value="15875">Hornchurch</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="filter_category">Ordered By:</label>
                            <select  className="form-control">
                                <option value="">* Select Staff Member</option>
                                <option value="87544">Andreea</option>
                                <option value="87540">Bianca T</option>
                                <option value="87546">Diana Bigu</option>
                                <option value="87548">Diana Burtea</option>
                                <option value="92084">Ioana Balan </option>
                                <option value="87545">Koe</option>
                                <option value="87543">Raluca</option>
                                <option value="85962">Sam A</option>
                                <option value="87547">Shakeel</option>
                                <option value="87550">Zafar</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Search Suppliers by Name</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="margt20 form-group col-xs-12 col-sm-12 col-md-2 col-lg-2">
                              <input className="add-other" value="Search" type="button" id=""/>
                        </div>
                        <div className="filter-by">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div className="checkbox checkbox-inline">
                                    <input type="checkbox" ref="ShowPriceDetails" id="ShowPriceDetails"/>
                                    <label htmlFor="ShowPriceDetails"> Show price details </label>
                                </div>
                                <div className="checkbox checkbox-inline">
                                    <input type="checkbox" ref="ShowSupplierDetails" id="ShowSupplierDetails"/>
                                    <label htmlFor="ShowSupplierDetails"> Show Supplier details </label>
                                </div>
                               <div className="checkbox checkbox-inline">
                                   <input type="checkbox" ref="ShowTillDetails" id="ShowTillDetails"/>
                                   <label htmlFor="ShowTillDetails"> Show Till details </label>
                                </div>
                            </div>
                        </div>
                        
                     </div>

                    
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
        StockData:state.Stock,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(StockActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddPucrchaseOrder));
