import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as StockActions from '../../../actions/management/stock_actions';
import { bindActionCreators } from 'redux';


let AddStockSuppliers = React.createClass({
    getInitialState() {
        return {
             addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        if(this.state.addClicked){
        alert(nextProps.StockData.SupplierAdder.message);
        this.setState({addClicked:false});
        if(nextProps.StockData.SupplierAdder.status_code === 200){
            this.props.router.push('/management/StockControl/suppliers');   
        }
        
      }
    },
    componentDidMount() {

    },

    AddStockSuppliers(){
         
        let data = {};
      
        data.name = this.refs.stock_name.value;
        data.description = this.refs.stock_description.value;
        data.type = this.refs.type.value;
        data.address1  = this.refs.address1.value;
        data.address2 = this.refs.address2.value;
        data.town = this.refs.town.value;
        data.country  = this.refs.county.value; 
        data.post_code = this.refs.postCode.value;
        data.count_num  = this.refs.contactNumber1.value;
        data.count_num2 = this.refs.contactNumber2.value;
        data.email = this.refs.email.value;
        debugger;
        this.props.dispatch(StockActions.addSupplierData(data));
        this.setState({addClicked:true});
    }, 

    back(event) {
        this.props.router.push('/management/StockControl/suppliers');
    },
    render() {
        return (
            <div class>
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Add a Supplier</h1>
                    </div>
                    <div className="parainfo">
                        <p>On this page you can add new suppliers, when you create a new product it will be selectable under the supplier drop down box.</p>
                    </div>
                    <div className="filter-by">
                        <h3>Supplier</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="stock_name">Name:</label>
                            <input ref="stock_name" className="form-control" id="stock_name"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="stock_description">Description</label>
                            <input ref="stock_description" className="form-control" id="stock_description"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="type">Type:</label>
                            <input ref="type" className="form-control" id="type"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="address1">Address Line 1:</label>
                            <input ref="address1" className="form-control" id="address1"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="address2">Address Line 2:</label>
                            <input ref="address2" className="form-control" id="address2"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="town">Town:</label>
                            <input ref="town" className="form-control" id="town"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="county">County:</label>
                            <input ref="county" className="form-control" id="county"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="postCode">Postcode</label>
                            <input ref="postCode" className="form-control" id="postCode"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="contactNumber1">Contact Number:</label>
                            <input ref="contactNumber1" className="form-control" id="contactNumber1"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="contactNumber2">Contact Number 2:</label>
                            <input ref="contactNumber2" className="form-control" id="contactNumber2"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="email">Email Address:</label>
                            <input ref="email" className="form-control" id="email"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="buttonBtn">
                            <div className="cancelBtn">
                               <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                            </div>
                            <div className="pull-right add-otherBtn">
                               <input class onClick={this.AddStockSuppliers} value="Add" type="button" id=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
        StockData:state.Stock,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(StockActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddStockSuppliers));
