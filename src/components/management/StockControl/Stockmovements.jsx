import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as StockActions from '../../../actions/management/stock_actions';
import { bindActionCreators } from 'redux';
import csv from '../../../img/csvicons.png';
import qq from '../../../img/question-mark.png';
import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Stock Movement">
         <p>This page contains your stock movements. Use the calendars and the dropdowns to filter the movements. You can select a stock movement to view more details by clicking the 'View Details' button or by scanning the barcode</p>
    </Popover>
  );

let StockMovements = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    StockMoovmnt(){
        this.props.router.push('/management/StockControl/Stockmovements/Add-StockMovement');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    render() {
        return (

            <div class>
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                         <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>

                        <h1 className="add-title">Stock Movement <img src = {qq} alt="qus mark"/></h1>
                      </OverlayTrigger>
                    </div>
                    <div className="">
                   
                        <input type="button" onClick={this.StockMoovmnt} className="add-btn pull-right" value="Add Stock" />
                    </div>
                    <div className="filter-by">
                      <div className="col-sm-3">
                          <span className="help-block txtalignRight">Scan Order Barcode:</span>
                      </div>
                      <div className="col-sm-8">
                        <input ref="Scan_order" className="form-control input-text-fields" id="role_discription"  type="text"/>
                        <input className="add-other" value="Search" type="button" id=""/>
                      </div>
                    </div>
                    
                    </div>


                    
                </div>
           
        );
    }
});

let mapStateToProps = (state) => {
    return {
        UserDetail:state.User,
        StockData:state.Stock,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(StockActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(StockMovements));
