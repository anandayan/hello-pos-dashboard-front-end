import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as StockActions from '../../../actions/management/stock_actions';
import { bindActionCreators } from 'redux';
import qq from '../../../img/question-mark.png';

import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Purchase Orders">
         <p>This page shows your Purchase Orders. Use the calendars to select a from and to date and the dropdowns to filter the products. You can select a Purchase Order to view more details by clicking the 'View Details' button or by scanning the barcode.</p>
    </Popover>
  );

let StockPucrchaseOrder = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    AddStockPurchse(){
        this.props.router.push('/management/StockControl/purchaseorder/Add-Purchaseorder');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    render() {
        return (

            <div className="">
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>

                        <h1 className="title">Purchase Orders <img src = {qq} alt="qus mark"/></h1>
                        </OverlayTrigger>
                        <input type="button" onClick={this.AddStockPurchse} className="add-btn pull-right" value="Add Stock Purchase" />
                    </div>
                    <div className="filter-by">
                      
                      <div className="col-sm-3">
                        <label>Scan Order Barcode:</label>
                        <select  className="form-control">
                                <option selected="selected" value="">All Locations</option>
                                <option value="15875">Hornchurch</option>
                        </select>
                      </div>
                      
                      <div className="col-sm-3">
                      <label>Scan Order Barcode:</label>
                        <select  className="form-control">
                            <option value="All Statuses">All Statuses</option>
                            <option value="All Statuses (Includes Drafts)">All Statuses (Includes Drafts)</option>
                            <option value="All Received (Includes Partials)">All Received (Includes Partials)</option>
                            <option value="Received">Received</option>
                            <option value="Received (Partial)">Received (Partial)</option>
                            <option value="Incomplete">Incomplete</option>
                            <option selected="selected" value="Ordered">Ordered</option>
                            <option value="All Cancelled (Includes Drafts)">All Cancelled (Includes Drafts)</option>
                            <option value="Cancelled">Cancelled</option>
                            <option value="Cancelled (Draft)">Cancelled (Draft)</option>
                            <option value="Draft">Draft</option>
                           </select>
                      </div>
                      <div className="col-sm-4">
                        <label>Scan Order Barcode:</label>
                        <input ref="role_discription" className="form-control" id="role_discription"  type="text"   autoComplete="off"/>
                      </div>
                      <div className="col-sm-2 margt25">
                        <input className="add-other" value="Search" type="button" id=""/>
                      </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
     UserDetail:state.User,
        StockData:state.Stock,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(StockActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(StockPucrchaseOrder));
