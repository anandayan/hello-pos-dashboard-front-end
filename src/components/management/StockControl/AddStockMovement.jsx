import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as StockActions from '../../../actions/management/stock_actions';
import { bindActionCreators } from 'redux';


let AddStockMovmnt = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
    },
    render() {
        return (
            <div class>
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Stock Movement Add</h1>
                    </div>
                    <div className="parainfo">
                        <p>The Stock Movement Add page can be used to add stock to a location, either from another location, or just adding new stock. This should be used each time you receive a delivery.</p>
                        <p>To import a product: select the locations, scan or lookup an item, set the quantity to be imported, then click "Insert".</p>
                        <p>Click "Add to Stock" when all delivered items have been added.</p>
                    </div>
                    <div className="filter-by">
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="name">Deliver stock from:</label>
                            <select  ref="staff_location" id="staff_location" className="form-control">
                               <option selected="selected" value="home">Hornchurch</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="name">Deliver stock to:</label>
                            <select  ref="staff_location" id="staff_location" className="form-control">
                               <option selected="selected" value="home">Hornchurch</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="name">Staff member:</label>
                            <select  ref="staff_location" id="staff_location" className="form-control">
                               <option selected="selected" value="">* Select Staff</option>
                                <option value="87544">Andreea</option>
                                <option value="87540">Bianca T</option>
                                <option value="87546">Diana Bigu</option>
                                <option value="87548">Diana Burtea</option>
                                <option value="92084">Ioana Balan </option>
                                <option value="87545">Koe</option>
                                <option value="87543">Raluca</option>
                                <option value="85962">Sam A</option>
                                <option value="87547">Shakeel</option>
                                <option value="87550">Zafar</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="name">Reason:</label>
                            <select  ref="staff_location" id="staff_location" className="form-control">
                               <option selected="selected" value="">* Select Reason</option>
                                <option value="100963">Excess Stock</option>
                                <option value="100959">External Branch Movement</option>
                                <option value="100960">Internal Movement</option>
                                <option value="100966">Missing Stock</option>
                                <option value="100962">New Stock</option>
                                <option value="100961">Returns</option>
                                <option value="100965">Shrinkage</option>
                                <option value="100967">Stock Take</option>
                                <option value="100964">Wastage</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <label htmlFor="filter_categorye">Search for an item using the search box below, enter a quantity and press "Insert". Once you have done that, click "Add to Stock".Product barcode or item search:</label>
                            <input ref="filter_categorye" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="buttonBtn">
                            <div className="pull-right add-otherBtn">
                               <input class  value="Insert" type="button" id=""/>
                            </div>
                          </div>
                    </div>
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Unit cost price</th>
                                    <th>Quantity to add</th>
                                    <th>Total cost price</th>
                                    <th>Remove</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div className="buttonBtn">
                      <div className="cancelBtn">
                         <input className="cancel btn-warning" onClick={this.stockReport} value="Stock report" type="button" id=""/>
                      </div>
                      <div className="pull-right add-otherBtn">
                         <input class onClick={this.addRoles} value="Add to stock" type="button" id=""/>
                      </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
        StockData:state.Stock,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(StockActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddStockMovmnt));
