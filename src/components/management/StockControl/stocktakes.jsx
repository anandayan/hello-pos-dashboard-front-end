import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import qq from '../../../img/question-mark.png';
import * as StockActions from '../../../actions/management/stock_actions';
import { bindActionCreators } from 'redux';
import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Stock Takes">
         <p>This page contains your stock takes. Use the calendars and the location dropdown to filter them. You can select a stock take to view more details by clicking the 'View Details' button or by scanning the barcode.</p>
    </Popover>
  );

let StockTakes = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },

    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    render() {
        return (

            <div class>
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>

                        <h1 className="title">Stock Takes <img src = {qq} alt="qus mark"/></h1>
                        </OverlayTrigger>

                    </div>
                    <div className="filter-by">
                        <h3>Add New Address</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_categorye">Filter by Location:</label>
                            <input ref="filter_categorye" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Scan Order Barcode:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 margt25">
                            <input className="add-other pull-left" value="Search" type="button" id=""/>
                        </div>
                    </div>

                    
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
        StockData:state.Stock,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(StockActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(StockTakes));
