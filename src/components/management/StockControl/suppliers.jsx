import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as StockActions from '../../../actions/management/stock_actions';
import { bindActionCreators } from 'redux';


let StockSupplier = React.createClass({
    getInitialState() {
        return {
             SupplierListRquested:false,deleteClicked:false,editClicked:false,SupplierListData:null,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        let nextSupply = nextProps.StockData.GetSupplier;
            if(this.state.SupplierListRquested && nextSupply){
                if(nextSupply.status_code === 200){
                    this.setState({SupplierListData:nextSupply.data,SupplierListRquested:false})
                }
            }

            let nextSupplyDelete = nextProps.StockData.DeleteSupplier;
            if(this.state.deleteClicked && nextSupplyDelete){
                if(nextSupplyDelete.status_code === 200){
                  this.getSupplierList();
                  this.setState({deleteClicked:false});
                }
            }
    },
    componentDidMount() {
        this.getSupplierList();
    },

    getSupplierList(){
      this.props.dispatch(StockActions.getListSupplierData());
      this.setState({SupplierListRquested:true})
    },

    AddStockSupplier(){
        this.props.router.push('/management/StockControl/suppliers/add-supplier');
    },
  

   deleteSupply(event){
      let data = {};    
      data.id = event.target.dataset.id;
      this.setState({deleteClicked:true});
      this.props.dispatch(StockActions.deleteSupplier(data));
    },

   editsupply(event){
      let data = {};
      let id = event.target.dataset.id;
      let SupplierListData = this.props.StockData.GetSupplier.data;
      
      data = _.filter(SupplierListData, function (i) {
      return i._id == id ;
      });

      //this.props.ProductData.ProductById = data;
        this.props.dispatch(StockActions.SupplierById(data[0]));

       this.props.router.push('/management/StockControl/edit-supplier');
      //this.setState({editClicked:true})
},
   generateSupplierList(){
        let SupplierList = this.state.SupplierListData;
        let SupplierTemplate = SupplierList ?  SupplierList.map(function (listItem, key) {
        return (<tr key={key}> <td>{listItem.name}</td> 
            <td>{listItem.description}</td>
            <td>{listItem.type}</td>
            <td>{listItem.address1}</td>
            <td>{listItem.address2}</td>
            <td>{listItem.town}</td>
            <td>{listItem.county}</td>
            <td>{listItem.post_code}</td>
            <td>{listItem.count_num }</td>
            <td>{listItem.count_num2}</td>
            <td>{listItem.email}</td>
            <td><span data-id={listItem._id} onClick={this.editsupply} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteSupply}></span></td></tr>)
        }, this):("No Data Found");
        return SupplierTemplate ;
    },
    render() {
        let list = this.generateSupplierList();
        return (

            <div class>
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                        <h1 className="title">Suppliers List</h1>
                        <input type="button" onClick={this.AddStockSupplier} className="add-btn pull-right" value="Add Stock Supplier" />
                    </div>

                    <div className="filter-by">
                        <div className="col-sm-4">
                            <span className="help-block txtalignRight">Filter by Description, Type or Address:</span>
                        </div>
                        <div className="col-sm-8">
                            <input ref="role_discription" className="form-control input-text-fields" id="role_discription"  type="text"   autoComplete="off"/>
                            <input className="add-other" value="Search" type="button" id=""/>
                        </div>
                    </div>
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Discription</th>
                                    <th>Type</th>
                                    <th>Address 1</th>
                                    <th>Address 2</th>
                                    <th>Town</th>
                                    <th>County</th>
                                    <th>Post code</th>
                                    <th>Contact number</th>
                                    <th>Contact number 2</th>
                                    <th>Email Address</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                                <tbody>
                                {list}
                                </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
        StockData:state.Stock,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(StockActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(StockSupplier));
