import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';


let AddPop = React.createClass({
    getInitialState() {
        return {
             addPopupClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        if(this.state.addPopupClicked){
            alert(nextProps.ProductData.PopUpBrandAdded.message)
        this.setState({addPopupClicked:false});
        if(nextProps.ProductData.PopUpBrandAdded.status_code === 200){
            this.props.router.push('/management/products/popupalert');

        }
      }
    },
    componentDidMount() {

    },

    addPopup(){
        
        let data = {};
      data.name = this.refs.popup_name.value;
      data.msg  = this.refs.pop_message.value;
      data.show_per_trans  = this.refs.showOncePerTransaction.checked ? 1 : 0;
      this.props.dispatch(ProductActions.addPopUpNoteData(data));
      this.setState({addPopupClicked:true})
      
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
    },
    back(event) {
        this.props.router.push('/management/products/popupalert');
    },
    render() {
        return (
            <div class>
         
            <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                <div className="upgrate license-link">
                    <h1 className="add-title">Popup Notes</h1>
                    
                </div>
                <div className="parainfo">
                    <p>"Name" is used to identify this popup note when using the Back Office.</p>
                    <p>"Message" will be displayed to the user when the product is added to the current transaction on the Till.</p>
                </div>
                <div className="filter-by">
                    <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label htmlFor="popup_name">Name:</label>
                        <input ref="popup_name" className="form-control"   type="text"   autoComplete="off"/>
                    </div>
                    <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label htmlFor="pop_message">Message:</label>
                        <input ref="pop_message" className="form-control"   type="text"   autoComplete="off"/>
                    </div>
                    <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div className="checkbox checkbox-inline">
                            <input type="checkbox" ref="showOncePerTransaction" id="showOncePerTransaction" value="1"/>
                            <label htmlFor="showOncePerTransaction"> Show Once Per Transaction:</label>
                        </div>
                    </div>
                </div>

                <div className="buttonBtn">
                      <div className="cancelBtn">
                         <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                      </div>
                      <div className="pull-right add-otherBtn">
                         <input class onClick={this.addPopup}  value="Add" type="button" id=""/>
                      </div>
                </div>
              </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
     UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddPop));
