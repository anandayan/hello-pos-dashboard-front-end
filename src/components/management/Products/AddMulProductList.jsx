import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';


let AddMulProduct = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
    },
    render() {
        return (
            <div class>
           
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        <h1 className="add-title">Add a Multiple Choice Product Group</h1>
                        
                    </div>
                    {/*<div className="parainfo">
                        <p>On this page you can add a new multiple choice product group, enter a name to identify this multiple choice product group and/or a description. To add products to the group, search for and find them in the drop down list and click the '▼' button. To remove them click the 'X' button.</p>
                        <p>Once you are finished adding products, click the "Add" button, or "Add Another" to save the current multiple choice product group and begin another.</p>
                    </div> */}
                        
                    <div className="filter-by-brand">
                        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                           <label htmlFor="filter_category">Filter by Category:</label>
                           <select  id="" className="form-control">
                                <option value="">* All Categories</option>
                                <option value="-1">* Top Category</option>
                                <option value="891353">Beverages</option>
                                <option value="898558">&nbsp;&nbsp;&nbsp;&nbsp;Cold Beverages</option>
                                <option value="897206">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bottles </option>
                                <option value="897242">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bottles | 1.5</option>
                                <option value="897204">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Freshly Squeezed Juice</option>
                                <option value="897203">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fruit Smoothies</option>
                                <option value="897202">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Milkshakes</option>
                                <option value="897241">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Premium | C.P Can 330ml</option>
                                <option value="897090">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Soft Drinks | Large</option>
                                <option value="897091">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Soft Drinks | Regular</option>
                                <option value="897231">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Soft Drinks | Small</option>
                                <option value="897205">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Veggie Smoothies</option>
                                <option value="891656">&nbsp;&nbsp;&nbsp;&nbsp;Hot Beverages</option>
                                <option value="897212">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Coffee</option>
                                <option value="897211">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hot Chocolate</option>
                                <option value="897210">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tea</option>
                                <option value="897079">Bulk products</option>
                                <option value="897081">&nbsp;&nbsp;&nbsp;&nbsp;BP- Beverages</option>
                                <option value="891352">Burgers</option>
                                <option value="897197">&nbsp;&nbsp;&nbsp;&nbsp;Burgers Double</option>
                                <option value="897196">&nbsp;&nbsp;&nbsp;&nbsp;Burgers Single</option>
                                <option value="897360">&nbsp;&nbsp;&nbsp;&nbsp;Gourment Burgers Double</option>
                                <option value="897358">&nbsp;&nbsp;&nbsp;&nbsp;Gourment Burgers Single</option>
                                <option value="892662">Dessert</option>
                                <option value="897218">&nbsp;&nbsp;&nbsp;&nbsp;Cakes</option>
                                <option value="915544">&nbsp;&nbsp;&nbsp;&nbsp;Cho co Late | sticks</option>
                                <option value="897220">&nbsp;&nbsp;&nbsp;&nbsp;Cookies</option>
                                <option value="897219">&nbsp;&nbsp;&nbsp;&nbsp;Ice Cream &amp; Sorbets</option>
                                <option value="897221">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 Scoop</option>
                                <option value="897222">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 Scoop</option>
                                <option value="897223">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3 Scoop</option>
                                <option value="897121">Dips</option>
                                <option value="902637">&nbsp;&nbsp;&nbsp;&nbsp;Dips</option>
                                <option value="898591">&nbsp;&nbsp;&nbsp;&nbsp;Pots | </option>
                                <option value="891647">Meals</option>
                                <option value="909513">Online orders | </option>
                                <option value="917546">Order Type</option>
                                <option value="891385">Peri Peri</option>
                                <option value="897137">&nbsp;&nbsp;&nbsp;&nbsp;Chicken </option>
                                <option value="897134">&nbsp;&nbsp;&nbsp;&nbsp;Chicken Strips</option>
                                <option value="897135">&nbsp;&nbsp;&nbsp;&nbsp;Chicken Wings</option>
                                <option value="891354">Pitta</option>
                                <option value="897130">&nbsp;&nbsp;&nbsp;&nbsp;Pittas | Double</option>
                                <option value="897128">&nbsp;&nbsp;&nbsp;&nbsp;Pittas | Single </option>
                                <option value="891657">Platters</option>
                                <option value="898658">&nbsp;&nbsp;&nbsp;&nbsp;Mega Platter</option>
                                <option value="898652">&nbsp;&nbsp;&nbsp;&nbsp;Mixed Platter </option>
                                <option value="898653">&nbsp;&nbsp;&nbsp;&nbsp;Skewer Platter </option>
                                <option value="898650">&nbsp;&nbsp;&nbsp;&nbsp;Whole Platter </option>
                                <option value="898651">&nbsp;&nbsp;&nbsp;&nbsp;Wings  Platter</option>
                                <option value="891646">Salads</option>
                                <option value="891648">Sides</option>
                                <option value="898592">&nbsp;&nbsp;&nbsp;&nbsp;Premium Sides</option>
                                <option value="898593">&nbsp;&nbsp;&nbsp;&nbsp;Regular Sides</option>
                                <option value="896397">Signature Flavours</option>
                                <option value="898573">&nbsp;&nbsp;&nbsp;&nbsp;Authentic Flavours</option>
                                <option value="898575">&nbsp;&nbsp;&nbsp;&nbsp;Fillings</option>
                                <option value="898629">&nbsp;&nbsp;&nbsp;&nbsp;Gourmet Fillings |</option>
                                <option value="898580">&nbsp;&nbsp;&nbsp;&nbsp;Peri Peri Flavours</option>
                                <option value="898574">&nbsp;&nbsp;&nbsp;&nbsp;Seasoning</option>
                                <option value="898590">&nbsp;&nbsp;&nbsp;&nbsp;Souce | Filling</option>
                                <option value="897259">&nbsp;&nbsp;&nbsp;&nbsp;Topping </option>
                                <option value="891645">Skewers</option>
                                <option value="901197">&nbsp;&nbsp;&nbsp;&nbsp;Skewers | Mains</option>
                                <option value="892660">The Little One Meal</option>
                                <option value="897227">&nbsp;&nbsp;&nbsp;&nbsp;Pittas</option>
                                <option value="897226">&nbsp;&nbsp;&nbsp;&nbsp;Skewers</option>
                                <option value="897228">&nbsp;&nbsp;&nbsp;&nbsp;Wraps</option>
                                <option value="891641">Wraps</option>
                                <option value="897159">&nbsp;&nbsp;&nbsp;&nbsp;Wraps | Large </option>
                                <option value="897158">&nbsp;&nbsp;&nbsp;&nbsp;Wraps | Regular</option>
                            </select>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label htmlFor="">Search by Name:</label>
                          <input ref="role_discription" className="form-control" id="role_discription"  type="text"   autoComplete="off"/>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 margt25">
                            <input className="add-other" value="Search" type="button" id=""/>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 margt25">
                           <select  id="" className="form-control">
                                <option value="11965430">1. Whole Chicken</option>
                                <option value="11928748">1/2 | Request</option>
                                <option value="11928763">12 Wings | Platter</option>
                                <option value="11928780">16 Wings | Platter</option>
                                <option value="12287956">Acai Burst | Smoothie</option>
                                <option value="11928138">Americano | Coffee</option>
                                <option value="12333634">Apple | Folkingtons</option>
                                <option value="11865214">Appletiser | Bottle </option>
                                <option value="11892046">Banana | Milkshake</option>
                                <option value="11891215">Barbeque |</option>
                                <option value="11928383">BBQ |</option>
                                <option value="11891996">Beef | Salad</option>
                                <option value="11771081">Beef Skewers</option>
                                <option value="11948595">Beef Skewers | LO</option>
                                <option value="11928711">Beef Tomato |</option>
                                <option value="11892059">Beetroot | Smoothie</option>
                                <option value="12333624">Berries | Folkingtons</option>
                                <option value="11892058">Berry Burst | Smoothie</option>
                                <option value="12287767">Blackcurrent | Milkshake</option>
                                <option value="11791702">Bottle of water</option>
                                <option value="12285723">Bun | Extras</option>
                                <option value="11928386">Burger |</option>
                                <option value="11891211">Burger Souce |</option>
                                <option value="11860681">Butterfly Chicken | Breast</option>
                                <option value="11928155">Caffe Latte | Coffee</option>
                                <option value="11928136">Cappucino | Coffee</option>
                                <option value="11927014">Capri Sun Orange </option>
                                <option value="11892130">Caramelita | Ice Cream</option>
                                <option value="11892118">Carrot | Cake</option>
                                <option value="11892062">Carrot Boost | Smoothie</option>
                                <option value="11890800">Cawston Press | Apple</option>
                                <option value="11892209">Cawston Press | Apple</option>
                                <option value="11892213">Cawston Press | Ginger Beer</option>
                                <option value="11892216">Cawston Press | Rhubab</option>
                                <option value="11928352">Cheese &amp; Mayo |</option>
                                <option value="11928350">Cheese |</option>
                                <option value="11860686">Chicken  | 1/2</option>
                                <option value="12110769">Chicken  | 1/4 | Leg Pcs</option>
                                <option value="11891994">Chicken | Salad</option>
                                <option value="11860684">Chicken 1/4 | Breast Pcs</option>
                                <option value="11891978">Chicken Breast | Double</option>
                                <option value="11891932">Chicken Breast | Single</option>
                                <option value="11891724">Chicken Pitta | Double</option>
                                <option value="11948549">Chicken Pitta | LO</option>
                                <option value="11891235">Chicken Pitta | Single</option>
                                <option value="11771089">Chicken Strips | 4PCS</option>
                                <option value="11860671">Chicken Strips | 6PCS</option>
                                <option value="11891980">Chicken Tikka | Double</option>
                                <option value="11891933">Chicken Tikka | Single</option>
                                <option value="11891666">Chicken Tikka Pitta | Double</option>
                                <option value="11948540">Chicken Tikka Pitta | LO</option>
                                <option value="11891257">Chicken Tikka Pitta | Single</option>
                                <option value="11891748">Chicken Tikka Wrap | Large</option>
                                <option value="11948628">Chicken Tikka Wrap | LO</option>
                                <option value="11891774">Chicken Tikka Wrap | Regular</option>
                                <option value="11948031">Chicken Wings | 12PCS</option>
                                <option value="11860672">Chicken Wings | 3PCS</option>
                                <option value="11860679">Chicken Wings | 6PCS</option>
                                <option value="11948330">Chicken Wings | 9PCS</option>
                                <option value="11891739">Chicken Wrap | Large</option>
                                <option value="11948563">Chicken Wrap | LO</option>
                                <option value="11891767">Chicken Wrap | Regular</option>
                                <option value="12287763">Choc Orange | Milkshake</option>
                                <option value="12287699">Choc-o-Lait | Coconut</option>
                                <option value="12287715">Choc-o-Lait | Contreau</option>
                                <option value="12287688">Choc-o-Lait | Dark</option>
                                <option value="12287711">Choc-o-Lait | Hazelnut</option>
                                <option value="12287674">Choc-o-Lait | Milk</option>
                                <option value="11892117">Chocolate | Gateau</option>
                                <option value="11892048">Chocolate Chip | Milkshake</option>
                                <option value="11891053">Classic Mayonnaise |</option>
                                <option value="11928356">Classic Mayonnaise |</option>
                                <option value="11928388">Classic Mayonnaise |</option>
                                <option value="11770806">Classic Meal</option>
                                <option value="11892121">Clowing Around | Cookie</option>
                                <option value="12287756">Coconut | Milkshake</option>
                                <option value="11892049">Coconut | Milkshake</option>
                                <option value="11892057">Coconut Crush | Smoothie</option>
                                <option value="11777111">Coleslaw</option>
                                <option value="11892045">Cookies &amp; Cream | Milkshake</option>
                                <option value="11777120">Corn on the cob (Plain)</option>
                                <option value="11777121">Corn on the cob (Seasoned)</option>
                                <option value="11860690">Diet Pepsi (Large)</option>
                                <option value="11791656">Diet Pepsi (Regular)</option>
                                <option value="11892191">Diet Pepsi (Small)</option>
                                <option value="11865945">Diet Pepsi | Bottle 1.5L</option>
                                <option value="11892123">Dotty | Cookie</option>
                                <option value="11892139">Double | Scoop</option>
                                <option value="12333645">Elder Flower | Folkingtons</option>
                                <option value="11928146">Espresso | Coffee</option>
                                <option value="11860495">Extra Hot</option>
                                <option value="11891214">Extra Hot Chilli |</option>
                                <option value="11928393">Extra Hot Chilli |</option>
                                <option value="11891982">Falafel | Double</option>
                                <option value="11892002">Falafel | Salad</option>
                                <option value="11891934">Falafel | Single</option>
                                <option value="11891284">Falafel Pitta | Double</option>
                                <option value="11948527">Falafel Pitta | LO</option>
                                <option value="11891267">Falafel Pitta | Single</option>
                                <option value="12322654">Falafel Skewer</option>
                                <option value="11891764">Falafel Wrap | Large</option>
                                <option value="11948581">Falafel Wrap | LO</option>
                                <option value="11891785">Falafel Wrap | Regular</option>
                                <option value="11928159">Flat White | Coffee</option>
                                <option value="11865953">Freshly Squeezed | Juice </option>
                                <option value="11777128">Fries (Extra seasoned)</option>
                                <option value="11768724">Fries (Plain)</option>
                                <option value="11768734">Fries (seasoned)</option>
                                <option value="11891210">Garlic Mayonnaise |</option>
                                <option value="11928395">Garlic Mayonnaise |</option>
                                <option value="11928718">Gherkins |</option>
                                <option value="11891988">Gourmet Beef | Double</option>
                                <option value="11891984">Gourmet Beef | Single</option>
                                <option value="11891290">Gourmet Beef Pitta | Double</option>
                                <option value="11948538">Gourmet Beef Pitta | LO</option>
                                <option value="11891263">Gourmet Beef Pitta | Single</option>
                                <option value="11891758">Gourmet Beef Wrap | Large</option>
                                <option value="11948577">Gourmet Beef Wrap | LO</option>
                                <option value="11891780">Gourmet Beef Wrap | Regular</option>
                                <option value="11892065">Green Riviver | Smoothie</option>
                                <option value="11948525">Grill Halloumi &amp; Veg | L.O</option>
                                <option value="11948560">Grill Halloumi &amp; Veg |LO </option>
                                <option value="11891272">Grill Halloumi &amp; Veg Pitta |</option>
                                <option value="11891283">Grill Halloumi &amp; Veg Pitta |</option>
                                <option value="11891788">Grill Halloumi &amp; Veg Wrap |</option>
                                <option value="11891765">Grill Halloumi &amp; Veg Wrap |</option>
                                <option value="11777137">Grilled Veg</option>
                                <option value="11777139">Grilled Veg &amp; Halloumi</option>
                                <option value="11928720">Guacamole |</option>
                                <option value="11777105">Halloumi (cheese)</option>
                                <option value="11892003">Halloumi | Salad</option>
                                <option value="11928212">Halloumi Cheese | Topping</option>
                                <option value="11860492">Hot</option>
                                <option value="11928169">Hot Chocolate | </option>
                                <option value="11928438">Hummaz | Pot</option>
                                <option value="11934949">icecream | 3 Scoop</option>
                                <option value="11928717">Jalapenos |</option>
                                <option value="12127207">Just Eat Order | </option>
                                <option value="11891216">Ketchup |</option>
                                <option value="11891998">Lamb | Salad</option>
                                <option value="11928214">Large Mushroom | Topping</option>
                                <option value="11928273">Lebanese | </option>
                                <option value="11891721">Lebanese Pitta | Double</option>
                                <option value="11948547">Lebanese Pitta | LO</option>
                                <option value="11891240">Lebanese Pitta | Single</option>
                                <option value="11771078">Lebanese Skewer</option>
                                <option value="11948591">Lebanese Skewer | LO</option>
                                <option value="11891740">Lebanese Wrap | Large</option>
                                <option value="11948566">Lebanese Wrap | LO</option>
                                <option value="11891768">Lebanese Wrap | Regular</option>
                                <option value="11860487">Lemon &amp; Herb</option>
                                <option value="11892228">Lemon Lime | Sorbet</option>
                                <option value="11892029">Lemonade | 330ml</option>
                                <option value="11928359">Lettuce |</option>
                                <option value="11928316">Lightly Seasoned |</option>
                                <option value="11892227">Mango | Sorbet</option>
                                <option value="12333605">Mango Juice | Folkingtons</option>
                                <option value="11777135">Mash Potato </option>
                                <option value="11860490">Medium</option>
                                <option value="11892016">Mega | Platter</option>
                                <option value="11928277">Mexican |</option>
                                <option value="11891708">Mexican Pitta | Double</option>
                                <option value="11948544">Mexican Pitta | LO</option>
                                <option value="11891241">Mexican Pitta | Single</option>
                                <option value="11771077">Mexican Skewer</option>
                                <option value="11948588">Mexican Skewer | LO</option>
                                <option value="11891743">Mexican Wrap | Large</option>
                                <option value="11948570">Mexican Wrap | LO</option>
                                <option value="11891771">Mexican Wrap | Regular</option>
                                <option value="11892131">Mint Chocolate | Ice Cream</option>
                                <option value="11892012">Mixed | Platter</option>
                                <option value="11928710">Mixed Leaves |</option>
                                <option value="11777112">Mixed Salad</option>
                                <option value="11777116">Mixed Salad Feta Cheese</option>
                                <option value="11928141">Moccachino | Coffee</option>
                                <option value="11928285">Moroccan |</option>
                                <option value="11891687">Moroccan Pitta | Double</option>
                                <option value="11948543">Moroccan Pitta | LO</option>
                                <option value="11891245">Moroccan Pitta | Single</option>
                                <option value="11891747">Moroccan Wrap | Large</option>
                                <option value="11948572">Moroccan Wrap | LO</option>
                                <option value="11891772">Moroccan Wrap | Regular</option>
                                <option value="11892219">MP - Cawston Press | Ginger </option>
                                <option value="11892220">MP - Cawston Press | Rhubab </option>
                                <option value="11890805">MP - Cawston Press |apple</option>
                                <option value="12332859">MP - Folkingtons | Apple</option>
                                <option value="12332894">MP - Folkingtons | Berries</option>
                                <option value="12332903">MP - Folkingtons | E Flower</option>
                                <option value="12332818">MP - Folkingtons | Mango</option>
                                <option value="12332869">MP - Folkingtons | Pear</option>
                                <option value="12332783">MP - Folkingtons | Pink Lemo</option>
                                <option value="11937445">MP- chicken wings</option>
                                <option value="11937356">MP- whole chicken</option>
                                <option value="11928366">No Fillings |</option>
                                <option value="11777109">Now Rice</option>
                                <option value="11777142">Olives (Side)</option>
                                <option value="11928360">Onion |</option>
                                <option value="11928712">Onion Rings |</option>
                                <option value="12127872">Online Order | </option>
                                <option value="12333630">Pear | Folkingtons</option>
                                <option value="11860688">Pepsi (Large)</option>
                                <option value="11791549">Pepsi (Regular)</option>
                                <option value="11892192">Pepsi (Small)</option>
                                <option value="11865944">Pepsi | Bottle 1.5L</option>
                                <option value="11948584">Peri Chicken Skewer | LO</option>
                                <option value="11928398">Peri Chilli |</option>
                                <option value="11928401">Peri Mayonnaise |</option>
                                <option value="11771075">Peri Peri Chicken Skewer</option>
                                <option value="11891213">Peri Peri Chilli |</option>
                                <option value="11891206">Peri Peri Mayonnaise |</option>
                                <option value="11771080">Peri Peri Prawn Skewer</option>
                                <option value="11928321">Peri Peri Salt |</option>
                                <option value="11928709">Pickles |</option>
                                <option value="11928209">Pineapple Slice | Topping</option>
                                <option value="11892056">Pineapple Sunset | Smoothie</option>
                                <option value="12332742">Pink Lemonade | Folkingtons</option>
                                <option value="11892134">Pistachio | Ice Cream</option>
                                <option value="11928445">Pitta | Extras</option>
                                <option value="11860497">Plain </option>
                                <option value="11928708">Plain |</option>
                                <option value="11892231">Pomegranate | Sorbet</option>
                                <option value="11928719">Portabelo Mushroom |</option>
                                <option value="11927002">Posh Hot One Double | Burger</option>
                                <option value="11926995">Posh Hot One Single | Burger</option>
                                <option value="11892001">Prawn | Salad</option>
                                <option value="11948592">Prawn Skewer | LO</option>
                                <option value="11891647">Prawns Pitta | Double</option>
                                <option value="11948539">Prawns Pitta | LO</option>
                                <option value="11891261">Prawns Pitta | Single</option>
                                <option value="11891757">Prawns Wrap | Large</option>
                                <option value="11948573">Prawns Wrap | LO</option>
                                <option value="11891777">Prawns Wrap | Regular</option>
                                <option value="11860665">Premium Meal</option>
                                <option value="11892053">Rasberry Heaven | Smoothie</option>
                                <option value="12287771">Raspberry | Milkshake</option>
                                <option value="11928132">Regular | Tea</option>
                                <option value="11928368">S.T.D Fillings |</option>
                                <option value="11928362">Salad |</option>
                                <option value="11928722">Salsa Sauce |</option>
                                <option value="11928810">Single (1) | Scoop</option>
                                <option value="11892014">Skewer Platter</option>
                                <option value="11865954">Soft Drink (Regular)</option>
                                <option value="11891990">Spicy Lamb  | Double</option>
                                <option value="11891986">Spicy Lamb | Single</option>
                                <option value="11891763">Spicy Lamb Kebab | Large</option>
                                <option value="11891783">Spicy Lamb Kebab | Regular</option>
                                <option value="11891287">Spicy Lamb Pitta | Double</option>
                                <option value="11948530">Spicy Lamb Pitta | LO</option>
                                <option value="11891264">Spicy Lamb Pitta | Single</option>
                                <option value="11771083">Spicy Lamb Skewer</option>
                                <option value="11948596">Spicy Lamb Skewer | LO</option>
                                <option value="11948580">Spicy Lamb Wrap | LO</option>
                                <option value="11928319">Spicy Seasoned |</option>
                                <option value="12136388">Staff Meal | </option>
                                <option value="11892115">Strawberry | Cheesecake</option>
                                <option value="11892129">Strawberry | Ice Cream</option>
                                <option value="11892042">Strawberry | Milkshake</option>
                                <option value="11892051">Strawberry Delight | Smoothi</option>
                                <option value="11860692">Sunkist Orange (Large)</option>
                                <option value="11791661">Sunkist Orange (Regular)</option>
                                <option value="11892190">Sunkist Orange (Small)</option>
                                <option value="11892070">Super Green | Smoothie</option>
                                <option value="11865211">Supermalt | Bottle </option>
                                <option value="11777130">Sweet Potato (Plain)</option>
                                <option value="11777131">Sweet Potato (Seasoned)</option>
                                <option value="11892128">Swiss Chocolate | Ice Cream</option>
                                <option value="12257897">Telephone Order | </option>
                                <option value="11926999">The Little Cow Double | Burg</option>
                                <option value="11926993">The Little Cow Single | Burg</option>
                                <option value="11892043">Toffe Fudge | Milkshake</option>
                                <option value="11928402">Tomato Ketchup |</option>
                                <option value="11928364">Tomatos |</option>
                                <option value="11926998">Top Notch Double | Burger</option>
                                <option value="11926982">Top Notch Single | Burger</option>
                                <option value="11892120">Triple Choc Fudge | Cookie</option>
                                <option value="11928439">Tzacick | Pot</option>
                                <option value="11892126">Vanilla Dream | Ice Cream</option>
                                <option value="11860691">Vimto (Large)</option>
                                <option value="11791665">Vimto (Regular)</option>
                                <option value="11892188">Vimto (Small)</option>
                                <option value="11776957">Wedges (Plain)</option>
                                <option value="11777098">Wedges (Seasoned)</option>
                                <option value="11892122">White Chocolate | Cookie</option>
                                <option value="11892135">White Chocolate | Ice Cream</option>
                                <option value="11892009">Whole | Platter</option>
                                <option value="11860683">Whole Chicken |</option>
                                <option value="11892010">Wings | Platter</option>
                                <option value="12285825">Wrap | Extras</option>
                            </select>
                        </div>
                        <div className="col-sm-4 col-md-4 margt25">
                            <input className="add-other" value="Add" type="button" id=""/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
   UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddMulProduct));
