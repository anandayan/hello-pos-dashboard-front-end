import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as ProductActions from '../../../actions/management/product_actions';
import * as BankingActions from '../../../actions/setup/banking_actions';
import * as StockActions from '../../../actions/management/stock_actions';
import { bindActionCreators } from 'redux';
import * as utils from '../../../util';

let ProductsAdder = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue',
             addProductClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      let nexPro = nextProps.ProductData
      if(this.state.addProductClicked && nexPro){
        if(nexPro.ProductAdded.status_code === 200){
         
          document.getElementById("product-details").reset();
          this.props.router.push('management/products/products')

        }

      }

    },
    componentDidMount() {
        this.props.dispatch(BankingActions.getListTaxRate());
        this.props.dispatch(ProductActions.listCategory());
        this.props.dispatch(ProductActions.getListBrandData());
        this.props.dispatch(StockActions.getListSupplierData());
    },
    addProduct(){

      let data = {};
      
      data.name = this.refs.product_name.value;
      data.description = this.refs.product_description.value;
      data.cost_price = this.refs.product_costPrice.value;
      data.tax_rate = this.refs.product_taxRate.value;
      data.tax_exempt_eligible = this.refs.taxExemptEligible.value;
      data.sale_price = this.refs.salePrice.value;
      data.variable_price = this.refs.variablePrice.value;
      data.margin_percent = this.refs.marginPercent.value;
      data.delivery_rate = this.refs.deliveryRate.value;
      data.delivery_tax_rate = this.refs.deliveryTaxRate.value;
      data.retail_price = this.refs.retailPrice.value;
      data.bar_code = this.refs.barCode.value;
      data.category_id = this.refs.category_id.value;
      data.brand_id  = this.refs.brand.value;
      data.supplier_id  = this.refs.supplier.value;
      data.product_order_code = this.refs.productOrderCode.value;
      data.article_code = this.refs.articleCode.value;
      data.unit_of_sale = this.refs.unitOfSale.value;
      data.volume_of_sale = this.refs.volumeOfSale.value;
      data.button_color = this.refs.buttonColor.value;
      data.popup_note = this.refs.popupNote.value;
      data.multiple_choice_note = this.refs.multipleChoiceNote.value;
      data.till_order = this.refs.tillOrder.value;
      data.sell_on_till = this.refs.sellOnTill.value;
      data.add_stock_qty  = this.refs.addStockQuantity.value;
      data.add_stock_qty_location  = this.refs.addStockQuantityLocation.value;



      this.props.dispatch(ProductActions.addProductData(data));
      this.setState({addProductClicked:true})
    },

    back(event) {
        this.props.router.push('/management/products/products');
    },
   
    render() {
        return (
            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                      <h1 className="add-title">Add a Product</h1>
                        
                    </div>
                    <div className="parainfo">
                        <p>On this page you can add new products, any product you create will be displayed on the Till under a category unless you select 'Top Level' in the category drop down box.</p>
                        <p>You can also add create new tax rates, categories, brands, suppliers, popup notes and multiple choice notes. Finally, you can generate a barcode at random</p>
                    </div>
                    <div className="filter-by">
                        <form className="product-details" id="product-details">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="sellOnTill" id="sell_till" value="1"/>
                                <label htmlFor="sell_till"> Sell on Till:</label>
                            </div>
                            <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="taxExemptEligible" id="ShowSupplierDetails" value="1"/>
                                <label htmlFor="ShowSupplierDetails"> Tax Exempt Eligible: </label>
                            </div>
                            <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="variablePrice" id="Variable_Price" value="1"/>
                                <label htmlFor="Variable_Price"> Variable Price: (Set the Price on the Till upon Sale.) </label>
                            </div>
                        </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Name: (Appears on Till)</label>
                                <input  ref="product_name" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Description: (Appears on receipt)</label>
                                <input  ref="product_description" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Cost Price: (excluding Tax)</label>
                                <input  ref="product_costPrice" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4 ">
                                <label htmlFor="filter_category">Tax Rate:</label>
                                <select  ref="product_taxRate" className="form-control">
                                    { utils.genderateOptionTemplate(this.props.TaxRate.data)}
                                </select>
                             {/* {   <div className="down-select-btn-commn">
                                    <input ref="filter_category"   onClick={this.props.router.push('/setup/banking/taxRate')} type="button" value="Create Tax Rate"/>
                                </div>} */}
                            </div>

                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Sale Price: (excluding Tax)  (<strong>Not</strong> saved, this is only used to calculate Sale Price inc. Tax)</label>
                                <input ref="salePrice" className="form-control"   type="text"   autoComplete="off"/>
                            </div>

                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Cost Price: (including Tax)  (<strong>Not</strong> saved, this is only used to calculate Cost Price exc. Tax)</label>
                                <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Sale Price: (including Tax)</label>
                                <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                            </div>

                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Margin(%)</label>
                                <input ref="marginPercent" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Take Away/Delivery Price: (excluding Tax)  (<strong>Not</strong> saved, this is only used to calculate Take Away/Delivery inc. Tax)</label>
                                <input ref="deliveryRateNoTax" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4 "> {/*single-row*/}
                                <label htmlFor="filter_category">Take Away/Delivery Tax Rate:</label>
                                <select  ref="deliveryTaxRate"  className="form-control">
                                { utils.genderateOptionTemplate(this.props.TaxRate.data)}
                                </select>
                                {/* {<div className="down-select-btn-commn">
                                    <input ref="filter_category"  onClick={this.props.router.push('/setup/banking/taxRate')}  type="button" value="Create Tax Rate"/>
                                </div>} */}
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Take Away/Delivery Price: (including Tax)</label>
                                <input ref="deliveryRate" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Recommended Retail Price (RRP):</label>
                                <input ref="retailPrice" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Barcode</label>
                                 <input ref="barCode" className="form-control"   type="text"   autoComplete="off"/>
                                
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Category:</label>
                                <select ref="category_id" className="form-control">
                                
                                { utils.genderateOptionTemplate(this.props.ProductData.CategoriesList.data)}

                                </select>
                                
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Brand:</label>
                                <select ref="brand" className="form-control">
                                { utils.genderateOptionTemplate(this.props.ProductData.GetBrand.data)}
 
                                </select>
                                
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Supplier:</label>
                                <select ref="supplier" className="form-control">
                                    <option value="0">None</option>
                                     { utils.genderateOptionTemplate(this.props.Supplier.data)}
                                </select>
                                
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Product Order Code: (for reference to supplier)</label>
                                <input ref="productOrderCode" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Article Code: (for use in accounting)</label>
                                <input ref="articleCode" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Unit of Sale: (Master Products only) Minimum measurement in which this product is used (e.g half pints in a barrel).</label>
                                <select ref="unitOfSale" className="form-control">
                                    <option value="">None</option>
                                    <option value="ml">ml</option>
                                    <option value="ml">ml</option>
                                    <option value="kg">kg</option>
                                    <option value="g">g</option>
                                    <option value="Each">Each</option>
                                    <option value="Half Pint">Half Pint</option>
                                    <option value="l">l</option>
                                    <option value="oz">oz</option>
                                    <option value="cm">cm</option>
                                    <option value="sq ml">sq m</option>
                                    <option value="cl">cl</option>
                                    <option value="units">Units</option>
                                    <option value="cards">Cards</option>
                                    <option value="m">m</option>
                                    <option value="lb">lb</option>
                                    <option value="yd">yd</option>
                                    <option value="in">in</option>
                                    <option value="ft">ft</option>
                                </select>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Volume of Sale: (Master Products only) Volume of 'Unit of Sale' in this product (e.g 352 half pints in a keg).</label>
                                <input ref="volumeOfSale" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Multiple Choice Note:</label>
                                <select ref="multipleChoiceNote" className="form-control">
                                    <option value="0">None</option>
                                    <option value="1">Burger (Filling)</option>
                                    <option value="2">Peri Peri Flavours</option>
                                    <option value="3">Seasoning</option>
                                    <option value="4">Optional Request</option>
                                    <option value="5">Platter | Request</option>
                                </select>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label htmlFor="filter_category">Till Order:</label>
                                <input ref="tillOrder" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <label  >Button Colour: (Leave blank for default)</label>
                                <select ref="buttonColor" className="form-control">

                                    <option value="0"  >None</option>
                                    <option value="7" >Black</option>
                                    <option value="4" >Dark Blue</option>
                                    <option value="8" >Dark Green</option>
                                    <option value="2"  >Dark Grey</option>
                                    <option value="3"  >Dark Orange</option>
                                    <option value="14"  >Deep Green</option>
                                    <option value="1"  >Grey</option>
                                    <option value="12"  >Grey Yellow</option>
                                    <option value="4"  >Light Blue</option>
                                    <option value="5"  >Light Orange</option>
                                    <option value="10"  >Light Red</option>
                                    <option value="13" >Navy Blue</option>
                                    <option value="11"  >Pink</option>
                                    <option value="9" >Red</option>
                                </select>

                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4 single-row">
                                <label htmlFor="filter_category">Popup Note:</label>
                                <select ref="popupNote" className="form-control">
                                    <option value="">None</option>
                                    <option value="29334">Cawston Press |</option>
                                    <option value="29732">Out of Stock</option>
                                </select>

                                
                            </div>

                            <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4 single-row">
                                <label htmlFor="filter_category">Add Stock at the selected Location:</label>
                                <input ref="addStockQuantity" className="form-control"    type="text"   autoComplete="off"/>
                                <select ref="addStockQuantityLocation" className="form-control" >

                                    <option value="0">None</option>
                                    <option value="1">All</option>
                                    <option value="2">Hornchurch</option>
                                </select>
                                <input ref="filter_category" className="form-control hide"   type="text"   autoComplete="off"/>
                            </div>

                        </form>
                        <div className="buttonBtn">
                          <div className="cancelBtn">
                             <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                          </div>
                          <div className="pull-right add-otherBtn">

                             <button className="btn btn-primary"  onClick={this.addProduct} >Add </button>
                          </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      ProductData:state.Product,
      TaxRate:state.Banking.GetTaxRate,
      Supplier:state.Stock.GetSupplier
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(ProductsAdder));
