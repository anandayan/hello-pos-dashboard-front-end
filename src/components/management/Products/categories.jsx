import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import qq from '../../../img/question-mark.png';
import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';

import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Categories">
         <p>This page contains a list of all of your products. Use the category dropdown to filter the products. You can edit and delete your products or add a new one by clicking on the 'Add Product' button.</p>
    </Popover>
  );

let CategorytList = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue',listingCalled:false,catListData:null
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      if(this.state.listingCalled && nextProps.ProductData.CategoriesList.status_code === 200 ){
       this.setState({
         listingCalled:false,
         catListData:nextProps.ProductData.CategoriesList.data
       })
      }
    },
    componentDidMount() {
      this.listCat(1);
    },
    AddCategories(){
        this.props.router.push('/management/products/categories/add-categories');
    },
    listCat(p){
      
      this.props.dispatch(ProductActions.listCategory());
      this.setState({listingCalled:true})
    },
    editCat(){

    },
    deleteCat(){

    },
    generateCategoList(){
        let categorList = this.state.catListData;

        let categorListTemplate = categorList ?  categorList.map(function (listItem, key) {

        return (<tr key={key}>
          <td>{listItem.till_order}</td>
          <td>{listItem.name}</td>
          <td>{listItem.till_order}</td>
          <td>{listItem.report_category}</td>
          <td>{listItem.order_printer}</td>
          <td>{listItem.course}</td>
          <td>{listItem.wet_dry ? "Wet" : "Dryv"}</td>
          <td>{listItem.button_color}</td>
          <td>{listItem.nominal_code}</td>
          <td><input type="checkbox" defaultChecked={listItem.showOnTill ? true : false}/></td>
          <td>{listItem.popup_note}</td>
              <td><span data-id={listItem._id} onClick={this.editCat} className="edit glyphicon glyphicon-edit ">
              </span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteCat}></span></td>
              </tr>)
        }, this):("No Data Found");
        return categorListTemplate ;
    },
    render() {
      let catList = this.generateCategoList()
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">

                    </div>
                    <div className="common-info">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                        <h1 className="title">Categories <img src = {qq} alt="qus mark"/></h1>
                         </OverlayTrigger>
                        <input type="button" onClick={this.AddCategories} className="add-btn pull-right" value="Add Categories" />
                    </div>
                    
                    <div className="filter-bye">
                      <div className="col-sm-4">
                          <span className="help-block txtalignRight">Filter by Name, Location or Role:Filter by Category, Parent Category or Printer Type:</span>
                      </div>
                      <div className="col-sm-8">
                        <input ref="role_discription" className="form-control input-text-fields" id="role_discription"  type="text"   autoComplete="off"/>
                        <input className="add-other" value="Search" type="button" id=""/>
                      </div>
                    </div>

                    <div className="staff-table">
                    <table className="table table-striped staff-list-tabel">
                        <thead>
                            <tr>
                                <th>Till Order</th>
                                <th>Name</th>
                                <th>Parent</th>
                                <th>Report Category</th>
                                <th>Order printer / Display</th>
                                <th>Course</th>
                                <th>Wet/Dry</th>
                                <th>BUtton Color</th>
                                <th>Nominal code</th>
                                <th>Show on till</th>
                                <th>Popup NOte</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>{catList}</tbody>
                    </table>
                </div>

                

                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
     UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(CategorytList));
