import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import qq from '../../../img/question-mark.png';
import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';


 import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Multiple Choice Product">
         <p>This page allows you to manage your multiple choice product groups. Multiple choice product groups allow you to add products on the till as add-ons to other products. Each group you add to a product will show as a pop-up once the product is added to a transaction. Multiple choice products may have specific prices assigned to them.</p>
    </Popover>
  );

let MultilpeChoiceProducts = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    AddMultProduct(){
        this.props.router.push('/management/products/multiplechoiceproducts/add-multiplechoiceproducts');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    render() {
        return (

            <div class>
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                        <h1 className="title">Multiple Choice Product <img src = {qq} alt="qus mark"/></h1>
                     </OverlayTrigger>
                        <input type="button" onClick={this.AddMultProduct} className="add-btn pull-right" value="Add Multiple Choice Product" />
                    </div>

                    <div className="staff-table">
                    <table className="table table-striped staff-list-tabel">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Discription</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>


                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(MultilpeChoiceProducts));
