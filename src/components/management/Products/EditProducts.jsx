import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';


let EditProduct = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue',
             EditProductClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      let nexPro = nextProps.ProductData
      if(this.state.EditProductClicked && nexPro){
        
        if(nexPro.ProductUpdate.status_code === 200){
          
          this.props.router.push('/products')

        }

      }

    },
    componentDidMount() {
        
    },
    EditProduct(){

      let data = {};
      
      data.name = this.refs.product_name.value;
      data.description = this.refs.product_description.value;
      data.cost_price = this.refs.product_costPrice.value;
      data.tax_rate = this.refs.product_taxRate.value;
      data.tax_exempt_eligible = this.refs.taxExemptEligible.value;
      data.sale_price = this.refs.salePrice.value;
      data.variable_price = this.refs.variablePrice.value;
      data.margin_percent = this.refs.marginPercent.value;
      data.delivery_rate = this.refs.deliveryRate.value;
      data.delivery_tax_rate = this.refs.deliveryTaxRate.value;
      data.retail_price = this.refs.retailPrice.value;
      data.bar_code = this.refs.barCode.value;
      data.category_id = this.refs.category.value;
      data.brand_id = this.refs.brand.value;
      data.supplier_id = this.refs.supplier.value;
      data.product_order_code = this.refs.productOrderCode.value;
      data.article_code = this.refs.articleCode.value;
      data.unit_of_sale = this.refs.unitOfSale.value;
      data.volume_of_sale = this.refs.volumeOfSale.value;
      data.button_color = this.refs.buttonColor.value;
      data.popup_note = this.refs.popupNote.value;
      data.multiple_choice_note = this.refs.multipleChoiceNote.value;
      data.till_order = this.refs.tillOrder.value;
      data.sell_on_till = this.refs.sellOnTill.value;
      data.add_stock_qty  = this.refs.addStockQuantity.value;
      data.add_stock_qty_location  = this.refs.addStockQuantityLocation.value;
      this.props.dispatch(ProductActions.getUpdateProductData(data));
      this.setState({EditProductClicked:true})
    },
back(event) {
        this.props.router.push('/management/products/products');
    },
    render() {
      let productData=this.props.ProductData.ProductById;
        return (
            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                      <h1 className="add-title">Edit Product</h1>

                    </div>

                    <div className="filter-by">
                        <form className="product-details" id="product-details">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="sellOnTill" id="sell_till" defaultValue={productData.sellOnTill} defaultChecked={productData.sellOnTill ? true : false}/>
                                <label htmlFor="sell_till"> Sell on Till:</label>
                            </div>
                            <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="taxExemptEligible" id="ShowSupplierDetails" defaultValue={productData.taxExemptEligible} defaultChecked={productData.taxExemptEligible ? true : false}/>
                                <label htmlFor="ShowSupplierDetails"> Tax Exempt Eligible: </label>
                            </div>
                            <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="variablePrice" id="Variable_Price" defaultValue={productData.variablePrice} defaultChecked={productData.variablePrice ? true : false}/>
                                <label htmlFor="Variable_Price"> Variable Price: (Set the Price on the Till upon Sale.) </label>
                            </div>
                        </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Name: (Appears on Till)</label>
                                <input  ref="product_name" className="form-control"  defaultValue={productData.name} type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Description: (Appears on receipt)</label>
                                <input  ref="product_description" className="form-control"  defaultValue={productData.description} type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Cost Price: (excluding Tax)</label>
                                <input  ref="product_costPrice" className="form-control"  defaultValue={productData.costPrice} type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 single-row single-row">
                                <label htmlFor="filter_category">Tax Rate:</label>
                                <select  ref="product_taxRate" className="form-control" defaultValue={productData.taxRate}>
                                    <option  value="20">20 VAT</option>
                                    <option value="0">Zero Vat</option>
                                </select>
                                <div className="down-select-btn-commn">
                                    <input ref="filter_category"   type="button" value="Create Tax Rate" defaultValue={productData.taxRate}/>
                                </div>
                            </div>

                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Sale Price: (excluding Tax)  (<strong>Not</strong> saved, this is only used to calculate Sale Price inc. Tax)</label>
                                <input ref="salePrice" className="form-control"    type="text"   autoComplete="off"/>
                            </div>

                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Cost Price: (including Tax)  (<strong>Not</strong> saved, this is only used to calculate Cost Price exc. Tax)</label>
                                <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Sale Price: (including Tax)</label>
                                <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                            </div>

                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Margin(%)</label>
                                <input ref="marginPercent" className="form-control" defaultValue={productData.marginPercent}   type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Take Away/Delivery Price: (excluding Tax)  (<strong>Not</strong> saved, this is only used to calculate Take Away/Delivery inc. Tax)</label>
                                <input ref="deliveryRateNoTax" className="form-control" defaultValue={productData.deliveryRateNoTax} type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 single-row">
                                <label htmlFor="filter_category">Take Away/Delivery Tax Rate:</label>
                                <select  ref="deliveryTaxRate"  className="form-control" defaultValue={productData.deliveryTaxRate} >
                                    <option value="20">20 VAT</option>
                                    <option value="0">Zero Vat</option>
                                </select>
                                <div className="down-select-btn-commn">
                                    <input ref="filter_category"   type="button" value="Create Tax Rate"/>
                                </div>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Take Away/Delivery Price: (including Tax)</label>
                                <input ref="deliveryRate" className="form-control" defaultValue={productData.deliveryRate} type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Recommended Retail Price (RRP):</label>
                                <input ref="retailPrice" className="form-control" defaultValue={productData.retailPrice} type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 single-row">
                                <label htmlFor="filter_category">Barcode</label>
                                 <input ref="barCode" className="form-control" defaultValue={productData.barCode} type="text"   autoComplete="off"/>
                                <div className="down-select-btn-commn">
                                    <input ref="filter_category"   type="button" value="Generate New Barcode"/>
                                </div>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 single-row">
                                <label htmlFor="filter_category">Category:</label>
                                <select ref="category" className="form-control" defaultValue={productData.category}>
                                    <option value="">Top Level</option>
                                        <option value="1">Beverages</option>
                                        <option value="2">&nbsp;&nbsp;&nbsp;&nbsp;Cold Beverages</option>
                                        <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bottles </option>
                                        <option value="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bottles | 1.5</option>
                                        <option value="5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Freshly Squeezed Juice</option>
                                        <option value="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fruit Smoothies</option>

                                </select>
                                <div className="down-select-btn-commn">
                                    <input ref="filter_category"   type="button" value="Create Category"/>
                                </div>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 single-row">
                                <label htmlFor="filter_category">Brand:</label>
                                <select ref="brand" className="form-control" defaultValue={productData.brand}>
                                    <option value="0">None</option>
                                        <option value="1">Peri Peri Flavours</option>
                                        <option value="2">Premium Drink</option>
                                        <option value="3">Soft Drinks</option>
                                </select>
                                <div className="down-select-btn-commn">
                                    <input ref="filter_category"   type="button" value="Create Brand"/>
                                </div>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 single-row">
                                <label htmlFor="filter_category">Supplier:</label>
                                <select ref="supplier" className="form-control" defaultValue={productData.supplier}>
                                    <option value="0">None</option>
                                        <option value="1">Draught soft Drinks</option>
                                        <option value="2">Essex</option>
                                        <option value="3">KFF </option>
                                        <option value="4">Star Catering</option>
                                </select>
                                <div className="down-select-btn-commn">
                                    <input ref="filter_category"   type="button" value="Create Supplier"/>
                                </div>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Product Order Code: (for reference to supplier)</label>
                                <input ref="productOrderCode" className="form-control" defaultValue={productData.productOrderCode} type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Article Code: (for use in accounting)</label>
                                <input ref="articleCode" className="form-control"  defaultValue={productData.articleCode}  type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Unit of Sale: (Master Products only) Minimum measurement in which this product is used (e.g half pints in a barrel).</label>
                                <select ref="unitOfSale" className="form-control"  defaultValue={productData.unitOfSale}  >
                                    <option value="">None</option>
                                    <option value="ml">ml</option>
                                    <option value="ml">ml</option>
                                    <option value="kg">kg</option>
                                    <option value="g">g</option>
                                    <option value="Each">Each</option>
                                    <option value="Half Pint">Half Pint</option>
                                    <option value="l">l</option>
                                    <option value="oz">oz</option>
                                    <option value="cm">cm</option>
                                    <option value="sq ml">sq m</option>
                                    <option value="cl">cl</option>
                                    <option value="units">Units</option>
                                    <option value="cards">Cards</option>
                                    <option value="m">m</option>
                                    <option value="lb">lb</option>
                                    <option value="yd">yd</option>
                                    <option value="in">in</option>
                                    <option value="ft">ft</option>
                                </select>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Volume of Sale: (Master Products only) Volume of 'Unit of Sale' in this product (e.g 352 half pints in a keg).</label>
                                <input ref="volumeOfSale" className="form-control" defaultValue={productData.volumeOfSale}  type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Multiple Choice Note:</label>
                                <select ref="multipleChoiceNote" className="form-control" defaultValue={productData.multipleChoiceNote}>
                                    <option value="0">None</option>
                                    <option value="1">Burger (Filling)</option>
                                    <option value="2">Peri Peri Flavours</option>
                                    <option value="3">Seasoning</option>
                                    <option value="4">Optional Request</option>
                                    <option value="5">Platter | Request</option>
                                </select>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label htmlFor="filter_category">Till Order:</label>
                                <input ref="tillOrder" className="form-control" defaultValue={productData.tillOrder} type="text"   autoComplete="off"/>
                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <label  >Button Colour: (Leave blank for default)</label>
                                <select ref="buttonColor" className="form-control"  defaultValue={productData.buttonColor}>

                                    <option value="0"  >None</option>
                                    <option value="7" >Black</option>
                                    <option value="4" >Dark Blue</option>
                                    <option value="8" >Dark Green</option>
                                    <option value="2"  >Dark Grey</option>
                                    <option value="3"  >Dark Orange</option>
                                    <option value="14"  >Deep Green</option>
                                    <option value="1"  >Grey</option>
                                    <option value="12"  >Grey Yellow</option>
                                    <option value="6"  >Light Blue</option>
                                    <option value="5"  >Light Orange</option>
                                    <option value="10"  >Light Red</option>
                                    <option value="13" >Navy Blue</option>
                                    <option value="11"  >Pink</option>
                                    <option value="9" >Red</option>
                                </select>

                            </div>
                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 single-row">
                                <label htmlFor="filter_category">Popup Note:</label>
                                <select ref="popupNote" className="form-control" defaultValue={productData.popupNote}>
                                    <option value="">None</option>
                                    <option value="29334">Cawston Press |</option>
                                    <option value="29732">Out of Stock</option>
                                </select>

                                <div className="down-select-btn-commn">
                                    <input ref="filter_category"   type="button" value="Create Supplier"/>
                                </div>
                            </div>

                            <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 single-row">
                                <label htmlFor="filter_category">Add Stock at the selected Location:</label>
                                <input ref="addStockQuantity" className="form-control" defaultValue={productData.addStockQuantity}  type="text"   autoComplete="off"/>
                                <select ref="addStockQuantityLocation" className="form-control" defaultValue={productData.addStockQuantityLocation} >

                                    <option value="0">None</option>
                                    <option value="1">All</option>
                                    <option value="2">Hornchurch</option>
                                </select>
                                <input ref="filter_category" className="form-control hide"   type="text"   autoComplete="off"/>
                            </div>

                        </form>
                        <div className="buttonBtn">
                          <div className="cancelBtn">
                             <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                          </div>
                          <div className="pull-right add-otherBtn">

                             <button className="btn btn-primary"  onClick={this.EditProduct} >Update </button>
                          </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(EditProduct));
