import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import qq from '../../../img/question-mark.png';
import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';

import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Multiple Choice Notes">
         <p>"Name" is used to identify this Multiple Choice Note when using the Back Office. "Choices" will be displayed to the user when the product is added to the current transaction on the till and they add a note.</p>
    </Popover>
  );

let MultilpeChoiceNotes = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue',listCalled:false,listData:null
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      //debugger;
      if(this.state.listCalled){
          this.setState({listCalled:true,listData:nextProps.ProductData.GetMultichoiceNote.data})
      }
    },
    componentDidMount() {
      this.listMulticoiseNote(1);
    },
    listMulticoiseNote(page){
        // this.props.router.push('staff/add-staff');
        let data = {};
         
        this.props.dispatch(ProductActions.getListMultiChoiceNoteData(data));
        this.setState({listCalled:true});
    },
    generateMultichoiseNoteList(){
        let choiseNoteList = this.state.listData;

        let choiseNoteTemplate = choiseNoteList ?  choiseNoteList.map(function (listItem, key) {

        let choices = JSON.parse(listItem.choices);
        let cotemp = "";
        choices.forEach(function(k,v){

          cotemp+=k.text+",";
        });
        return (<tr key={key}>
                  <td>{listItem.name}</td>
                   <td>{cotemp}</td>
                    <td>
                    <div className="checkbox checkbox">
                        <input type="checkbox" ref="popupByDefault" id="PopupByDefault" checked={listItem.popupByDefault ? true :false} />
                        <label htmlFor="PopupByDefault"  >  </label>
                    </div>
                    </td>
                  <td><span data-id={listItem._id} onClick={this.editMisc} className="edit glyphicon glyphicon-edit "></span></td>
                  <td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteMisc}></span></td>
                </tr>)
        }, this):("No Data Found");
        return choiseNoteTemplate ;
    },
    addNote(){

             this.props.router.push('/management/products/Addmultiplechoicenotes');

    },
    render() {
      let list = this.generateMultichoiseNoteList();

        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">

                    </div>
                    <div className="common-info">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>

                        <h1 className="title">Multiple Choice Notes <img src = {qq} alt="qus mark"/></h1>
                      </OverlayTrigger>
                        <button className="btn btn-default pull-right" onClick={this.addNote} >Add Notes</button>
                    </div>

                    <div className="filter-by-name">

                      <div className="col-sm-4">
                        <label htmlFor="filter_category">Name:</label>
                        <input ref="role_discription" className="form-control" id="role_discription"  type="text"   autoComplete="off"/>
                      </div>
                      <div className="col-sm-2 margt25">
                          <input className="add-other" value="Search" type="button" id=""/>
                      </div>

                      <div className="col-sm-4">
                        <label htmlFor="filter_category">Filter by Name or Choices:</label>
                        <input ref="role_discription" className="form-control" id="role_discription"  type="text"   autoComplete="off"/>
                      </div>
                      <div className="col-sm-2 margt25">
                          <input className="add-other" value="Search" type="button" id=""/>
                      </div>
                    </div>



                    <div className="staff-table">
                    <table className="table table-striped staff-list-tabel">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Choices</th>
                                <th>Popup by default</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                          {list}
                        </tbody>
                    </table>
                </div>


                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
   UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(MultilpeChoiceNotes));
