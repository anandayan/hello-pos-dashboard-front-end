import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import qq from '../../../img/question-mark.png';
import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';

import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Miscellaneous Products">
         <p>This page contains Miscellaneous Products, you can either edit or add a brand new miscellaneous product by clicking on the 'Add Miscellaneous Product' button.</p>
    </Popover>
  );

let MiscProducts = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue',listingCalled:false,listingData:null
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      if(this.state.listingCalled && nextProps.ProductData.GetMiscProduct.status_code === 200 ){
       this.setState({
         listingCalled:false,
         listingData:nextProps.ProductData.GetMiscProduct.data
       })
      }
    },
    componentDidMount() {
      this.listCat(1)
    },
    AddMiscProducts(){
        this.props.router.push('/management/products/miscproducts/add-miscproducts');
    },
    listCat(p){
      let data = {};
   
      this.props.dispatch(ProductActions.getListMiscProductData(data));
      this.setState({listingCalled:true})
    },
    editMisc(){},
    deleteMisc(){},
    generateMiscList(){
        let miscList = this.state.listingData;

        let miscListTemplate = miscList ?  miscList.map(function (listItem, key) {

        return (<tr key={key}>
                  <td>{listItem.name}</td>  <td>{listItem.taxRate}</td>
                  <td><span data-id={listItem._id} onClick={this.editMisc} className="edit glyphicon glyphicon-edit ">
                  </span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteMisc}></span></td>
                </tr>)
        }, this):("No Data Found");
        return miscListTemplate ;
    },
    render() {
      let list = this.generateMiscList();
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">

                    </div>
                    <div className="common-info">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                        <h1 className="title">Miscellaneous Products <img src = {qq} alt="qus mark"/></h1>
                    </OverlayTrigger>
                        <input type="button" onClick={this.AddMiscProducts} className="add-btn pull-right" value="Add Misc Product" />
                    </div>
                    
                    <div className="filter-by-product">
                      <div className="col-sm-4">
                          <span className="help-block txtalignRight">Filter by Product:</span>
                      </div>
                      <div className="col-sm-8">
                        <input ref="role_discription" className="form-control input-text-fields" id="role_discription"  type="text"   autoComplete="off"/>
                        <input className="add-other" value="Search" type="button" id=""/>
                      </div>
                    </div>

                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Tax rate:</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>{list}</tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
     UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(MiscProducts));
