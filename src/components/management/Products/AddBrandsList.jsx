import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Popover,OverlayTrigger } from 'react-bootstrap';
import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Brands">
     <p>On this page you can add new brands, simply insert a name and description and you will be able to choose a brand when you create a new product.</p>
    </Popover>
  );
let AddBrand = React.createClass({
    getInitialState() {
        return {
             addBrandClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        var brand_data = nextProps.ProductData.BrandAdd;
        if(this.state.addBrandClicked){
           alert(brand_data.message)
        brand_data.status_code === 200 && this.props.router.push('/management/products/brands');
       
        this.setState({addBrandClicked:false});
      }
    },
    componentDidMount() {

    },
    addBrand(){
        
        let data = {};
     
      data.name = this.refs.brand_name.value;
      data.description = this.refs.brand_description.value;
      this.props.dispatch(ProductActions.addBrandData(data));
      this.setState({addBrandClicked:true})
      
    },

    back(event) {
        this.props.router.push('/management/products/brands');
    },
    render() {
        return (
            <div class>
           
            <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                <div className="upgrate license-link ">
               
                    <div className="parainfo">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                    <h1 className="add-title">Add Brands</h1>
                    </OverlayTrigger>
                        </div>
         
                  
                    
                </div>             
              
                
                <div className="filter-by">
                    <div className="form-group col-xs-12 col-sm-12 col-md-4 col-md-offset-2 col-lg-4">
                        <label htmlFor="brand_name">Name:</label>
                        <input ref="brand_name" className="form-control" id="brand_name"  type="text"   autoComplete="off"/>
                    </div>
                    <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label htmlFor="brand_description">Discription:</label>
                        <input ref="brand_description" className="form-control" id="brand_description"  type="text"   autoComplete="off"/>
                    </div>
                  <div className="buttonBtn">
                        <div className="cancelBtn">
                           <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                        </div>
                        <div className="pull-right add-otherBtn">
                           <input class onClick={this.addBrand} value="Add" type="button" />
                        </div>
                  </div>
                </div>
              </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
     UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddBrand));
