import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as ProductActions from '../../../actions/management/product_actions';
import * as BankingActions from '../../../actions/setup/banking_actions';
import { bindActionCreators } from 'redux';
import * as utils from '../../../util';

let AddMiscProduct = React.createClass({
    getInitialState() {
        return {
             addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        
        if(this.state.addClicked){
 
        alert(nextProps.ProductData.MiscProductAdded.message)
        this.setState({addClicked:false});
      }
    },
    componentDidMount() {
this.props.dispatch(BankingActions.getListTaxRate());
    },

    addMiscProduct(){
        
        let data = {};
      
        data.name = this.refs.misc_name.value;
        data.tax_rate = this.refs.misc_taxRate.value;
        this.props.dispatch(ProductActions.addMiscProductData(data));
        this.setState({addClicked:true});
        console.log(data)
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
    },
    back(event) {
        this.props.router.push('/management/products/miscproducts');
    },
    render() {
        return (
            <div class>
             
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        <h1 className="add-title">Add a Miscellaneous Product</h1>
                        
                    </div>
                    <div className="parainfo">
                        <p>On this page you can add Miscellaneous Products, simply insert a name and what tax rate you want to be applied to this product.</p>
                    </div>
                    <div className="filter-by">
                        <div className="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <label htmlFor="misc_name">Name:</label>
                            <input ref="misc_name" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <label htmlFor="misc_taxRate">Tax Rate:</label>
                            <select  ref="misc_taxRate" className="form-control">
                                 { utils.genderateOptionTemplate(this.props.TaxRate.data)}
                            </select>
                        </div>
                        <div className=" col-md-4 col-lg-4 margt25">
                            <input ref="filter_category"   type="button" value="Create Tax Rate"/>
                        </div>
                        <div className="buttonBtn">
                          <div className="cancelBtn">
                             <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                          </div>
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.addMiscProduct} value="Add" type="button" id=""/>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      ProductData:state.Product,
      TaxRate:state.Banking.GetTaxRate,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddMiscProduct));
