import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';


let AddCategry = React.createClass({
    getInitialState() {
        return {
             addCategoriesClicked:true,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        if(this.state.addCategoriesClicked && nextProps.ProductData.CategoriesAdded.status_code === 200 ){
         
          this.props.router.push('/management/products/categories');
        }
    },
    componentDidMount() {
     
    },

   addCategory(){

      let data = {};
    
      data.name =this.refs.name.value;
      data.print_category = parseInt(this.refs.printCategory.value) ;
      data.report_category = parseInt(this.refs.reportCategory.value);
      data.order_printer  = parseInt(this.refs.orderPrinter.value);
      data.course  = parseInt(this.refs.course.value);
      data.wet_dry  = this.refs.wetDry.value;
      data.button_color = this.refs.buttonColor.value;
      data.till_order = this.refs.till_order.value;
      data.nominal_code =this.refs.nominalCode.value;
      data.show_on_till = this.refs.showOnTill.checked ? 1 : 0;
      data.popup_note = parseInt(this.refs.popupNote.value);
      debugger;
      this.props.dispatch(ProductActions.addCategory(data));
      this.setState({addCategoriesClicked:true})
   },

   back(event) {
        this.props.router.push('/management/products/categories');
    },
    render() {
        return (
            <div class>

	            <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                      <h1 className="add-title">Categories</h1>

                    </div>
                    
                    <div className="filter-by">
                    <div className="product-details">
                    	<div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Name:</label>
                            <input ref="name" className="form-control"   type="text"    autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Parent Category:</label>
                            <select ref="printCategory" id="" className="form-control">
								<option value="">Top Level</option>
								<option value="891353">Beverages</option>

							</select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Report Category:</label>
                            <select ref="reportCategory" className="form-control">
                                   <option value="0">None</option>
                                    <option value="1">Top Level</option>
									<option value="891353">Beverages</option>

                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Order Printer / Display:</label>
                            <select ref="orderPrinter" className="form-control">
                                <option value="">None</option>
								<option value="1">BAR</option>
								<option value="2">KITCHEN</option>
								<option value="3">Order A</option>
								<option value="4">Order B</option>
								<option value="5">Order C</option>
								<option value="6">Rear</option>
								<option value="7">Front</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Course:</label>
                            <select ref="course" className="form-control">
                                <option value="">None</option>
                								<option value="12344">Main</option>
                								<option value="12538">Sides</option>
                								<option value="12880">Drink</option>
                								<option value="12497">Extras</option>
                								<option value="12345">Dessert</option>
                								<option value="12343">Starter</option>
                								<option value="13178">Misc</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Wet/Dry Category:</label>
                            <select ref="wetDry" className="form-control">
                                <option value="0">Dry</option>
								<option value="1">Wet</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Button Colour: (Leave blank for default):</label>
                            <select ref="buttonColor" className="form-control">
                                <option value="none">None</option>
                                <option value="black" >Black</option>
                                <option value="blue">Dark Blue</option>
                                <option value="green">Dark Green</option>
                                <option value="gey">Dark Grey</option>
                                <option value="">Dark Orange</option>
                                <option value="">Deep Green</option>
                                <option value="">Grey</option>
                                <option value="">Grey Yellow</option>
                                <option value="">Light Blue</option>
                                <option value="">Light Orange</option>
                                <option value="">Light Red</option>
                                <option value="">Navy Blue</option>
                                <option value="">Pink</option>
                                <option value="">Red</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Till Order:</label>
                            <input ref="filter_category" className="form-control"  ref="till_order" type="text"    autoComplete="off"/>
                        </div>


                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Nominal Code:</label>
                            <input ref="filter_category" className="form-control"  ref="nominalCode" type="text"    autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="showOnTill" id="sell_till" value="1"/>
                                <label htmlFor="sell_till"> Show on Till:</label>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label htmlFor="filter_category">Popup Note:</label>
                            <select ref="popupNote" className="form-control">
                                <option value="">None</option>
								<option value="29334">Cawston Press |</option>
								<option value="29732">Out of Stock</option>
                            </select>
                        </div>
                        
                    </div>
                    <div className="buttonBtn">
                      <div className="cancelBtn">
                         <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                      </div>
                      <div className="pull-right add-otherBtn">

                         <button onClick={this.addCategory} className="btn btn-default"> Add</button>
                      </div>
                    </div>
                    </div>
                    
	            </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddCategry));
