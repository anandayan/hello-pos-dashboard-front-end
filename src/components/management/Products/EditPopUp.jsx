import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';


let EditPopUp = React.createClass({
    getInitialState() {
        return {
             addPopupClicked:false,
        }
    },
    componentWillMount() {

    },

    componentWillReceiveProps(nextProps) {
      var pop_data = nextProps.ProductData.UpdatePopUpBrand;
        if(this.state.addPopupClicked){
            if(pop_data.status_code === 200){
                alert(pop_data.message)
                this.props.router.push('/management/products/popupalert');
            }
          this.setState({addPopupClicked:false});
      }
    },
    componentDidMount() {

    },

    EditPopup(){
        
        let data = {};
        data.id = this.props.ProductData.popupById._id;
      data.name = this.refs.popup_name.value;
      data.message = this.refs.pop_message.value;
      data.show_once_per_transaction = this.refs.showOncePerTransaction.value;
      this.props.dispatch(ProductActions.getUpdatePopUpNoteData(data));
      this.setState({addPopupClicked:true})
      
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
    },
    back(event) {
        this.props.router.push('/management/products/popupalert');
    },
    render() {
        return (
            <div class>
         
            <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                <div className="upgrate license-link">
                    <h1 className="add-title">Popup Notes</h1>
                    
                </div>
                <div className="parainfo">
                    <p>"Name" is used to identify this popup note when using the Back Office.</p>
                    <p>"Message" will be displayed to the user when the product is added to the current transaction on the Till.</p>
                </div>
                <div className="filter-by">
                    <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label htmlFor="popup_name">Name:</label>
                        <input ref="popup_name" className="form-control" id="popup_name"  defaultValue={this.props.ProductData.popupById.name} type="text"   autoComplete="off"/>
                    </div>
                    <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label htmlFor="pop_message">Message:</label>
                        <input ref="pop_message" className="form-control"   defaultValue={this.props.ProductData.popupById.message} type="text"   autoComplete="off"/>
                    </div>
                    <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div className="checkbox checkbox-inline">
                            <input type="checkbox" ref="showOncePerTransaction" id="showOncePerTransaction" value="1"/>
                            <label htmlFor="showOncePerTransaction"> Show Once Per Transaction:</label>
                        </div>
                    </div>
                </div>

                <div className="buttonBtn">
                      <div className="cancelBtn">
                         <input className="cancel btn-danger" value="Back" type="button" onClick={this.back}/>
                      </div>
                      <div className="pull-right add-otherBtn">
                         <input class onClick={this.EditPopup}  value="Update" type="button" id=""/>
                      </div>
                </div>
              </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
     UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(EditPopUp));
