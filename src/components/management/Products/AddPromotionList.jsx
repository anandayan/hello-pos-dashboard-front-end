import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as ProductActions from '../../../actions/management/product_actions';
import * as CompanyActions from '../../../actions/setup/company_actions';
import { bindActionCreators } from 'redux';
import * as utils from '../../../util';

let PromotionAdder = React.createClass({
    getInitialState() {
        return {
             addClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {
        debugger;
        this.props.dispatch(CompanyActions.getCustomerData());
    },
        
    addPromotion(){
        // debugger;
        let data = {};
        data.name = this.refs.promotion_name.value;
        data.description = this.refs.promotion_description.value;
        data.dur = this.refs.duration.value;
        data.from_dur = this.refs.fromDuration.value;
        data.to_dur = this.refs.toDuration.value;
        data.meal_deal = this.refs.mealDeal.value;
        data.meal_deal_groups = this.refs.meal_deal_groups.value;
        data.type = this.refs.promotion_type.value;
        data.req_qty = this.refs.requiredQuantityreq_qty.value;
        data.mix_and_match = this.refs.mixAndMatch.value;
        data.no_usage_with_others = this.refs.no_usage_with_others.value;
        data.enabled = this.refs.enabled.value;
        data.mon_enabled = this.refs.monEnabled.value;
        data.tue_enabled = this.refs.tueEnabled.value;
        data.wed_enabled = this.refs.wedEnabled.value;
        data.thu_enabled = this.refs.thuEnabled.value;
        data.fri_enabled = this.refs.friEnabled.value;
        data.sat_enabled = this.refs.satEnabled.value;
        data.sun_enabled = this.refs.sunEnabled.value;
        data.eat_in_enabled = this.refs.eatInEnabled.value;
        data.eat_out_enabled = this.refs.eatOutEnabled.value;
        data.customer_type_id = this.refs.customer_type_id.value;
        data.category_id = this.refs.category_id.value;
        data.product_id = this.refs.product_id.value;
        this.props.dispatch(ProductActions.sendPromotionData(data));
        this.setState({addClicked:true});

    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
    },

    back(event) {
        this.props.router.push('/management/products/promotions');
    },
    render() {
        return (
            <div>
          
                <div className="col-sm-12 col-md-10 col-lg-10">
                    <div className="upgrate license-link">
                      <h1 className="add-title">Add a Promotion</h1>
                    </div>
                    <div className="parainfo">
                        <p>This is the promotion add page, here you can add a new promotion, insert the promotion name and select a pre-defined type and then insert the figures into the 'Required Quantity and Amount' boxes , once you have completed it, click on the 'Add' button.</p>
                    </div>
                    <div className="filter-by">
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="promotion_name">Name:</label>
                            <input  ref="promotion_name" className="form-control" id="promotion_name"  type="text"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="promotion_description">Description:</label>
                            <input  ref="promotion_description" className="form-control" id="promotion_description"  type="text" />
                        </div>
                        <div className="form-group">
                            <label className="col-sm-3 control-label">Promotion Duration</label>
                            <div className="col-sm-9">
                                <div className="radio">
                                    <label> <input type="radio"  id="" value="" />No Duration</label>
                                </div>
                                <div className="radio">
                                    <label> <input type="radio"  ref="duration" id="duration" value="1" />Between Dates</label>
                                </div>
                                <div className="radio">
                                    <label> <input type="radio"  id="" value="" />Between Times</label>
                                </div>
                                <span>Promotion only active between two dates or times</span>
                            </div>
                        </div>
                        
                        <div className="col-xs-12 col-sm-12 col-md-10 col-lg-10 margt30 margb30">
                            <div className="col-sm-4">
                                <span>Required Quantity and Amount:</span>
                            </div>
                            <div className="col-sm-1">
                                <label htmlFor="Buy">Buy</label>
                            </div>
                            <div className="col-sm-3">
                                <input ref="" className="form-control" id=""  type="text" />
                            </div>
                            <div className="col-sm-1">
                                <label htmlFor="For">For</label>
                            </div>
                            <div className="col-sm-3">
                                <input ref="req_qty" className="form-control" id="req_qty"  type="text" />
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="promotion_type">Type:</label>
                            <select ref="promotion_type" className="form-control">
                                <option value="">Top Level</option>
                                <option value="1">X For Y</option>
                                <option value="2">X For £</option>
                                <option value="11">% Discount</option>
                                <option value="21">Spend £ Save %</option>
                                <option value="22">Spend £ Save £</option>
                            </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="customer_type_id">Customer Type:</label>
                            <select ref="customer_type_id" className="form-control">
                                { utils.genderateOptionTemplate(this.props.CompanyType.data)}
                            </select>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label className="col-sm-3 control-label">Mix and Match:</label>
                            <div className="col-sm-9">
                                <div className="checkbox">
                                    <input type="checkbox" ref="mixAndMatch" id="mixAndMatch" value="1"/>
                                    <label htmlFor="mixAndMatch"></label>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label className="col-sm-4">Not to be used in conjunction with any other offers:</label>
                            <div className="col-sm-8">
                                <div className="checkbox">
                                    <input type="checkbox" ref="no_usage_with_others" id="no_usage_with_others" value="1"/>
                                    <label htmlFor="no_usage_with_others"></label>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label className="col-sm-3 control-label">Enabled:</label>
                            <div className="col-sm-9">
                                <div className="checkbox">
                                    <input type="checkbox" ref="enabled" id="enabled" value="1"/>
                                    <label htmlFor="enabled"></label>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-12 col-lg-12 ">
                            <span className="col-sm-3">Promotion Duration</span>
                            <div className="col-sm-9">
                                <div className="checkbox">
                                    <input type="checkbox" ref="monEnabled" id="monEnabled" value="1"/>
                                    <label htmlFor="monEnabled">Mon</label>
                                </div>
                                <div className="checkbox">
                                    <input type="checkbox" ref="tueEnabled" id="tueEnabled" value="1"/>
                                    <label htmlFor="tueEnabled">Tus</label>
                                </div>
                                <div className="checkbox">
                                    <input type="checkbox" ref="wedEnabled" id="wedEnabled" value="1"/>
                                    <label htmlFor="wedEnabled">Wed</label>
                                </div>
                                <div className="checkbox">
                                    <input type="checkbox" ref="thuEnabled" id="thuEnabled" value="1"/>
                                    <label htmlFor="thuEnabled">Thu</label>
                                </div>
                                <div className="checkbox">
                                    <input type="checkbox" ref="friEnabled" id="friEnabled" value="1"/>
                                    <label htmlFor="friEnabled">Fry</label>
                                </div>
                                <div className="checkbox">
                                    <input type="checkbox" ref="satEnabled" id="satEnabled" value="1"/>
                                    <label htmlFor="satEnabled">Sat</label>
                                </div>
                                <div className="checkbox">
                                    <input type="checkbox" ref="sunEnabled" id="sunEnabled" value="1"/>
                                    <label htmlFor="sunEnabled">Sun</label>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label className="col-sm-3 control-label">Enabled for Eat In:</label>
                            <div className="col-sm-9">
                                <div className="checkbox">
                                    <input type="checkbox" ref="eatInEnabled" id="eatInEnabled" value="1"/>
                                    <label htmlFor="eatInEnabled"></label>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label className="col-sm-3 control-label">Enabled for Eat Out:</label>
                            <div className="col-sm-9">
                                <div className="checkbox">
                                    <input type="checkbox" ref="eatOutEnabled" id="eatOutEnabled" value="1"/>
                                    <label htmlFor="eatOutEnabled"></label>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label className="col-sm-3 control-label">Meal Deal:</label>
                            <div className="col-sm-9">
                                <div className="checkbox">
                                    <input type="checkbox" ref="mealDeal" id="mealDeal" value="1"/>
                                    <label htmlFor="mealDeal"></label>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                            <label className="col-sm-3 control-label">From and To Dates or Times:</label>
                            <div className="form-group col-xs-12 col-md-4 col-lg-4">
                                <input  ref="fromDuration" className="form-control" id="fromDuration"  type="text"/>
                            </div>
                            <div className="form-group col-xs-12 col-md-4 col-lg-4">
                                <input  ref="toDuration" className="form-control" id="toDuration"  type="text"/>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label className="col-sm-3">Number of Meal Deal Groups:</label>
                            <div className="col-sm-9">
                                <select ref="meal_deal_groups" className="form-control">
                                   <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="category_id">Category:</label>
                            <input  ref="category_id" className="form-control" id="category"  type="text"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="product_id">Product:</label>
                            <input  ref="product_id" className="form-control" id="product"  type="text" />
                        </div>
                        <div className="buttonBtn">
                          <div className="cancelBtn">
                             <input className="cancel btn-danger" onClick={this.back} value="Back" type="button" id=""/>
                          </div>
                          <div className="pull-right add-otherBtn">
                             <input class onClick={this.addPromotion} value="Add Promotion" type="button" id=""/>
                          </div>
                        </div>
                    </div>
                </div>
            </div>



        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      ProductData:state.Product,

      CompanyType:state.Company.ListCustomerType
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(PromotionAdder));
