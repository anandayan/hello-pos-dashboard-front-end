import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';
import qq from '../../../img/question-mark.png';


import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Products">
         <p>This page contains a list of all of your products. Use the category dropdown to filter the products. You can edit and delete your products or add a new one by clicking on the 'Add Product' button.</p>
    </Popover>
  );

let ProductList = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue',productListRquested:false,ProductListData:null,
             deleteClicked:false,editClicked:false
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

      let nexPro = nextProps.ProductData.ProductList;
      if(this.state.productListRquested && nexPro){
        if(nexPro.status_code === 200){
          this.setState({ProductListData:nexPro.data,productListRquested:false})
        }

      }
      let nexProDelet = nextProps.ProductData.ProductDelete;
      if(this.state.deleteClicked && nexProDelet){
        if(nexProDelet.status_code === 200){
          this.getProductList();
          this.setState({deleteClicked:false});
        }
      }
    },
    componentDidMount() {
      this.getProductList();

    },
    getProductList(){
   
      this.props.dispatch(ProductActions.getListProductData());
      this.setState({productListRquested:true})
    },
    AddProduct(){
        this.props.router.push('/management/products/add-product');
    },

    editproduct(){

    },

    deleteProduct(event){
      let data = {};
      
      data.id = event.target.dataset.id;
      this.setState({deleteClicked:true});
      this.props.dispatch(ProductActions.deleteProduct(data));
    },

    editproduct(event){
      let data = {};
      let id = event.target.dataset.id;
      let ProductListData = this.props.ProductData.ProductList.data;

      data = _.filter(ProductListData, function (i) {
      return i._id == id ;
      });

      //this.props.ProductData.ProductById = data;
        this.props.dispatch(ProductActions.productById(data[0]));

       this.props.router.push('management/products/edit-product');
      //this.setState({editClicked:true})
    },
    generateProductList(){
        let productList = this.state.ProductListData;
        let productListTemplate = productList ?  productList.map(function (listItem, key) {
        return (<tr key={key}> <td>{listItem.name}</td> <td>{listItem.description}</td> <td>{listItem.bar_code}</td> <td>{listItem.category_id && listItem.category_id}</td>   <td><span data-id={listItem._id} onClick={this.editproduct} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon glyphicon-trash" onClick={this.deleteProduct}></span></td></tr>)
        }, this):("No Data Found");
        return productListTemplate ;
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    render() {
      let list = this.generateProductList();
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">

                    </div>
                    <div className="common-info">
                     <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                        <h1 className="title">Products <img src = {qq} alt="qus mark"/></h1>
                        </OverlayTrigger>
                        <input type="button" onClick={this.AddProduct} className="add-btn pull-right" value="Add Product" />
                    </div>
                    
                    <div className="filter-by">
                            
                        <div className="checkbox-chk">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <p>Show additional information</p>
                            <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="ShowPriceDetails" id="ShowPriceDetails" value="1"/>
                                <label htmlFor="ShowPriceDetails"> Show price details </label>
                            </div>
                            <div className="checkbox checkbox-inline">
                                <input type="checkbox" ref="ShowSupplierDetails" id="ShowSupplierDetails" value="1"/>
                                <label htmlFor="ShowSupplierDetails"> Show Supplier details </label>
                            </div>
                           <div className="checkbox checkbox-inline">
                               <input type="checkbox" ref="ShowTillDetails" id="ShowTillDetails" value="1"/>
                               <label htmlFor="ShowTillDetails"> Show Till details </label>
                            </div>
                        </div>
                    </div>
                    </div>

                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Discription</th>
                                    <th>Barcode:</th>
                                    <th>Category</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                              {list}
                            </tbody>
                        </table>
                    </div>

                    <div className="export-to-file">
                         <h2>Export to:</h2>
                         <ul className="export-btns">

                         </ul>
                     </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
     UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(ProductList));
