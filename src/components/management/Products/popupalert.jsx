import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';
import qq from '../../../img/question-mark.png';
import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Popup Notes">
         <p>Popup Notes can be attached to a product. On this page you can view, edit and delete your Popup Notes. To add a new one, hit the 'Add Popup Note' button at the top of the page.
                         "Name" is used to identify this Popup Note when using the Back Office.
                        "Message" will be displayed to the user when the product is added to the current transaction on the Till.</p>
    </Popover>
  );

let PopUpAlert = React.createClass({
    getInitialState() {
        return {
             PopListRquested:false,deleteClicked:false,editClicked:false,PopListData:null,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        let nextPopUP = nextProps.ProductData.GetPopUpBrand;
            if(this.state.PopListRquested && nextPopUP){
                if(nextPopUP.status_code === 200){
                    this.setState({PopListData:nextPopUP.data,PopListRquested:false})
                }
            }

            let nextPopDelete = nextProps.ProductData.DeletePopUpBrand;
            if(this.state.deleteClicked && nextPopDelete){
                if(nextPopDelete.status_code === 200){
                  this.getPopList();
                  this.setState({deleteClicked:false});
                }
            }
    },
    componentDidMount() {
        this.getPopList();
    },

    getPopList(){
      let data = {};
       
      this.props.dispatch(ProductActions.getListPopUpNoteData(data));
      this.setState({PopListRquested:true})
    },

    Addpopup(){
        this.props.router.push('/management/products/popupalert/add-popupalert');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },

   deletePop(event){
      let data = {};
      data.id = event.target.dataset.id;

      this.setState({deleteClicked:true});
      this.props.dispatch(ProductActions.deletePopUpNote(data));
    },

   editPop(event){
      let data = {};
      let id = event.target.dataset.id;
      let PopListData = this.props.ProductData.GetPopUpBrand.data;

      data = _.filter(PopListData, function (i) {
      return i._id == id ;
      });

      //this.props.ProductData.ProductById = data;
        this.props.dispatch(ProductActions.popupById(data[0]));

       this.props.router.push('management/products/edit-popup');
      //this.setState({editClicked:true})
},

   generatePopupList(){
        let PopUpList = this.state.PopListData;
        let PopupTemplate = PopUpList ?  PopUpList.map(function (listItem, key) {
        return (<tr key={key}> <td>{listItem.name}</td> <td>{listItem.msg}</td><td>{listItem.show_per_trans ? "Yes" : "No"}</td><td><span data-id={listItem._id} onClick={this.editPop} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deletePop}></span></td></tr>)
        }, this):("No Data Found");
        return PopupTemplate ;
    },
    render() {
        let list = this.generatePopupList();
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">

                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">
                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                        <h1 className="title">Popup Notes <img src = {qq} alt="qus mark"/></h1>
                    </OverlayTrigger>
                        <input type="button" onClick={this.Addpopup} className="add-btn pull-right" value="Add Popup" />
                    </div>
                    

                    <div className="filter-by">
                      <div className="col-sm-4">
                          <span className="help-block txtalignRight">Filter by Name or Message:</span>
                      </div>
                      <div className="col-sm-8">
                        <input ref="role_discription" className="form-control input-text-fields" id="role_discription"  type="text"   autoComplete="off"/>
                        <input className="add-other" value="Search" type="button" id=""/>
                      </div>
                    </div>

                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Message</th>
                                    <th>Show once per transaction</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
     UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(PopUpAlert));
