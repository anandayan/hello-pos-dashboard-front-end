import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { WithContext as ReactTags } from 'react-tag-input';
import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';


let AddMultilpeChoiceNotes = React.createClass({

    getInitialState() {
        return {
             someState:'SomeValue',listCalled:false,listData:null,tags:[]

        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
      //debugger;

    },
    componentDidMount() {

    },
    addMultiChoiseNotes(){
        let data = {};
        data.name  = this.refs.name.value;
        data.popup_by_default = this.refs.popup_by_default.checked ? 1 : 0;
        data.choices = this.state.tags.length ? JSON.stringify(this.state.tags) : "";
        this.props.dispatch(ProductActions.addMultiChoiceNoteData(data));

    },
    handleDelete(i) {
      let tags = this.state.tags;
      tags.splice(i, 1);
      this.setState({tags: tags});
  }
,
  handleAddition(tag) {

      let tags = this.state.tags;
      tags.push({
          id: tags.length + 1,
          text: tag
      });
      this.setState({tags: tags});
  }
,
    render() {

      let style = {
        marginLeft: '20px',
        marginTop: '8px',
      };

        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">

                    </div>
                    <div className="common-info">

                    <h1 className="title">Add Notes</h1>
                    </div>


                    <div className="filter-by-name">

                      <div className="col-sm-4">
                        <label htmlFor="filter_category">Name:</label>
                        <input ref="name" className="form-control" id="role_discription"  type="text"    autoComplete="off"/>
                      </div>


                      <div className="col-sm-4">
                          <label class htmlFor="popup_by_default"> Popup By Default </label>
                          <div className="checkbox checkbox">
                              <input type="checkbox" ref="popup_by_default" id="popup_by_default"  />
                              <label htmlFor="popup_by_default" style={style}>  </label>
                          </div>

                      </div>
                      <div className="col-sm-12 col-md-12">
                        <label htmlFor="filter_category">Choices:</label>
                        <ReactTags  tags={this.state.tags} placeholder = "Add new choice"  handleDelete={this.handleDelete}   handleAddition={this.handleAddition} />
                      </div>
                        <p className="col-sm-12 col-md-12">
                        <button className="btn btn-default pull-right" onClick={this.addMultiChoiseNotes} >Add Notes</button>
                        </p>
                    </div>


                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
   UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddMultilpeChoiceNotes));
