import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';
import qq from '../../../img/question-mark.png';
import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Promotions">
         <p>This page contains a list of all of the promotions within your business. 'Show Details' will give you a more detailed view of the promotion.</p>
    </Popover>
  );

let ProductPromotion = React.createClass({
    getInitialState() {
        return {
             PromotionListData:null,promotionListRquested:false,deleteClicked:false,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
        let nexPro = nextProps.ProductData.GetPromotion;
        if(this.state.promotionListRquested && nexPro){
            if(nexPro.status_code === 200){
          this.setState({PromotionListData:nexPro.data,promotionListRquested:false})
            }
        }
        let nextPromotionDelete = nextProps.ProductData.DeletePromotion;
            if(this.state.deleteClicked && nextPromotionDelete){
                if(nextPromotionDelete.status_code === 200){
                  this.getPromotionList();
                  this.setState({deleteClicked:false});
                }
            }
    },
    componentDidMount() {
        this.getPromotionList();
    },

    getPromotionList(){
      let data = {};
    
      this.props.dispatch(ProductActions.getPromotionData(data));
      this.setState({promotionListRquested:true})
    },

    deletePomotion(event){
      let data = {};
      data.id = event.target.dataset.id;

      this.setState({deleteClicked:true});
      this.props.dispatch(ProductActions.deltePromotionData(data));
    },

    editproduct(event){
      let data = {};
      let id = event.target.dataset.id;
      let PromotionListData = this.props.ProductData.GetPromotion.data;

      data = _.filter(PromotionListData, function (i) {
      return i._id == id ;
      });

      //this.props.ProductData.ProductById = data;
        this.props.dispatch(ProductActions.PromtionById(data[0]));

       this.props.router.push('/management/products/edit-promotion');
      //this.setState({editClicked:true})
    },

        
    AddPromotion(){
        this.props.router.push('/management/products/promotions/add-promotions');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
   generatePromotionList(){
        let promotionList = this.state.PromotionListData;
        let promotionListTemplate = promotionList ?  promotionList.map(function (listItem, key) {
        return (<tr key={key}> <td>{listItem.name}</td> 
                <td>{listItem.description}</td> 
                <td>{listItem.duration}</td> 
                <td>{listItem.fromDuration}</td> 
                <td>{listItem.toDuration}</td> 
                <td><span data-id={listItem._id} onClick={this.editproduct} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deletePomotion}></span></td></tr>)
        }, this):("No Data Found");
        return promotionListTemplate ;
    },
    render() {
        let list = this.generatePromotionList();
        return (

            <div className="">
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                  
                        <div className="common-info">
                        <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                            <h1 className="title">Promotions <img src = {qq} alt="qus mark"/></h1>
                         </OverlayTrigger>
                            <button  onClick={this.AddPromotion} type="button" className="pull-right btn btn-primary add-staff-btn">Add Promotions
                            </button>
                        </div>
                  
                    
                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Discription</th>
                                    <th>duration</th>
                                    <th>Starts</th>
                                    <th>Ends</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                              {list}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
       UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(ProductPromotion));
