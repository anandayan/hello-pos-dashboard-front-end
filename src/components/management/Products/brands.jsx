import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import _ from 'lodash';
import * as ProductActions from '../../../actions/management/product_actions';
import { bindActionCreators } from 'redux';
import { Popover,OverlayTrigger } from 'react-bootstrap';
import qq from '../../../img/question-mark.png';
const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Add Brands">
     <p>On this page you can view, edit and delete your Brands. To add a new one, hit the 'Add Brand' button at the top of the page.</p>
                        <p>Brands created here can then be selected when creating a new product. Brands can be used to group products for reporting purposes.</p>
    </Popover>
  );

let Brands = React.createClass({
    getInitialState() {
        return {
             BrandListRquested:false,deleteClicked:false,editClicked:false,BrandListData:null,
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {
            let nextBrand = nextProps.ProductData.GetBrand;
            if(this.state.BrandListRquested && nextBrand){
                if(nextBrand.status_code === 200){
                    this.setState({BrandListData:nextBrand.data,BrandListRquested:false})
                }
            }

            let nextBrandDelete = nextProps.ProductData.DeleteBrand;
            if(this.state.deleteClicked && nextBrandDelete){
                if(nextBrandDelete.status_code === 200){
                  this.getBrandList();
                  this.setState({deleteClicked:false,BrandListRquested:true});
                }
            }
    },
    componentDidMount() {
        this.getBrandList();
    },

    getBrandList(){
        
      this.props.dispatch(ProductActions.getListBrandData());
      this.setState({BrandListRquested:true})
    }, 
    AddBrands(){
        this.props.router.push('/management/products/brands/add-brand');
    },
    

   deleteProduct(event){
      let data = {};     
      data.id = event.target.dataset.id;
      this.setState({deleteClicked:true});
      this.props.dispatch(ProductActions.deleteBrand(data));
    },

   editbrand(event){
      let data = {};
      let id = event.target.dataset.id;
      let BrandListData = this.props.ProductData.GetBrand.data;
      
      data = _.filter(BrandListData, function (i) {
      return i._id == id ;
      });
      
        this.props.dispatch(ProductActions.brandById(data[0]));

       this.props.router.push('/management/products/edit-brands');
      //this.setState({editClicked:true})
},
   generateBrandList(){
        let BrandList = this.state.BrandListData;
        let BrandTemplate = BrandList ?  BrandList.map(function (listItem, key) {
        return (<tr key={key}> <td>{listItem.name}</td> <td>{listItem.description}</td><td><span data-id={listItem._id} onClick={this.editbrand} className="edit glyphicon glyphicon-edit "></span></td><td><span data-id={listItem._id} className="delete glyphicon glyphicon-trash" onClick={this.deleteProduct}></span></td></tr>)
        }, this):("No Data Found");
        return BrandTemplate ;
    },
    render() {
        let list = this.generateBrandList();
        return (

            <div class>
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    
                    <div className="common-info">
                       
                        <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
                        <h1 className="title">Brands <img src = {qq} alt="qus mark"/></h1>
                    </OverlayTrigger>
                        <input type="button" onClick={this.AddBrands} className="add-btn pull-right" value="Add Brands" />
                    </div>
                     
                    <div className="filter-by-brand">
                      <div className="col-sm-4">
                          <span className="help-block txtalignRight">Filter by Brand or Description:</span>
                      </div>
                      <div className="col-sm-8">
                        <input ref="role_discription" className="form-control input-text-fields" id="role_discription"  type="text"   autoComplete="off"/>
                        <input className="add-other" value="Search" type="button" id=""/>
                      </div>
                    </div>

                    <div className="staff-table">
                    <table className="table table-striped staff-list-tabel">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Discription</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        {list}
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
     UserDetail:state.User,
      ProductData:state.Product,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(ProductActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(Brands));
