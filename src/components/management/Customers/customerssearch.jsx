import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import qq from '../../../img/question-mark.png';
import * as CustomersActions from '../../../actions/management/customer_actions';
import { bindActionCreators } from 'redux';


import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Customer Search">
        <p>Drive brand loyalty through customer interaction with apps from the Epos Now App Store</p>
                        <p>On this page you can view, edit and delete your Customers. To add a new one, hit the 'Add Customer' button at the top of the page.</p>
                        <p>Use the search box below to find a certain customer that has been stored on your database. Either insert their name or postcode.</p>
    </Popover>
  );

let CustomerSearch = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    AddCustomer(){
        this.props.router.push('/management/Customers/customerssearch/add-customer');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    render() {
        return (

            <div class>
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">

                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>

                        <h1 className="title">Customer Search <img src = {qq} alt="qus mark"/></h1>
                    </OverlayTrigger>
                        <input type="button" onClick={this.AddCustomer} className="add-btn pull-right" value="Add Customer" />
                    </div>
                   
                    <div className="filter-by">
                        <div className="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <label htmlFor="filter_category">Filter by Category:</label>
                            <select  className="form-control">
                                <option selected="selected" value="">* Show All</option>
                                <option value="16547">Basic</option>
                                <option value="16894">Public services</option>
                           </select>
                        </div>
                    </div>
                    <div className="filter-by">
                      <div className="col-sm-3">
                          <span className="help-block txtalignRight">Filter by Name, Business, Main Address or Contact Number:</span>
                      </div>
                      <div className="col-sm-8">
                        <input ref="role_discription" className="form-control input-text-fields" id="role_discription"  type="text"   autoComplete="off"/>
                        <input className="add-other" value="Search" type="button" id=""/>
                      </div>
                    </div>

                    <div className="staff-table">
                        <table className="table table-striped staff-list-tabel">
                            <thead>
                                <tr>
                                    <th>TITLE</th>
                                    <th>FORENAME</th>
                                    <th>SURNAME</th>
                                    <th>BUSSINESS NAME</th>
                                    <th>ADDRESS LINE 1</th>
                                    <th>TOWN</th>
                                    <th>POST CADE</th>
                                    <th>TYPE</th>
                                    <th>SIGNUP LOCATION</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
      CustomerData:state.Customer,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CustomersActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(CustomerSearch));
