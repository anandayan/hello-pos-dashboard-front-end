import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as CustomersActions from '../../../actions/management/customer_actions';
import { bindActionCreators } from 'redux';


let CustomerAdder = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    addStaff(){
        // this.props.router.push('staff/add-staff');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    render() {
        return (

            <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">

                    </div>
                    <div className="common-info">
                        <h1 className="title">Add a new Customer</h1>
                    </div>
                    <div className="filter-by">
                    <h3>Customer</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Title:</label>
                            <select  className="form-control">
                                <option value="0"></option>
                                <option value="1">Mr</option>
                                <option value="2">Mrs</option>
                                <option value="3">Ms</option>
                                <option value="4">Miss</option>
                                <option value="5">Dr</option>
                                <option value="6">Sir</option>
                                <option value="7">Master</option>
                                <option value="8">Prof</option>
                                <option value="9">Rev</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Forename:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Surname</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Business Name:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Date of Birth:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Contact Number:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Contact Number 2:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Email Address:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Max Credit:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Sign Up Location:</label>
                            <select  className="form-control">
                                <option selected="selected" value="">Select Location</option>
                                <option value="15875">Hornchurch</option>
                           </select>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Type:</label>
                            <select  className="form-control">
                                <option selected="selected" value="">None</option>
                                <option value="16547">Basic</option>
                                <option value="16894">Public services</option>
                           </select>
                           <div className="margt10">
                               <input className="add-other" value="Create type" type="button" id=""/>
                           </div>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Current Balance:   </label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                    </div>
                    <div className="filter-by">
                        <h3>Add New Additional Member</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Forename</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Surname</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Date of Birth:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                    </div>
                    <div className="filter-by">
                        <h3>Add New Address</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">FAddress Name: </label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Address Line 1:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Address Line 2:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Town:</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">County</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="filter_category">Postcode</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                            <div className="margt10">
                                <input className="add-other" value="Search" type="button" id=""/>
                            </div>
                        </div>
                    </div>
                    <div className="buttonBtn">
                        <div className="cancelBtn">
                           <input className="cancel btn-danger" value="Back" type="button" id=""/>
                        </div>
                        <div className="pull-right add-otherBtn">
                           <input class  value="Add" type="button" id=""/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CustomerData:state.Customer,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CustomersActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(CustomerAdder));
