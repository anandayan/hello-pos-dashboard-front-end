import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import qq from '../../../img/question-mark.png';
import * as CustomersActions from '../../../actions/management/customer_actions';
import { bindActionCreators } from 'redux';


import { Popover,OverlayTrigger } from 'react-bootstrap';

const popoverHoverFocus = (
    <Popover id="popover-trigger-hover-focus" title="Invoice Message">
         <p>Invoice Messages can be used to save and quickly select commonly-used Customer Invoice messages.</p>
        <p>No invoice messages have been created. Use the panel below to create a new invoice message.</p>
    </Popover>
  );

let InvoiceMeg = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    addStaff(){
        this.props.router.push('staff/add-staff');
    },
    menuClicked(event) {
     this.props.router.push(event.target.dataset.routerpath)
   },
    render() {
        return (

            <div class>
              
                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="common-info">

                    <OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>

                        <h1 className="title">Invoice Message <img src = {qq} alt="qus mark"/></h1>
                        </OverlayTrigger>
                    </div>
                   
                    
                    <div className="filter-by">
                        <h3>Invoice Message</h3>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="filter_category">Message Name</label>
                            <input ref="filter_category" className="form-control"   type="text"   autoComplete="off"/>
                        </div>
                        <div className="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label htmlFor="filter_category">Message:</label>
                            <textarea className="form-control" rows="5" colspan="10" id="comment"></textarea>
                        </div>
                        <div className="buttonBtn">
                            <div className="cancelBtn">
                               <input className="cancel btn-danger" value="Back" type="button" id=""/>
                            </div>
                            <div className="pull-right add-otherBtn">
                               <input class  value="Save" type="button" id=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserDetail:state.User,
       CustomerData:state.Customer,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(CustomersActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(InvoiceMeg));
