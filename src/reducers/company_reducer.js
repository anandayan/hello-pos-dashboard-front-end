import * as CompanyActions from './../actions/setup/company_actions';
import extend from 'lodash/extend';


export var Company = (state = {
      UpdateCompanyAdded:null,
      GetCompanyAdded:null,
      AddReciptAdded:null,
      GetReciptAdded:null,
      UpdateReciptAdded:null,
      DeleteReciptAdded:null,
      AddDisscount:null,
      GetDisscount:null,
      UpdateDisscount:null,
      DeleteDisscount:null,
      AddRefund:null,
      GetRefund:null,
      UpdateRefund:null,
      DeleteRefund:null,
      AddNoSale:null,
      GetNoSale:null,
      UpdateNoSale:null,
      DeleteNoSale:null,
      AddStockMovement:null,
      GetStockMovement:null,
      UpdateStockMovement:null,
      DeleteStockMovement:null,
      AddCourse:null,
      GetCourse:null,
      UpdateCourse:null,
      DeleteCourse:null,
      AddClock:null,
      GetClockType:null,
      UpdateClock:null,
      DeleteClock:null,
      noSaleById:null,
      disscountById:null,
      refundById:null,
      StockMovementById:null,
      CourseById:null,
      CustomerType:null,
      ListCustomerType:null,
      DeleteCustomerType:null,
      UpdateCustomerType:null,
      ClockById:null,
      customerTypeById:null,
  }, action) => {

    switch (action.type) {

        case 'persist/REHYDRATE': {
           if (action.payload && action.payload.Company) {
                return extend({}, state, action.payload.Company);
           } else {
               return state;
           }
       }
       case CompanyActions.UPDATE_COMPANY_DETAILS:
            {
                return extend({}, state, {

                    UpdateCompanyAdded:action.data

                });
            }
            break;
            case CompanyActions.GET_COMPANY_DETAILS:
            {
                return extend({}, state, {

                    GetCompanyAdded:action.data

                });
            }
            break;
            
            case CompanyActions.ADD_RECEIPT_DETAILS:
            {
                return extend({}, state, {

                    AddReciptAdded:action.data

                });
            }

            break;
            case CompanyActions.GET_RECEIPT_DETAILS:
            {
                return extend({}, state, {

                    GetReciptAdded:action.data

                });
            }
            
            break;
            case CompanyActions.UPDATE_RECEIPT_DETAIL:
            {
                return extend({}, state, {

                    UpdateReciptAdded:action.data

                });
            }
            
            break;
            case CompanyActions.DELETE_RECEIPT_DETAIL:
            {
                return extend({}, state, {

                    DeleteReciptAdded:action.data

                });
            }
            
            break;
            case CompanyActions.ADD_DISSCOUNT:
            {
                return extend({}, state, {

                    AddDisscount:action.data

                });
            }
            break;
            case CompanyActions.GET_DISSCOUNT:
            {
                return extend({}, state, {

                    GetDisscount:action.data

                });
            }
            break;
            case CompanyActions.UPDATE_DISSCOUNT:
            {
                return extend({}, state, {

                    UpdateDisscount:action.data

                });
            }
            break;
            case CompanyActions.DELETE_DISSCOUNT:
            {
                return extend({}, state, {

                    DeleteDisscount:action.data

                });
            }
            break;
            case CompanyActions.ADD_REFUND:
            {
                return extend({}, state, {

                    AddRefund:action.data

                });
            }
            break;
            case CompanyActions.GET_REFUND:
            {
                return extend({}, state, {

                    GetRefund:action.data

                });
            }
            break;
            case CompanyActions.UPDATE_REFUND:
            {
                return extend({}, state, {

                    UpdateRefund:action.data

                });
            }
            break;
            case CompanyActions.DELETE_REFUND:
            {
                return extend({}, state, {

                    DeleteRefund:action.data

                });
            }
            break;
            case CompanyActions.ADD_NOSALE:
            {
                return extend({}, state, {

                    AddNoSale:action.data

                });
            }
            break;

            case CompanyActions.GET_NOSALE:
            {

                return extend({}, state, {

                    GetNoSale:action.data

                });
            }
            break;
            case CompanyActions.UPDATE_NOSALE:
            {
                return extend({}, state, {

                    UpdateNoSale:action.data

                });
            }
            break;
            case CompanyActions.DELETE_NOSALE:
            {
                return extend({}, state, {

                    DeleteNoSale:action.data

                });
            }
            break;
            case CompanyActions.ADD_STOCKMOVEMENT:
            {
                return extend({}, state, {

                    AddStockMovement:action.data

                });
            }
            break;
            case CompanyActions.GET_STOCKMOVEMENT:
            {
                return extend({}, state, {

                    GetStockMovement:action.data

                });
            }
            break;
            case CompanyActions.UPDATE_STOCKMOVEMENT:
            {
                return extend({}, state, {

                    UpdateStockMovement:action.data

                });
            }
            break;
            case CompanyActions.DELETE_STOCKMOVEMENT:
            {
                return extend({}, state, {

                    DeleteStockMovement:action.data

                });
            }
            break;
            case CompanyActions.ADD_COURSE:
            {
                return extend({}, state, {

                    AddCourse:action.data

                });
            }
            break;
            case CompanyActions.GET_COURSE:
            {
                return extend({}, state, {

                    GetCourse:action.data

                });
            }
            break;
            case CompanyActions.UPDATE_COURSE:
            {
                return extend({}, state, {

                    UpdateCourse:action.data

                });
            }
            break;
            case CompanyActions.DELETE_COURSE:
            {
                return extend({}, state, {

                    DeleteCourse:action.data

                });
            }
            break;
            case CompanyActions.ADD_CLOCK:
            {
                return extend({}, state, {

                    AddClock:action.data

                });
            }
            break;
            case CompanyActions.GET_CLOCK:
            {
                return extend({}, state, {

                    GetClockType:action.data

                });
            }
            break;
            case CompanyActions.UPDATE_CLOCK:
            {
                return extend({}, state, {

                    UpdateClock:action.data

                });
            }
            break;
            case CompanyActions.DELETE_CLOCK:
            {
                return extend({}, state, {

                    DeleteClock:action.data

                });
            }

            break;
            case CompanyActions.NOSALE_BY_ID:
            {
                return extend({}, state, {

                    noSaleById:action.data

                });
            }
            break;
            case CompanyActions.DISSCOUNT_BY_ID:
            {
                return extend({}, state, {

                    disscountById:action.data

                });
            }
            break;
            case CompanyActions.REFUND_BY_ID:
            {
                return extend({}, state, {

                    refundById:action.data

                });
            }
            break;
            case CompanyActions.STOCKMOVEMENT_BY_ID:
            {
                return extend({}, state, {

                    StockMovementById:action.data

                });
            }
            break;
            case CompanyActions.COURSE_BY_ID:
            {
                return extend({}, state, {

                    CourseById:action.data

                });
            }
            break;
            case CompanyActions.ADD_CUSTOMER_TYPE: {
                return extend({}, state, {
                    CustomerType:action.data
                });
            }
            break;
            case CompanyActions.LIST_CUSTOMER_TYPE: {
                return extend({}, state, {
                    ListCustomerType:action.data
                });
            }
            break;
            case CompanyActions.DELETE_CUSTOMER_TYPE: {
                return extend({}, state, {
                    DeleteCustomerType:action.data
                });
            }
            break;
            case CompanyActions.UPDATE_CUSTOMER_TYPE: {
                return extend({}, state, {
                    UpdateCustomerType:action.data
                });
            }
            break;
            case CompanyActions.CLOCK_BY_ID: {
                return extend({}, state, {
                    ClockById:action.data
                });
            }
            break;
            case CompanyActions.CUSTOMER_BY_ID: {
                return extend({}, state, {
                    customerTypeById:action.data
                });
            }
            break;

            case CompanyActions.LOGOUT: {
              return extend({}, state, {



              });
            }
        default:
            return state;
    }
};
