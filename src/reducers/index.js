import { User } from './user_reducer';
import { Staff } from './staff_reducer';
import { Customer } from './customer_reducer';
import { Stock } from './stock_reducer';
import { Product } from './product_reducer';
import { Company } from './company_reducer';
import { Location } from './location_reducer';
import { Banking } from './banking_reducer';
import { Restores } from './restore_reducer';

import { Dashboard } from './dashboard_reducer';
import { Transaction } from './transactions_reducer';
import { Sales } from './sales_reducer';
import { Stocks } from './stocks_reducer';
import { ReportCustomer } from './report_customer_reducer';
import { ReportBanking } from './report_banking_reducer';
import { Accounting } from './accounting_reducer';
import { Auditing } from './auditing_reducer';
import { ScheduleEmail } from './schedule_email_reducer';

import { combineReducers } from 'redux';


let AppReducer = combineReducers({
    User,
    Staff,
    Product,
    Customer,
    Stock,
    Company,
    Location,
    Banking,
    Restores,
    Dashboard,
    Transaction,
    Sales,
    Stocks,
    ReportCustomer,
    ReportBanking,
    Accounting,
    Auditing,
    ScheduleEmail,
});
export default AppReducer;
