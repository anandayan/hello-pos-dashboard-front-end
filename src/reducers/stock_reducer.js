import * as StockActions from './../actions/management/stock_actions';
import extend from 'lodash/extend';


export var Stock = (state = {

  SupplierAdder:null,
  GetSupplier:null,
  DeleteSupplier:null,
  UpdateSupplier:null,
  SupplierById:null,
      
  }, action) => {
    // ;
    switch (action.type) {

        case 'persist/REHYDRATE': {
           if (action.payload && action.payload.Stock) {
                return extend({}, state, action.payload.Stock);
           } else {
               return state;
           }
       }
            break;
            case StockActions.ADD_SUPPLIER:
            {
                return extend({}, state, {

                    SupplierAdder:action.data

                });
            }
            break;
        case StockActions.GET_SUPPLIER:
            {
              return extend({}, state, {

                  GetSupplier:action.data

              });
            }
            break;
         case StockActions.DELETE_SUPPLIER:
            {
              return extend({}, state, {

                  DeleteSupplier:action.data

              });
            }
            break;

         case StockActions.UPDATE_SUPPLIER:
            {
              return extend({}, state, {

                  UpdateSupplier:action.data

              });
            }
            break;
            case StockActions.SUPPLIER_BY_ID:
            {
              return extend({}, state, {

                  SupplierById:action.data

              });
            }
            break;
            case StockActions.LOGOUT: {
              return extend({}, state, {

                  

              });
            }
        default:
            return state;
    }
};
