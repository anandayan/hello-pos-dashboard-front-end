import * as ProductActions from './../actions/management/product_actions';
import extend from 'lodash/extend';


export var Product = (state = {
      ProductAdded:null,
      ProductList:null,
      ProductDelete:null,
      ProductUpdate:null,
      ProductById:null,
      BrandAdd:null,
      GetBrand:null,
      DeleteBrand:null,
      UpdateBrand:null,
      brandById:null,
      PopUpBrandAdded:null,
      GetPopUpBrand:null,
      DeletePopUpBrand:null,
      UpdatePopUpBrand:null,
      popupById:null,
      MultichoiceNoteAdded:null,
      GetMultichoiceNote:null,
      DeleteMultichoiceNote:null,
      UpdateMultichoiceNote:null,
      MiscProductAdded:null,
      GetMiscProduct:null,
      DeleteMiscProduct:null,
      UpdateMiscProduct:null,
      MiscProductById:null,
      AddPromotion:null,
      GetPromotion:null,
      DeletePromotion:null,
      UpdatePromotion:null,
      PromtionById:null,
      CategoriesAdded:null,
      CategoriesList:null,
      CategoriesDeleted:null,
      CategoriesUpdateded:null,
  }, action) => {
     
    switch (action.type) {

        case 'persist/REHYDRATE': {
           if (action.payload && action.payload.Product) {
                return extend({}, state, action.payload.Product);
           } else {
               return state;
           }
       }
            break;
      case ProductActions.ADD_PRODUCT :{
        return extend({}, state, {

            ProductAdded:action.data

        });
      }
      break;
      case ProductActions.LIST_PRODUCT :{
        return extend({}, state, {

            ProductList:action.data

        });
      }
      break;

      case ProductActions.PRODUCT_BY_ID :{
        return extend({}, state, {
          ProductById:action.data

        });
      }

      break;
      case ProductActions.UPDATE_PRODUCT :{
        return extend({}, state, {
          ProductUpdate:action.data

        });
      }

      break;
      case ProductActions.DELETE_PRODUCT :{
        return extend({}, state, {

            ProductDelete:action.data

        });
      }
      break;
      case ProductActions.ADD_BRAND :{
        return extend({}, state, {

            BrandAdd:action.data

        });
      }
      break;
      case ProductActions.GET_BRAND :{
        return extend({}, state, {

            GetBrand:action.data

        });
      }
      break;
      case ProductActions.DELETE_BRAND :{
        return extend({}, state, {

            DeleteBrand:action.data

        });
      }
      break;
      case ProductActions.UPDATE_BRAND :{
        return extend({}, state, {

            UpdateBrand:action.data

        });
      }
      break;
      case ProductActions.BRAND_BY_ID :{
        return extend({}, state, {

            brandById:action.data

        });
      }
      break;
      case ProductActions.ADD_POPUP_NOTE :{
        return extend({}, state, {

            PopUpBrandAdded:action.data

        });
      }
      break;
      case ProductActions.GET_POPUP_NOTE :{
        return extend({}, state, {

            GetPopUpBrand:action.data

        });
      }
      break;
      case ProductActions.DELETE_POPUP_NOTE :{
        return extend({}, state, {

            DeletePopUpBrand:action.data

        });
      }
      break;
      case ProductActions.UPDATE_POPUP_NOTE :{
        return extend({}, state, {

            UpdatePopUpBrand:action.data

        });
      }
      break;
      case ProductActions.POPUP_NOTE_BY_ID :{
        return extend({}, state, {

            popupById:action.data

        });
      }
      break;
      case ProductActions.ADD_MULTICHOICE_NOTE :{
        return extend({}, state, {

            MultichoiceNoteAdded:action.data

        });
      }
      break;
      case ProductActions.GET_MULTICHOICE_NOTE :{
        return extend({}, state, {

            GetMultichoiceNote:action.data

        });
      }
      break;
      case ProductActions.DELETE_MULTICHOICE_NOTE :{
        return extend({}, state, {

            DeleteMultichoiceNote:action.data

        });
      }
      break;
      case ProductActions.UPDATE_MULTICHOICE_NOTE :{
        return extend({}, state, {

            UpdateMultichoiceNote:action.data

        });
      }
      break;
      case ProductActions.ADD_MISC_PRODUCT :{
        return extend({}, state, {

            MiscProductAdded:action.data

        });
      }
      break;
      case ProductActions.GET_MISC_PRODUCT :{
        return extend({}, state, {

            GetMiscProduct:action.data

        });
      }
      break;
      case ProductActions.DELETE_MISC_PRODUCT :{
        return extend({}, state, {

            DeleteMiscProduct:action.data

        });
      }
      break;
      case ProductActions.UPDATE_MISC_PRODUCT :{
        return extend({}, state, {

            UpdateMiscProduct:action.data

        });
      }
      break;
      case ProductActions.MISC_PRODUCT_BY_ID :{
        return extend({}, state, {

            MiscProductById:action.data

        });
      }
      break;


            case ProductActions.ADD_CATEGORY :{
              return extend({}, state, {

                  CategoriesAdded:action.data

              });
            }
            break;

            case ProductActions.LIST_CATEGORY :{
              return extend({}, state, {

                  CategoriesList:action.data

              });
            }
            break;

            case ProductActions.DELETE_CATEGORY :{
              return extend({}, state, {

                  CategoriesDeleted:action.data

              });
            }
            break;

            case ProductActions.UPDATE_CATEGORY :{
              return extend({}, state, {

                  CategoriesUpdateded:action.data

              });
            }
            break;
            case ProductActions.ADD_PROMOTION :{
              return extend({}, state, {

                  AddPromotion:action.data

              });
            }
            break;

            case ProductActions.GET_PROMOTION :{
              return extend({}, state, {

                  GetPromotion:action.data

              });
            }
            break;

            case ProductActions.DELETE_PROMOTION :{
              return extend({}, state, {

                  DeletePromotion:action.data

              });
            }
            break;

            case ProductActions.UPDATE_PROMOTION :{
              return extend({}, state, {

                  UpdatePromotion:action.data

              });
            }
            break;
            case ProductActions.PROMOTION_BY_ID :{
              return extend({}, state, {

                  PromtionById:action.data

              });
            }
            break;
            case ProductActions.LOGOUT: {
              return extend({}, state, {

                  ProductAdded:null,ProductList:null

              });
            }
        default:
            return state;
    }
};
