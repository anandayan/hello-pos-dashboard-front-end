import * as StaffActions from './../actions/management/staff_actions';
import extend from 'lodash/extend';


export var Staff = (state = {
      StaffAdded:null,
      ListStaff:null,
      DeleteStaff:null,
      UpdateStaff:null,
      RoleAdded:null,
      UpdateRole:null,
      ListRole:null,
      StaffById:null,
      HourAdded:null,
      ListHour:null,
      UpdateHour:null,
      DeleteHour:null
  }, action) => {
    // ;
    switch (action.type) {

        case 'persist/REHYDRATE': {
           if (action.payload && action.payload.Staff) {
                return extend({}, state, action.payload.Staff);
           } else {
               return state;
           }
       }
           break;
        case StaffActions.ADD_STAFF:
            {
                return extend({}, state, {

                    StaffAdded:action.data

                });
            }
            break;
        case StaffActions.LIST_STAFF:
            {
              return extend({}, state, {

                  ListStaff:action.data

              });
            }
            break;
         case StaffActions.DELETE_STAFF:
            {
              return extend({}, state, {

                  DeleteStaff:action.data

              });
            }
            break;

         case StaffActions.UPDATE_STAFF:
            {
              return extend({}, state, {

                  UpdateStaff:action.data

              });
            }
            break;

            case StaffActions.ADD_ROLE:
            {
              return extend({}, state, {

                  RoleAdded:action.data

              });
            }
            break;

            case StaffActions.UPDATE_ROLE:
            {
              return extend({}, state, {

                  UpdateRole:action.data

              });
            }
            break;

            case StaffActions.LIST_ROLE:
            {
              return extend({}, state, {

                  ListRole:action.data

              });
            }
            break;
            case StaffActions.ADD_HOUR:
            {
              return extend({}, state, {

                  HourAdded:action.data

              });
            }
            break;
            case StaffActions.LIST_HOUR:
            {
              return extend({}, state, {

                  ListHour:action.data

              });
            }
            break;
            case StaffActions.DELETE_HOUR:
            {
              return extend({}, state, {

                  DeleteHour:action.data

              });
            }
            break;
            case StaffActions.UPDATE_HOUR:
            {
              return extend({}, state, {

                  UpdateHour:action.data

              });
            }
            break;


            case StaffActions.LOGOUT: {
              return extend({}, state, {

                  StaffAdded:null

              });
            }
        default:
            return state;
    }
};
