import * as UserActions from './../actions/user_actions';
import extend from 'lodash/extend';


export var User = (state = {
    IsLoggedIn: false,
    IsRegisterd: "",
    register:"",
    loginDetail:"",
    forgotpassword:null,
    resetpassword:null,
    updateprofile:null,
    logout:null,
    menu:null
}, action) => {
    // ;
    switch (action.type) {
        // // Uncomment below block only if you want to persist store data
        // case 'persist/REHYDRATE': {
        //
        //     if (action.payload.User) {
        //         //action.payload.User.IsLoggingIn = false;
        //     } else {
        //         return state;
        //     }
        // }
        // break;
        case 'persist/REHYDRATE': {
           if (action.payload && action.payload.User) {
               action.payload.User.IsLoggingIn = false;
               return extend({}, state, action.payload.User);
           } else {
               return state;
           }
       }
           break;
        case UserActions.USER_LOGIN:
            {
                return extend({}, state, {
                    IsLoggedIn: true,
                    LoginFailed: false,
                    IsLoggingIn: false,
                    MobileNumber: action.mobileNumber,
                    loginDetail:action.data

                });
            }
            break;
            case UserActions.REGISTERATION:
                {
                  return extend({}, state, { register: action.data });

                }
                break;

                case UserActions.USER_LOGOUT:
                {
                  return extend({}, state, {
                      IsLoggedIn:"",
                      LoginFailed:"",
                      IsLoggingIn:"",
                      loginDetail:"",
                      logout: true

                   });

                }
                break;

                case UserActions.UPDATE_PROFILE:
                  {
                      return extend({}, state, { updateprofile: action.data });
                  }
                  break;

                case UserActions.FORGOT_PASSWORD:
                  {
                      return extend({}, state, { forgotpassword: action.data });
                  }
                  break;
                case UserActions.RESET_PASSWORD:
                {
                  return extend({}, state, { resetpassword: action.data });

                }
                break;
                case UserActions.USER_LOGOUT:
                {
                  return extend({}, state, { 
                    IsLoggedIn: false,
                    IsRegisterd: "",
                    register:"",
                    loginDetail:"",
                    forgotpassword:null,
                    resetpassword:null,
                    updateprofile:null,
                    logout:null,
                    menu:null
                   });

                }
                break;
                
        default:
            return state;
    }
};
