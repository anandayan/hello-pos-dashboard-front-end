import * as LocationsActions from './../actions/setup/locations_actions';
import extend from 'lodash/extend';


export var Location = (state = {
      LocationAdded:null,
      GetLocation:null,
      DeleteLocation:null,
      UpdateLocation:null,
      DeviceAdded:null,
      GetDevice:null,
      DeleteDevice:null,
      UpdateDevice:null,
      deviceById:null,
      LocationById:null,
      AddOrderPrinter:null,
      GetOrderPrinter:null,
      DeleteOrderPrinter:null,
      UpdateOrderPrinter:null,
      AddOpeningHour:null,
      GetOpeningHour:null,
      DeleteOpeningHour:null,
      UpdateOpeningHour:null,
      OpeningHourById:null,
      orderPrinterById:null,
  }, action) => {
    // ;
    switch (action.type) {

        case 'persist/REHYDRATE': {
           if (action.payload && action.payload.Location) {
                return extend({}, state, action.payload.Location);
           } else {
               return state;
           }
        }
            break;
            case LocationsActions.ADD_LOCATION :{
            return extend({}, state, {

                LocationAdded:action.data

            });
          }
          break;
          case LocationsActions.GET_LOCATION :{
            return extend({}, state, {

                GetLocation:action.data

            });
          }
          break;
          case LocationsActions.DELETE_LOCATION :{
            return extend({}, state, {

                DeleteLocation:action.data

            });
          }
          break;
          case LocationsActions.UPDATE_LOCATION :{
            return extend({}, state, {

                UpdateLocation:action.data

            });
          }
          break;
          case LocationsActions.LOCATION_BY_ID :{
            return extend({}, state, {

                LocationById:action.data

            });
          }
          break;
          case LocationsActions.ADD_DEVICE :{
              return extend({}, state, {

                  DeviceAdded:action.data

              });
            }
          break;
          case LocationsActions.GET_DEVICE :{
            return extend({}, state, {

                GetDevice:action.data

            });
          }
          break;
          case LocationsActions.DELETE_DEVICE :{
            return extend({}, state, {

                DeleteDevice:action.data

            });
          }
          break;
          case LocationsActions.UPDATE_DEVICE :{
            return extend({}, state, {

                UpdateDevice:action.data

            });
          }
          break;
          case LocationsActions.DEVICE_BY_ID :{
            return extend({}, state, {

                deviceById:action.data

            });
          }
          break;
          case LocationsActions.ADD_ORDER_PRINTER :{
            return extend({}, state, {
                AddOrderPrinter:action.data
            });
          }
          break;
          case LocationsActions.GET_ORDER_PRINTER :{
            return extend({}, state, {
                GetOrderPrinter:action.data
            });
          }
          break;
          case LocationsActions.DELETE_ORDER_PRINTER :{
            return extend({}, state, {
                DeleteOrderPrinter:action.data
            });
          }
          break;
          case LocationsActions.UPDATE_ORDER_PRINTER :{
            return extend({}, state, {
                UpdateOrderPrinter:action.data
            });
          }
          break;
          case LocationsActions.ADD_OPENING_HOUR :{
            return extend({}, state, {
                AddOpeningHour:action.data
            });
          }
          break;
          case LocationsActions.GET_OPENING_HOUR :{
            return extend({}, state, {
                GetOpeningHour:action.data
            });
          }
          break;
          case LocationsActions.DELETE_OPENING_HOUR :{
            return extend({}, state, {
                DeleteOpeningHour:action.data
            });
          }
          break;
          case LocationsActions.UPDATE_OPENING_HOUR :{
            return extend({}, state, {
                UpdateOpeningHour:action.data
            });
          }
          break;
          case LocationsActions.OPENING_HOUR_BY_ID :{
            return extend({}, state, {
                OpeningHourById:action.data
            });
          }
          break;
          case LocationsActions.ORDER_PRINTER_BY_ID :{
            return extend({}, state, {
                orderPrinterById:action.data
            });
          }
          break;
            case LocationsActions.LOGOUT: {
              return extend({}, state, {

                  

              });
            }
        default:
            return state;
    }
};
