import * as BankingActions from './../actions/setup/banking_actions';
import extend from 'lodash/extend';


export var Banking = (state = {
        AddTenderType:null,
        GetTenderType:null,
        DeleteTenderType:null,
        UpdateTenderType:null,
        AddTaxRate:null,
        GetTaxRate:null,
        DeleteTaxRate:null,
        UpdateTaxRate:null,
        AddPettyCashReason:null,
        GetPettyCashReason:null,
        DeletePettyCashReason:null,
        UpdatePettyCashReason:null,
        PettyCashReasonById:null,
        TaxRateById:null,
        TenderTypeById:null,
  }, action) => {
    // ;
    switch (action.type) {

        case 'persist/REHYDRATE': {
           if (action.payload && action.payload.Banking) {
                return extend({}, state, action.payload.Banking);
           } else {
               return state;
           }
       }
            break;
            case BankingActions.ADD_TENDER_TYPE :{
            return extend({}, state, {

                AddTenderType:action.data

            });
          }
          break;
          case BankingActions.GET_TENDER_TYPE :{
            return extend({}, state, {

                GetTenderType:action.data

            });
          }
          break;
          case BankingActions.DELETE_TENDER_TYPE :{
            return extend({}, state, {

                DeleteTenderType:action.data

            });
          }
          break;
          case BankingActions.UPDATE_TENDER_TYPE :{
            return extend({}, state, {

                UpdateTenderType:action.data

            });
          }
          break;
            case BankingActions.ADD_TAX_RATE :{
            return extend({}, state, {

                AddTaxRate:action.data

            });
          }
          break;
          case BankingActions.GET_TAX_RATE :{
            return extend({}, state, {

                GetTaxRate:action.data

            });
          }
          break;
          case BankingActions.DELETE_TAX_RATE :{
            return extend({}, state, {

                DeleteTaxRate:action.data

            });
          }
          break;
          case BankingActions.UPDATE_TAX_RATE :{
            return extend({}, state, {

                UpdateTaxRate:action.data

            });
          }
          break;
          case BankingActions.ADD_PETTY_CASH_REASON :{
            return extend({}, state, {

                AddPettyCashReason:action.data

            });
          }
          break;
          case BankingActions.GET_PETTY_CASH_REASON :{
            return extend({}, state, {

                GetPettyCashReason:action.data

            });
          }
          break;
          case BankingActions.DELETE_PETTY_CASH_REASON :{
            return extend({}, state, {

                DeletePettyCashReason:action.data

            });
          }
          break;
          case BankingActions.UPDATE_PETTY_CASH_REASON :{
            return extend({}, state, {

                UpdatePettyCashReason:action.data

            });
          }
          break;
          case BankingActions.PETTY_CASH_BY_ID :{
            return extend({}, state, {

                PettyCashReasonById:action.data

            });
          }
          break;
          case BankingActions.TAX_RATE_BY_ID :{
            return extend({}, state, {

                TaxRateById:action.data

            });
          }
          break;
          case BankingActions.TENDER_TYPE_BY_ID :{
            return extend({}, state, {

                TenderTypeById:action.data

            });
          }
          break;
            case BankingActions.LOGOUT: {
              return extend({}, state, {

                  

              });
            }
        default:
            return state;
    }
};
