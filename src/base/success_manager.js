import * as api_urls from './../config/api_urls';
import * as UserActions from './../actions/user_actions';
import * as StaffActions from './../actions/management/staff_actions';
import * as ProductActions from './../actions/management/product_actions';
import * as StockActions from './../actions/management/stock_actions';
import * as CustomersActions from './../actions/management/customer_actions';
import * as CompanyActions from './../actions/setup/company_actions';
import * as BankingActions from './../actions/setup/banking_actions';
import * as LocationsActions from './../actions/setup/locations_actions';
import * as RestoreActions from './../actions/setup/restore_actions';
import * as DashbordActions from './../actions/reports/dashbord_actions';
import * as TransactionActions from './../actions/reports/transaction_actions';
import * as SalesActions from './../actions/reports/sales_actions';
import * as StocksActions from './../actions/reports/stocks_actions';
import * as ReportCustomersActions from './../actions/reports/report_customers_actions';
import * as ReportBankingsActions from './../actions/reports/report_bankings_actions';
import * as AccountingActions from './../actions/reports/accounting_actions';
import * as AuditingActions from './../actions/reports/auditing_actions';
import * as ScheduleActions from './../actions/reports/schedule_email_actions';

export var success_manager = (dispatch, endpoint, payload, response) => {

    switch (endpoint) {

            case api_urls.USER_LOGIN:
                {
                    login(dispatch, response);
                };
                break;
            case api_urls.REGISTERATION:
                    {
                      regsiter(dispatch, response);
                    };
                break;
                case api_urls.FORGOT_PASSWORD:
                        {
                          forgotpassword(dispatch, response);
                        }
                    break;
                case api_urls.RESET_PASSWORD:
                        {
                          gotresetpwd_data(dispatch, response);
                        }
                    break;

                    case api_urls.UPDATE_PROFILE:
                        {
                          upadte_profile(dispatch, response);
                        }
                    break;

                    case api_urls.USER_LOGOUT:
                        {
                          logout_data(dispatch, response);
                        }
                    break;

                    case api_urls.ADD_STAFF:
                        {
                          add_staff(dispatch, response);
                        }
                    break;
                    case api_urls.LIST_STAFF:
                        {
                          lists_staff(dispatch, response);
                        }
                    break;
                    case api_urls.DELETE_STAFF:
                        {
                          dlt_staff(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_STAFF:
                        {
                          updt_staff(dispatch, response);
                        }
                    break;
                    case api_urls.ADD_ROLE:
                        {
                          add_role(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_ROLE:
                        {
                          update_role(dispatch, response);
                        }
                    break;
                    case api_urls.LIST_ROLE:
                        {
                          list_role(dispatch, response);
                        }
                    break;
                    case api_urls.ADD_PRODUCT:
                        {
                          add_product(dispatch, response);
                        }
                    break;
                    case api_urls.LIST_PRODUCT:
                        {
                          list_product(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_PRODUCT:
                        {
                          update_product(dispatch, response);
                        }
                    break;
                    case api_urls.DELETE_PRODUCT:
                        {
                          delete_product(dispatch, response);
                        }
                    break;

                    case api_urls.ADD_CATEGORY:
                        {
                          add_category(dispatch, response);
                        }
                    break;

                    case api_urls.LIST_CATEGORY:
                        {
                          list_category(dispatch, response);
                        }
                    break;

                    case api_urls.DELETE_CATEGORY:
                        {
                          delete_category(dispatch, response);
                        }
                    break;

                    case api_urls.UPDATE_CATEGORY:
                        {
                          update_category(dispatch, response);
                        }
                    break;
                    case api_urls.ADD_HOUR:
                        {
                          add_hour(dispatch, response);
                        }
                    break;
                    case api_urls.LIST_HOUR:
                        {
                          list_hour(dispatch, response);
                        }
                    break;
                    case api_urls.DELETE_HOUR:
                        {
                          delete_hour(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_HOUR:
                        {
                          update_hour(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_COMPANY_DETAILS:
                        {
                          upadte_companyDetails(dispatch, response);
                        }
                    break;
                    case api_urls.GET_COMPANY_DETAILS:
                        {
                          get_companyDetails(dispatch, response);
                        }
                    break;
                    case api_urls.ADD_RECEIPT_DETAILS:
                        {
                          add_ReceiptsDetail(dispatch, response);
                        }
                    break;
                    case api_urls.GET_RECEIPT_DETAILS:
                        {
                          get_ReceiptsDetail(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_RECEIPT_DETAIL:
                        {
                          update_ReceiptsDetail(dispatch, response);
                        }
                    break;
                    case api_urls.DELETE_RECEIPT_DETAIL:
                        {
                          delete_ReceiptsDetail(dispatch, response);
                        }
                    break;
                    case api_urls.ADD_DISSCOUNT:
                        {
                          add_disscounts(dispatch, response);
                        }
                    break;
                    case api_urls.GET_DISSCOUNT:
                        {
                          get_disscounts(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_DISSCOUNT:
                        {
                          update_disscounts(dispatch, response);
                        }
                    break;
                    case api_urls.DELETE_DISSCOUNT:
                        {
                          delete_disscounts(dispatch, response);
                        }
                    break;
                    case api_urls.ADD_REFUND:
                        {
                          add_refunds(dispatch, response);
                        }
                    break;
                    case api_urls.GET_REFUND:
                        {
                          get_refunds(dispatch, response);
                        }
                    break;
                    case api_urls.DELETE_REFUND:
                        {
                          delete_refunds(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_REFUND:
                        {
                          update_refunds(dispatch, response);
                        }
                    break;
                    case api_urls.ADD_NOSALE:
                        {
                          add_nosale(dispatch, response);
                        }
                    break;
                    case api_urls.GET_NOSALE:
                        {
                          get_nosale(dispatch, response);
                        }
                    break;
                    case api_urls.DELETE_NOSALE:
                        {
                          delete_nosale(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_NOSALE:
                        {
                          update_nosale(dispatch, response);
                        }
                    break;
                    case api_urls.ADD_STOCKMOVEMENT:
                        {
                          add_stock(dispatch, response);
                        }
                    break;
                    case api_urls.GET_STOCKMOVEMENT:
                        {
                          get_stock(dispatch, response);
                        }
                    break;
                    case api_urls.DELETE_STOCKMOVEMENT:
                        {
                          delete_stock(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_STOCKMOVEMENT:
                        {
                          update_stock(dispatch, response);
                        }
                    break;
                    case api_urls.ADD_COURSE:
                        {
                          add_course(dispatch, response);
                        }
                    break;
                    case api_urls.GET_COURSE:
                        {
                          get_course(dispatch, response);
                        }
                    break;
                    case api_urls.DELETE_COURSE:
                        {
                          delete_course(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_COURSE:
                        {
                          update_course(dispatch, response);
                        }
                    break;
                    case api_urls.ADD_CLOCK:
                        {
                          add_clock(dispatch, response);
                        }
                    break;
                    case api_urls.GET_CLOCK:
                        {
                          get_clock(dispatch, response);
                        }
                    break;
                    case api_urls.DELETE_CLOCK:
                        {
                          delete_clock(dispatch, response);
                        }
                    break;
                    case api_urls.UPDATE_CLOCK:
                        {
                          update_clock(dispatch, response);
                        }
                    break;
                    case api_urls.ADD_CUSTOMER_TYPE:
                       {
                         add_Customer_Data(dispatch, response);
                       }
                   break;
                   case api_urls.LIST_CUSTOMER_TYPE:
                       {
                         list_Customer_Data(dispatch, response);
                       }
                   break;
                   case api_urls.DELETE_CUSTOMER_TYPE:
                       {
                         update_Customer_Data(dispatch, response);
                       }
                   break;
                   case api_urls.UPDATE_CUSTOMER_TYPE:
                       {
                         delete_Customer_Data(dispatch, response);
                       }
                   break;
                   case api_urls.ADD_LOCATION:
                         {
                           add_Location_Data(dispatch, response);
                         }
                     break;
                     case api_urls.GET_LOCATION:
                         {
                           get_Location_Data(dispatch, response);
                         }
                     break;
                     case api_urls.DELETE_LOCATION:
                         {
                           delete_Location_Data(dispatch, response);
                         }
                     break;
                     case api_urls.UPDATE_LOCATION:
                         {
                           update_Location_Data(dispatch, response);
                         }
                     break;
                     case api_urls.ADD_DEVICE:
                     {
                       add_Device_Data(dispatch, response);
                     }
                 break;
                 case api_urls.GET_DEVICE:
                     {
                       get_Device_Data(dispatch, response);
                     }
                 break;
                 case api_urls.DELETE_DEVICE:
                     {
                       delete_Device_Data(dispatch, response);
                     }
                 break;
                 case api_urls.UPDATE_DEVICE:
                     {
                       update_Device_Data(dispatch, response);
                     }
                 break;
                 case api_urls.ADD_BRAND:

                   {
                     add_Brand_Data(dispatch, response);
                   }
               break;
               case api_urls.GET_BRAND:
                   {
                     get_Brand_Data(dispatch, response);
                   }
               break;
               case api_urls.DELETE_BRAND:
                   {
                     delete_Brand_Data(dispatch, response);
                   }
               break;
               case api_urls.UPDATE_BRAND:
                   {
                     update_Brand_Data(dispatch, response);
                   }
               break;
               case api_urls.ADD_POPUP_NOTE:

               {
                 add_PopUp_Note_Data(dispatch, response);
               }
           break;
           case api_urls.GET_POPUP_NOTE:
               {
                 get_PopUp_Note_Data(dispatch, response);
               }
           break;
           case api_urls.DELETE_POPUP_NOTE:
               {
                 delete_PopUp_Note_Data(dispatch, response);
               }
           break;
           case api_urls.UPDATE_POPUP_NOTE:
               {
                 update_PopUp_Note_Data(dispatch, response);
               }
           break;
           case api_urls.ADD_MULTICHOICE_NOTE:

             {
               add_Multi_Note_Data(dispatch, response);
             }
         break;
         case api_urls.GET_MULTICHOICE_NOTE:
             {
               get_Multi_Note_Data(dispatch, response);
             }
         break;
         case api_urls.DELETE_MULTICHOICE_NOTE:
             {
               delete_Multi_Note_Data(dispatch, response);
             }
         break;
         case api_urls.UPDATE_MULTICHOICE_NOTE:
             {
               update_Multi_Note_Data(dispatch, response);
             }
         break;
         case api_urls.ADD_MISC_PRODUCT:

                 {
                   add_Misc_product_Data(dispatch, response);
                 }
             break;
             case api_urls.GET_MISC_PRODUCT:
                 {
                   get_Misc_product_Data(dispatch, response);
                 }
             break;
             case api_urls.DELETE_MISC_PRODUCT:
                 {
                   delete_Misc_product_Data(dispatch, response);
                 }
             break;
             case api_urls.UPDATE_MISC_PRODUCT:
                 {
                   update_Misc_product_Data(dispatch, response);
                 }
             break;
             case api_urls.ADD_ORDER_PRINTER:

                 {
                   add_Order_printer_Data(dispatch, response);
                 }
             break;
             case api_urls.GET_ORDER_PRINTER:
                 {
                   get_Order_printer_Data(dispatch, response);
                 }
             break;
             case api_urls.DELETE_ORDER_PRINTER:
                 {
                   delete_Order_printer_Data(dispatch, response);
                 }
             break;
             case api_urls.UPDATE_ORDER_PRINTER:
                 {
                   update_Order_printer_Data(dispatch, response);
                 }
             break;
             case api_urls.ADD_OPENING_HOUR:

                 {
                   add_Opening_Hour_Data(dispatch, response);
                 }
             break;
             case api_urls.GET_OPENING_HOUR:
                 {
                   get_Opening_Hour_Data(dispatch, response);
                 }
             break;
             case api_urls.DELETE_OPENING_HOUR:
                 {
                   delete_Opening_Hour_Data(dispatch, response);
                 }
             break;
             case api_urls.UPDATE_OPENING_HOUR:
                 {
                   update_Opening_Hour_Data(dispatch, response);
                 }
             break;
             case api_urls.ADD_TENDER_TYPE:

                 {
                   add_Tender_Type_Data(dispatch, response);
                 }
             break;
             case api_urls.GET_TENDER_TYPE:
                 {
                   get_Tender_Type_Data(dispatch, response);
                 }
             break;
             case api_urls.DELETE_TENDER_TYPE:
                 {
                   delete_Tender_Type_Data(dispatch, response);
                 }
             break;
             case api_urls.UPDATE_TENDER_TYPE:
                 {
                   update_Tender_Type_Data(dispatch, response);
                 }
             break;
             case api_urls.ADD_TAX_RATE:

                 {
                   add_Tax_Rate_Data(dispatch, response);
                 }
             break;
             case api_urls.GET_TAX_RATE:
                 {
                   get_Tax_Rate_Data(dispatch, response);
                 }
             break;
             case api_urls.DELETE_TAX_RATE:
                 {
                   delete_Tax_Rate_Data(dispatch, response);
                 }
             break;
             case api_urls.UPDATE_TAX_RATE:
                 {
                   update_Tax_Rate_Data(dispatch, response);
                 }
             break;
             case api_urls.ADD_PETTY_CASH_REASON:

                 {
                   add_Petty_Cash_Rate_Data(dispatch, response);
                 }
             break;
             case api_urls.GET_PETTY_CASH_REASON:
                 {
                   get_Petty_Cash_Rate_Data(dispatch, response);
                 }
             break;
             case api_urls.DELETE_PETTY_CASH_REASON:
                 {
                   delete_Petty_Cash_Rate_Data(dispatch, response);
                 }
             break;
             case api_urls.UPDATE_PETTY_CASH_REASON:
                 {
                   update_Petty_Cash_Rate_Data(dispatch, response);
                 }
             break;
             case api_urls.ADD_PROMOTION:

                 {
                   add_Promotion_Data(dispatch, response);
                 }
             break;
             case api_urls.GET_PROMOTION:
                 {
                   get_Promotion_Data(dispatch, response);
                 }
             break;
             case api_urls.DELETE_PROMOTION:
                 {
                   delete_Promotion_Data(dispatch, response);
                 }
             break;
             case api_urls.UPDATE_PROMOTION:
                 {
                   update_Promotion_Data(dispatch, response);
                 }
             break;
             case api_urls.ADD_SUPPLIER:

                 {
                   add_Supplier_Data(dispatch, response);
                 }
             break;
             case api_urls.GET_SUPPLIER:
                 {
                   get_Supplier_Data(dispatch, response);
                 }
             break;
             case api_urls.DELETE_SUPPLIER:
                 {
                   delete_Supplier_Data(dispatch, response);
                 }
             break;
             case api_urls.UPDATE_SUPPLIER:
                 {
                   update_Supplier_Data(dispatch, response);
                 }
             break;



        default:
            {
                if (response) {
                    return response;
                } else {
                    return { status: 200, message: 'No data.' };
                }
            }
    }
};

/*  ----------------Deals Sucess Handler------------------------------  */
function login(dispatch, response) {
    dispatch(UserActions.userLogin(response));
}
function regsiter(dispatch, response) {
  // ;
    dispatch(UserActions.userRegisterationData(response));
}

function upadte_profile(dispatch, response) {
dispatch(UserActions.userLogoutData(response));
}

function logout_data(dispatch, response) {
dispatch(UserActions.userLogoutData(response));
}

function forgotpassword(dispatch, response) {
    //;
    dispatch(UserActions.userPasswordDetail(response));
}

function gotresetpwd_data(dispatch, response) {
    // ;
    dispatch(UserActions.resetpwd_data(response));
}

function add_staff(dispatch, response) {

dispatch(StaffActions.getStaffData(response));
}

function lists_staff(dispatch, response) {

dispatch(StaffActions.getListStaffData(response));
}
// ;
function dlt_staff(dispatch, response) {

dispatch(StaffActions.getDeleteStaffData(response));
}

function updt_staff(dispatch, response) {

dispatch(StaffActions.getUpdateStaffData(response));
}



function add_role(dispatch, response) {

dispatch(StaffActions.getRoleData(response));
}

function update_role(dispatch, response) {

dispatch(StaffActions.getUpdateRoleData(response));
}

function list_role(dispatch, response) {

dispatch(StaffActions.getListRoleData(response));
}

function add_product(dispatch, response) {
dispatch(ProductActions.getAddProductData(response));
}

function list_product(dispatch, response) {
dispatch(ProductActions.listProductData(response));
}
function delete_product(dispatch, response) {
dispatch(ProductActions.deleteProductData(response));
}
function update_product(dispatch, response) {
dispatch(ProductActions.updateProductData(response));
}
//Category

function add_category(dispatch, response) {
dispatch(ProductActions.addCategoryData(response));
}

function list_category(dispatch, response) {
dispatch(ProductActions.listCategoryData(response));
}

function delete_category(dispatch, response) {
dispatch(ProductActions.deleteCategorytData(response));
}

function update_category(dispatch, response) {
dispatch(ProductActions.updateCategoryData(response));
}

//hours
function add_hour(dispatch, response) {
dispatch(StaffActions.getAddHourData(response));
}
function list_hour(dispatch, response) {
dispatch(StaffActions.getListHourData(response));
}
function update_hour(dispatch, response) {
dispatch(StaffActions.getUpadteHourData(response));
}
function delete_hour(dispatch, response) {
dispatch(StaffActions.getDeleteRoleData(response));
}


//Company details
function upadte_companyDetails(dispatch, response) {
dispatch(CompanyActions.getCompanyData(response));
}
function get_companyDetails(dispatch, response) {
dispatch(CompanyActions.getCompanyData(response));
}

//receipts

function add_ReceiptsDetail(dispatch , response){
    dispatch(CompanyActions.addReceiptDetail(response));
}

function get_ReceiptsDetail(dispatch , response){
    dispatch(CompanyActions.getReceiptDetail(response));
}


function update_ReceiptsDetail(dispatch , response){
    dispatch(CompanyActions.updateReceiptData(response));
}

function delete_ReceiptsDetail(dispatch , response){
    dispatch(CompanyActions.deleteReceiptDetail(response));
}


//add disscount in company
function add_disscounts(dispatch, response) {

dispatch(CompanyActions.getDisscounts(response));
}
function get_disscounts(dispatch, response) {

dispatch(CompanyActions.getListDisscountsData(response));
}

function update_disscounts(dispatch, response) {

dispatch(CompanyActions.updateDisscountData(response));
}

function delete_disscounts(dispatch, response) {

dispatch(CompanyActions.deleteDisscounts(response));
}

//add refunds reason in company
function add_refunds(dispatch, response) {

dispatch(CompanyActions.getRefunds(response));
}

function get_refunds(dispatch, response) {

dispatch(CompanyActions.getListRefundData(response));
}

function update_refunds(dispatch, response) {

dispatch(CompanyActions.updateRefundData(response));
}

function delete_refunds(dispatch, response) {

dispatch(CompanyActions.deleteRefund(response));
}

//add no sale reason in company
function add_nosale(dispatch, response) {

dispatch(CompanyActions.getNoSale(response));
}

function get_nosale(dispatch, response) {

dispatch(CompanyActions.getListNoSales(response));
}

function update_nosale(dispatch, response) {

dispatch(CompanyActions.updateNoSaleData(response));
}

function delete_nosale(dispatch, response) {

dispatch(CompanyActions.deleteNoSale(response));
}

//add Stock movement reason in company
function add_stock(dispatch, response) {

dispatch(CompanyActions.getStockMovement(response));
}

function get_stock(dispatch, response) {

dispatch(CompanyActions.getListStockMovement(response));
}

function update_stock(dispatch, response) {

dispatch(CompanyActions.updateStockMovementData(response));
}

function delete_stock(dispatch, response) {

dispatch(CompanyActions.deleteStockMovement(response));
}

//add Course reason in company
function add_course(dispatch, response) {

dispatch(CompanyActions.getCourse(response));
}

function get_course(dispatch, response) {

dispatch(CompanyActions.getListCourse(response));
}

function update_course(dispatch, response) {

dispatch(CompanyActions.updateCourseData(response));
}

function delete_course(dispatch, response) {

dispatch(CompanyActions.deleteCourse(response));
}

//product misc product realted
function add_Misc_product_Data(dispatch, response) {
  dispatch(ProductActions.getAddMiscProductData(response));
}
function get_Misc_product_Data(dispatch, response) {
  dispatch(ProductActions.getMiscProductData(response));
}
function update_Misc_product_Data(dispatch, response) {
  dispatch(ProductActions.updateMiscProductData(response));
}
function delete_Misc_product_Data(dispatch, response) {
  dispatch(ProductActions.deleteMiscProductData(response));
}


//add Clock reason in company
function add_clock(dispatch, response) {

dispatch(CompanyActions.getClock(response));
}

function get_clock(dispatch, response) {

dispatch(CompanyActions.getListClocks(response));
}

function update_clock(dispatch, response) {

dispatch(CompanyActions.updateClockData(response));
}

function delete_clock(dispatch, response) {

dispatch(CompanyActions.deleteClock(response));
}

//company customer realted
function add_Customer_Data(dispatch, response) {
dispatch(CompanyActions.addCustomerData(response));
}
function list_Customer_Data(dispatch, response) {
dispatch(CompanyActions.ListCustomerData(response));
}
function update_Customer_Data(dispatch, response) {
dispatch(CompanyActions.getUpdatedCustomerData(response));
}
function delete_Customer_Data(dispatch, response) {
dispatch(CompanyActions.getDeletedCustomerData(response));
}

//company location & device realted
function add_Location_Data(dispatch, response) {
  dispatch(LocationsActions.getLocation(response));
}
function get_Location_Data(dispatch, response) {
  dispatch(LocationsActions.getListLocationData(response));
}
function update_Location_Data(dispatch, response) {
  dispatch(LocationsActions.updateLocationData(response));
}
function delete_Location_Data(dispatch, response) {
  dispatch(LocationsActions.deleteLocations(response));
}


//location device realted
function add_Device_Data(dispatch, response) {
  dispatch(LocationsActions.getAddDeviceData(response));
}
function get_Device_Data(dispatch, response) {
  dispatch(LocationsActions.getDeviceData(response));
}
function update_Device_Data(dispatch, response) {
  dispatch(LocationsActions.updateDeviceData(response));
}
function delete_Device_Data(dispatch, response) {
  dispatch(LocationsActions.deleteDeviceData(response));
}

//product brand realted
function add_Brand_Data(dispatch, response) {
  dispatch(ProductActions.getAddBrandData(response));
}
function get_Brand_Data(dispatch, response) {
  dispatch(ProductActions.getBrandData(response));
}
function update_Brand_Data(dispatch, response) {
  dispatch(ProductActions.updateBrandData(response));
}
function delete_Brand_Data(dispatch, response) {
  dispatch(ProductActions.deleteBrandData(response));
}

//product popup note realted
function add_PopUp_Note_Data(dispatch, response) {
  dispatch(ProductActions.getAddPopUpNoteData(response));
}
function get_PopUp_Note_Data(dispatch, response) {
  dispatch(ProductActions.getPopUpNoteData(response));
}
function update_PopUp_Note_Data(dispatch, response) {
  dispatch(ProductActions.updatePopUpNoteData(response));
}
function delete_PopUp_Note_Data(dispatch, response) {
  dispatch(ProductActions.deletePopUpNoteData(response));
}

//product multi choice note note realted
function add_Multi_Note_Data(dispatch, response) {
  dispatch(ProductActions.getAddMultiChoiceNoteData(response));
}
function get_Multi_Note_Data(dispatch, response) {
  dispatch(ProductActions.getMultiChoiceNoteData(response));
}
function update_Multi_Note_Data(dispatch, response) {
  dispatch(ProductActions.updateMultiChoiceNoteData(response));
}
function delete_Multi_Note_Data(dispatch, response) {
  dispatch(ProductActions.deleteMultiChoiceNoteData(response));
}

//location order printer 
function add_Order_printer_Data(dispatch, response) {
  dispatch(LocationsActions.getAddOrderPrinterData(response));
}
function get_Order_printer_Data(dispatch, response) {
  dispatch(LocationsActions.getOrderPrinterData(response));
}
function update_Order_printer_Data(dispatch, response) {
  dispatch(LocationsActions.updateOrderPrinterData(response));
}
function delete_Order_printer_Data(dispatch, response) {
  dispatch(LocationsActions.deleteOrderPrinterData(response));
}
//location opening hours 
function add_Opening_Hour_Data(dispatch, response) {
  dispatch(LocationsActions.getAddOpeningHourData(response));
}
function get_Opening_Hour_Data(dispatch, response) {
  dispatch(LocationsActions.getOpeningHourData(response));
}
function update_Opening_Hour_Data(dispatch, response) {
  dispatch(LocationsActions.updateOpeningHourData(response));
}
function delete_Opening_Hour_Data(dispatch, response) {
  dispatch(LocationsActions.deleteOpeningHourData(response));
}

//Banking tender types
function add_Tender_Type_Data(dispatch, response) {
  dispatch(BankingActions.getTenderType(response));
}
function get_Tender_Type_Data(dispatch, response) {
  dispatch(BankingActions.getListTenderTypeData(response));
}
function update_Tender_Type_Data(dispatch, response) {
  dispatch(BankingActions.updateTenderTypeData(response));
}
function delete_Tender_Type_Data(dispatch, response) {
  dispatch(BankingActions.deleteTenderType(response));
}

//Banking tax rate
function add_Tax_Rate_Data(dispatch, response) {
  dispatch(BankingActions.getTaxRate(response));
}
function get_Tax_Rate_Data(dispatch, response) {
  dispatch(BankingActions.getListTaxRateData(response));
}
function update_Tax_Rate_Data(dispatch, response) {
  dispatch(BankingActions.updateTaxRateData(response));
}
function delete_Tax_Rate_Data(dispatch, response) {
  dispatch(BankingActions.deleteTaxRate(response));
}

//Banking petty cash reason
function add_Petty_Cash_Rate_Data(dispatch, response) {
  dispatch(BankingActions.getPettyCashReason(response));
}
function get_Petty_Cash_Rate_Data(dispatch, response) {
  dispatch(BankingActions.getListPettyCashReasonData(response));
}
function update_Petty_Cash_Rate_Data(dispatch, response) {
  dispatch(BankingActions.updatePettyCashReasonData(response));
}
function delete_Petty_Cash_Rate_Data(dispatch, response) {
  dispatch(BankingActions.deletePettyCashReason(response));
}

//Promotion in product
function add_Promotion_Data(dispatch, response) {
  dispatch(ProductActions.addPromotionData(response));
}
function get_Promotion_Data(dispatch, response) {
  dispatch(ProductActions.ListPromotionData(response));
}
function update_Promotion_Data(dispatch, response) {
  dispatch(ProductActions.getUpdatedPromotionData(response));
}
function delete_Promotion_Data(dispatch, response) {
  dispatch(ProductActions.getDeletedPromotionData(response));
}

//Stock management 
function add_Supplier_Data(dispatch, response) {
  dispatch(StockActions.getAddSupplierData(response));
}
function get_Supplier_Data(dispatch, response) {
  dispatch(StockActions.listSupplierData(response));
}
function update_Supplier_Data(dispatch, response) {
  dispatch(StockActions.updateSupplierData(response));
}
function delete_Supplier_Data(dispatch, response) {
  dispatch(StockActions.deleteSupplierData(response));
}
