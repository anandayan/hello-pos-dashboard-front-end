import {
    success_manager
} from './success_manager';
import {
    failed_manager
} from './failed_manager';
import * as UserActions from './../actions/user_actions';

export var response_Manager = (isNetworkError, dispatch,
    endpoint, payload, response, error) => {
    
    if (!response) {
        return failed_manager(dispatch, endpoint,
            payload, response, error);
    } else if (response && response.status_code == 200) {
        return success_manager(dispatch, endpoint, payload, response);
    }  else if (response && response.status_code !== 200) {
      return failed_manager(dispatch, endpoint,
          payload, response, error);
    }
    return failed_manager(dispatch, endpoint,
        payload, response, response || error);
};
