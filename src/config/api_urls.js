// ------ Declare Constants Here --------//
export var BASE_URL = 'http://ec2-35-154-246-28.ap-south-1.compute.amazonaws.com/api/public/api/';
 

export var API_KEY='';
export var API_GET = 'GET';
export var API_POST = 'POST';

//-------  DECLARE ENDPOINTS HERE  --------//
export var USER_LOGIN = 'auth/login' ;
export var REGISTERATION = 'auth/signup' ;
export var FORGOT_PASSWORD = 'resetpasswordrequest' ;
export var RESET_PASSWORD = 'resetpassword' ;
export var USER_LOGOUT = 'logout' ;
export var UPDATE_PROFILE = 'updateprofile' ;

export var ADD_STAFF = 'staff/add' ;
export var LIST_STAFF = 'staff/all' ;
export var DELETE_STAFF = 'staff/delete' ;
export var UPDATE_STAFF = 'staff/update' ;

export var ADD_ROLE = 'roles/add' ;
export var UPDATE_ROLE = 'roles/update' ;
export var LIST_ROLE = 'roles/all' ;
export var ROLE_STATUS = 'roles/status/update' ;

//Products
export var ADD_PRODUCT = 'products/add' ;
export var LIST_PRODUCT = 'products/all' ;
export var DELETE_PRODUCT = 'products/delete' ;
export var UPDATE_PRODUCT = 'products/update' ;
export var PRODUCT_CATEGORY = 'products/category' ;
//Categories
export var ADD_CATEGORY = 'categories/add' ;
export var LIST_CATEGORY = 'categories/all' ;
export var DELETE_CATEGORY = 'categories/delete' ;
export var UPDATE_CATEGORY = 'categories/update' ;

//Hours
export var ADD_HOUR = 'hours/add' ;
export var LIST_HOUR = 'hours/all' ;
export var UPDATE_HOUR = 'hours/update' ;
export var DELETE_HOUR = 'hours/delete' ;
export var UPDATE_COMPANY_DETAILS = 'company/add';
export var GET_COMPANY_DETAILS = 'company/get';

export var GET_RECEIPT_DETAILS = 'receipts/all';
export var ADD_RECEIPT_DETAILS = 'receipts/add';
export var UPDATE_RECEIPT_DETAIL = 'receipts/update';
export var DELETE_RECEIPT_DETAIL = 'receipts/delete';

export var ADD_DISSCOUNT = 'discountreasons/add';
export var GET_DISSCOUNT = 'discountreasons/all';
export var UPDATE_DISSCOUNT = 'discountreasons/update';
export var DELETE_DISSCOUNT = 'discountreasons/delete';

export var ADD_REFUND = 'refundreasons/add';
export var GET_REFUND = 'refundreasons/all';
export var DELETE_REFUND = 'refundreasons/delete';
export var UPDATE_REFUND = 'refundreasons/update';

export var ADD_NOSALE = 'nosalereasons/add';
export var GET_NOSALE = 'nosalereasons/all';
export var DELETE_NOSALE = 'nosalereasons/delete';
export var UPDATE_NOSALE = 'nosalereasons/update';

export var ADD_STOCKMOVEMENT = 'stocks/add';
export var GET_STOCKMOVEMENT = 'stocks/all';
export var DELETE_STOCKMOVEMENT = 'stocks/delete';
export var UPDATE_STOCKMOVEMENT = 'stocks/update';

export var ADD_COURSE = 'courses/add';
export var GET_COURSE = 'courses/all';
export var DELETE_COURSE = 'courses/delete';
export var UPDATE_COURSE = 'courses/update';

export var ADD_CLOCK = 'clocktypes/add';
export var GET_CLOCK = 'clocktypes/all';
export var DELETE_CLOCK = 'clocktypes/delete';
export var UPDATE_CLOCK = 'clocktypes/update';


export var ADD_CUSTOMER_TYPE = 'customertypes/add';
export var LIST_CUSTOMER_TYPE = 'customertypes/all';
export var DELETE_CUSTOMER_TYPE = 'customertypes/delete';
export var UPDATE_CUSTOMER_TYPE = 'customertypes/update';

export var ADD_LOCATION = 'locations/add';
export var GET_LOCATION = 'locations/all';
export var DELETE_LOCATION = 'locations/delete';
export var UPDATE_LOCATION = 'locations/update';
export var ALLOCATE_LOCATION = 'locations/allocate';
 
export var ADD_DEVICE = 'devices/add';
//export var GET_DEVICE = 'devices/all';
export var GET_DEVICE = 'devices/location';
export var GET_DEVICE_LOCATION = 'devices/location';
export var DELETE_DEVICE = 'devices/delete';
export var UPDATE_DEVICE = 'devices/update';
export var GET_ACTIVE_DEVICES_LOCATION = 'devices/location';

export var ADD_BRAND = 'brands/add';
export var GET_BRAND = 'brands/all';
export var DELETE_BRAND = 'brands/delete';
export var UPDATE_BRAND = 'brands/update';

export var ADD_POPUP_NOTE = 'popupnotes/add';
export var GET_POPUP_NOTE = 'popupnotes/all';
export var DELETE_POPUP_NOTE = 'popupnotes/delete';
export var UPDATE_POPUP_NOTE = 'popupnotes/update';

export var ADD_MULTICHOICE_NOTE = 'multichoicenotes/add';
export var GET_MULTICHOICE_NOTE = 'multichoicenotes/all';
export var DELETE_MULTICHOICE_NOTE = 'multichoicenotes/delete';
export var UPDATE_MULTICHOICE_NOTE = 'multichoicenotes/update';

export var ADD_MISC_PRODUCT = 'addMiscProduct';
export var GET_MISC_PRODUCT = 'getMiscProducts';
export var DELETE_MISC_PRODUCT = 'deleteMiscProduct';
export var UPDATE_MISC_PRODUCT = 'updateMiscProduct';

export var ADD_ORDER_PRINTER = 'orderprinters/add';
export var GET_ORDER_PRINTER = 'orderprinters/all';
export var DELETE_ORDER_PRINTER = 'orderprinters/delete';
export var UPDATE_ORDER_PRINTER = 'orderprinters/update';

export var ADD_OPENING_HOUR = 'openinghours/add';
export var GET_OPENING_HOUR = 'openinghours/all';
export var DELETE_OPENING_HOUR = 'openinghours/delete';
export var UPDATE_OPENING_HOUR = 'openinghours/update';

//BANKING URL IN SETUP MODULE
export var ADD_TENDER_TYPE = 'tendertypes/add';
export var GET_TENDER_TYPE = 'tendertypes/all';
export var DELETE_TENDER_TYPE = 'tendertypes/delete';
export var UPDATE_TENDER_TYPE = 'tendertypes/update';

export var ADD_TAX_RATE = 'taxrates/add';
export var GET_TAX_RATE = 'taxrates/all';
export var DELETE_TAX_RATE = 'taxrates/delete';
export var UPDATE_TAX_RATE = 'taxrates/update';

export var ADD_PETTY_CASH_REASON = 'pettycashreasons/add';
export var GET_PETTY_CASH_REASON = 'pettycashreasons/all';
export var DELETE_PETTY_CASH_REASON = 'pettycashreasons/delete';
export var UPDATE_PETTY_CASH_REASON = 'pettycashreasons/update';

export var ADD_PROMOTION = 'promotions/add';
export var GET_PROMOTION = 'promotions/all';
export var DELETE_PROMOTION = 'promotions/delete';
export var UPDATE_PROMOTION = 'promotions/update';

export var ADD_SUPPLIER = 'suppliers/add';
export var GET_SUPPLIER = 'suppliers/all';
export var DELETE_SUPPLIER = 'suppliers/delete';
export var UPDATE_SUPPLIER = 'suppliers/update';


//--------- RETURN FINAL URLS -------------//
export var get = (url, data) => {
    switch (url) {
        case USER_LOGIN:{
            return  BASE_URL + USER_LOGIN;
        }
          break;

          case REGISTERATION:{
              return  BASE_URL + REGISTERATION;
          }
            break;

          case FORGOT_PASSWORD:{
              return  BASE_URL + FORGOT_PASSWORD;
          }
            break;

          case RESET_PASSWORD:{
              return  BASE_URL + RESET_PASSWORD;
          }
          break;

          case USER_LOGOUT:{
              return  BASE_URL + USER_LOGOUT;
          }
          break;

          case UPDATE_PROFILE:{
              return  BASE_URL + UPDATE_PROFILE;
          }
          break;

          case ADD_STAFF:{
              return  BASE_URL + ADD_STAFF;
          }
          break;

          case LIST_STAFF:{
              return  BASE_URL + LIST_STAFF;
          }
          break;

          case DELETE_STAFF:{
              return  BASE_URL + DELETE_STAFF;
          }
          break;

          case UPDATE_STAFF:{
              return  BASE_URL + UPDATE_STAFF;
          }
          break;

          case ADD_ROLE:{
              return  BASE_URL + ADD_ROLE;
          }
          break;

          case UPDATE_ROLE:{
              return  BASE_URL + UPDATE_ROLE;
          }
          break;

          case LIST_ROLE:{
              return  BASE_URL + LIST_ROLE;
          }

          case ROLE_STATUS:{
              return  BASE_URL + ROLE_STATUS;
          }
          
          break;

          case ADD_PRODUCT:{
              return  BASE_URL + ADD_PRODUCT;
          }
          break;

          case LIST_PRODUCT:{
              return  BASE_URL + LIST_PRODUCT;
          }
          break;

          case DELETE_PRODUCT:{
              return  BASE_URL + DELETE_PRODUCT;
          }
          break;

          case UPDATE_PRODUCT:{
              return  BASE_URL + UPDATE_PRODUCT;
          }
          break;
          case ADD_CATEGORY:{
              return  BASE_URL + ADD_CATEGORY;
          }
          break;

          case LIST_CATEGORY:{
              return  BASE_URL + LIST_CATEGORY;
          }
          break;

          case DELETE_CATEGORY:{
              return  BASE_URL + DELETE_CATEGORY;
          }
          break;

          case UPDATE_CATEGORY:{
              return  BASE_URL + UPDATE_CATEGORY;
          }
          break;
          case ADD_HOUR:{
              return  BASE_URL + ADD_HOUR;
          }
          break;

          case LIST_HOUR:{
              return  BASE_URL + LIST_HOUR;
          }
          break;

          case UPDATE_HOUR:{
              return  BASE_URL + UPDATE_HOUR;
          }
          break;
          case DELETE_HOUR:{
              return  BASE_URL + DELETE_HOUR;
          }
          break;

          case UPDATE_COMPANY_DETAILS:{
              return  BASE_URL + UPDATE_COMPANY_DETAILS;
          }
          break;

          case GET_COMPANY_DETAILS:{
              return  BASE_URL + GET_COMPANY_DETAILS;
          }
          break;

          case ADD_RECEIPT_DETAILS:{
              return  BASE_URL + ADD_RECEIPT_DETAILS;
          }
          break;
          case GET_RECEIPT_DETAILS:{
              return  BASE_URL + GET_RECEIPT_DETAILS;
          }
          break;
          case UPDATE_RECEIPT_DETAIL:{
              return  BASE_URL + UPDATE_RECEIPT_DETAIL;
          }
          break;


          case DELETE_RECEIPT_DETAIL:{
              return  BASE_URL + DELETE_RECEIPT_DETAIL;
          }
          break;

          case ADD_DISSCOUNT:{
              return  BASE_URL + ADD_DISSCOUNT;
          }
          break;

          case GET_DISSCOUNT:{
              return  BASE_URL + GET_DISSCOUNT;
          }
          break;

          case UPDATE_DISSCOUNT:{
              return  BASE_URL + UPDATE_DISSCOUNT;
          }
          break;

          case DELETE_DISSCOUNT:{
              return  BASE_URL + DELETE_DISSCOUNT;
          }
          break;

          case ADD_REFUND:{
              return  BASE_URL + ADD_REFUND;
          }
          break;
          case GET_REFUND:{
              return  BASE_URL + GET_REFUND;
          }
          break;

          case DELETE_REFUND:{
              return  BASE_URL + DELETE_REFUND;
          }
          break;

          case UPDATE_REFUND:{
              return  BASE_URL + UPDATE_REFUND;
          }
          break;

          case ADD_NOSALE:{
              return  BASE_URL + ADD_NOSALE;
          }
          break;

          case GET_NOSALE:{
              return  BASE_URL + GET_NOSALE;
          }
          break;

          case DELETE_NOSALE:{
              return  BASE_URL + DELETE_NOSALE;
          }
          break;

          case UPDATE_NOSALE:{
              return  BASE_URL + UPDATE_NOSALE;
          }
          break;

          case ADD_STOCKMOVEMENT:{
              return  BASE_URL + ADD_STOCKMOVEMENT;
          }
          break;

          case GET_STOCKMOVEMENT:{
              return  BASE_URL + GET_STOCKMOVEMENT;
          }
          break;

          case DELETE_STOCKMOVEMENT:{
              return  BASE_URL + DELETE_STOCKMOVEMENT;
          }
          break;

          case UPDATE_STOCKMOVEMENT:{
              return  BASE_URL + UPDATE_STOCKMOVEMENT;
          }
          break;

          case ADD_COURSE:{
              return  BASE_URL + ADD_COURSE;
          }
          break;

          case GET_COURSE:{
              return  BASE_URL + GET_COURSE;
          }
          break;

          case DELETE_COURSE:{
              return  BASE_URL + DELETE_COURSE;
          }
          break;

          case UPDATE_COURSE:{
              return  BASE_URL + UPDATE_COURSE;
          }
          break;

          case ADD_CLOCK:{
              return  BASE_URL + ADD_CLOCK;
          }
          break;

          case GET_CLOCK:{
              return  BASE_URL + GET_CLOCK;
          }
          break;

          case DELETE_CLOCK:{
              return  BASE_URL + DELETE_CLOCK;
          }
          break;

          case UPDATE_CLOCK:{
              return  BASE_URL + UPDATE_CLOCK;
          }
          break;
          case ADD_CUSTOMER_TYPE:{
              return  BASE_URL + ADD_CUSTOMER_TYPE;
          }
          break;
          case LIST_CUSTOMER_TYPE:{
              return  BASE_URL + LIST_CUSTOMER_TYPE;
          }
          break;
          case DELETE_CUSTOMER_TYPE:{
              return  BASE_URL + DELETE_CUSTOMER_TYPE;
          }
          break;
          case UPDATE_CUSTOMER_TYPE:{
              return  BASE_URL + UPDATE_CUSTOMER_TYPE;
          }
          break;
          case ADD_LOCATION:{
              return  BASE_URL + ADD_LOCATION;
          }
          break;

          case GET_LOCATION:{
              return  BASE_URL + GET_LOCATION;
          }
          break;

          case DELETE_LOCATION:{
              return  BASE_URL + DELETE_LOCATION;
          }
          break;

          case UPDATE_LOCATION:{
              return  BASE_URL + UPDATE_LOCATION;
          }
          break;
          case ALLOCATE_LOCATION:{
              return  BASE_URL + ALLOCATE_LOCATION;
          }
          break;
          
          case ADD_DEVICE:{
              return  BASE_URL + ADD_DEVICE;
          }
          break;
          case GET_DEVICE:{
              return  BASE_URL + GET_DEVICE;
          }
          break;
          case DELETE_DEVICE:{
              return  BASE_URL + DELETE_DEVICE;
          }
          break;
          case UPDATE_DEVICE:{
              return  BASE_URL + UPDATE_DEVICE;
          }
          break;
          case GET_ACTIVE_DEVICES_LOCATION:{
              return  BASE_URL + GET_ACTIVE_DEVICES_LOCATION;
          }
          break;
          
          case ADD_BRAND:{
              return  BASE_URL + ADD_BRAND;
          }
          break;
          case GET_BRAND:{
              return  BASE_URL + GET_BRAND;
          }
          break;
          case DELETE_BRAND:{
              return  BASE_URL + DELETE_BRAND;
          }
          break;
          case UPDATE_BRAND:{
              return  BASE_URL + UPDATE_BRAND;
          }
          break;
          case ADD_POPUP_NOTE:{
              return  BASE_URL + ADD_POPUP_NOTE;
          }
          break;
          case GET_POPUP_NOTE:{
              return  BASE_URL + GET_POPUP_NOTE;
          }
          break;
          case DELETE_POPUP_NOTE:{
              return  BASE_URL + DELETE_POPUP_NOTE;
          }
          break;
          case UPDATE_POPUP_NOTE:{
              return  BASE_URL + UPDATE_POPUP_NOTE;
          }
          break;
          case ADD_MULTICHOICE_NOTE:{
              return  BASE_URL + ADD_MULTICHOICE_NOTE;
          }
          break;
          case GET_MULTICHOICE_NOTE:{
              return  BASE_URL + GET_MULTICHOICE_NOTE;
          }
          break;
          case DELETE_MULTICHOICE_NOTE:{
              return  BASE_URL + DELETE_MULTICHOICE_NOTE;
          }
          break;
          case UPDATE_MULTICHOICE_NOTE:{
              return  BASE_URL + UPDATE_MULTICHOICE_NOTE;
          }
          break;
          case ADD_MISC_PRODUCT:{
              return  BASE_URL + ADD_MISC_PRODUCT;
          }
          break;
          case GET_MISC_PRODUCT:{
              return  BASE_URL + GET_MISC_PRODUCT;
          }
          break;
          case DELETE_MISC_PRODUCT:{
              return  BASE_URL + DELETE_MISC_PRODUCT;
          }
          break;
          case UPDATE_MISC_PRODUCT:{
              return  BASE_URL + UPDATE_MISC_PRODUCT;
          }
          break;

          case ADD_ORDER_PRINTER:{
              return  BASE_URL + ADD_ORDER_PRINTER;
          }
          break;
          case GET_ORDER_PRINTER:{
              return  BASE_URL + GET_ORDER_PRINTER;
          }
          break;
          case DELETE_ORDER_PRINTER:{
              return  BASE_URL + DELETE_ORDER_PRINTER;
          }
          break;
          case UPDATE_ORDER_PRINTER:{
              return  BASE_URL + UPDATE_ORDER_PRINTER;
          }
          break;

          case ADD_OPENING_HOUR:{
              return  BASE_URL + ADD_OPENING_HOUR;
          }
          break;
          case GET_OPENING_HOUR:{
              return  BASE_URL + GET_OPENING_HOUR;
          }
          break;
          case DELETE_OPENING_HOUR:{
              return  BASE_URL + DELETE_OPENING_HOUR;
          }
          break;
          case UPDATE_OPENING_HOUR:{
              return  BASE_URL + UPDATE_OPENING_HOUR;
          }
          break;

          case ADD_TENDER_TYPE:{
              return  BASE_URL + ADD_TENDER_TYPE;
          }
          break;
          case GET_TENDER_TYPE:{
              return  BASE_URL + GET_TENDER_TYPE;
          }
          break;
          case DELETE_TENDER_TYPE:{
              return  BASE_URL + DELETE_TENDER_TYPE;
          }
          break;
          case UPDATE_TENDER_TYPE:{
              return  BASE_URL + UPDATE_TENDER_TYPE;
          }
          break;

          case ADD_TAX_RATE:{
              return  BASE_URL + ADD_TAX_RATE;
          }
          break;
          case GET_TAX_RATE:{
              return  BASE_URL + GET_TAX_RATE;
          }
          break;
          case DELETE_TAX_RATE:{
              return  BASE_URL + DELETE_TAX_RATE;
          }
          break;
          case UPDATE_TAX_RATE:{
              return  BASE_URL + UPDATE_TAX_RATE;
          }
          break;

          case ADD_PETTY_CASH_REASON:{
              return  BASE_URL + ADD_PETTY_CASH_REASON;
          }
          break;
          case GET_PETTY_CASH_REASON:{
              return  BASE_URL + GET_PETTY_CASH_REASON;
          }
          break;
          case DELETE_PETTY_CASH_REASON:{
              return  BASE_URL + DELETE_PETTY_CASH_REASON;
          }
          break;
          case UPDATE_PETTY_CASH_REASON:{
              return  BASE_URL + UPDATE_PETTY_CASH_REASON;
          }
          break;
          case ADD_PROMOTION:{
              return  BASE_URL + ADD_PROMOTION;
          }
          break;
          case GET_PROMOTION:{
              return  BASE_URL + GET_PROMOTION;
          }
          break;
          case DELETE_PROMOTION:{
              return  BASE_URL + DELETE_PROMOTION;
          }
          break;
          case UPDATE_PROMOTION:{
              return  BASE_URL + UPDATE_PROMOTION;
          }
          break;
          case ADD_SUPPLIER:{
              return  BASE_URL + ADD_SUPPLIER;
          }
          break;
          case GET_SUPPLIER:{
              return  BASE_URL + GET_SUPPLIER;
          }
          break;
          case DELETE_SUPPLIER:{
              return  BASE_URL + DELETE_SUPPLIER;
          }
          break;
          case UPDATE_SUPPLIER:{
              return  BASE_URL + UPDATE_SUPPLIER;
          }
          break;



        default:
            break;
    }
    return 'URL Not configured';
};
