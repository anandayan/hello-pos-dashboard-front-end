import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import createExpirationTransform from 'redux-persist-transform-expire';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import asyncLocalStoragefrom from 'redux-persist/storages';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import './css/app.css';
import AppReducer from './reducers';


/** Screen components */
import * as UserActions from './actions/user_actions';

/*Component and screens*/
import Home from './screens/home';
import Registration from './screens/registration';
import APP from './screens/App';
import ForgotPwd from './screens/fogotpassword';
import ResetPwd from './screens/resetpassword';
import profile from './screens/userprofile';

/** Screen components for Reports tabs*/
import Dashboard from './components/reports/dashboard/dashboard';

/** Screen components for menagements tabs*/
import CustomersSearch from './components/management/Customers/customerssearch';
import Addc from './components/management/Customers/customerAdd';
import InvoiceMessages from './components/management/Customers/Invoicemessages';

import ProductBrands from './components/management/Products/brands';
import EditBrands from './components/management/Products/EditBrand';
import ProductCategories from './components/management/Products/categories';
import ProductMiscProducts from './components/management/Products/miscproducts';
import ProductMultipleChoiceProducts from './components/management/Products/multiplechoiceproducts';
import ProductMultipleChoiceNotes from './components/management/Products/multiplechoicenotes';
import AddMultilpeChoiceNotes from './components/management/Products/Addmultiplechoicenotes';

import ProductPopupalert from './components/management/Products/popupalert';
import ProductPrdts from './components/management/Products/products';
import ProductPromotion from './components/management/Products/promotions';
import Editpromotion from './components/management/Products/EditPromotion';

import AddProductBrands from './components/management/Products/AddBrandsList';
import AddProductCategorie from './components/management/Products/AddCategoriesList';
import AddMiscProducts from './components/management/Products/AddMiscProductList';
import AddMulChoiceProduct from './components/management/Products/AddMulProductList';
import AddPopupalert from './components/management/Products/AddPopUpList';
import EditPopupalert from './components/management/Products/EditPopUp';
import AddProducts from './components/management/Products/AddProductsList';
import EditProducts from './components/management/Products/EditProducts';
import AddProductPromotion from './components/management/Products/AddPromotionList';

import StaffHours from './components/management/Staff/hours';
import AddHours from './components/management/Staff/AddHours';
import StaffRole from './components/management/Staff/roles';
import AddRole from './components/management/Staff/Addroles';
import Staffs from './components/management/Staff/staff';
import AddStaff from './components/management/Staff/staffAdd';
import StaffEdited from './components/management/Staff/staff_edit';
import RoleEdited from './components/management/Staff/edit_roles';
import HoursEdited from './components/management/Staff/edit-hour';

import StockMovement from './components/management/StockControl/Stockmovements';
import AddStockMovement from './components/management/StockControl/AddStockMovement';
import StockPurchas from './components/management/StockControl/purchaseorder';
import AddStockPurchas from './components/management/StockControl/AddPurchaseOrder';
import StockTakes from './components/management/StockControl/stocktakes';
import StockSupplier from './components/management/StockControl/suppliers';
import AddStockSupplier from './components/management/StockControl/AddSupplier';
import EditSupplier from './components/management/StockControl/EditSuppliers';



import PettyCash from './components/Setup/Banking/PettyCashReasons';
import AddPettyCash from './components/Setup/Banking/AddPrettyCash';
import AddTaxRts from './components/Setup/Banking/AddTaxReason';
import TaxRts from './components/Setup/Banking/TaxRates';
import TenderType from './components/Setup/Banking/TenderTypes';
import AddTender from './components/Setup/Banking/AddTenderType';
import EditPettyCash from './components/Setup/Banking/EditPettyCash';
import EditTaxRate from './components/Setup/Banking/EditTaxRate';
import EditTenderType from './components/Setup/Banking/EditTenderType';

import NoSale from './components/Setup/Company/NosaleReasons';
import AddNoSale from './components/Setup/Company/AddNoSale';
import StockMovementReasons from './components/Setup/Company/StockMovementReasons';
import EditAddStockMovement from './components/Setup/Company/EditStockMov';
import AddStockReasons from './components/Setup/Company/AddStockReason';
import ClockTyp from './components/Setup/Company/clockingTypes';
import CompnyType from './components/Setup/Company/companyDetails';
import Cource from './components/Setup/Company/cources';
import AddCource from './components/Setup/Company/addCourse';
import EditCource from './components/Setup/Company/EditCourse';
import CustomerTyp from './components/Setup/Company/customerTypes';
import Disscount from './components/Setup/Company/discountReasons';
import EditDisscount from './components/Setup/Company/EditDisscount';
import AddDisscount from './components/Setup/Company/AddDiscount';
import Receipt from './components/Setup/Company/receipts';
import RefundsReson from './components/Setup/Company/refundsReasons';
import AddRefundsReson from './components/Setup/Company/AddRefunds';
import EditRefund from './components/Setup/Company/EditRefunds';
import EditNosale from './components/Setup/Company/EditNosales';
import EditClockType from './components/Setup/Company/EditClockType';
import EditCustomerType from './components/Setup/Company/EditCustomerType';

import LocationDvce from './components/Setup/Location/LocationDevice';
import ShowDevice from './components/Setup/Location/showDeviceLocation';
import AddDevice from './components/Setup/Location/addDevice';
import EditDevice from './components/Setup/Location/editDevice';
import EditLocationDvce from './components/Setup/Location/EditLocation';
import AddLocationDvce from './components/Setup/Location/AddLocation';
import OpenHour from './components/Setup/Location/OpeningHours';
import OrderPrint from './components/Setup/Location/OrderPrinting';
import EditOpenHour from './components/Setup/Location/EditOpenHour';
import EditPrinter from './components/Setup/Location/EditPrinter';
import RestoreDta from './components/Setup/Restore Data/RestoreData';

import DashboardReport from './components/reports/dashboard/dashboard';
import NewDashboards from './components/reports/dashboard/NewDashboard';
import WeeklyDashboards from './components/reports/dashboard/WeeklyDashboard';
import AddDashboards from './components/reports/dashboard/AddDashboard';

import CompteTransaction from './components/reports/transactions/CompleteTransactions';
import HeldTransaction from './components/reports/transactions/HeldTransactions';
import OrderService from './components/reports/transactions/OrderServiceStage';
import OrderProd from './components/reports/transactions/OrderedProduct';
import OrderdTrans from './components/reports/transactions/OrderedTransactions';

import SalesBrand from './components/reports/sales/Brands';
import SalesCover from './components/reports/sales/Covers';
import SalesCutomers from './components/reports/sales/CustomerTypess';
import SalesEatOut from './components/reports/sales/EatInOut';
import SalesEmployee from './components/reports/sales/Employess';
import SalesLocation from './components/reports/sales/Location';
import SalesMiscProduct from './components/reports/sales/MiscProduct';
import SalesProduct from './components/reports/sales/Product';
import SalesPromotion from './components/reports/sales/Promotions';
import SalesPurchaseType from './components/reports/sales/PurchaseTypes';
import SalesReports from './components/reports/sales/ReportingCategoires';
import SalesSize from './components/reports/sales/Size';
import SalesSuppliers from './components/reports/sales/Suppliers';
import SalesTillCategoires from './components/reports/sales/TillCategories';
import SalesTimeInterval from './components/reports/sales/TimeIntervals';
import SalesTime from './components/reports/sales/TimePeriod';
import SalesWetDry from './components/reports/sales/WetAndDry';
import TimeComparison from './components/reports/sales/TimeComparisons';

import StocksChanges from './components/reports/stocks/Changes';
import StocksDiscrepancies from './components/reports/stocks/Discrepancies';
import StocksHistory from './components/reports/stocks/History';
import StocksLevels from './components/reports/stocks/Levels';
import StocksNonSellings from './components/reports/stocks/NonSellings';
import StocksWarnings from './components/reports/stocks/Warnings';

import CustomerCr from './components/reports/customer/CustomerCredit';
import CustomerReports from './components/reports/customer/CustomerReport';

import BankingEndOfDay from './components/reports/banking/EndOfDay';
import BankingFloatAdjustments from './components/reports/banking/FloatAdjustments';
import BankingPettyCash from './components/reports/banking/PettyCash';
import BankingServiceCharges from './components/reports/banking/ServiceCharges';
import BankingTenders from './components/reports/banking/Tenders';

import AccountingBookkeeping from './components/reports/accounting/Bookkeeping';
import AccountingDailyTax from './components/reports/accounting/DailyTax';
import AccountingEndYearTax from './components/reports/accounting/EndYearTax';
import AccountingMonthlyTax from './components/reports/accounting/MonthlyTax';
import AccountingPayroll from './components/reports/accounting/Payroll';
import AccountingQuarterlyTax from './components/reports/accounting/QuarterlyTax';

import AuditDiscounts from './components/reports/auditing/Discounts';
import AuditNoSales from './components/reports/auditing/NoSales';
import AuditRefunds from './components/reports/auditing/Refunds';
import AuditVoidLines from './components/reports/auditing/VoidLines';

import DashboardEmail from './components/reports/schedule/DashboardEmails';




let reduxLogger = createLogger();
var startTime = new Date().getTime();
var timeOutTime = startTime + (44 * 60 * 1000);
var previousInterVal;
let store = createStore(AppReducer,
    window.devToolsExtension ? window.devToolsExtension() : f => f,
    process.env.NODE_ENV === 'production'
        ? applyMiddleware(thunk)
        : applyMiddleware(thunk, reduxLogger),
    autoRehydrate()

);
//handleSession(timeOutTime);
let reduxInitTime = new Date();
const expireTransform = createExpirationTransform({
    expireKey: reduxInitTime.setHours(reduxInitTime.getHours() + 2)
});

// if you don't want to use browser history
// createMemoryHistory from react-router
// let history = createMemoryHistory(location);
let persistor = persistStore(store,
    {
        storage: asyncLocalStoragefrom,
        transforms: [expireTransform]
    },
    (err, prevSate) => {
        if (err) {
            console.log(err);
        }

        ReactDOM.render((
            <Provider store={store} persistor={persistor}>
                <Router history={browserHistory}>

                        <Route path='/' component={Home} onEnter={onRouteEnter} />
                        <Route path='/registration' component={Registration} onEnter={onRouteEnter} />
                        <Route path='/forgotpassword' component={ForgotPwd} onEnter={onRouteEnter} />
                        <Route path='/resetpassword' component={ResetPwd} onEnter={onRouteEnter} />

                        <Route path='/app' component={APP} onEnter={onRouteEnter} >
                        {/* profile related */}
                        <Route path='/profile' component={profile} onEnter={onRouteEnter} />

                        
                        <Route path='/reports/summery/dashboard' component={DashboardReport} onEnter={onRouteEnter} />
                        <Route path='/reports/summery/new/dashboard' component={NewDashboards} onEnter={onRouteEnter} />
                        <Route path='/reports/summery/weekly/dashboard' component={WeeklyDashboards} onEnter={onRouteEnter} />
                        <Route path='/reports/summery/add/dashboard' component={AddDashboards} onEnter={onRouteEnter} />
                        <Route path='/reports/Transactions/CompleteTransactions' component={CompteTransaction} onEnter={onRouteEnter} />
                        <Route path='/reports/Transactions/HeldTransactions' component={HeldTransaction} onEnter={onRouteEnter} />
                        <Route path='/reports/Transactions/OrderedTransactions' component={OrderService} onEnter={onRouteEnter} />
                        <Route path='/reports/Transactions/OrderedProduct' component={OrderProd} onEnter={onRouteEnter} />
                        <Route path='/reports/Transactions/OrderServiceStage' component={OrderdTrans} onEnter={onRouteEnter} />

                        <Route path='/reports/Sales/Brands' component={SalesBrand} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/Covers' component={SalesCover} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/CustomerTypes' component={SalesCutomers} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/EatInEatOut' component={SalesEatOut} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/Employess' component={SalesEmployee} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/Location' component={SalesLocation} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/MiscProduct' component={SalesMiscProduct} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/Product' component={SalesProduct} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/Promotions' component={SalesPromotion} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/PurchaseTypes' component={SalesPurchaseType} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/ReportingCategoires' component={SalesReports} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/Size' component={SalesSize} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/Suppliers' component={SalesSuppliers} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/ReportingCategoires' component={SalesTillCategoires} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/TimeIntervals' component={SalesTimeInterval} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/TimePeriod' component={SalesTime} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/WetDry' component={SalesWetDry} onEnter={onRouteEnter} />
                        <Route path='/reports/Sales/TimeComparisons' component={TimeComparison} onEnter={onRouteEnter} />

                        <Route path='/reports/stock/Changes' component={StocksChanges} onEnter={onRouteEnter} />
                        <Route path='/reports/stock/Discrepancies' component={StocksDiscrepancies} onEnter={onRouteEnter} />
                        <Route path='/reports/stock/History' component={StocksHistory} onEnter={onRouteEnter} />
                        <Route path='/reports/stock/Levels' component={StocksLevels} onEnter={onRouteEnter} />
                        <Route path='/reports/stock/Non-sellings' component={StocksNonSellings} onEnter={onRouteEnter} />
                        <Route path='/reports/stock/Warnings' component={StocksWarnings} onEnter={onRouteEnter} />

                        <Route path='/reports/customer/CustomerReport' component={CustomerCr} onEnter={onRouteEnter} />
                        <Route path='/reports/customer/CustomerCredit' component={CustomerReports} onEnter={onRouteEnter} />

                        <Route path='/reports/Banking/EndOfDay' component={BankingEndOfDay} onEnter={onRouteEnter} />
                        <Route path='/reports/Banking/FloatAdjustments' component={BankingFloatAdjustments} onEnter={onRouteEnter} />
                        <Route path='/reports/Banking/PettyCash' component={BankingPettyCash} onEnter={onRouteEnter} />
                        <Route path='/reports/Banking/ServiceCharges' component={BankingServiceCharges} onEnter={onRouteEnter} />
                        <Route path='/reports/Banking/Tenders' component={BankingTenders} onEnter={onRouteEnter} />

                        <Route path='/reports/account/Bookkeeping' component={AccountingBookkeeping} onEnter={onRouteEnter} />
                        <Route path='/reports/account/DailyTax' component={AccountingDailyTax} onEnter={onRouteEnter} />
                        <Route path='/reports/account/EndYearTax' component={AccountingEndYearTax} onEnter={onRouteEnter} />
                        <Route path='/reports/account/MonthlyTax' component={AccountingMonthlyTax} onEnter={onRouteEnter} />
                        <Route path='/reports/account/Payroll' component={AccountingPayroll} onEnter={onRouteEnter} />
                        <Route path='/reports/account/QuarterlyTax' component={AccountingQuarterlyTax} onEnter={onRouteEnter} />

                        <Route path='/reports/auditing/Discounts' component={AuditDiscounts} onEnter={onRouteEnter} />
                        <Route path='/reports/auditing/NoSales' component={AuditNoSales} onEnter={onRouteEnter} />
                        <Route path='/reports/auditing/Refunds' component={AuditRefunds} onEnter={onRouteEnter} />
                        <Route path='/reports/auditing/VoidLines' component={AuditVoidLines} onEnter={onRouteEnter} />

                        <Route path='/reports/scheduled/DashboardEmails' component={DashboardEmail} onEnter={onRouteEnter} />
                        
                        {/* Mangement */}
                        <Route path='/management/Staff/staff' component={Staffs} onEnter={onRouteEnter} />
                        <Route path='/management/Staff/staff-edit' component={StaffEdited} onEnter={onRouteEnter} />
                        <Route path='/management/Staff/hours' component={StaffHours} onEnter={onRouteEnter} />
                        <Route path='/management/Staff/hours/clock-Add' component={AddHours} onEnter={onRouteEnter} />
                        <Route path='/management/Staff/roles' component={StaffRole} onEnter={onRouteEnter} />
                        <Route path='/management/Staff/roles/add-roles' component={AddRole} onEnter={onRouteEnter} />
                        <Route path='/management/Staff/add-staff' component={AddStaff} onEnter={onRouteEnter} />
                        <Route path='/management/Staff/roles/edit-roles' component={RoleEdited} onEnter={onRouteEnter} />
                        <Route path='/management/Staff/hours/edit-hour' component={HoursEdited} onEnter={onRouteEnter} />

                        <Route path='/management/Customers/customerssearch' component={CustomersSearch} onEnter={onRouteEnter} />
                        <Route path='/management/Customers/Invoicemessages' component={InvoiceMessages} onEnter={onRouteEnter} />
                        <Route path='/management/Customers/customerssearch/add-customer' component={Addc} onEnter={onRouteEnter} />

                        <Route path='/management/products/brands' component={ProductBrands} onEnter={onRouteEnter} />
                        <Route path='/management/products/edit-brands' component={EditBrands} onEnter={onRouteEnter} />
                        <Route path='/management/products/categories' component={ProductCategories} onEnter={onRouteEnter} />
                        <Route path='/management/products/miscproducts' component={ProductMiscProducts} onEnter={onRouteEnter} />
                        <Route path='/management/products/multiplechoiceproducts' component={ProductMultipleChoiceProducts} onEnter={onRouteEnter} />
                        <Route path='/management/products/multiplechoicenotes' component={ProductMultipleChoiceNotes} onEnter={onRouteEnter} />
                        <Route path='/management/products/Addmultiplechoicenotes' component={AddMultilpeChoiceNotes} onEnter={onRouteEnter} />

                        <Route path='/management/products/popupalert' component={ProductPopupalert} onEnter={onRouteEnter} />
                        <Route path='/management/products/edit-popup' component={EditPopupalert} onEnter={onRouteEnter} />
                        <Route path='/management/products/products' component={ProductPrdts} onEnter={onRouteEnter} />
                        <Route path='/management/products/promotions' component={ProductPromotion} onEnter={onRouteEnter} />
                        <Route path='/management/products/edit-promotion' component={Editpromotion} onEnter={onRouteEnter} />
                        <Route path='/management/products/add-product' component={AddProducts} onEnter={onRouteEnter} />
                        <Route path='/management/products/edit-product' component={EditProducts} onEnter={onRouteEnter} />
                        <Route path='/management/products/brands/add-brand' component={AddProductBrands} onEnter={onRouteEnter} />
                        <Route path='/management/products/categories/add-categories' component={AddProductCategorie} onEnter={onRouteEnter} />
                        <Route path='/management/products/miscproducts/add-miscproducts' component={AddMiscProducts} onEnter={onRouteEnter} />
                        <Route path='/management/products/multiplechoiceproducts/add-multiplechoiceproducts' component={AddMulChoiceProduct} onEnter={onRouteEnter} />
                        <Route path='/management/products/popupalert/add-popupalert' component={AddPopupalert} onEnter={onRouteEnter} />
                        <Route path='/management/products/promotions/add-promotions' component={AddProductPromotion} onEnter={onRouteEnter} />

                        <Route path='/management/StockControl/Stockmovements' component={StockMovement} onEnter={onRouteEnter} />
                        <Route path='/management/StockControl/Stockmovements/Add-StockMovement' component={AddStockMovement} onEnter={onRouteEnter} />
                        <Route path='/management/StockControl/purchaseorder' component={StockPurchas} onEnter={onRouteEnter} />
                        <Route path='/management/StockControl/purchaseorder/Add-Purchaseorder' component={AddStockPurchas} onEnter={onRouteEnter} />
                        <Route path='/management/StockControl/stocktakes' component={StockTakes} onEnter={onRouteEnter} />
                        <Route path='/management/StockControl/suppliers' component={StockSupplier} onEnter={onRouteEnter} />
                        <Route path='/management/StockControl/suppliers/add-supplier' component={AddStockSupplier} onEnter={onRouteEnter} />
                        <Route path='/management/StockControl/edit-supplier' component={EditSupplier} onEnter={onRouteEnter} />


                        <Route path='/setup/banking/pettycash' component={PettyCash} onEnter={onRouteEnter} />
                        <Route path='/setup/company/edit-pettyCash' component={EditPettyCash} onEnter={onRouteEnter} />
                        <Route path='/setup/banking/add-PettyCash' component={AddPettyCash} onEnter={onRouteEnter} />
                        <Route path='/setup/banking/tender' component={TenderType} onEnter={onRouteEnter} />
                        <Route path='/setup/banking/add-Tender' component={AddTender} onEnter={onRouteEnter} />
                        <Route path='/setup/banking/taxRate' component={TaxRts} onEnter={onRouteEnter} />
                        <Route path='/setup/banking/add-tax' component={AddTaxRts} onEnter={onRouteEnter} />
                        <Route path='/setup/company/edit-tax-rate' component={EditTaxRate} onEnter={onRouteEnter} />
                        <Route path='/setup/company/edit-tender' component={EditTenderType} onEnter={onRouteEnter} />

                        <Route path='/setup/Company/NosaleReasons' component={NoSale} onEnter={onRouteEnter} />
                        <Route path='/setup/company/edit-nosale' component={EditNosale} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/NosaleReasons/add-noSale' component={AddNoSale} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/StockMovementReasons' component={StockMovementReasons} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/add-stockReason' component={AddStockReasons} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/edit-stockReason' component={EditAddStockMovement} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/clockingTypes' component={ClockTyp} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/companyDetails' component={CompnyType} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/cources' component={Cource} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/add-cources' component={AddCource} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/customerTypes' component={CustomerTyp} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/discountReasons' component={Disscount} onEnter={onRouteEnter} />
                        <Route path='/setup/company/edit-disscount' component={EditDisscount} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/add-Discount' component={AddCource} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/receipts' component={Receipt} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/refundsReasons' component={RefundsReson} onEnter={onRouteEnter} />
                        <Route path='/setup/Company/add-Refund' component={AddRefundsReson} onEnter={onRouteEnter} />
                        <Route path='/setup/company/edit-refund' component={EditRefund} onEnter={onRouteEnter} />
                        <Route path='/setup/company/edit-stock' component={EditCource} onEnter={onRouteEnter} />
                        <Route path='/setup/company/edit-Clock-type' component={EditClockType} onEnter={onRouteEnter} />
                        <Route path='/setup/company/edit-Customer-type' component={EditCustomerType} onEnter={onRouteEnter} />

                        <Route path='/setup/location/locationDevices' component={LocationDvce} onEnter={onRouteEnter} />
                        <Route path='/setup/location/add-device' component={ShowDevice} onEnter={onRouteEnter} />
                        <Route path='/setup/location/add-devices' component={AddDevice} onEnter={onRouteEnter} />
                        <Route path='/setup/location/edit-device' component={EditDevice} onEnter={onRouteEnter} />
                        <Route path='/setup/location/edit-location' component={EditLocationDvce} onEnter={onRouteEnter} />
                        <Route path='/setup/location/add-location' component={AddLocationDvce} onEnter={onRouteEnter} />
                        <Route path='/setup/location/orderPrinting' component={OrderPrint} onEnter={onRouteEnter} />
                        <Route path='/setup/location/openingHr' component={OpenHour} onEnter={onRouteEnter} />
                        <Route path='/setup/location/edit-opningHour' component={EditOpenHour} onEnter={onRouteEnter} />
                        <Route path='/setup/location/edit-order-print' component={EditPrinter} onEnter={onRouteEnter} />

                        <Route path='/setup/restore' component={RestoreDta} onEnter={onRouteEnter} />

                      </Route>


                </Router>
            </Provider >
        ), document.getElementById('root'));
    });

window.onerror = (message, source, lineno, colno, error) => {
    console.log(message, source, lineno, colno, error);
};

/*global ga*/

function onRouteEnter({routes, params, location}, replaceFunc) {
  // function to be called when route is entered or the page is rendererd
  console.info('On Route Enter Called.....');
}


function handleSession(endTime) {
    if (previousInterVal) {
        clearInterval(previousInterVal);
    }
    setInterval(function () {
        var currentTime = new Date().getTime();
        if (currentTime >= endTime) {
            localStorage.clear();
            window.location.reload();
        }
    }, 1000)
}
