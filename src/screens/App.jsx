import React from 'react';
import { connect } from 'react-redux';
import { withRouter,Link } from 'react-router';

import * as UserActions from './../actions/user_actions';
import { MenuItem ,DropdownButton,Dropdown, } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import logo from '../img/logo.svg';
import { Menu } from './menu';
import { MenuSetup } from './menu-setup';
import { MenuReport } from './menu-report';

let APP = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue',logoutclicked:false,setupMenu:true,reportMenu:false,sideMenuToggle:false

        }
    },
    componentWillMount() {

       //;
       if(!this.props.UserRegisteration.IsLoggedIn){
         this.props.router.push('/');
       }
    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    loginHandler(){

      this.props.dispatch(UserActions.userRegisteration());
    },
 
    ProfileHandler(){
       this.props.router.push('/profile');
    },


    navigationHandler(){
    },

    menuClicked(event) {
     this.props.router.push(event.currentTarget.dataset.routerpath);
     if(screen.width < 768){
      this.setState({sideMenuToggle:true})
     }
    
   },
   logout(){
     
     this.props.router.push('/');
     this.props.dispatch(UserActions.logout());
   },
   sideMenuToggle(){
    let toggleState = this.state.sideMenuToggle ? false : true;
    
    this.setState({sideMenuToggle:toggleState})
   },
    render() {

      let url = window.location.href,setupMenu=true,reportMenu=false,managementActive="",setUpActive="",reportActive="";
      url.indexOf("setup")  !== -1 ? setupMenu=true : setupMenu=false;
      url.indexOf("reports")  !== -1 ? reportMenu=true : reportMenu=false;
      setupMenu ? managementActive = "active" : reportMenu ?reportActive="active": setUpActive="active";
        return (
          <div className=" dashboard ">
          <nav className="navbar top-navigation navbar-fixed-top nav-background">
             <div className="container-fluid">
               <div className="navbar-header col-sm-12 col-md-2 col-lg-2">

                 <a className="navbar-brand" onClick={this.sideMenuToggle} href="#"><img src={logo}/> 
                 <span className={this.state.sideMenuToggle ? "toggleMbtn t":"toggleMbtn f"}></span>
                 </a>
               </div>
               <div className="col-sm-12 col-md-9 col-lg-9 main-nav no-padding navbar navbar-default">

                    <ul className="nav navbar-nav" id="navbar">
                      <li  className={managementActive} onClick={this.menuClicked} data-routerpath='/setup/Company/companyDetails'>
                        <a><span className="icon icon-settings"></span><span className="text">Setup</span></a>
                      </li>
                      <li  className={setUpActive} onClick={this.menuClicked} data-routerpath='/management/Staff/staff'>
                        <a><span className="icon icon-mangement"></span>  <span className="text">MANAGEMENT</span></a>

                      </li>
                      <li className={reportActive} onClick={this.menuClicked} data-routerpath='/reports/summery/dashboard'>
                        <a><span className="icon icon-reports"></span>   <span className="text">REPORTING</span></a>
                      </li>
                      <li   >
                        <a href='http://ec2-52-66-154-177.ap-south-1.compute.amazonaws.com/' target="blank"><span className="icon icon-till"></span> <span className="text">TILL</span></a>
                      </li>
                      <li  onClick={this.menuClicked} >
                      <a><span className="icon icon-support"></span> <span className="text">SUPPORT</span></a>
                      </li>
                      <li  onClick={this.menuClicked} >
                        <a><span className="icon icon-webstore"></span> <span className="text"> WEB STORE </span></a>

                        </li>
                      <li  onClick={this.menuClicked} >
                      <a><span className="icon icon-app"></span> <span className="text">  APP  </span></a>

                      </li>
                      <li  onClick={this.menuClicked} >
                        <a><span className="icon icon-mangement"></span>  <span className="text"> GUIDE ME  </span></a>
                      </li>
                    </ul>

               </div>
               <ul className="nav navbar-nav navbar-right col-sm-12 col-md-1 col-lg-1 userprofile">

                   <li className="user-profile">
                   <DropdownButton   title={   this.props.UserRegisteration.loginDetail && this.props.UserRegisteration.loginDetail.data.fname+" "+this.props.UserRegisteration.loginDetail.data.lname}   id={`dropdown-basic`}>
                        <li><Link to='/Profile' activeClassName="active">Profile</Link></li>
                        <li><Link to='/Profile' activeClassName="active">Users</Link></li>
                        <li><Link to='/Profile' activeClassName="active">Password</Link></li>
                        <li><a onClick={this.logout}>Logout</a></li>

                      </DropdownButton>

                      </li>

                 </ul>
             </div>
             
           </nav>
    <div className="container-fluid main-container">
      <div className="row">
       
        
      </div>
      <div className="row app-container">
      <div className={this.state.sideMenuToggle? "sidebar toggleState":"col-sm-12 col-md-2 col-lg-2 sidebar" }>
        {setupMenu ? <MenuSetup/> : reportMenu ?<MenuReport/>:<Menu/> }
      </div>
      <div className={this.state.sideMenuToggle?"mainDiv menuToggle":"mainDiv"}>
        {this.props.children}
      </div>
       

      </div>
    </div>

          </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserRegisteration:state.User,
       UserLogin:state.User,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(UserActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(APP));
