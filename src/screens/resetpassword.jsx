import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as UserActions from './../actions/user_actions';
import { bindActionCreators } from 'redux';
import logo from '../img/logo.svg';

let ResetPassword = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    loginHandler(){
      var data = "jp@gmail.com";

      this.props.dispatch(UserActions.get_resetpwd_data(data));

    },

    render() {
        return (
          <div className="wrapper ">
            <div className="regsiter container">
                <div className="row">
                    <p className="text-center">
                        <img src={logo}/>
                    </p>
                    <h1 className="title text-center">Join now</h1>

                        <p className="col-md-6 col-lg-6">
                            <input type="password" ref="password"  placeholder="new password"   />
                        </p>
                        <p className="col-md-6 col-lg-6">
                            <input type="password" ref="cpassword"  placeholder="confirm password"   />
                        </p>
                        <div className="text-center">
                            <p>
                                <button  onClick={this.loginHandler} type="button" className="btn btn-primary register-btn">Change password
                                </button>
                            </p>
                        </div>
                </div>
            </div>
          </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserRegisteration:state.User,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(UserActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(ResetPassword));
