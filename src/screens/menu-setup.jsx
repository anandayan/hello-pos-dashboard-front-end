import React from 'react';
import { connect } from 'react-redux';
import { withRouter,Link } from 'react-router';
import Collapsible from 'react-collapsible';
export  var MenuSetup = React.createClass({

  getInitialState() {
      return {
        company:true,
        location:false,
        banking:false,
        restore:false
      }
  },

  companyaccodOpen(){
      this.setState({company:true, location:false,banking:false,restore:false});
  },
  locationaccodOpen(){
      this.setState({company:false, location:true,banking:false,restore:false});
  },
  bankingaccodOpen(){
      this.setState({company:false, location:false,banking:true,restore:false});
  },
restoreccodOpen(){
      this.setState({company:false, location:false,banking:false,restore:true});
  },
    render() {

        return (

          <ul class ref='sideMenu'>
          <li>
          <Collapsible trigger="Company"  open={this.state.company} onOpen={this.companyaccodOpen} >
            <ul>
            <li><Link to='/setup/Company/NosaleReasons' activeClassName="active">- No Sale Reasons</Link></li>
            <li><Link to='/setup/Company/StockMovementReasons' activeClassName="active">- Stock Movement Reasons</Link></li>
            <li><Link to='/setup/Company/clockingTypes' activeClassName="active">- Clocking Types</Link></li>
            <li><Link to='/setup/Company/companyDetails' activeClassName="active">- Company Details</Link></li>
            <li><Link to='/setup/Company/cources' activeClassName="active">- Cources</Link></li>
            <li><Link to='/setup/Company/customerTypes' activeClassName="active">- Customer Types</Link></li>
            <li><Link to='/setup/Company/discountReasons' activeClassName="active">- Discount Reasons</Link></li>
            <li><Link to='/setup/Company/receipts' activeClassName="active">- Receipts</Link></li>
            <li><Link to='/setup/Company/refundsReasons' activeClassName="active">- Refunds Reasons</Link></li>
            </ul>
          </Collapsible>
    </li>

          <li>

                <Collapsible trigger="Locations" open={this.state.location} onOpen={this.locationaccodOpen} >
                <ul>
                    <li><Link to='/setup/location/locationDevices' activeClassName="active">- Location & Devices</Link></li>
                    <li><Link to='/setup/location/orderPrinting' activeClassName="active">- Order Printing</Link></li>
                    <li><Link to='/setup/location/openingHr' activeClassName="active">- Opening Hours</Link></li>
                </ul>
                </Collapsible>

          </li>
          <li>
            <Collapsible trigger="Banking" open={this.state.banking} onOpen={this.bankingaccodOpen}>
            <ul>
              <li><Link to='/setup/banking/tender' activeClassName="active">- Tender Types</Link></li>
              <li><Link to='/setup/banking/taxRate' activeClassName="active">- Tax Rates</Link></li>
              <li><Link to='/setup/banking/pettyCash' activeClassName="active">- Petty Cash Reasons</Link></li>
            </ul></Collapsible>

          </li>

            <li>

              <Collapsible trigger="Restore Data"  open={this.state.restore} onOpen={this.restoreccodOpen}>
              <ul>
                <li><Link to='/setup/restore' activeClassName="active">- Restore Data</Link></li>
              </ul>
              </Collapsible>

            </li>
          </ul>
        );
    }
});
