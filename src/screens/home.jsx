import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
let screenIdentifier = 'deiveryPickupScreen';
import * as UserActions from './../actions/user_actions';
import { bindActionCreators } from 'redux';
import logo from '../img/logo_login.svg';


let PickupDelivery = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue',loginclicked:false
        }
    },
    componentWillReceiveProps(nextProps) {
      //;
      let loginData = nextProps.UserLogin.loginDetail;
    
      if(this.state.loginclicked){
        if(loginData.status_code === 200){
          //this.props.router.push('/setup/Company/companyDetails');
          this.props.router.push('management/Staff/staff');
          
          this.setState({loginclicked:false});
        }
        else{
          alert(loginData.message)
        }
      }


    },
    componentWillMount() {
     
      if(this.props.UserLogin.loginDetail && this.props.UserLogin.loginDetail.token){
        
      // this.props.router.push('/setup/Company/companyDetails');
      this.props.router.push('/reports/summery/dashboard');
   
      }
    },
    componentDidMount() {
      
    },
    loginHandler(){
      let data = {
        email: this.refs.username.value, //'jp@gmail.com',
        password:this.refs.password.value//'sachin161'
      }
      this.props.dispatch(UserActions.userLlogin(data));
      this.setState({loginclicked:true});
    },
    navigationHandler(){

    },
    register(){
       this.props.router.push('/registration');
    },
    frgtpwd(){
    this.props.router.push('/forgotpassword');
    },
    render() {
        return (
          <div className="wrapper container">
            <div className="row">
            <form className="login col-md-6 col-lg-6">
                <p className="title text-center">User login</p>
                <input type="text" ref="username" placeholder="Username"  />
                <input type="password" ref="password" placeholder="Password"   />
                <p>
                  <input type="checkbox" value="1" id="loginTrue"/> <label htmlFor="loginTrue" >Keep me loged in</label>
                </p>
                <p>
                  <input type="checkbox" value="1" id="tillLogin"/> <label htmlFor="tillLogin">Loggin into till</label>
                </p>
                <p className="text-right">
                  <a onClick={this.frgtpwd}>Forgot your password?</a>
                </p>

                <div className="text-right">
                  <button onClick={this.loginHandler} type="button" className="btn btn-primary">LOGIN</button>
                </div>
            </form>
            <div className="epos-main-content col-md-6 col-lg-6 text-center">
              <p><img className="login-logo" src={logo}/></p>
                <div className="epos-content">
                  <h2>Free <strong>30 DAYS</strong> TRAIL</h2>
                  <p className="instantAcc">Instant access</p>
                  <h3>No credit card required</h3>
                  <p className="reated-eops">See for yourself why more than 10,000 business love Epos Now the #1 rated POS software</p>
                </div>
               <div className="text-center">
                 <button onClick={this.register} type="button" className="btn btn-primary">SIGN UP FOR A FREE TRAIL</button>
               </div>
            </div>
            </div>
          </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
        UserLogin:state.User,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(UserActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(PickupDelivery));
