import React from 'react';
import { connect } from 'react-redux';
import { withRouter,Link } from 'react-router';
import Collapsible from 'react-collapsible';
export  var MenuReport = React.createClass({

  getInitialState() {
      return {
        company:true,
        location:false,
        sales:false,
        restore:false,
        stock:false,
        customer:false,
        banking1:false,
        account:false,
        auditing:false,
        scheduled:false,
      }
  },

  companyaccodOpen(){
      this.setState({company:true, 
        location:false,
        sales:false,
        restore:false, 
        stock:false,
        customer:false,
        banking1:false,
        account:false,
        auditing:false,
        scheduled:false,});
  },
  locationaccodOpen(){
    this.setState({company:false, 
      location:true,
      sales:false,
      restore:false, 
      stock:false,
      customer:false,
      banking1:false,
      account:false,
      auditing:false,
      scheduled:false,});
  },
  bankingaccodOpen(){
    this.setState({company:false, 
      location:false,
      sales:true,
      restore:false, 
      stock:false,
      customer:false,
      banking1:false,
      account:false,
      auditing:false,
      scheduled:false,});
  },
  stockccodOpen(){
    this.setState({company:false, 
      location:false,
      sales:false,
      restore:false, 
      stock:true,
      customer:false,
      banking1:false,
      account:false,
      auditing:false,
      scheduled:false,});
  },
  customerccodOpen(){
    this.setState({company:false, 
      location:false,
      sales:false,
      restore:false, 
      stock:false,
      customer:true,
      banking1:false,
      account:false,
      auditing:false,
      scheduled:false,});
  },
  banking1ccodOpen(){
    this.setState({company:false, 
      location:false,
      sales:false,
      restore:false, 
      stock:false,
      customer:false,
      banking1:true,
      account:false,
      auditing:false,
      scheduled:false,});
  },
  accountccodOpen(){
    this.setState({company:false, 
      location:false,
      sales:false,
      restore:false, 
      stock:false,
      customer:false,
      banking1:false,
      account:true,
      auditing:false,
      scheduled:false,});
  },
  auditingccodOpen(){
    this.setState({company:false, 
      location:false,
      sales:false,
      restore:false, 
      stock:false,
      customer:false,
      banking1:false,
      account:false,
      auditing:true,
      scheduled:false,});
  },
  ScheduledccodOpen(){
    this.setState({company:false, 
      location:false,
      sales:false,
      restore:false, 
      stock:false,
      customer:false,
      banking1:false,
      account:false,
      auditing:false,
      scheduled:true,});
  },
    render() {

        return (

          <ul className="sidemenu-butto  " ref='sideMenu'>
          <li>
              <Collapsible trigger="Summery"  open={this.state.company} onOpen={this.companyaccodOpen} >
                <ul>
                <li><Link to='/reports/summery/dashboard' activeClassName="active">- Dashboard</Link></li>
                <li><Link to='/reports/summery/new/dashboard' activeClassName="active">- New Dashboard</Link></li>
                <li><Link to='/reports/summery/weekly/dashboard' activeClassName="active">- Weekly Dashboard</Link></li>
                <li><Link to='/reports/summery/add/dashboard' activeClassName="active">- Add Dashboard</Link></li>
                </ul>
              </Collapsible>
            </li>

          <li>

                <Collapsible trigger="Transactions" open={this.state.location} onOpen={this.locationaccodOpen} >
                <ul>
                    <li><Link to='/reports/Transactions/CompleteTransactions' activeClassName="active">- Complete Transactions</Link></li>
                    <li><Link to='/reports/Transactions/HeldTransactions' activeClassName="active">- Held Transactions</Link></li>
                    <li><Link to='/reports/Transactions/OrderedTransactions' activeClassName="active">- Ordered Transactions</Link></li>
                    <li><Link to='/reports/Transactions/OrderedProduct' activeClassName="active">- Ordered Product</Link></li>
                    <li><Link to='/reports/Transactions/OrderServiceStage' activeClassName="active">- Order Service Stage</Link></li>
                </ul>
                </Collapsible>

          </li>
          <li>
            <Collapsible trigger="Sales" open={this.state.sales} onOpen={this.bankingaccodOpen}>
            <ul>
              <li><Link to='/reports/Sales/TimeIntervals' activeClassName="active">- Time Intervals</Link></li>
              <li><Link to='/reports/Sales/TimePeriod' activeClassName="active">- Time Period</Link></li>
              <li><Link to='/reports/Sales/TimeComparisons' activeClassName="active">- Time Comparisons</Link></li>
              <li><Link to='/reports/Sales/Product' activeClassName="active">- Product</Link></li>
              <li><Link to='/reports/Sales/Employess' activeClassName="active">- Employess</Link></li>
              <li><Link to='/reports/Sales/Location' activeClassName="active">- Location</Link></li>
              <li><Link to='/reports/Sales/Size' activeClassName="active">- Size</Link></li>
              <li><Link to='/reports/Sales/MiscProduct' activeClassName="active">- Misc Product</Link></li>
              <li><Link to='/reports/Sales/WetDry' activeClassName="active">- Wet and Dry</Link></li>
              <li><Link to='/reports/Sales/Covers' activeClassName="active">- Covers</Link></li>
              <li><Link to='/reports/Sales/Promotions' activeClassName="active">- Promotions</Link></li>
              <li><Link to='/reports/Sales/Brands' activeClassName="active">- Brands</Link></li>
              <li><Link to='/reports/Sales/Suppliers' activeClassName="active">- Suppliers</Link></li>
              <li><Link to='/reports/Sales/CustomerTypess' activeClassName="active">- Customer Typess</Link></li>
              <li><Link to='/reports/Sales/ReportingCategoires' activeClassName="active">- Reporting Categoires</Link></li>
              <li><Link to='/reports/Sales/TillCatefories' activeClassName="active">- Till Categories</Link></li>
              <li><Link to='/reports/Sales/EatInEatOut' activeClassName="active">- Eat in / Eat Out</Link></li>
              <li><Link to='/reports/Sales/PurchaseTypes' activeClassName="active">- Purchase Types</Link></li>
            </ul></Collapsible>

          </li>

            <li>

              <Collapsible trigger="Stock"  open={this.state.stock} onOpen={this.stockccodOpen}>
              <ul>
                <li><Link to='/reports/stock/Levels' activeClassName="active">- Levels</Link></li>
                <li><Link to='/reports/stock/Warnings' activeClassName="active">- Warnings</Link></li>
                <li><Link to='/reports/stock/Discrepancies' activeClassName="active">- Discrepancies</Link></li>
                <li><Link to='/reports/stock/Non-sellings' activeClassName="active">- Non-sellings</Link></li>
                <li><Link to='/reports/stock/History' activeClassName="active">- History</Link></li>
                <li><Link to='/reports/stock/Changes' activeClassName="active">- Changes</Link></li>
              </ul>
              </Collapsible>

            </li>
            <li>
            
              <Collapsible trigger="Customer"  open={this.state.customer} onOpen={this.customerccodOpen}>
               <ul>
               <li><Link to='/reports/customer/CustomerReport' activeClassName="active">- Customer Report</Link></li>
               <li><Link to='/reports/customer/CustomerCredit' activeClassName="active">- Customer Credit</Link></li>
               </ul>
               </Collapsible>
               </li> 
               <li>
               
              <Collapsible trigger="Banking"  open={this.state.banking1} onOpen={this.banking1ccodOpen}>
               <ul>
               <li><Link to='/reports/Banking/Tenders' activeClassName="active">- Tenders</Link></li>
               <li><Link to='/reports/Banking/EndOfDay' activeClassName="active">- End of Day</Link></li>
               <li><Link to='/reports/Banking/FloatAdjustments' activeClassName="active">- Float Adjustments</Link></li>
               <li><Link to='/reports/Banking/PettyCash' activeClassName="active">- Petty Cash</Link></li>
               <li><Link to='/reports/Banking/ServiceCharges' activeClassName="active">- Service Charges</Link></li>
               </ul>
               </Collapsible>
               </li> 
               <li>
               
              <Collapsible trigger="Accounting"  open={this.state.account} onOpen={this.accountccodOpen}>
               <ul>
               <li><Link to='/reports/account/Bookkeeping' activeClassName="active">- Bookkeeping</Link></li>
               <li><Link to='/reports/account/DailyTax' activeClassName="active">- Daily Tax</Link></li>
               <li><Link to='/reports/account/MonthlyTax' activeClassName="active">- Monthly Tax</Link></li>
               <li><Link to='/reports/account/QuarterlyTax' activeClassName="active">- Quarterly Tax</Link></li>
               <li><Link to='/reports/account/EndYearTax' activeClassName="active">- End of Year Tax</Link></li>
               <li><Link to='/reports/account/Payroll' activeClassName="active">- Payroll</Link></li>
               </ul>
               </Collapsible>
               </li> 
               <li>
               
              <Collapsible trigger="Auditing"  open={this.state.auditing} onOpen={this.auditingccodOpen}>
               <ul>
               <li><Link to='/reports/auditing/Refunds' activeClassName="active">- Refunds</Link></li>
               <li><Link to='/reports/auditing/Discounts' activeClassName="active">- Discounts</Link></li>
               <li><Link to='/reports/auditing/NoSales' activeClassName="active">- No Sales</Link></li>
               <li><Link to='/reports/auditing/VoidLines' activeClassName="active">- Void Lines</Link></li>
               </ul>
               </Collapsible>
               </li> 
               <li>
               
              <Collapsible trigger="Scheduled Emails"  open={this.state.scheduled} onOpen={this.ScheduledccodOpen}>
               <ul>
               <li><Link to='/reports/scheduled/DashboardEmails' activeClassName="active">- Dashboard Emails</Link></li>
              
               </ul>
               </Collapsible>
               </li> 
          </ul>
        );
    }
});


