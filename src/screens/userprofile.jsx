import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import * as UserActions from './../actions/user_actions';
 
import { bindActionCreators } from 'redux';
import Collapsible from 'react-collapsible';
import logo from '../img/logo.svg';

let UserProfile = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillMount() {

    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {

    },
    loginHandler(){


    },

    render() {
        return (
          <div class>

                <div className="col-sm-12 col-md-12 col-lg-12 right-container">
                    <div className="upgrate license-link">
                        
                    </div>
                    <div className="filter-by-brand">
                        <h1 className="title">Account</h1>
                        <p>Company: Now Peri Peri Limited</p>
                    </div>

                    <div className="filter-by-brand">
                       <h1 className="title">User</h1>
                        <p>User Name: Now Peri peri</p>
                    </div>

                    <div className="filter-by-brand">
                      <div className="col-sm-4">
                          <span className="help-block txtalignRight">Registration Email:</span>
                      </div>
                      <div className="col-sm-8">
                        <input ref="role_discription" className="form-control input-text-fields" id="role_discription"  type="text"   autoComplete="off"/>
                        <input className="add-other" value="Save" type="button" id=""/>
                      </div>
                    </div>
                    <div className="filter-by-brand">
                      <div className="col-sm-12">
                        <input className="add-other" value="Change Password" type="button" id=""/>
                      </div>
                    </div>

                    <div className="filter-by-brand">
                      <div className="col-sm-12">
                        <h1 className="title">Access</h1>
                        <p><span>FULL ACCESS</span></p>
                        <p>Activity</p>
                        <p>Signup Date : 20/06/2017 17:21:58</p>
                        <p>Last Login : 07/08/2017 18:43:44</p>
                        <p>Last Activity : 07/08/2017 18:44:33</p>
                        <p>ActivitLast Lockout : nevery</p>
                        <p>Last Password Change : 20/06/2017 17:21:58</p>
                        <p>Unique ID : 5124b5db-2508-4108-99da-f7791e5a48a6</p>
                      </div>
                    </div>
                </div>
            </div>
        );
    }
});


let mapStateToProps = (state) => {
    return {
      UserLogin:state.User,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(UserActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(UserProfile));
