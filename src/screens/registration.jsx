import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
let screenIdentifier = 'deiveryPickupScreen';
import * as UserActions from './../actions/user_actions';
import { bindActionCreators } from 'redux';
import logo from '../img/logo.svg';

let Registration = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue',
             registerClicked:false
        }
    },
    componentWillReceiveProps(nextProps) {
        
        let registerData = nextProps.UserRegisteration.register;
        if(registerData.status_code == 200){
              alert("Please login with your email to proceed further")
              this.props.router.push('/');
        }

      //  UserRegisteration
    },
    componentDidMount() {

    },
    loginHandler(){
        
      var data = {
        fname:this.refs.fname.value,
        lname:this.refs.lname.value,
        email:this.refs.email.value,
        business_name:this.refs.businessName.value,
        phone:this.refs.phone.value,
        industry:this.refs.industry.value,
        looking_for:this.refs.lookingFor.value,
        password:this.refs.password.value,
        role_id:'super_admin'

      }

      this.props.dispatch(UserActions.userRegisteration(data));

    },
    navigationHandler(){
    },
    render() {
        return (
          <div className="wrapper ">
    <div className="regsiter container">
        <div className="row">
            <p className="text-center">
                <img src={logo}/>
            </p>
            <h1 className="title text-center">Join now</h1>
                <p className=" col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <input type="text" ref="fname" className="pull-right" placeholder="First name"  />
                </p>
                <p className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <input type="text" ref="lname" placeholder="Last name"  />
                </p>
                <p className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <input type="text" ref="email" className="pull-right" placeholder="Email"  />
                </p>
                <p className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <input type="text" ref="businessName" placeholder="Business Name"  />
                </p>
                <p className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <input type="text" ref="phone" className="pull-right" placeholder="Phone"  />
                </p>
                <p className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <input type="text" ref="industry"  placeholder="Industry" />
                </p>
                <p className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <input type="text" ref="lookingFor" className="pull-right" placeholder="Looking For"  />
                </p>
                <p className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <input type="password" ref="password"  placeholder="Password"   />
                </p>
                <div className="text-center">
                    <p>
                        <button  onClick={this.loginHandler} type="button" className="btn btn-primary register-btn">Register
                        </button>
                    </p>
                </div>
        </div>
    </div>
  </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
      UserRegisteration:state.User,
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(UserActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(Registration));
