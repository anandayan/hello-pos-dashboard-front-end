import React from 'react';
import { connect } from 'react-redux';
import { withRouter,Link } from 'react-router';
import Collapsible from 'react-collapsible';
import Accordion from 'react-responsive-accordion';
import user from '../img/epos_users_icons_64x64.png';



export  var Menu = React.createClass({
  getInitialState() {
      return {
        staff:true,
        product:false,
        stock:false,
        customer:false
      }
  },

staffaccodOpen(){
    this.setState({staff:true, product:false,stock:false,customer:false});
},
productaccodOpen(){
    this.setState({staff:false, product:true,stock:false,customer:false});
},
stockaccodOpen(){
    this.setState({staff:false, product:false,stock:true,customer:false});
},
customeraccodOpen(){
    this.setState({staff:false, product:false,stock:false,customer:true});
},

    render() {

        return (

          <ul className="sidemenu-buttons " ref='sideMenu'>
            
          <li>
          
         <Collapsible trigger="STAFF" open={this.state.staff} onOpen={this.staffaccodOpen} >
            <ul>
            <li><Link to='/management/Staff/staff' activeClassName="active">- Staff</Link></li>
            <li><Link to='/management/Staff/roles' activeClassName="active">- Roles</Link></li>
            <li><Link to='/management/Staff/hours' activeClassName="active">- Hours</Link></li>
            </ul>
          </Collapsible>
    </li>

          <li>

                <Collapsible trigger="Products" open={this.state.product} onOpen={this.productaccodOpen}>
                <ul>
                    <li><Link to='/management/products/products' activeClassName="active">- Products</Link></li>
                    <li><Link to='/management/products/categories' activeClassName="active">- Categories</Link></li>
                    <li><Link to='/management/products/brands' activeClassName="active">- brands</Link></li>
                    <li><Link to='/management/products/miscproducts' activeClassName="active">- misc products</Link></li>
                    <li><Link to='/management/products/multiplechoiceproducts' activeClassName="active">- multiple choice products</Link></li>
                    <li><Link to='/management/products/multiplechoicenotes' activeClassName="active">- multiple choice notes</Link></li>
                    <li><Link to='/management/products/popupalert' activeClassName="active">- popup alert</Link></li>
                    <li><Link to='/management/products/promotions' activeClassName="active">- promotions</Link></li>

                </ul>
                </Collapsible>

          </li>
          <li>
            <Collapsible trigger="Stock Control" open={this.state.stock} onOpen={this.stockaccodOpen}>
            <ul>
              <li><Link to='/management/StockControl/Stockmovements' activeClassName="active">- Stock movements</Link></li>
              <li><Link to='/management/StockControl/purchaseorder' activeClassName="active">- purchase order</Link></li>
              <li><Link to='/management/StockControl/stocktakes' activeClassName="active">- stock takes</Link></li>
              <li><Link to='/management/StockControl/suppliers' activeClassName="active">- suppliers</Link></li>
            </ul></Collapsible>

          </li>

            <li>

              <Collapsible trigger="Customers" open={this.state.customer} onOpen={this.customeraccodOpen}>
              <ul>
                <li><Link to='/management/Customers/customerssearch' activeClassName="active">- Customer Search</Link></li>
                <li><Link to='/management/Customers/Invoicemessages' activeClassName="active">- Invoice Messages</Link></li>
              </ul>
              </Collapsible>

            </li>
          </ul>
        );
    }
});
