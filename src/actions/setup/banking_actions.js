import * as API from './../../base/api_service';
import * as serviceCallConfig from './../../config/api_urls';


export var ADD_TENDER_TYPE = 'ADD_TENDER_TYPE';
export var getTenderType = (data) => ({
    type: ADD_TENDER_TYPE,
    data: data
});


export var addTenderTypeData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_TENDER_TYPE),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_TENDER_TYPE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var GET_TENDER_TYPE = 'GET_TENDER_TYPE';
export var getListTenderTypeData = (data) => ({
    type: GET_TENDER_TYPE,
    data: data
});

export var getListTenderType = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_TENDER_TYPE),//endPoint
            data,//payLoad
            serviceCallConfig.GET_TENDER_TYPE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_TENDER_TYPE = 'DELETE_TENDER_TYPE';
export var deleteTenderType = (data) => ({
    type: DELETE_TENDER_TYPE,
    data: data
});

export var dltTenderTypeData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_TENDER_TYPE),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_TENDER_TYPE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var TENDER_TYPE_BY_ID = 'TENDER_TYPE_BY_ID';
export var TenderTypeById = (data) => ({
    type: TENDER_TYPE_BY_ID,
    data: data
});

export var UPDATE_TENDER_TYPE = 'UPDATE_TENDER_TYPE';
export var updateTenderTypeData = (data) => ({
    type: UPDATE_TENDER_TYPE,
    data: data
});


export var updtTenderTypeData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_TENDER_TYPE),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_TENDER_TYPE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};


export var ADD_TAX_RATE = 'ADD_TAX_RATE';
export var getTaxRate = (data) => ({
    type: ADD_TAX_RATE,
    data: data
});


export var addTaxRateData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_TAX_RATE),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_TAX_RATE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var GET_TAX_RATE = 'GET_TAX_RATE';
export var getListTaxRateData = (data) => ({
    type: GET_TAX_RATE,
    data: data
});

export var getListTaxRate = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_TAX_RATE),//endPoint
            data,//payLoad
            serviceCallConfig.GET_TAX_RATE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_TAX_RATE = 'DELETE_TAX_RATE';
export var deleteTaxRate = (data) => ({
    type: DELETE_TAX_RATE,
    data: data
});

export var dltTaxRateData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_TAX_RATE),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_TAX_RATE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var TAX_RATE_BY_ID = 'TAX_RATE_BY_ID';
export var TaxRateById = (data) => ({
    type: TAX_RATE_BY_ID,
    data: data
});

export var UPDATE_TAX_RATE = 'UPDATE_TAX_RATE';
export var updateTaxRateData = (data) => ({
    type: UPDATE_TAX_RATE,
    data: data
});


export var updtTaxRateData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_TAX_RATE),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_TAX_RATE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};


export var ADD_PETTY_CASH_REASON = 'ADD_PETTY_CASH_REASON';
export var getPettyCashReason = (data) => ({
    type: ADD_PETTY_CASH_REASON,
    data: data
});


export var addPettyCashReasonData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_PETTY_CASH_REASON),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_PETTY_CASH_REASON,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var GET_PETTY_CASH_REASON = 'GET_PETTY_CASH_REASON';
export var getListPettyCashReasonData = (data) => ({
    type: GET_PETTY_CASH_REASON,
    data: data
});

export var getListPettyCashReason = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_PETTY_CASH_REASON),//endPoint
            data,//payLoad
            serviceCallConfig.GET_PETTY_CASH_REASON,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_PETTY_CASH_REASON = 'DELETE_PETTY_CASH_REASON';
export var deletePettyCashReason = (data) => ({
    type: DELETE_PETTY_CASH_REASON,
    data: data
});

export var dltPettyCashReasonData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_PETTY_CASH_REASON),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_PETTY_CASH_REASON,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var PETTY_CASH_BY_ID = 'PETTY_CASH_BY_ID';
export var PettyCashReasonById = (data) => ({
    type: PETTY_CASH_BY_ID,
    data: data
});

export var UPDATE_PETTY_CASH_REASON = 'UPDATE_PETTY_CASH_REASON';
export var updatePettyCashReasonData = (data) => ({
    type: UPDATE_PETTY_CASH_REASON,
    data: data
});


export var updtPettyCashReasonData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_PETTY_CASH_REASON),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_PETTY_CASH_REASON,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};