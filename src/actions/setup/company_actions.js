import * as API from './../../base/api_service';
import * as serviceCallConfig from './../../config/api_urls';



export var CUSTOMER_BY_ID = 'CUSTOMER_BY_ID';
export var customerTypeById = (data) => ({
    type: CUSTOMER_BY_ID,
    data: data
});


export var ADD_CUSTOMER_TYPE = 'ADD_CUSTOMER_TYPE';
export var addCustomerData = (data) => ({
    type: ADD_CUSTOMER_TYPE,
    data: data
});


export var sendCustomerData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_CUSTOMER_TYPE),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_CUSTOMER_TYPE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var LIST_CUSTOMER_TYPE = 'LIST_CUSTOMER_TYPE';
export var ListCustomerData = (data) => ({
    type: LIST_CUSTOMER_TYPE,
    data: data
});


export var getCustomerData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.LIST_CUSTOMER_TYPE),//endPoint
            data,//payLoad
            serviceCallConfig.LIST_CUSTOMER_TYPE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};




export var UPDATE_CUSTOMER_TYPE = 'UPDATE_CUSTOMER_TYPE';
export var getUpdatedCustomerData = (data) => ({
    type: UPDATE_CUSTOMER_TYPE,
    data: data
});


export var updateCustomerData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_CUSTOMER_TYPE),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_CUSTOMER_TYPE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_CUSTOMER_TYPE = 'DELETE_CUSTOMER_TYPE';
export var getDeletedCustomerData = (data) => ({
    type: DELETE_CUSTOMER_TYPE,
    data: data
});


export var delteCustomerData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_CUSTOMER_TYPE),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_CUSTOMER_TYPE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var UPDATE_COMPANY_DETAILS = 'UPDATE_COMPANY_DETAILS';
export var getCompanyData = (data) => ({
    type: UPDATE_COMPANY_DETAILS,
    data: data
});


export var upadeCompanyData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_COMPANY_DETAILS),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_COMPANY_DETAILS,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var GET_COMPANY_DETAILS = 'GET_COMPANY_DETAILS';
export var getCompanyDetails = (data) => ({
    type: GET_COMPANY_DETAILS,
    data: data
});


export var CompanyDetails = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_COMPANY_DETAILS),//endPoint
            data,//payLoad
            serviceCallConfig.GET_COMPANY_DETAILS,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};


export var GET_RECEIPT_DETAILS = 'GET_RECEIPT_DETAILS';
export var getReceiptDetail = (data) => ({
    type: GET_RECEIPT_DETAILS,
    data: data
});


export var getReceiptDetails = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_RECEIPT_DETAILS),//endPoint
            data,//payLoad
            serviceCallConfig.GET_RECEIPT_DETAILS,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var ADD_RECEIPT_DETAILS = 'ADD_RECEIPT_DETAILS';
export var addReceiptDetail = (data) => ({
    type: ADD_RECEIPT_DETAILS,
    data: data
});


export var addReceiptDetails = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_RECEIPT_DETAILS),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_RECEIPT_DETAILS,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var UPDATE_RECEIPT_DETAIL = 'UPDATE_RECEIPT_DETAIL';
export var updateReceiptData = (data) => ({
    type: UPDATE_RECEIPT_DETAIL,
    data: data
});


export var sendUpdateReceiptData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_RECEIPT_DETAIL),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_RECEIPT_DETAIL,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};


export var DELETE_RECEIPT_DETAIL = 'DELETE_RECEIPT_DETAIL';
export var deleteReceiptDetail = (data) => ({
    type: DELETE_RECEIPT_DETAIL,
    data: data
});


export var deleteReceiptDetails = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_RECEIPT_DETAIL),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_RECEIPT_DETAIL,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

//Disscount reasons

export var ADD_DISSCOUNT = 'ADD_DISSCOUNT';

export var getDisscounts = (data) => ({
    type: ADD_DISSCOUNT,
    data: data
});


export var addDisscountsData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_DISSCOUNT),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_DISSCOUNT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var GET_DISSCOUNT = 'GET_DISSCOUNT';
export var getListDisscountsData = (data) => ({
    type: GET_DISSCOUNT,
    data: data
});

export var getListDisscounts = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_DISSCOUNT),//endPoint
            data,//payLoad
            serviceCallConfig.GET_DISSCOUNT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_DISSCOUNT = 'DELETE_DISSCOUNT';
export var deleteDisscounts = (data) => ({
    type: DELETE_DISSCOUNT,
    data: data
});

export var dltDisscountsData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_DISSCOUNT),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_DISSCOUNT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DISSCOUNT_BY_ID = 'DISSCOUNT_BY_ID';
export var disscountById = (data) => ({
    type: DISSCOUNT_BY_ID,
    data: data
});

export var UPDATE_DISSCOUNT = 'UPDATE_DISSCOUNT';
export var updateDisscountData = (data) => ({
    type: UPDATE_DISSCOUNT,
    data: data
});


export var updtDisscountsData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_DISSCOUNT),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_DISSCOUNT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};


//Refunds reasons

export var ADD_REFUND = 'ADD_REFUND';

export var getRefunds = (data) => ({
    type: ADD_REFUND,
    data: data
});


export var addRefundsData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_REFUND),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_REFUND,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var GET_REFUND = 'GET_REFUND';
export var getListRefundData = (data) => ({
    type: GET_REFUND,
    data: data
});

export var getListRefunds = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_REFUND),//endPoint
            data,//payLoad
            serviceCallConfig.GET_REFUND,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_REFUND = 'DELETE_REFUND';
export var deleteRefund = (data) => ({
    type: DELETE_REFUND,
    data: data
});

export var dltRefundData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_REFUND),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_REFUND,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var REFUND_BY_ID = 'REFUND_BY_ID';
export var refundById = (data) => ({
    type: REFUND_BY_ID,
    data: data
});

export var UPDATE_REFUND = 'UPDATE_REFUND';
export var updateRefundData = (data) => ({
    type: UPDATE_REFUND,
    data: data
});


export var updtRefundData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_REFUND),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_REFUND,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

//Nosale reasons

export var ADD_NOSALE = 'ADD_NOSALE';

export var getNoSale = (data) => ({
    type: ADD_NOSALE,
    data: data
});


export var addNoSaleData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_NOSALE),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_NOSALE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var GET_NOSALE = 'GET_NOSALE';
export var getListNoSales = (data) => ({
    type: GET_NOSALE,
    data: data
});

export var getListNoSale = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_NOSALE),//endPoint
            data,//payLoad
            serviceCallConfig.GET_NOSALE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_NOSALE = 'DELETE_NOSALE';
export var deleteNoSale = (data) => ({
    type: DELETE_NOSALE,
    data: data
});

export var dltNoSaleData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_NOSALE),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_NOSALE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var NOSALE_BY_ID = 'NOSALE_BY_ID';
export var noSaleById = (data) => ({
    type: NOSALE_BY_ID,
    data: data
});

export var UPDATE_NOSALE = 'UPDATE_NOSALE';
export var updateNoSaleData = (data) => ({
    type: UPDATE_NOSALE,
    data: data
});


export var updtNoSaleData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_NOSALE),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_NOSALE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

//Stock movement reasons

export var ADD_STOCKMOVEMENT = 'ADD_STOCKMOVEMENT';

export var getStockMovement = (data) => ({
    type: ADD_STOCKMOVEMENT,
    data: data
});


export var addStockMovementData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_STOCKMOVEMENT),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_STOCKMOVEMENT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var GET_STOCKMOVEMENT = 'GET_STOCKMOVEMENT';
export var getListStockMovement = (data) => ({
    type: GET_STOCKMOVEMENT,
    data: data
});

export var getListStockMovements = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_STOCKMOVEMENT),//endPoint
            data,//payLoad
            serviceCallConfig.GET_STOCKMOVEMENT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_STOCKMOVEMENT = 'DELETE_STOCKMOVEMENT';
export var deleteStockMovement = (data) => ({
    type: DELETE_STOCKMOVEMENT,
    data: data
});

export var dltStockMovementData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_STOCKMOVEMENT),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_STOCKMOVEMENT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var STOCKMOVEMENT_BY_ID = 'STOCKMOVEMENT_BY_ID';
export var StockMovementById = (data) => ({
    type: STOCKMOVEMENT_BY_ID,
    data: data
});

export var UPDATE_STOCKMOVEMENT = 'UPDATE_STOCKMOVEMENT';
export var updateStockMovementData = (data) => ({
    type: UPDATE_STOCKMOVEMENT,
    data: data
});


export var updtStockMovementData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_STOCKMOVEMENT),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_STOCKMOVEMENT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

//Course reasons

export var ADD_COURSE = 'ADD_COURSE';

export var getCourse = (data) => ({
    type: ADD_COURSE,
    data: data
});


export var addCourseData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_COURSE),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_COURSE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var GET_COURSE = 'GET_COURSE';
export var getListCourse = (data) => ({
    type: GET_COURSE,
    data: data
});

export var getListCourses = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_COURSE),//endPoint
            data,//payLoad
            serviceCallConfig.GET_COURSE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_COURSE = 'DELETE_COURSE';
export var deleteCourse = (data) => ({
    type: DELETE_COURSE,
    data: data
});

export var dltCourseData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_COURSE),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_COURSE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var COURSE_BY_ID = 'COURSE_BY_ID';
export var CourseById = (data) => ({
    type: COURSE_BY_ID,
    data: data
});

export var UPDATE_COURSE = 'UPDATE_COURSE';
export var updateCourseData = (data) => ({
    type: UPDATE_COURSE,
    data: data
});


export var updtCourseData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_COURSE),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_COURSE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

//Clock reasons

export var ADD_CLOCK = 'ADD_CLOCK';

export var getClock = (data) => ({
    type: ADD_CLOCK,
    data: data
});


export var addClockData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_CLOCK),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_CLOCK,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var GET_CLOCK = 'GET_CLOCK';
export var getListClocks = (data) => ({
    type: GET_CLOCK,
    data: data
});

export var getListClock = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_CLOCK),//endPoint
            data,//payLoad
            serviceCallConfig.GET_CLOCK,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_CLOCK = 'DELETE_CLOCK';
export var deleteClock = (data) => ({
    type: DELETE_CLOCK,
    data: data
});

export var dltClockData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_CLOCK),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_CLOCK,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var CLOCK_BY_ID = 'CLOCK_BY_ID';
export var ClockById = (data) => ({
    type: CLOCK_BY_ID,
    data: data
});

export var UPDATE_CLOCK = 'UPDATE_CLOCK';
export var updateClockData = (data) => ({
    type: UPDATE_CLOCK,
    data: data
});


export var updtClockData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_CLOCK),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_CLOCK,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};
