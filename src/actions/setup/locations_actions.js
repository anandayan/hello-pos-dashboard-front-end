import * as API from './../../base/api_service';
import * as serviceCallConfig from './../../config/api_urls';

//LOcation

export var ADD_LOCATION = 'ADD_LOCATION';

export var getLocation = (data) => ({
    type: ADD_LOCATION,
    data: data
});


export var addLocationData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_LOCATION),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_LOCATION,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var GET_LOCATION = 'GET_LOCATION';
export var getListLocationData = (data) => ({
    type: GET_LOCATION,
    data: data
});

export var getListLocation = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_LOCATION),//endPoint
            data,//payLoad
            serviceCallConfig.GET_LOCATION,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_LOCATION = 'DELETE_LOCATION';
export var deleteLocations = (data) => ({
    type: DELETE_LOCATION,
    data: data
});

export var dltLocationData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_LOCATION),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_LOCATION,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var LOCATION_BY_ID = 'LOCATION_BY_ID';
export var LocationById = (data) => ({
    type: LOCATION_BY_ID,
    data: data
});

export var UPDATE_LOCATION = 'UPDATE_LOCATION';
export var updateLocationData = (data) => ({
    type: UPDATE_LOCATION,
    data: data
});


export var updtLocationData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_LOCATION),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_LOCATION,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var ADD_DEVICE = 'ADD_DEVICE';
export var getAddDeviceData = (data) => ({
    type: ADD_DEVICE,
    data: data
});


export var addDeviceData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_DEVICE),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_DEVICE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var GET_DEVICE = 'GET_DEVICE';
export var getDeviceData = (data) => ({
    type: GET_DEVICE,
    data: data
});


export var getListDeviceData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_DEVICE),//endPoint
            data,//payLoad
            serviceCallConfig.GET_DEVICE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DEVICE_BY_ID = 'DEVICE_BY_ID';
export var deviceById = (data) => ({
    type: DEVICE_BY_ID,
    data: data
});


export var UPDATE_DEVICE = 'UPDATE_DEVICE';
export var updateDeviceData = (data) => ({
    type: UPDATE_DEVICE,
    data: data
});


export var getUpdateDeviceData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_DEVICE),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_DEVICE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var DELETE_DEVICE = 'DELETE_DEVICE';
export var deleteDeviceData = (data) => ({
    type: DELETE_DEVICE,
    data: data
});


export var deleteDevice = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_DEVICE),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_DEVICE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var ADD_ORDER_PRINTER = 'ADD_ORDER_PRINTER';
export var getAddOrderPrinterData = (data) => ({
    type: ADD_ORDER_PRINTER,
    data: data
});


export var addOrderPrinterData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_ORDER_PRINTER),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_ORDER_PRINTER,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var GET_ORDER_PRINTER = 'GET_ORDER_PRINTER';
export var getOrderPrinterData = (data) => ({
    type: GET_ORDER_PRINTER,
    data: data
});


export var getListOrderPrinterData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_ORDER_PRINTER),//endPoint
            data,//payLoad
            serviceCallConfig.GET_ORDER_PRINTER,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var ORDER_PRINTER_BY_ID = 'ORDER_PRINTER_BY_ID';
export var orderPrinterById = (data) => ({
    type: ORDER_PRINTER_BY_ID,
    data: data
});


export var UPDATE_ORDER_PRINTER = 'UPDATE_ORDER_PRINTER';
export var updateOrderPrinterData = (data) => ({
    type: UPDATE_ORDER_PRINTER,
    data: data
});


export var getUpdateOrderPrinterData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_ORDER_PRINTER),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_ORDER_PRINTER,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var DELETE_ORDER_PRINTER = 'DELETE_ORDER_PRINTER';
export var deleteOrderPrinterData = (data) => ({
    type: DELETE_ORDER_PRINTER,
    data: data
});


export var deleteOrderPrinter = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_ORDER_PRINTER),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_ORDER_PRINTER,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};


export var ADD_OPENING_HOUR = 'ADD_OPENING_HOUR';
export var getAddOpeningHourData = (data) => ({
    type: ADD_OPENING_HOUR,
    data: data
});


export var addOpeningHourData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_OPENING_HOUR),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_OPENING_HOUR,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var GET_OPENING_HOUR = 'GET_OPENING_HOUR';
export var getOpeningHourData = (data) => ({
    type: GET_OPENING_HOUR,
    data: data
});


export var getListOpeningHourData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_OPENING_HOUR),//endPoint
            data,//payLoad
            serviceCallConfig.GET_OPENING_HOUR,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var OPENING_HOUR_BY_ID = 'OPENING_HOUR_BY_ID';
export var OpeningHourById = (data) => ({
    type: OPENING_HOUR_BY_ID,
    data: data
});


export var UPDATE_OPENING_HOUR = 'UPDATE_OPENING_HOUR';
export var updateOpeningHourData = (data) => ({
    type: UPDATE_OPENING_HOUR,
    data: data
});


export var getUpdateOpeningHourData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_OPENING_HOUR),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_OPENING_HOUR,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var DELETE_OPENING_HOUR = 'DELETE_OPENING_HOUR';
export var deleteOpeningHourData = (data) => ({
    type: DELETE_OPENING_HOUR,
    data: data
});


export var deleteOpeningHour = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_OPENING_HOUR),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_OPENING_HOUR,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};
