import * as API from './../base/api_service';
import * as serviceCallConfig from './../config/api_urls';


export var USER_LOGIN = 'USER_LOGIN';
export var userLogin = (data) => {
   return {type: USER_LOGIN,data:data};
};
export var userLlogin = (data) => {
    return (dispatch, getState) => {
            API.makeServiceCall(
                serviceCallConfig.API_POST,
                dispatch,
                serviceCallConfig.get(serviceCallConfig.USER_LOGIN),
                data,
                serviceCallConfig.USER_LOGIN,
                false,
                ''
            );

    };
};
export var USER_LOGOUT = 'USER_LOGOUT';
export var logout = (data) => {
   return {type: USER_LOGOUT};
};

export var REGISTERATION = 'REGISTERATION';
export var userRegisterationData = (data) => {

   return {type: REGISTERATION,data:data};
};
export var userRegisteration = (data) => {
    return (dispatch, getState) => {
            API.makeServiceCall(
                serviceCallConfig.API_POST,
                dispatch,
                serviceCallConfig.get(serviceCallConfig.REGISTERATION),
                data,
                serviceCallConfig.REGISTERATION,
                false,
                ''
            );

    };
};

export var UPDATE_PROFILE = 'UPDATE_PROFILE';
export var userUpdateData = (data) => {

   return {type: UPDATE_PROFILE,data:data};
};
export var user_update_data = (data) => {
    return (dispatch, getState) => {
            API.makeServiceCall(
                serviceCallConfig.API_POST,
                dispatch,
                serviceCallConfig.get(serviceCallConfig.UPDATE_PROFILE),
                data,
                serviceCallConfig.UPDATE_PROFILE,
                false,
                ''
            );

    };
};

// ;
export var USER_LOGOUT = 'USER_LOGOUT';
export var userLogoutData = (data) => {

   return {type: USER_LOGOUT,data:data};
};
export var userLogout = (data) => {
    return (dispatch, getState) => {
            API.makeServiceCall(
                serviceCallConfig.API_POST,
                dispatch,
                serviceCallConfig.get(serviceCallConfig.USER_LOGOUT),
                data,
                serviceCallConfig.USER_LOGOUT,
                false,
                ''
            );

    };
};



export var FORGOT_PASSWORD = 'FORGOT_PASSWORD';

export var userPasswordDetail = (data) => ({
    type: FORGOT_PASSWORD,
    data: data
});


export var get_forgotpwd_data = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.FORGOT_PASSWORD),//endPoint
            data,//payLoad
            serviceCallConfig.FORGOT_PASSWORD,//requestFrom
            false,//isURLEncoded
            '',//getState().User.loginData.data.accessToken, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var RESET_PASSWORD = 'RESET_PASSWORD';
export var resetpwd_data = (data) => {

   return {
            type: RESET_PASSWORD,
            data:data
       };
};
export var get_resetpwd_data = (data) => {
    return (dispatch, getState) => {
            API.makeServiceCall(
                serviceCallConfig.API_POST,
                dispatch,
                serviceCallConfig.get(serviceCallConfig.RESET_PASSWORD),
                data,
                serviceCallConfig.RESET_PASSWORD,
                false,
                ''
            );

    };
};
