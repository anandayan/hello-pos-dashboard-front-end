import * as API from './../../base/api_service';
import * as serviceCallConfig from './../../config/api_urls';

export var ADD_STAFF = 'ADD_STAFF';

export var getStaffData = (data) => ({
    type: ADD_STAFF,
    data: data
});


export var addStaffData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_STAFF),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_STAFF,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

/* edit staff */
export var STAFF_BY_ID = 'ADD_STAFF';
export var getStaffDataByID = (data) => ({
    type: STAFF_BY_ID,
    data: data
});

export var LIST_STAFF = 'LIST_STAFF';
export var getListStaffData = (data) => ({
    type: LIST_STAFF,
    data: data
});

export var addListStaffData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.LIST_STAFF),//endPoint
            data,//payLoad
            serviceCallConfig.LIST_STAFF,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_STAFF = 'DELETE_STAFF';
export var getDeleteStaffData = (data) => ({
    type: DELETE_STAFF,
    data: data
});

export var dltStaffData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_STAFF),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_STAFF,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var UPDATE_STAFF = 'UPDATE_STAFF';
export var getUpdateStaffData = (data) => ({
    type: UPDATE_STAFF,
    data: data
});


export var updtStaffData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_STAFF),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_STAFF,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

//Add role action here


export var ADD_ROLE = 'ADD_ROLE';
export var getRoleData = (data) => ({
    type: ADD_ROLE,
    data: data
});


export var addRoleData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_ROLE),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_ROLE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};


export var UPDATE_ROLE = 'UPDATE_ROLE';
export var getUpdateRoleData = (data) => ({
    type: UPDATE_ROLE,
    data: data
});


export var updateRoleData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_ROLE),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_ROLE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var LIST_ROLE = 'LIST_ROLE';
export var getListRoleData = (data) => ({
    type: LIST_ROLE,
    data: data
});


export var addtListRoleData = (data) => {
    
    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.LIST_ROLE),//endPoint
            data,//payLoad
            serviceCallConfig.LIST_ROLE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var ADD_HOUR = 'ADD_HOUR';
export var getAddHourData = (data) => ({
    type: ADD_HOUR,
    data: data
});


export var addHourData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_HOUR),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_HOUR,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey, 
            '',//api_key
            ''//getParams
        );

    };
};

export var LIST_HOUR = 'LIST_HOUR';
export var getListHourData = (data) => ({
    type: LIST_HOUR,
    data: data
});

export var addListHourData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.LIST_HOUR),//endPoint
            data,//payLoad
            serviceCallConfig.LIST_HOUR,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var UPDATE_HOUR = 'UPDATE_HOUR';
export var getUpadteHourData = (data) => ({
    type: UPDATE_HOUR,
    data: data
});

export var updateHourData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_HOUR),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_HOUR,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};


export var DELETE_HOUR = 'DELETE_HOUR';
export var getDeleteHour = (data) => ({
    type: DELETE_HOUR,
    data: data
});


export var deleteHourData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_HOUR),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_HOUR,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};
