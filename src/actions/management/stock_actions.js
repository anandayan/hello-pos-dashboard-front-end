import * as API from './../../base/api_service';
import * as serviceCallConfig from './../../config/api_urls';



export var ADD_SUPPLIER = 'ADD_SUPPLIER';
export var getAddSupplierData = (data) => ({
    type: ADD_SUPPLIER,
    data: data
});


export var addSupplierData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_SUPPLIER),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_SUPPLIER,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var GET_SUPPLIER = 'GET_SUPPLIER';
export var listSupplierData = (data) => ({
    type: GET_SUPPLIER,
    data: data
});


export var getListSupplierData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_SUPPLIER),//endPoint
            data,//payLoad
            serviceCallConfig.GET_SUPPLIER,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var SUPPLIER_BY_ID = 'SUPPLIER_BY_ID';
export var SupplierById = (data) => ({
    type: SUPPLIER_BY_ID,
    data: data
});


export var UPDATE_SUPPLIER = 'UPDATE_SUPPLIER';
export var updateSupplierData = (data) => ({
    type: UPDATE_SUPPLIER,
    data: data
});


export var getUpdateSupplierData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_SUPPLIER),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_SUPPLIER,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var DELETE_SUPPLIER = 'DELETE_SUPPLIER';
export var deleteSupplierData = (data) => ({
    type: DELETE_SUPPLIER,
    data: data
});


export var deleteSupplier = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_SUPPLIER),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_SUPPLIER,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

