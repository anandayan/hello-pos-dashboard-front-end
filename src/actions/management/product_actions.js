import * as API from './../../base/api_service';
import * as serviceCallConfig from './../../config/api_urls';

export var ADD_PRODUCT = 'ADD_PRODUCT';
export var getAddProductData = (data) => ({
    type: ADD_PRODUCT,
    data: data
});


export var addProductData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_PRODUCT),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_PRODUCT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var LIST_PRODUCT = 'LIST_PRODUCT';
export var listProductData = (data) => ({
    type: LIST_PRODUCT,
    data: data
});


export var getListProductData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.LIST_PRODUCT),//endPoint
            data,//payLoad
            serviceCallConfig.LIST_PRODUCT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var PRODUCT_BY_ID = 'PRODUCT_BY_ID';
export var productById = (data) => ({
    type: PRODUCT_BY_ID,
    data: data
});


export var UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export var updateProductData = (data) => ({
    type: UPDATE_PRODUCT,
    data: data
});


export var getUpdateProductData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_PRODUCT),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_PRODUCT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var DELETE_PRODUCT = 'DELETE_PRODUCT';
export var deleteProductData = (data) => ({
    type: DELETE_PRODUCT,
    data: data
});


export var deleteProduct = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_PRODUCT),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_PRODUCT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};


export var ADD_BRAND = 'ADD_BRAND';
export var getAddBrandData = (data) => ({
    type: ADD_BRAND,
    data: data
});


export var addBrandData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_BRAND),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_BRAND,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var GET_BRAND = 'GET_BRAND';
export var getBrandData = (data) => ({
    type: GET_BRAND,
    data: data
});


export var getListBrandData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_BRAND),//endPoint
            data,//payLoad
            serviceCallConfig.GET_BRAND,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var BRAND_BY_ID = 'BRAND_BY_ID';
export var brandById = (data) => ({
    type: BRAND_BY_ID,
    data: data
});


export var UPDATE_BRAND = 'UPDATE_BRAND';
export var updateBrandData = (data) => ({
    type: UPDATE_BRAND,
    data: data
});


export var getUpdateBrandData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_BRAND),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_BRAND,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var DELETE_BRAND = 'DELETE_BRAND';
export var deleteBrandData = (data) => ({
    type: DELETE_BRAND,
    data: data
});


export var deleteBrand = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_BRAND),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_BRAND,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};
export var ADD_POPUP_NOTE = 'ADD_POPUP_NOTE';
export var getAddPopUpNoteData = (data) => ({
    type: ADD_POPUP_NOTE,
    data: data
});


export var addPopUpNoteData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_POPUP_NOTE),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_POPUP_NOTE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var GET_POPUP_NOTE = 'GET_POPUP_NOTE';
export var getPopUpNoteData = (data) => ({
    type: GET_POPUP_NOTE,
    data: data
});


export var getListPopUpNoteData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_POPUP_NOTE),//endPoint
            data,//payLoad
            serviceCallConfig.GET_POPUP_NOTE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var POPUP_NOTE_BY_ID = 'POPUP_NOTE_BY_ID';
export var popupById = (data) => ({
    type: POPUP_NOTE_BY_ID,
    data: data
});


export var UPDATE_POPUP_NOTE = 'UPDATE_POPUP_NOTE';
export var updatePopUpNoteData = (data) => ({
    type: UPDATE_POPUP_NOTE,
    data: data
});


export var getUpdatePopUpNoteData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_POPUP_NOTE),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_POPUP_NOTE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var DELETE_POPUP_NOTE = 'DELETE_POPUP_NOTE';
export var deletePopUpNoteData = (data) => ({
    type: DELETE_POPUP_NOTE,
    data: data
});


export var deletePopUpNote = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_POPUP_NOTE),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_POPUP_NOTE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var ADD_MULTICHOICE_NOTE = 'ADD_MULTICHOICE_NOTE';
export var getAddMultiChoiceNoteData = (data) => ({
    type: ADD_MULTICHOICE_NOTE,
    data: data
});


export var addMultiChoiceNoteData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_MULTICHOICE_NOTE),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_MULTICHOICE_NOTE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var GET_MULTICHOICE_NOTE = 'GET_MULTICHOICE_NOTE';
export var getMultiChoiceNoteData = (data) => ({
    type: GET_MULTICHOICE_NOTE,
    data: data
});


export var getListMultiChoiceNoteData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_MULTICHOICE_NOTE),//endPoint
            data,//payLoad
            serviceCallConfig.GET_MULTICHOICE_NOTE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var MULTICHOICE_NOTE_BY_ID = 'MULTICHOICE_NOTE_BY_ID';
export var multichoiceById = (data) => ({
    type: MULTICHOICE_NOTE_BY_ID,
    data: data
});


export var UPDATE_MULTICHOICE_NOTE = 'UPDATE_MULTICHOICE_NOTE';
export var updateMultiChoiceNoteData = (data) => ({
    type: UPDATE_MULTICHOICE_NOTE,
    data: data
});


export var getUpdateMultiChoiceNoteData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_MULTICHOICE_NOTE),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_MULTICHOICE_NOTE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var DELETE_MULTICHOICE_NOTE = 'DELETE_MULTICHOICE_NOTE';
export var deleteMultiChoiceNoteData = (data) => ({
    type: DELETE_MULTICHOICE_NOTE,
    data: data
});


export var deleteMultiChoiceNote = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_MULTICHOICE_NOTE),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_MULTICHOICE_NOTE,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var ADD_MISC_PRODUCT = 'ADD_MISC_PRODUCT';
export var getAddMiscProductData = (data) => ({
    type: ADD_MISC_PRODUCT,
    data: data
});


export var addMiscProductData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_MISC_PRODUCT),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_MISC_PRODUCT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var GET_MISC_PRODUCT = 'GET_MISC_PRODUCT';
export var getMiscProductData = (data) => ({
    type: GET_MISC_PRODUCT,
    data: data
});


export var getListMiscProductData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_MISC_PRODUCT),//endPoint
            data,//payLoad
            serviceCallConfig.GET_MISC_PRODUCT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var MISC_PRODUCT_BY_ID = 'MISC_PRODUCT_BY_ID';
export var MiscProductById = (data) => ({
    type: MISC_PRODUCT_BY_ID,
    data: data
});


export var UPDATE_MISC_PRODUCT = 'UPDATE_MISC_PRODUCT';
export var updateMiscProductData = (data) => ({
    type: UPDATE_MISC_PRODUCT,
    data: data
});


export var getUpdateMiscProductData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_MISC_PRODUCT),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_MISC_PRODUCT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};



export var DELETE_MISC_PRODUCT = 'DELETE_MISC_PRODUCT';
export var deleteMiscProductData = (data) => ({
    type: DELETE_MISC_PRODUCT,
    data: data
});


export var deleteMiscProduct = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_MISC_PRODUCT),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_MISC_PRODUCT,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};
export var ADD_CATEGORY = 'ADD_CATEGORY';
export var addCategoryData = (data) => ({
 type: ADD_CATEGORY,
 data: data
});

export var addCategory = (data) => {
 
    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_CATEGORY),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_CATEGORY,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

 export var LIST_CATEGORY = 'LIST_CATEGORY';
export var listCategoryData = (data) => ({
    type: LIST_CATEGORY,
	    data: data
});
export var listCategory = (data) => {
    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
              serviceCallConfig.get(serviceCallConfig.LIST_CATEGORY),//endPoint
            data,//payLoad
            serviceCallConfig.LIST_CATEGORY,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_CATEGORY = 'DELETE_CATEGORY';
export var deleteCategorytData = (data) => ({
    type: DELETE_CATEGORY,
	    data: data
});
export var deleteCategory = (data) => {
    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_CATEGORY),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_CATEGORYDELETE_CATEGORY,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var UPDATE_CATEGORY = 'UPDATE_CATEGORY';
export var updateCategoryData = (data) => ({
    type: UPDATE_CATEGORY,
	data: data
});


export var updateCategory = (data) => {

  return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
			serviceCallConfig.get(serviceCallConfig.UPDATE_CATEGORY),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_CATEGORY,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};


export var ADD_PROMOTION = 'ADD_PROMOTION';
export var addPromotionData = (data) => ({
    type: ADD_PROMOTION,
    data: data
});


export var sendPromotionData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.ADD_PROMOTION),//endPoint
            data,//payLoad
            serviceCallConfig.ADD_PROMOTION,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var GET_PROMOTION = 'GET_PROMOTION';
export var ListPromotionData = (data) => ({
    type: GET_PROMOTION,
    data: data
});


export var getPromotionData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.GET_PROMOTION),//endPoint
            data,//payLoad
            serviceCallConfig.GET_PROMOTION,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};


export var PROMOTION_BY_ID = 'PROMOTION_BY_ID';
export var PromtionById = (data) => ({
    type: PROMOTION_BY_ID,
    data: data
});


export var UPDATE_PROMOTION = 'UPDATE_PROMOTION';
export var getUpdatedPromotionData = (data) => ({
    type: UPDATE_PROMOTION,
    data: data
});


export var updatePromotionData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.UPDATE_PROMOTION),//endPoint
            data,//payLoad
            serviceCallConfig.UPDATE_PROMOTION,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};

export var DELETE_PROMOTION = 'DELETE_PROMOTION';
export var getDeletedPromotionData = (data) => ({
    type: DELETE_PROMOTION,
    data: data
});


export var deltePromotionData = (data) => {

    return (dispatch, getState) => {

        API.makeServiceCall(
            serviceCallConfig.API_POST,//reqType
            dispatch,//dispatch
            serviceCallConfig.get(serviceCallConfig.DELETE_PROMOTION),//endPoint
            data,//payLoad
            serviceCallConfig.DELETE_PROMOTION,//requestFrom
            false,//isURLEncoded
            getState().User.loginDetail.token, //authKey
            '',//api_key
            ''//getParams
        );

    };
};
